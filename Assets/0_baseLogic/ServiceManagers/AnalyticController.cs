﻿using System.Collections.Generic;
using UnityEngine.Analytics;

namespace baseLogic
{
	public class AnalyticController
	{
		public static void QuestComleted(string questName)
		{
			AnalyticsEvent.Custom("quest", new Dictionary<string, object>
			{
				{ "completed", questName },
			});		
		}
		
		public static void QuestActivated(string questName)
		{
			AnalyticsEvent.Custom("quest", new Dictionary<string, object>
			{
				{ "activated", questName },
			});		
		}
	}
}