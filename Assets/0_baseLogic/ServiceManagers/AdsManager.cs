﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_ADS
using UnityEngine.Advertisements;

#endif

namespace baseLogic.monetization
{
    public class AdsManager
    {
        private static bool _defaultAdsEnabled = true;

        public static bool DefaultAdsEnabled
        {
            get { return _defaultAdsEnabled; }
            set { _defaultAdsEnabled = value; }
        }

        public static void ShowDefaultAd()
        {
            if(_defaultAdsEnabled == false)
                return;
            
            #if UNITY_ADS
            if (!Advertisement.IsReady())
            {
                Debug.Log("Ads not ready for default placement");
                return;
            }

            Advertisement.Show();
            #endif
        }

        private static Action OnAdComplitedStat = null;
        private static Action OnAdFailedStat = null;
        
        public static bool IsRewardAdAvailable => (DateTime.Now - LastAdRewardTime).TotalSeconds > 200 && IsAdAvailable();

        private static DateTime lastRewardTime = new DateTime(0);
        
        private static DateTime LastAdRewardTime
        {
            /*get { return DateTime.FromBinary(Convert.ToInt64(PlayerPrefs.GetString("lastTimeRewardAd", "0"))); }
            set {PlayerPrefs.SetString("lastTimeRewardAd", value.ToBinary().ToString()); }*/
            get { return lastRewardTime; }
            set { lastRewardTime = value; }
        }
        
        public static bool IsAdAvailable()
        {
            const string RewardedPlacementId = "rewardedVideo";
#if UNITY_ADS
            return Advertisement.IsReady(RewardedPlacementId);
#endif
            return false;
        }
        
        public static void ShowRewardedAd(Action OnAdComplited, Action OnAdFailed = null)
        {
            if(!IsRewardAdAvailable)
                return;
            
            const string RewardedPlacementId = "rewardedVideo";

            #if UNITY_ADS
            if (!Advertisement.IsReady(RewardedPlacementId))
            {
                Debug.Log(string.Format("Ads not ready for placement '{0}'", RewardedPlacementId));
                return;
            }

            OnAdFailedStat?.Invoke();
            
            OnAdComplitedStat = OnAdComplited;
            OnAdFailedStat = OnAdFailed;

            if (Advertisement.IsReady() == false)
            {
                OnAdFailedStat?.Invoke();
                OnAdComplitedStat = null;
                OnAdFailedStat = null;
                return;
            }
                
            var options = new ShowOptions {resultCallback = HandleShowResult};
            Advertisement.Show(RewardedPlacementId, options);
            #endif
        }

        #if UNITY_ADS
        private static void HandleShowResult(ShowResult result)
        {
            
            switch (result)
            {
                case ShowResult.Finished:
                    LastAdRewardTime = DateTime.Now;
                    OnAdComplitedStat?.Invoke();
                    Debug.Log("The ad was successfully shown.");
                    //
                    // YOUR CODE TO REWARD THE GAMER
                    // Give coins etc.
                    break;
                case ShowResult.Skipped:
                    OnAdFailedStat?.Invoke();
                    Debug.Log("The ad was skipped before reaching the end.");
                    break;
                case ShowResult.Failed:
                    OnAdFailedStat?.Invoke();
                    Debug.LogError("The ad failed to be shown.");
                    break;
            }

            OnAdFailedStat = null;
            OnAdComplitedStat = null;
        }

        #endif
    }
}