﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTimeScale
{
    public class TimeScaleLimit
    {
        public System.Action OnValueChanged;

        float _maxTimeScale = 0;

        public float MaxTimeScale
        {
            get
            {
                return _maxTimeScale;
            }
            set
            {
                _maxTimeScale = value;
                if (OnValueChanged != null)
                    OnValueChanged();
            }
        }


        public TimeScaleLimit(float maxTimeScale)
        {
            _maxTimeScale = maxTimeScale;
        }
	
    }
}