﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTimeScale
{
    public class TimeScaleLimitContainer : MonoBehaviour
    {
        [SerializeField]
        float _maxTimeScale = 0;

        public float MaxTimeScale
        {
            get
            {
                return _maxTimeScale;
            }
            set
            {
                _maxTimeScale = value;
                if (_currentLimit != null)
                {
                    TimeScaleManager.Remove(_currentLimit);
                    _currentLimit = new TimeScaleLimit(_maxTimeScale);
                    TimeScaleManager.Add(_currentLimit);
                }
            }
        }

        TimeScaleLimit _currentLimit = null;

        // Use this for initialization
        void OnEnable()
        {
            _currentLimit = new TimeScaleLimit(_maxTimeScale);
            TimeScaleManager.Add(_currentLimit);
        }
	
        void OnDisable()
        {
            TimeScaleManager.Remove(_currentLimit);
            _currentLimit = null;
        }
    }
}