﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace DZTimeScale
{
    public class TimeScaleManager
    {

        static List<TimeScaleLimit> _limits = new List<TimeScaleLimit>();

        public static void Add(TimeScaleLimit limit)
        {
            _limits.Add(limit);

            if(limit != null)   
                limit.OnValueChanged += RecalcCurrentTimeScale;
            
            RecalcCurrentTimeScale();
        }

        public static void Remove(TimeScaleLimit limit)
        {
            _limits.Remove(limit);

            if(limit != null)
                limit.OnValueChanged -= RecalcCurrentTimeScale;
            
            RecalcCurrentTimeScale();
        }

            
        static void RecalcCurrentTimeScale()
        {
            float timeScale = 1;

            if (_limits.Count == 0)
            {
                
            }
            else
            {
                foreach (TimeScaleLimit limit in _limits)
                {
                    if (limit.MaxTimeScale < timeScale)
                    {
                        timeScale = limit.MaxTimeScale;
                    }
                }
            }


            Time.timeScale = timeScale;
            Time.fixedDeltaTime = Mathf.Clamp(timeScale * 0.03f, 0.001f, 0.03f);
            /*if (timeScale > 0.8f)
            {
                Time.fixedDeltaTime = 0.03f;
            }
            else if (timeScale > 0.5f)
            {
                Time.fixedDeltaTime = 0.015f;
            }
            else if (timeScale > 0.1f)
            {
                Time.fixedDeltaTime = 0.003f;
            }
            else
            {
                Time.fixedDeltaTime = 0.0015f;
            }*/

        }

    }
}