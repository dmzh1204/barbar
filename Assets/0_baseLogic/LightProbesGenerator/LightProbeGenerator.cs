﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace baseLogic.light
{
	public class LightProbeGenerator : MonoBehaviour {

		[Serializable]
		public class AreaAndOffset
		{
			public int area = 0;
			public Vector3 offset = Vector3.up * 1.5f; 
		}

		[SerializeField] private AreaAndOffset[] groups = new[]
		{
			new AreaAndOffset(), 
		};
		
		[ContextMenu("Regenerate")]
		public void Regenerate()
		{
			Clear();
			Generate();
		}

		void Clear()
		{
			LightProbeGroup lpg = GetComponent<LightProbeGroup>();
			
			if(lpg != null)
				DestroyImmediate(lpg);
		}

		void Generate()
		{
#if UNITY_EDITOR
			
			LightProbeGroup lpg = gameObject.AddComponent<LightProbeGroup>();

			List<Vector3> positions = new List<Vector3>();
			
			NavMeshTriangulation nmT = NavMesh.CalculateTriangulation();
			
			for (int i = 0; i < nmT.areas.Length; i++)
			{
				
				foreach (var group in groups)
				{
					if (group.area == nmT.areas[i])
					{

						for (int ai = i * 3; ai < i * 3 + 3; ai++)
						{
							positions.Add(
								nmT.vertices[nmT.indices[ai]]
								+ group.offset);
						}
						
					}
				}
				
			}

			lpg.probePositions = positions.ToArray();

			#endif

		}
	}
	
}