﻿using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using UnityEngine;

namespace baseLogic
{
    public class LocalizeAdapter
    {
        public static string GetText(string key)
        {
            string translation = LocalizationManager.GetTranslation(key);

            if (translation != null && translation != "")
                return translation;
            
            return key;
        }
    }
    
    public static class StringExtension
    {
        public static string Localize(this string str)
        {
            return LocalizeAdapter.GetText(str);
        }
    } 
}