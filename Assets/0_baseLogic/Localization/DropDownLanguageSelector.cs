﻿using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using UnityEngine;
using UnityEngine.UI;

public class DropDownLanguageSelector : MonoBehaviour
{
	private Dropdown dd;

	private void Awake()
	{
		dd = GetComponent<Dropdown>();
		dd.options.Clear();
		foreach (var l in LocalizationManager.GetAllLanguages())
		{
			dd.options.Add(new Dropdown.OptionData(l == "Russian" ? l + " (Original)" : l));
			if (l == LocalizationManager.CurrentLanguage)
				dd.value = dd.options.Count - 1;
		}

		dd.onValueChanged.AddListener(OnSelect);
	}

	private void OnDestroy()
	{
		dd.onValueChanged.RemoveAllListeners();
	}
	
	private void OnSelect(int v)
	{
		LocalizationManager.CurrentLanguage = LocalizationManager.GetAllLanguages()[v];
	}
	
	
}
