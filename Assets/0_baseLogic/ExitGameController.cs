﻿using System.Collections;
using System.Collections.Generic;
using baseLogic.uiScreens;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class ExitGameController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (UIScreensManager.CurrentScreen.ScreenType != UIScreen.ScreenTypes.mainMenu)
			return;

		if (CrossPlatformInputManager.GetButtonDown("Cancel"))
			SystemDialog.Show("EXIT?", "", "Yes", "No", Application.Quit);

	}
}
