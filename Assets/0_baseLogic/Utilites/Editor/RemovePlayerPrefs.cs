﻿using UnityEngine;
using UnityEditor;

public class RemovePlayerPrefs : MonoBehaviour
{

	[MenuItem("BARBARIAN/УДАЛИТЬ НАСТРОЙКИ ИГРОКА")]
	static void Remove()
	{
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
	}
}