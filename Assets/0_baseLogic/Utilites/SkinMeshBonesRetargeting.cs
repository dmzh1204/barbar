﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace baseLogic.utilites
{

	public static class TransformHelper
	{
		public static Transform FindTransform(Transform root, string name)
		{
			foreach (var t in root.GetComponentsInChildren<Transform>())
			{
				if (t.name == name)
					return t;
			}
			return null;
		}
	}
	

	public static class SkinMeshBonesRetargeting
	{

		public static void RetargetBones(SkinnedMeshRenderer sourceSkin, Transform[] targetScelet)
		{
			Transform[] bones = sourceSkin.bones;

			for (int i = 0; i < bones.Length; i++)
			{
				Transform sourceBone = bones[i];
				bones[i] = bones[0];
				foreach (var boneTarget in targetScelet)
				{
					if (sourceBone != null && boneTarget != null && sourceBone.name == boneTarget.name)
						bones[i] = boneTarget;
				}

			}

			sourceSkin.bones = bones;
			Transform rootBone = sourceSkin.rootBone;
			sourceSkin.rootBone = bones[0];
			foreach (var boneTarget in targetScelet)
			{
				if (rootBone != null && boneTarget != null && rootBone.name == boneTarget.name)
					sourceSkin.rootBone = boneTarget;
			}
			
		}
		
	}
}