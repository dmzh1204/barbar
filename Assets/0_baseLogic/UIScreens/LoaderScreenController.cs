﻿using System.Collections;
using System.Collections.Generic;
using baseLogic.sceneLoading;
using UnityEngine;

namespace baseLogic.uiScreens
{
	public class LoaderScreenController : MonoBehaviour
	{
		[SerializeField]
		private UIScreen _loadScreen;

		[SerializeField] private ScreenForScene[] _defScreens = new ScreenForScene[0];
		
		[SerializeField] private int _startScene = 2;
		public int TutorialScene = 4;

		[System.Serializable]
		struct ScreenForScene
		{
			public int sceneIndex;
			public UIScreen screen;
		}

		private bool NeedTutorialOnStartApp => PlayerPrefs.GetInt("TutorialComplited", 0) == 0;
		
		// Use this for initialization
		public void InitStart()
		{
			//UIScreensManager.ShowScreen(_loadScreen);
			SceneLoader.OnSceneStartLoading += OnStartLoad;
			SceneLoader.OnSceneLoadingComplite += OnLoaded;
			SceneLoader.LoadScene(NeedTutorialOnStartApp ? TutorialScene : _startScene, 0.5f);
		}

		void OnStartLoad(int index)
		{
			UIScreensManager.ShowScreen(_loadScreen);
		}
		
		void OnLoaded(int index)
		{
			foreach (ScreenForScene sfs in _defScreens)
			{
				if (sfs.sceneIndex == index)
				{
					UIScreensManager.ShowScreen(sfs.screen);
					return;

				}
			}				
		}

		// Update is called once per frame
		void Update()
		{

		}
	}
}