﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace baseLogic.uiScreens
{

	public class BtnShowScreen : MonoBehaviour, IPointerClickHandler
	{
		[SerializeField] private UIScreen _screen = null;
		
		public void OnPointerClick(PointerEventData eventData)
		{
			UIScreensManager.ShowScreen(_screen);
		}

	}
}