﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

namespace baseLogic.uiScreens
{
	public class UIScreen : MonoBehaviour
	{
		public enum ScreenTypes
		{
			none = 0,
			loading = 1,
			mainMenu = 2,
			mainMenuSetting = 3,
			
			gameController = 4,
			inventory = 5,
			traiding = 6,
			
			interactionOptions = 7,
			dialogScreen = 11,
			pauseGame = 8,
			failGame = 10,
			
			comix = 9,

		}

		[SerializeField] private ScreenTypes _screenType = ScreenTypes.none;

		public ScreenTypes ScreenType => _screenType;


		private UIScreensManager _manager;
		private CanvasGroup _canvGroup = null;

		private float _fadeTime = -1;

		public float FadeTime
		{
			get { return _fadeTime; }
		}

		public void Init(UIScreensManager manager)
		{
			_manager = manager;
			if(_fadeTime < 0)
				_fadeTime = _manager.DefaultFadeTime;
			_canvGroup = GetComponent<CanvasGroup>();
		}


		private float _handlerStartTime = 0;
		
		public void Show()
		{
			_canvGroup.alpha = 0;

			gameObject.SetActive(true);
			_handlerStartTime = Time.time;
			_updateHandler = ShowingHandler;
		}
		
		public void Hide()
		{
			_handlerStartTime = Time.time;
			_updateHandler = HideningHandler;
		}

		void ShowingHandler()
		{
			float normalixeTime = (Time.time - _handlerStartTime) / _fadeTime;
			_canvGroup.alpha = normalixeTime;
			if (normalixeTime > 1)
			{
				_canvGroup.alpha = 1;

				_updateHandler = null;
			}
		}
		void HideningHandler()
		{
			float normalixeTime = (Time.time - _handlerStartTime) / _fadeTime;

			normalixeTime = 1f - normalixeTime;
			
			_canvGroup.alpha = normalixeTime;
			if (normalixeTime < 0)
			{
				_canvGroup.alpha = 0;
				gameObject.SetActive(false);
				_updateHandler = null;
			}
		}

		void OnEnable()
		{
			_canvGroup.alpha = 0;
		}

		void OnDisable()
		{
			_canvGroup.alpha = 0;
		}

		private System.Action _updateHandler = null;
		
		void Update()
		{
			if (_updateHandler != null)
				_updateHandler();
		}
	}
}