﻿using System;
using System.Collections;
using System.Collections.Generic;
using baseLogic.uiScreens;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;

public class InapsController : MonoBehaviour
{
	public Button BtnShow;
	public Button BtnClose;

	public RectTransform Panel;

	public Vector3 PanelOpenPosition;

	public Vector3 PanelClosePosition;

	private Vector3 panelTargetPosition;
	
	// Use this for initialization
	void Awake () {
		if(UserController.IsPCControl)
			gameObject.SetActive(false);
		
		BtnShow.onClick.AddListener(OnShow);
		BtnClose.onClick.AddListener(OnClose);
		InitDisabled();
		Panel.anchoredPosition3D = panelTargetPosition;
		UIScreensManager.OnShowScreen += OnShowScreen;
	}

	private void OnDestroy()
	{
		UIScreensManager.OnShowScreen -= OnShowScreen;
	}

	private void OnShowScreen(UIScreen screen)
	{
		switch (screen.ScreenType)
		{
			case UIScreen.ScreenTypes.mainMenu:
				InitShowed();
				break;
			case UIScreen.ScreenTypes.pauseGame:
				InitClosed();
				break;
			default:
				InitDisabled();
				break;
		}
	}

	private void OnClose()
	{
		panelTargetPosition = PanelClosePosition;
		BtnShow.gameObject.SetActive(true);
	}

	private void OnShow()
	{
		BtnShow.gameObject.SetActive(false);
		panelTargetPosition = PanelOpenPosition;
	}

	private void InitDisabled()
	{
		BtnShow.gameObject.SetActive(false);
		panelTargetPosition = PanelClosePosition;
	}
	
	private void InitShowed()
	{
		BtnShow.gameObject.SetActive(false);
		panelTargetPosition = PanelOpenPosition;
	}
	
	private void InitClosed()
	{
		BtnShow.gameObject.SetActive(true);
		panelTargetPosition = PanelClosePosition;
	}

	private void Update()
	{
		Panel.anchoredPosition3D = Vector3.MoveTowards(Panel.anchoredPosition3D, panelTargetPosition, Panel.rect.width * 4 * Time.unscaledDeltaTime);
	}
}
