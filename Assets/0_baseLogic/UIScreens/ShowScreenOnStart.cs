﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace baseLogic.uiScreens
{

	public class ShowScreenOnStart : MonoBehaviour
	{
		[SerializeField] private UIScreen _screen = null;

		void Start()
		{
			UIScreensManager.ShowScreen(_screen);
		}
		

	}
}