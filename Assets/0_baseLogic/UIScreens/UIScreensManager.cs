﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace baseLogic.uiScreens
{
	public class UIScreensManager : MonoBehaviour
	{

		[SerializeField]
		private float _defaultFadeTime = 0.5f;

		[SerializeField] private GameObject _fader;

		public float DefaultFadeTime
		{
			get { return _defaultFadeTime; }
		}

		static UIScreen _currentScreen = null;

		public static UIScreen CurrentScreen => _currentScreen;

		[SerializeField]
		private UIScreen _startScreen = null;

		private static UIScreen[] _allScreens = new UIScreen[0];

		[SerializeField] private UnityEvent OnInited;
		
		// Use this for initialization
		void Start()
		{
			OnShowScreen += OnScreenShowing;
			_allScreens = GetComponentsInChildren<UIScreen>(true);
			foreach (UIScreen scr in _allScreens)
			{
				scr.Init(this);
			}
			
			ShowScreen(_startScreen);

			OnInited.Invoke();
		}

		// Update is called once per frame
		void Update()
		{

		}

		void OnScreenShowing(UIScreen screen)
		{
			_fader.SetActive(true);
			Invoke("HideFader", screen.FadeTime);
		}

		void HideFader()
		{
			_fader.SetActive(false);
		}
		public static System.Action<UIScreen> OnShowScreen;

		public static void ShowScreen(UIScreen.ScreenTypes scrType)
		{
			if(_currentScreen != null && scrType == _currentScreen.ScreenType)
				return;
			
			foreach (var scr in _allScreens)
			{
				if (scr.ScreenType == scrType)
				{
					ShowScreen(scr);
					break;
				}
			}
		}

		public static void ShowScreen(UIScreen screen)
		{
			if(!screen)
				return;
			
			if(_currentScreen == screen)
				return;
			
			if(_currentScreen)
				_currentScreen.Hide();
			screen.Show();
			_currentScreen = screen;

			if (OnShowScreen != null)
				OnShowScreen(screen);
		}

	}
}