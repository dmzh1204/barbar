﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace baseLogic.sceneLoading
{
    public class SceneLoader : MonoBehaviour
    {
        [SerializeField] int _defaultScene = 1;

        [SerializeField] private Transform _preloaderObject = null;

        // Use this for initialization
        void Start()
        {
#if UNITY_ANDROID || UNITY_IOS
            Application.targetFrameRate = 60;
#endif
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            OnSceneLoadingComplite += OnLoadedFirstScene;

            LoadScene(_defaultScene);
        }

        void OnLoadedFirstScene(int index)
        {
            Destroy(_preloaderObject.gameObject);
            OnSceneLoadingComplite -= OnLoadedFirstScene;
        }

        // Update is called once per frame
        void Update()
        {
            _currentProgress = 1;

            foreach (var job in _jobs.ToArray())
            {
                job.TryExecute();
                _currentProgress = 0;
            }

            List<AsyncOperation> toRemove = new List<AsyncOperation>();

            foreach (KeyValuePair<AsyncOperation, int> kvp in _currentLoadings)
            {
                if (kvp.Key.isDone)
                {
                    toRemove.Add(kvp.Key);
                    continue;
                }

                _currentProgress = Mathf.Min(_currentProgress, kvp.Key.progress);
            }

            foreach (AsyncOperation ao in toRemove)
            {
                if (OnSceneLoadingComplite != null && _currentLoadings.Count == 1)
                {
                    OnSceneLoadingComplite(_currentLoadings[ao]);
                }


                _currentLoadings.Remove(ao);
            }
        }

        private static float _currentProgress = 1;

        public static float CurrentProgress
        {
            get { return _currentProgress; }
        }


        public static System.Action<int> OnSceneStartLoading;
        public static System.Action<int> OnSceneLoadingComplite;

        static Dictionary<AsyncOperation, int> _currentLoadings = new Dictionary<AsyncOperation, int>();

        static List<LoadJob> _jobs = new List<LoadJob>();

        class LoadJob
        {
            public int index = 0;
            public float time = 0;

            public LoadJob(int i, float t)
            {
                index = i;
                time = t;
            }

            public void TryExecute()
            {
                if (Time.unscaledTime < time || _currentLoadings.Count > 0)
                    return;

                _jobs.Remove(this);

                AsyncOperation ao;

                ao = SceneManager.LoadSceneAsync(index, UnityEngine.SceneManagement.LoadSceneMode.Single);

                _currentLoadings.Add(ao, index);
            }
        }

        public static void LoadScene(int index, float delay = 0)
        {
            foreach (Scene scn in SceneManager.GetAllScenes())
            {
                if (scn.buildIndex == index)
                    return;
            }

            if (OnSceneStartLoading != null)
                OnSceneStartLoading(index);

            _jobs.Add(new LoadJob(index, Time.unscaledTime + delay));
        }
    }
}