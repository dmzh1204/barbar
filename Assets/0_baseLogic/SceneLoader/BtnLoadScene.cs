﻿using System.Collections;
using System.Collections.Generic;
using baseLogic.uiScreens;
using UnityEngine;
using UnityEngine.EventSystems;

namespace baseLogic.sceneLoading
{




	public class BtnLoadScene : MonoBehaviour, IPointerClickHandler
	{
		[SerializeField] private int _index = 2;

		public void OnPointerClick(PointerEventData eventData)
		{
			SceneLoader.LoadScene(_index, 0.5f);
		}

	}
}