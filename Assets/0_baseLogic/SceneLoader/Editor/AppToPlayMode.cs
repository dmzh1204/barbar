﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace baseLogic.sceneLoading
{

	public class AppToPlayMode : MonoBehaviour
	{

		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{
			Debug.Log("sdfsdf");
		}

		private static string _prevScene = "";
		
		[MenuItem("BaseLogic/Play Shortcut Key %k + m")]
		static void Play()
		{
			//Debug.LogError(_prevScene+ "   sfsdgsdg");
			if ( EditorApplication.isPlaying == true )
			{
				EditorApplication.isPlaying = false;
				if(_prevScene != "")
					EditorApplication.OpenScene(_prevScene);
				_prevScene = "";

				return;
			}

			_prevScene = EditorApplication.currentScene;
			
		
			
			//Debug.LogError(EditorApplication.currentScene + "   sfsdgsdg");

			
			EditorApplication.SaveCurrentSceneIfUserWantsTo();
			EditorApplication.OpenScene("Assets/0_actionRpg/base/Scenes/0_Boot.unity");
			EditorApplication.isPlaying = true;
		}

	}
}