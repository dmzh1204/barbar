﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace baseLogic.sceneLoading
{

	public class LoadSceneOnStart : MonoBehaviour
	{
		[SerializeField] private int _index = 2;
		// Use this for initialization
		void Start()
		{
			Invoke("Load", 1);
		}

		void Load()
		{
			SceneLoader.LoadScene(_index, 0.5f);

		}
	}
}