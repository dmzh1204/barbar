﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace baseLogic.sceneLoading
{
	[RequireComponent(typeof(Image))]
	public class LoadingFillingImage : MonoBehaviour
	{
		private Image _img;
		// Use this for initialization
		void Start()
		{
			_img = GetComponent<Image>();
		}

		private float _progress = 0;
		
		void OnEnable()
		{
			_progress = 0;
		}

		
		// Update is called once per frame
		void Update()
		{
			_progress += Time.deltaTime * 0.05f;
			_progress = Mathf.Max(SceneLoader.CurrentProgress, _progress);
			_img.fillAmount = _progress;
		}
	}
}