﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace baseLogic.Audio
{

	public class AudioSystem : MonoBehaviour
	{
		private static AudioSystem _inst = null;

	
		private AudioListener _listner = null;

		[SerializeField] private AudioSource _music;

		[SerializeField] private AudioSource _effectPrefub;
		[SerializeField] private AudioSource _effect2D;

		List<AudioSource> _poolEffects = new List<AudioSource>();

		private const int _sizeEffectPool = 5;

		private int _currentIndexForEffect = 0;
		// Use this for initialization
		void Start()
		{
			if (_inst != null)
			{
				Destroy(gameObject);
				return;
			}
			_inst = this;
			_listner = GetComponent<AudioListener>();

			_poolEffects.Add(_effectPrefub);
			
			for (int i = 0; i < _sizeEffectPool - 1; i++)
			{
				_poolEffects.Add((AudioSource) Instantiate(_effectPrefub, _effectPrefub.transform.parent));
			}
			
			
		}

		// Update is called once per frame
		void Update()
		{
			if (_musicHandler != null)
				_musicHandler();
			
			if (Camera.main)
				_listner.enabled = false;
			else
				_listner.enabled = true;
		}

		private System.Action _musicHandler = null;
		private AudioClip _musikTargetClip = null;
		
		void StarterNewMusic()
		{
			_music.volume -= Time.unscaledDeltaTime;

			if (_music.volume > 0)
			{
				return;
			}

			_music.volume = 1;
			_music.clip = _musikTargetClip;
			_music.Play();
			_musicHandler = null;
		}

		
		void PlayMusicPrivate(AudioClip clip)
		{
			_musikTargetClip = clip;
			_musicHandler = StarterNewMusic;
		}
		
		public static void PlayMusic(AudioClip clip)
		{
			if(_inst == null)
				return;
			
			_inst.PlayMusicPrivate(clip);
		}

		public static void PlayEffect(AudioClip clip)
		{

			Vector3 pos = Vector3.zero;

			if (Camera.main != null)
				pos = Camera.main.transform.position;
			
			PlayEffect(clip, pos);
			
		}

		public static void PlayEffect(AudioClip clip, Vector3 position)
		{
			if (_inst._currentIndexForEffect >= _sizeEffectPool)
				_inst._currentIndexForEffect = 0;
			
			AudioSource audSorce = _inst._poolEffects[_inst._currentIndexForEffect];

			audSorce.transform.position = position;
			
			audSorce.clip = clip;
			audSorce.Play();
			
			_inst._currentIndexForEffect++;
		}

		public static void PlayEffect2D(AudioClip clip)
		{
			_inst._effect2D.Stop();

			_inst._effect2D.clip = clip;
			_inst._effect2D.Play();
		}

		public static void SetActive(bool isActive)
		{
			_inst.gameObject.SetActive(isActive);
		}
	}
	
}