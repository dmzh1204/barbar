﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace baseLogic.Audio
{
	public class PlayOnStart : MonoBehaviour
	{
		[SerializeField]
		private AudioClip _clip;

		// Use this for initialization
		void Start()
		{
			AudioSystem.PlayMusic(_clip);
		}

		// Update is called once per frame
		void Update()
		{

		}
	}
}