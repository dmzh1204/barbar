﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_PURCHASING
using UnityEngine.Purchasing;
#endif

public class IOSRestoreIAPBtn : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        if (!(Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer ||
            Application.platform == RuntimePlatform.tvOS))
            Destroy(gameObject);
    }

    public void OnClick() => RestorePurchases();

    public void RestorePurchases()
    {
#if UNITY_PURCHASING
        
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
              Application.platform == RuntimePlatform.OSXPlayer ||
              Application.platform == RuntimePlatform.tvOS)
            CodelessIAPStoreListener.Instance.ExtensionProvider.GetExtension<IAppleExtensions>()
                .RestoreTransactions(x => { });
        
        
#endif
    }
}
