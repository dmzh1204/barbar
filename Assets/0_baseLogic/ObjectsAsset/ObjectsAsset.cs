﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

using System.IO;

namespace baseLogic.Asset
{



	public class ObjectsAsset : ScriptableObject
	{

		static ObjectsAsset _instance = null;

		public static ObjectsAsset Instance
		{
			get
			{
				if (_instance == null)
				{
					
					_instance = Resources.Load<ObjectsAsset>("ObjectsAsset");
					if (_instance == null)
					{
						_instance = ScriptableObject.CreateInstance<ObjectsAsset>();

#if UNITY_EDITOR
						AssetDatabase.CreateAsset(_instance, Path.Combine(Path.Combine("Assets", "Resources"),
							"ObjectsAsset" + ".asset"
						));
#endif
					}
				}

				return _instance;
			}
		}

		[SerializeField] ObjectData[] _objects = new ObjectData[0];



		[System.Serializable]
		public class ObjectData
		{
			[HideInInspector]
			public int id;
			public string name;
			public string description;
			public Sprite icon;
			public GameObject prefub;
		}

		public GameObject GetPrefub(int id)
		{
			foreach (ObjectData obj in _objects)
			{
				if (obj.id == id)
					return obj.prefub;
			}
			
			
			return null;
		}
		
		public string GetName(int id)
		{
			foreach (ObjectData obj in _objects)
			{
				if (obj.id == id)
					return obj.name;
			}
			
			
			return "";
		}
	}
}