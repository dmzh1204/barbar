﻿using System;
using System.Collections;
using System.Collections.Generic;
using baseLogic.Audio;
using DZTPC;
using UnityEngine;
using Random = System.Random;

public class MusicController : MonoBehaviour
{
    private MusicZone[] _zones = null;

    private const float updateInterval = 3f;

    private MusicZone _activeZone = null;

    [SerializeField] AudioClip[] _combatTracks = new AudioClip[0];

    [SerializeField] AudioClip[] _deathClips = new AudioClip[0];

    enum States
    {
        free,
        combat,
        death,
    }

    private static States _currentState = States.free;

    private static States CurrentState
    {
        get { return _currentState; }
        set
        {
            if (_currentState == value)
                return;


            _currentState = value;

            //Debug.Log(_currentState);

            switch (_currentState)
            {
                case States.combat:
                    AudioSystem.PlayMusic(_inst._combatTracks[UnityEngine.Random.Range(0, _inst._combatTracks.Length)]);
                    break;
                case States.death:
                    AudioSystem.PlayMusic(_inst._deathClips[UnityEngine.Random.Range(0, _inst._deathClips.Length)]);
                    break;
                case States.free:
                    _inst._activeZone = null;
                    break;
            }
        }
    }

    private static MusicController _inst = null;

    // Use this for initialization
    void Start()
    {
        _inst = this;
        _zones = GetComponentsInChildren<MusicZone>();
        CurrentState = States.free;

        InvokeRepeating("ManualUpdate", 0, updateInterval);
        LifeObject.GlobalOnDeath += OnDeath;
    }

    private void OnDestroy()
    {
        LifeObject.GlobalOnDeath -= OnDeath;
    }

    private void OnDeath(LifeObject obj)
    {
        if (obj == UserController.PlayerLifeObj)
        {
            CurrentState = States.death;
        }
    }


    // Update is called once per frame
    void ManualUpdate()
    {
        if (CurrentState == States.death)
        {
            return;
        }

        if (AnimalAI.AnyAttackPlayer || FightingAIState.AnyAttackPlayer)
        {
            CurrentState = States.combat;
            return;
        }
        else
        {
            CurrentState = States.free;
        }


        if (_activeZone != null && _activeZone != _zones[0] && _activeZone.isCameraInZone())
            return;

        foreach (MusicZone zone in _zones)
        {
            if (zone.isCameraInZone() == false)
                continue;

            if (zone == _activeZone)
                return;

            _activeZone = zone;
            _activeZone.Play();
            return;
        }

        if (_activeZone != _zones[0])
        {
            _activeZone = _zones[0];
            _activeZone.Play();
        }
    }
}