﻿using System.Collections;
using System.Collections.Generic;
using baseLogic.Audio;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class MusicZone : MonoBehaviour {

	[SerializeField]
	AudioClip[] _clips = new AudioClip[0];
	
	[SerializeField]
	AudioClip[] _clipsForNight = new AudioClip[0];

	public void Play()
	{

		if ((GameGlobalTimer.CurrentData.Hour > 18 || GameGlobalTimer.CurrentData.Hour < 6) && _clipsForNight.Length > 0)
		{
			AudioSystem.PlayMusic(_clipsForNight[Random.Range(0, _clips.Length - 1)]);
			return;
		}
		
		if (_clips.Length > 0)
			AudioSystem.PlayMusic(_clips[Random.Range(0, _clips.Length - 1)]);
		
	}

	public bool isCameraInZone()
	{
		if (Camera.main == null)
			return false;

		foreach (Transform subzone in GetComponentsInChildren<Transform>())
		{
			if (subzone.InverseTransformPoint(Camera.main.transform.position).magnitude < 0.5f)
				return true;
		}

		return false;
	}

#if UNITY_EDITOR

	private void OnDrawGizmos()
	{
		if(Application.isPlaying)
			return;
		if (Selection.activeTransform == null)
			return;
		if (Selection.activeTransform.root != transform.root)
			return;
		Gizmos.color = new Color(0.3f,0.3f,1,0.2f);
		Gizmos.DrawSphere(transform.position, 0.5f * transform.localScale.x);
		
		foreach (Transform subzone in GetComponentsInChildren<Transform>())
		{
			Gizmos.DrawSphere(subzone.position, 0.5f * subzone.localScale.x);
		}
	}

#endif

}
