﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DZTPC
{
    public class PivotPoints : MonoBehaviour
    {
        public enum Types
        {
            none = -1,
            rightHand = 0,
            leftHand = 1,
            rightBelt = 2,
            leftBelt = 3,
            rightSpine = 4,
            leftSpine = 5,
            body = 6,

        }

        [SerializeField]
        Pivot[] _pivots = new Pivot[0];



        [System.Serializable]
        struct Pivot
        {
            public Transform bone;
            public Types type;

            public Pivot(Transform b, Types t)
            {
                bone = b;
                type = t;
            }
        }

        public Transform GetPivot(Types pivotType)
        {
            foreach(Pivot p in _pivots)
            {
                if (p.type == pivotType)
                    return p.bone;
            }

            if(pivotType != Types.rightHand && pivotType != Types.leftHand)
                return null;

            if (pivotType == Types.body)
                return transform;
            
            Animator antr = transform.GetComponent<Animator>();

            if (antr == null)
                return null;

            if (pivotType == Types.rightHand)
                return antr.GetBoneTransform(HumanBodyBones.RightHand);
            else
                return antr.GetBoneTransform(HumanBodyBones.LeftHand);
        }

        
        
        public void PlacingGOInPivot(Transform objTrans, Types pivotType)
        {
            PivotPoints pp = objTrans.GetComponent<PivotPoints>();
            Transform pivotOther = objTrans;
            if (pp != null && pp.GetPivot(pivotType) != null)
                pivotOther = pp.GetPivot(pivotType);

            Transform pivot = transform;

            if (GetPivot(pivotType) != null)
                pivot = GetPivot(pivotType);

            objTrans.SetParent(pivot);

            pivotOther.SetParent(pivot);
                        objTrans.SetParent(pivotOther);
            pivotOther.localEulerAngles = Vector3.zero;
            pivotOther.localPosition = Vector3.zero;

            objTrans.SetParent(pivot);

            pivotOther.SetParent(objTrans);

        }

        [SerializeField] private PivotPoints _samplePivPoints;

        [ContextMenu("InitBySample")]
        public void InitBySample()
        {
            InitBySample(_samplePivPoints);
        }
        
        public void InitBySample(PivotPoints samplePivotPoints)
        {
            foreach (var pivot in _pivots)
            {
                if(pivot.bone == null)
                    continue;
                
                DestroyImmediate(pivot.bone.gameObject);
                //Destroy(pivot.bone.gameObject);
            }
            _pivots = new Pivot[0];
            
            Animator sampleAntr = samplePivotPoints.GetComponent<Animator>();
            Animator myAntr = GetComponent<Animator>();
            
            List<Pivot> newPivots = new List<Pivot>();
            
            foreach (var pivot in samplePivotPoints._pivots)
            {
                HumanBodyBones bone = GetBone(sampleAntr, myAntr, pivot.bone);
                Transform sempleBoneTrans = sampleAntr.GetBoneTransform(bone);
                Vector3 locPos = sempleBoneTrans.InverseTransformPoint(pivot.bone.position);
                Vector3 locForward = sempleBoneTrans.InverseTransformDirection(pivot.bone.forward);
                Vector3 locUp = sempleBoneTrans.InverseTransformDirection(pivot.bone.up);
                
                Transform myBoneTrans = myAntr.GetBoneTransform(bone);
                GameObject newBone = new GameObject("pivotPoint");
                newBone.transform.parent = myBoneTrans;

                newBone.transform.localPosition = locPos;
                newBone.transform.rotation = Quaternion.LookRotation(
                    myBoneTrans.TransformDirection(locForward)
                    , myBoneTrans.TransformDirection(locUp)
                    );
                
                newPivots.Add(new Pivot(newBone.transform, pivot.type));
            }

            _pivots = newPivots.ToArray();
        }

        HumanBodyBones GetBone(Animator antr, Animator myAntr, Transform t)
        {
            HumanBodyBones[] allHumBones = new HumanBodyBones[]
            {               
                HumanBodyBones.LeftHand,
                HumanBodyBones.RightHand,

                HumanBodyBones.Head,
                HumanBodyBones.Neck,
                HumanBodyBones.UpperChest,
                HumanBodyBones.Chest,
                HumanBodyBones.Spine,
                HumanBodyBones.Hips,
            };
            
            List<HumanBodyBones> filteredBonesList = new List<HumanBodyBones>();

            foreach (var bone in allHumBones)
            {
                if (myAntr.GetBoneTransform(bone) != null)
                {
                    filteredBonesList.Add(bone);
                }
            }

            allHumBones = filteredBonesList.ToArray();
            
            foreach (var bone in allHumBones)
            {
                Transform boneTrans = antr.GetBoneTransform(bone);
                Transform parent = t;

                while (parent != null)
                {
                    if (parent == boneTrans)
                        return bone;
                    parent = parent.parent;
                }
            }
            
            return HumanBodyBones.Hips;
        }
    }
}