﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

namespace RPGDataBase
{
    public class RPGMath
    {
        const int coofA = 200;
        const int coofB = 2;
        const int coofC = 50000;


        private const int minLevel = 1;
        private const int maxLevel = 40;

        public static int GetCurrentLevel(int expirence)
        {
            int level = minLevel;

            while (expirence >= GetExpirence(level + 1))
            {
                level++;
                if (level >= maxLevel)
                    return maxLevel;
            }
            
            return level;
        }

        public static int GetExpirence(int level)
        {
            if(level < minLevel || level > maxLevel)
                Debug.LogError("ErrorLevel = " + level);
            level = Mathf.Clamp(level, minLevel, maxLevel);
            
            if (level <= 1)
                return 0;
            
            if (level == 2)
                return coofA;

            var prevExp = GetExpirence(level - 1);
            return Mathf.Min(prevExp * coofB, prevExp + coofC);
        } 
        
        
        public static int GetExpirenceReward(int levelKilledLife)
        {
            return levelKilledLife * 50;
        }

        public static int GetMaxHealth(int level)
        {
            return level * 20;
        }

        public static float DefAttackTime(WeaponObject.WeaponTypes weapnType)
        {
            switch (weapnType)
            {
                case WeaponObject.WeaponTypes.bows:
                    return 1.5f;
                case WeaponObject.WeaponTypes.crossBow:
                    return 2f;
                case WeaponObject.WeaponTypes.oneHandMelee:
                    return 1f;
                case WeaponObject.WeaponTypes.twoHandMelee:
                    return 1.5f;
            }

            return 1;
        }

        public const float MeleeAttackAngle = 40;
        
        public static float GetBlockChance(LifeObject attacker, LifeObject defender)
        {
            if (!defender.HoldInHandMeleeWeapon() || !attacker.HoldInHandMeleeWeapon())
                return 0;

            if (defender.GetParametr(LifeObject.DependetParams.attackDistMeele) >
                attacker.GetParametr(LifeObject.DependetParams.attackDistMeele))
                return 0.9f;
            
            if (defender.GetParametr(LifeObject.DependetParams.attackDistMeele) <
                attacker.GetParametr(LifeObject.DependetParams.attackDistMeele))
                return 0.1f;
            
            return 0.5f;
        }
        
    }
}