﻿using System;
using System.Collections;
using System.Collections.Generic;
using baseLogic;
using ProBuilder2.Common;
using UnityEditor;
using UnityEngine;

namespace DZTPC
{
    public class BarbarEWUtils
    {
        private class NameID
        {
            public int ID;
            public string Name;
        } 

        private static Rect textLocPanel = new Rect(0, 0, 300, 100);
        private static bool showLocKeys = false;

        private const string separator =
            "-----------------------------------------------------------------------------------";


        private static List<NameID> allTakebleObjects = new List<NameID>();
        private static string[] allTakebleObjectsNames;

        public static void Init()
        {
            var types = new[] {typeof(GameDataBase.NoTradeble),typeof(GameDataBase.Food),typeof(GameDataBase.Weapons),};
            allTakebleObjectsNames = new string[0];
            foreach (var t in types)
            {
                foreach (var p in GameDataBase.GetAll(t))
                {
                    allTakebleObjects.Add(new NameID()
                    {
                        ID = p.id,
                        Name = p.name.Localize()
                    });
                }
            }

            foreach (var o in allTakebleObjects)
                allTakebleObjectsNames = allTakebleObjectsNames.Add(o.Name);
        }

        private static int SelectTakebleObject(int currentID)
        {
            int index = 0;

            for (int i = 0; i < allTakebleObjects.Count; i++)
                if (allTakebleObjects[i].ID == currentID)
                {
                    index = i;
                    break;
                }
            
            index = EditorGUILayout.Popup(index, allTakebleObjectsNames);

            return allTakebleObjects[index].ID;
        }
        
        public static string DrawGUILocalizedText(string lable, string locKey)
        {
            EditorGUILayout.LabelField(separator);

            //GUILayout.BeginArea(textLocPanel);
            showLocKeys = EditorGUILayout.Foldout(showLocKeys, LocalizeAdapter.GetText(locKey));
            if (showLocKeys)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(lable);
                string outString = EditorGUILayout.TextField(locKey);
                EditorGUILayout.EndHorizontal();
                //GUILayout.EndArea();
                return outString;
            }
            else
            {
                //GUILayout.EndArea();
                return locKey;
            }
        }

        public static DialogsData.DialogData.CharCollectebleItem[] DrawGUIItemsBlock(
            DialogsData.DialogData.CharCollectebleItem[] items, string lable = "Передать игроку ")
        {
            for (int i = 0; i < items.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(lable + "(Предметы): ");

                items[i].type
                    = (CollectebleObjects.Types) EditorGUILayout.EnumPopup(items[i].type);

                items[i].count
                    = EditorGUILayout.IntField(items[i].count);

                if (GUILayout.Button("Удалить"))
                    return items.RemoveAt(i);
                ;

                EditorGUILayout.EndHorizontal();
            }

            if (GUILayout.Button(lable + "(Коллекции)"))
                return items.Add(new DialogsData.DialogData.CharCollectebleItem());

            return items;
        }

        public static DialogsData.DialogData.CharItem[] DrawGUIItemsBlock(DialogsData.DialogData.CharItem[] items, string lable = "Передать игроку ")
        {
            for (int i = 0; i < items.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(lable);

                items[i].prefubID = SelectTakebleObject(items[i].prefubID);

                EditorGUILayout.LabelField("Кол-во: ");
                items[i].count
                    = EditorGUILayout.IntField(items[i].count);

                if (GUILayout.Button("Удалить"))
                {
                    return items.RemoveAt(i);
                }

                EditorGUILayout.EndHorizontal();
            }

            if (GUILayout.Button(lable + "(типы)"))
                return items.Add(new DialogsData.DialogData.CharItem());

            return items;
        }
        
    }
}