﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using baseLogic;
using DZTPC;
using ProBuilder2.Common;
using UnityEditor;
using UnityEngine;

public class DialogEditorWindow : EditorWindow
{
    //public 

    static string[] charactersNames = new string[0];
    static string[] dialogsNames = new string[0];

    static string[] questesNames = new string[0];

    private static int selectedChar = -1;
    private static Stack<DialogsData.DialogData> selectedDialogsStack = new Stack<DialogsData.DialogData>();

    private static int nextID = 1;

    [MenuItem("BARBARIAN/ДИАЛОГИ")]
    static void Init()
    {
        BarbarEWUtils.Init();
        // Get existing open window or if none, make a new one:
        DialogEditorWindow window = (DialogEditorWindow) EditorWindow.GetWindow(typeof(DialogEditorWindow));
        window.Show();

        List<string> names = new List<string>();

        for (int i = 0; i < GameDataBase.Chars.Length; i++)
            names.Add(i + " -> " + LocalizeAdapter.GetText(GameDataBase.Chars[i].name));

        charactersNames = names.ToArray();

        names.Clear();

        names.Add("none");

        for (int i = 0; i < GameDataBase.Questes.Length; i++)
        {
            names.Add(i + " -> " + LocalizeAdapter.GetText(GameDataBase.Questes[i].name));
        }

        questesNames = names.ToArray();
    }

    void OnGUI()
    {
        EditorUtility.SetDirty(GameDataBase.Instance);
        EditorUtility.SetDirty(DialogsData.Inst);
        CharacterBlock();

        if (selectedDialogsStack.Count == 0)
        {
            SelectDialog();
        }
        else
        {
            EditDialog();
        }
    }

    void CharacterBlock()
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("выбрать персонажа - ");

        int index = EditorGUILayout.Popup(selectedChar, charactersNames);
        EditorGUILayout.EndHorizontal();

        if (index == selectedChar)
            return;

        selectedChar = index;
        OnCharacterSelect();
    }

    void OnCharacterSelect()
    {
        selectedDialogsStack.Clear();

        List<string> names = new List<string>();

        for (int i = 0; i < GameDataBase.Chars[selectedChar].GetDialogs().Length; i++)
        {
            if (GameDataBase.Chars[selectedChar].GetDialogs()[i].dialog.Length == 0)
            {
                names.Add("dialog N: " + i);
                continue;
            }

            names.Add(
                LocalizeAdapter.GetText(GameDataBase.Chars[selectedChar].GetDialogs()[i].dialog[0].replicKey)
            );
        }

        dialogsNames = names.ToArray();
    }

    void SelectDialog()
    {
        EditorGUILayout.LabelField("выбрать диалог");

        for (int i = 0; i < dialogsNames.Length; i++)
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button(dialogsNames[i] + " [ID " + GameDataBase.Chars[selectedChar].GetDialogs()[i].id + "]"))
            {
                selectedDialogsStack.Push(GameDataBase.Chars[selectedChar].GetDialogs()[i]);
            }

            if (GUILayout.Button("Удалить"))
            {
                DialogsData.RemoveDialog(GameDataBase.Chars[selectedChar].GetDialogs()[i].id);
                OnCharacterSelect();
            }

            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Добавить новый диалог"))
        {
            DialogsData.AddDialogToChar(GameDataBase.Chars[selectedChar].id, DialogsData.CreateDialog());
            OnCharacterSelect();
        }
    }

    


    private Vector2 scrollPos;

    void EditDialog()
    {
        if (GUILayout.Button("Назад"))
        {
            selectedDialogsStack.Pop();
            if (selectedDialogsStack.Count == 0)
            {
                OnCharacterSelect();
            }

            return;
        }

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

        DialogsData.DialogData dialogData = selectedDialogsStack.Peek();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Тип запуска: ");
        dialogData.StartType = (DialogsData.DialogData.StartTypes) EditorGUILayout.EnumPopup(dialogData.StartType);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginToggleGroup("Условия Запуска", true);
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Область действия: ");
            dialogData.conditionToStart.AreasType =
                (AreasTypes) EditorGUILayout.EnumPopup(dialogData.conditionToStart.AreasType);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Активный квест: ");
            dialogData.conditionToStart.activeQueste =
                EditorGUILayout.Popup(dialogData.conditionToStart.activeQueste + 1, questesNames) - 1;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Выполненый квест: ");
            dialogData.conditionToStart.completedQueste =
                EditorGUILayout.Popup(dialogData.conditionToStart.completedQueste + 1, questesNames) - 1;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Одежда ниже этой цены: ");
            dialogData.conditionToStart.MaxClothPrice =
                EditorGUILayout.IntField(dialogData.conditionToStart.MaxClothPrice);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Одежда выше этой цены: ");
            dialogData.conditionToStart.MinClothPrice =
                EditorGUILayout.IntField(dialogData.conditionToStart.MinClothPrice);
            EditorGUILayout.EndHorizontal();

            dialogData.conditionToStart.CharCollectebelItemChanges =
                BarbarEWUtils.DrawGUIItemsBlock(dialogData.conditionToStart.CharCollectebelItemChanges, "Игрок имеет:");
            dialogData.conditionToStart.items = BarbarEWUtils.DrawGUIItemsBlock(dialogData.conditionToStart.items, "Игрок имеет:");
        }
        EditorGUILayout.EndToggleGroup();

        EditorGUILayout.BeginToggleGroup("Действия", true);
        {
            if (dialogData.actions.trading == false)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Драка: ");
                dialogData.actions.fighting = EditorGUILayout.Toggle(dialogData.actions.fighting);
                EditorGUILayout.EndHorizontal();
            }

            if (dialogData.actions.fighting == false)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Торговать: ");
                dialogData.actions.trading = EditorGUILayout.Toggle(dialogData.actions.trading);
                EditorGUILayout.EndHorizontal();
            }

            if (dialogData.actions.trading == false)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Активирует квест: ");
                dialogData.actions.activateQuest =
                    EditorGUILayout.Popup(dialogData.actions.activateQuest + 1, questesNames) - 1;
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Выполняет квест: ");
                dialogData.actions.completeQuest =
                    EditorGUILayout.Popup(dialogData.actions.completeQuest + 1, questesNames) - 1;
                EditorGUILayout.EndHorizontal();
            }

            if (dialogData.actions.trading == false && dialogData.actions.fighting == false)
            {
                for (int i = 0; i < dialogData.actions.CharSpecParamChanges.Length; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Добавить игроку характеристику: ");

                    dialogData.actions.CharSpecParamChanges[i].charParam
                        = (LifeObject.SpecifedParams) EditorGUILayout.EnumPopup(dialogData.actions
                            .CharSpecParamChanges[i].charParam);

                    dialogData.actions.CharSpecParamChanges[i].charParamDelta
                        = EditorGUILayout.FloatField(dialogData.actions.CharSpecParamChanges[i].charParamDelta);

                    if (GUILayout.Button("Удалить"))
                    {
                        dialogData.actions.CharSpecParamChanges = dialogData.actions.CharSpecParamChanges.RemoveAt(i);
                        return;
                    }

                    EditorGUILayout.EndHorizontal();
                }

                if (GUILayout.Button("Добавить характеристик"))
                {
                    dialogData.actions.CharSpecParamChanges
                        = dialogData.actions.CharSpecParamChanges.Add(new DialogsData.DialogData.CharSpecParamValue());
                }

                dialogData.actions.CharCollectebleItemChanges =
                    BarbarEWUtils.DrawGUIItemsBlock(dialogData.actions.CharCollectebleItemChanges);

                dialogData.actions.CharItemChanges =
                    BarbarEWUtils.DrawGUIItemsBlock(dialogData.actions.CharItemChanges);
            }
        }
        EditorGUILayout.EndToggleGroup();

        for (int i = 0; i < dialogData.dialog.Length; i++)
        {
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Реплика № " + i);

            dialogData.dialog[i].replicKey =
                BarbarEWUtils.DrawGUILocalizedText("Кллюч Локализации: ", dialogData.dialog[i].replicKey);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Говорит игрок: ");
            dialogData.dialog[i].isPlayersReplic = EditorGUILayout.Toggle(dialogData.dialog[i].isPlayersReplic);
            EditorGUILayout.LabelField("Добавить в журнал: ");
            dialogData.dialog[i].addToLog = EditorGUILayout.Toggle(dialogData.dialog[i].addToLog);
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Удалить"))
            {
                dialogData.dialog = dialogData.dialog.RemoveAt(i);
                return;
            }

            EditorGUILayout.Separator();
        }

        if (GUILayout.Button("Добавить реплику"))
        {
            dialogData.dialog = dialogData.dialog.Add(DialogsData.CreateNewReplica());
        }

        EditorGUILayout.LabelField("ВАРИАНТЫ ОТВЕТОВ");

        for (int i = 0; i < dialogData.answers.Length; i++)
        {
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Ответ № " + i);

            dialogData.answers[i].replicKey =
                BarbarEWUtils.DrawGUILocalizedText("Текст ответа: ", dialogData.answers[i].replicKey);

            EditorGUILayout.BeginHorizontal();

            if (dialogData.answers[i].nextDialog == null || dialogData.answers[i].nextDialog.dialog.Length == 0)
            {
                if (GUILayout.Button("Создать Диалог"))
                {
                    dialogData.answers[i].nextDialogID = DialogsData.CreateDialog();
                    return;
                }
            }
            else
            {
                if (GUILayout.Button($"Открыть Диалог id = {dialogData.answers[i].nextDialog.id}"))
                {
                    selectedDialogsStack.Push(dialogData.answers[i].nextDialog);
                    return;
                }

                if (GUILayout.Button("Удалить Диалог"))
                {
                    DialogsData.RemoveDialog(dialogData.answers[i].nextDialogID);
                    return;
                }
            }

            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Удалить ответ"))
            {
                dialogData.answers = dialogData.answers.RemoveAt(i);
                return;
            }
        }

        EditorGUILayout.Separator();

        if (GUILayout.Button("Добавить ответ"))
        {
            dialogData.answers = dialogData.answers.Add(new DialogsData.DialogData.Answer()
            {
                nextDialogID = -1,
                replicKey = "answerFailedTextKey",
            });
        }

        EditorGUILayout.EndScrollView();
    }
}