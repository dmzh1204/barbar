﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class LandscapeRecalculatorWindow : EditorWindow
{
    private static LandscapeRoster instRoster;

    [MenuItem("BARBARIAN/ЛАНДШАФТ")]
    static void Init()
    {
        EditorUtility.ClearProgressBar();
        LandscapeRecalculatorWindow window =
            (LandscapeRecalculatorWindow) EditorWindow.GetWindow(typeof(LandscapeRecalculatorWindow));
        window.Show();

        instRoster = GameObject.FindObjectOfType<LandscapeRoster>();
    }

    void OnGUI()
    {
        if (GUILayout.Button("Выравнять Текстуры")) 
            MenuRecalcUV();

        if (GUILayout.Button("Пересажать Растительность"))
            RefreshFoliageProps();

        if (GUILayout.Button("Запустить Расчет Освещения"))
            ReBakeLightMap();

        if (GUILayout.Button("Перегенерировать эффекты на воде"))
            RegenerateWaterFX();


        if (GUILayout.Button("Сделать Все!!!"))
            MakeAll();
    }

    async void MenuRecalcUV()
    {
        EditorUtility.DisplayProgressBar("", "", 0f);
        //await RecalcUV();
        EditorUtility.ClearProgressBar();
    }

    async void MakeAll()
    {
        EditorUtility.DisplayProgressBar("", "", 0f);
        //await RecalcUV();
        EditorUtility.DisplayProgressBar("", "", 0.25f);
        await RefreshFoliageProps();
        EditorUtility.DisplayProgressBar("", "", 0.5f);
        await RegenerateWaterFX();
        EditorUtility.DisplayProgressBar("", "", 0.75f);
        await ReBakeLightMap();
        EditorUtility.ClearProgressBar();
    }

    async Task RefreshFoliageProps()
    {
        const float upOffset = 3;
        const float bottomOffset = 3;
        const float randomAngle = 3;

        foreach (var root in instRoster.foliagePropsRoot)
            for (var i = 0; i < root.childCount; i++)
            {
                var t = root.GetChild(i);

                foreach (var rh in Physics.RaycastAll(t.position + Vector3.up * upOffset, Vector3.down,
                    upOffset + bottomOffset))
                {
                    if (rh.transform != t)
                    {
                        t.position = rh.point;
                        t.eulerAngles = new Vector3(
                            Random.Range(-randomAngle, randomAngle),
                            Random.Range(0, 360),
                            Random.Range(-randomAngle, randomAngle));
                        break;
                    }
                }
            }
    }

    async Task RegenerateWaterFX()
    {
        foreach (var root in instRoster.waterSurfEffect)
            root.GeneratEffect();
    }

    async Task ReBakeLightMap()
    {
        UnityEditor.AI.NavMeshBuilder.BuildNavMesh();


        instRoster.lightProbeGenerator.Regenerate();

        UnityEditor.Lightmapping.ForceStop();

        await Task.Delay(1000);

        UnityEditor.Lightmapping.BakeAsync();
    }

    /*async Task RecalcUV()
    {
        var wind = pb_Editor.MenuOpenWindow();
        await Task.Delay(100);


        foreach (var pbObj in instRoster.PBObjects)
        {
            Selection.activeGameObject = pbObj.gameObject;
            pbObj.SetSelectedFaces(pbObj.faces);
            pb_UV_Editor.MenuOpenUVEditor();
            await Task.Delay(100);
            pb_UV_Editor.instance.Menu_BoxProject();

            await Task.Delay(100);
            pb_UV_Editor.instance.Close();
            await Task.Delay(100);
        }

        await Task.Delay(100);

        wind.Close();
    }*/
}