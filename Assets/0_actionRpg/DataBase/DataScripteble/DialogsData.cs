﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DZTPC;
using ProBuilder2.Common;
using UnityEngine;
using CharacterController = DZTPC.CharacterController;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "DialogsData", menuName = "Barbarian/DialogsData", order = 1)]
public class DialogsData : ScriptableObject
{
	[Serializable]
        public class DialogData
        {
            public int id = -1;

            [Serializable]
            public class CharSpecParamValue
            {
                public LifeObject.SpecifedParams charParam = LifeObject.SpecifedParams.dexterity;
                public float charParamDelta = 0;
            }

            [Serializable]
            public class CharCollectebleItem
            {
                public CollectebleObjects.Types type = CollectebleObjects.Types.golds;
                public int count = 0;
            }

            [Serializable]
            public class CharItem
            {
                public int prefubID = 0;
                public int count = 0;

                public static bool IsHumContain(HumanoidObject humUser, CharItem[] items)
                {
                    foreach (var item in items)
                    {
                        if (humUser.GetItemsByPrefubId(item.prefubID).Count < item.count)
                            return false;
                    }

                    return true;
                }
            }

            public enum StartTypes
            {
                manualyOnce = 0,
                manualyRepiteble = 1,
                autoStartByUse = 2,
                autoStartIfVisible = 3,
                autoStartEvryware = 4,
            }

            public StartTypes StartType = StartTypes.manualyOnce;

            public bool HideIfNoCondition;
            
            [Serializable]
            public class Actions
            {
                public bool trading = false;
                public bool fighting = false;

                public int activateQuest = -1;
                public int completeQuest = -1;

                public CharSpecParamValue[] CharSpecParamChanges = new CharSpecParamValue[0];
                public CharCollectebleItem[] CharCollectebleItemChanges = new CharCollectebleItem[0];
                public CharItem[] CharItemChanges = new CharItem[0];
            }


            [Serializable]
            public class Conditions
            {
                public int activeQueste = -1;
                public int completedQueste = -1;
                public CharItem[] items = new CharItem[0];
                public CharCollectebleItem[] CharCollectebelItemChanges = new CharCollectebleItem[0];
                public AreasTypes AreasType = AreasTypes.everywhere;
                public int MaxClothPrice = -1;
                public int MinClothPrice = -1;
            }

            public Conditions conditionToStart = new Conditions();
            public Actions actions = new Actions();

            public bool CanBeStartedBy(HumanoidObject humUser, HumanoidObject humNPC, bool forceRepiteble = false)
            {
                if (conditionToStart.AreasType != AreasTypes.everywhere &&
                    GlobalData.IsPointInArea(conditionToStart.AreasType, humUser.Position) == false)
                    return false;

                if (conditionToStart.MaxClothPrice > 0 &&
                    (humUser.GetCurrentEquipedID(HumanoidObject.EquipTypes.armor) >= 0 &&
                     TPCObjectsManager.Get(humUser.GetCurrentEquipedID(HumanoidObject.EquipTypes.armor)).GetPrice() >= conditionToStart.MaxClothPrice))
                    return false;
                
                if (conditionToStart.MinClothPrice > 0 &&
                    (humUser.GetCurrentEquipedID(HumanoidObject.EquipTypes.armor) < 0 ||
                     TPCObjectsManager.Get(humUser.GetCurrentEquipedID(HumanoidObject.EquipTypes.armor)).GetPrice() < conditionToStart.MinClothPrice))
                    return false;
                
                if (conditionToStart.activeQueste >= 0)
                    if (humUser.HumAiKnowelegeBase.IsQuestActive(conditionToStart.activeQueste) == false)
                        return false;

                if (conditionToStart.completedQueste >= 0)
                    if (humUser.HumAiKnowelegeBase.IsQuestCompleted(conditionToStart.completedQueste) == false)
                        return false;

                if (!CharItem.IsHumContain(humUser, conditionToStart.items)) return false;


                foreach (var items in conditionToStart.CharCollectebelItemChanges)
                    if (humUser.GetCollectebleCount(items.type) - items.count < 0)
                        return false;


                if (actions.activateQuest >= 0)
                    if (humUser.HumAiKnowelegeBase.IsQuestActive(actions.activateQuest))
                        return false;

                if (actions.completeQuest >= 0)
                    if (humUser.HumAiKnowelegeBase.IsQuestCompleted(actions.completeQuest))
                        return false;

                if (!forceRepiteble && StartType != StartTypes.manualyRepiteble &&
                    humUser.HumAiKnowelegeBase.completedDialogs.Contains(id))
                    return false;

                return true;
            }

            public bool CheckActionsPosobility(HumanoidObject humUser, HumanoidObject humNPC)
            {
                foreach (var param in actions.CharSpecParamChanges)
                {
                    if (humUser.GetParametr(param.charParam) + param.charParamDelta < 0)
                    {
                        if (param.charParam == LifeObject.SpecifedParams.expPoints)
                            humUser.OnInteractionFailureEvent(humUser.UniqID,
                                InteractiveObject.FailureTypes.needExpPoint,
                                -(int) param.charParamDelta);
                        return false;
                    }
                }

                foreach (var items in actions.CharCollectebleItemChanges)
                {
                    if (humUser.GetCollectebleCount(items.type) + items.count < 0)
                    {
                        if (items.type == CollectebleObjects.Types.golds)
                            humUser.OnInteractionFailureEvent(humUser.UniqID, InteractiveObject.FailureTypes.needGold
                                , -items.count);
                        return false;
                    }
                }

                if (!CharItem.IsHumContain(humUser, conditionToStart.items)) return false;

                return true;
            }

            public void ApplyActions(HumanoidObject humUser, HumanoidObject humNPC)
            {
                if (id > 0 && humUser.HumAiKnowelegeBase.completedDialogs.Contains(id) == false)
                    humUser.HumAiKnowelegeBase.completedDialogs.Add(id);

                if (actions.trading)
                {
	                DialogController.DialogInProgress = false;
                    humUser.StartActionOnActiveObject(InteractiveObject.InteractionType.tradeContainer);
                    return;
                }

                if (actions.fighting)
                {
	                DialogController.DialogInProgress = false;
	                humNPC.StopUse(humUser.UniqID);
                    humNPC.HumAiKnowelegeBase.SetCurrentAIJob(new AIJobData(AIJobTypes.kill, humUser.UniqID));
	                
	                if(Vector3.Distance(humNPC.Position, humUser.Position) < 5)
		                CharacterController.GetController(humUser)?.AvalibleActions.DoActions(CCSingleActions.ToMeleeFight);
	                
                    return;
                }

                if (actions.activateQuest >= 0)
                {
                    humUser.HumAiKnowelegeBase.ActivateQuest(actions.activateQuest);
                }

                if (actions.completeQuest >= 0)
                {
                    humUser.HumAiKnowelegeBase.CompleteQuest(actions.completeQuest);
                }

                foreach (var param in actions.CharSpecParamChanges)
                {
                    humUser.AddToParametr(param.charParam, param.charParamDelta);
                }

                foreach (var items in actions.CharCollectebleItemChanges)
                {
                    humUser.AddCollectebleCount(items.type, items.count);
                    humNPC.AddCollectebleCount(items.type, -items.count);
                }

                foreach (var items in actions.CharItemChanges)
                {
                    var list = (items.count < 0 ? humUser : humNPC).GetItemsByPrefubId(items.prefubID);

                    for (int i = 0; i < Mathf.Abs(items.count) && list.Count > 0; i++)
                    {
                        var to = TPCObjectsManager.Get(list[0]) as TakebleObject;
                        if (to == null)
                            break;

                        to.StartUse((items.count < 0 ? humNPC : humUser).UniqID);
                        list.RemoveAt(0);
                    }
                }
            }

            [Serializable]
            public class ReplicDate
            {
                public string replicKey;
                public bool isPlayersReplic;
                public bool addToLog = false;
            }

            public ReplicDate[] dialog = new ReplicDate[0];

            [Serializable]
            public class Answer
            {
                public string replicKey;
                public DialogData nextDialog => DialogsData.GetDialog(nextDialogID);
                public int nextDialogID;
            }

            public Answer[] answers = new Answer[0];
        }

	
	
	private static DialogsData inst;
	public static DialogsData Inst
	{
		get
		{
			if (inst == null)
			{
				inst = Resources.LoadAll<DialogsData>("")[0];
				inst._dialogs = new Dictionary<int, DialogData>();
				inst._dialogsByChars = new Dictionary<int, DialogData[]>();

				foreach (var dialogData in inst.AllDialogs)
				{
					inst._dialogs.Add(dialogData.id, dialogData);
				}

				foreach (var npc in Inst.NpcDatas)
				{
					var ds = new DialogData[0];
					foreach (var sd in npc.StartDialogs)
						ds = ds.Add(GetDialog(sd));
					inst._dialogsByChars.Add(npc.NpcID, ds);
				}
			}
			return inst;
		}
	}

	private Dictionary<int, DialogData> _dialogs;
	private Dictionary<int, DialogData[]> _dialogsByChars;
	
	public static void Reset()
	{
		SetDirty();
		inst = null;
	}

	[Serializable]
	public struct NpcData
	{
		public int NpcID;
		public int[] StartDialogs;
	}
	
	public DialogData[] AllDialogs;
	public NpcData[] NpcDatas;

	public static DialogData GetDialog(int id) => Inst._dialogs.ContainsKey(id) ? Inst._dialogs[id] : null;

	public static int CreateDialog()
	{
		var id = 0;
		foreach (var dialogData in Inst.AllDialogs)
		{
			if (dialogData.id >= id)
				id = dialogData.id + 1;
		}

		Inst.AllDialogs = Inst.AllDialogs.Add(CreateNewDialog(id));
		
		Reset();
		return id;
	}

	public static void RemoveDialog(int id)
	{
		Inst.AllDialogs = Inst.AllDialogs.Remove(GetDialog(id));

		for (int i = 0; i < Inst.NpcDatas.Length; i++)
		{
			Inst.NpcDatas[i].StartDialogs = Inst.NpcDatas[i].StartDialogs.Remove(id);
		}

		foreach (var d in Inst.AllDialogs)
		{
			foreach (var answer in d.answers)
			{
				if (answer.nextDialogID == id)
					answer.nextDialogID = -1;
			}
		}
		
		Reset();
	}

	public static DialogData[] GetCharDialogs(int idChar)
	{
		if (Inst._dialogsByChars.ContainsKey(idChar) == false)
		{
			#if !UNITY_EDITOR
				return new DialogData[0];
			#endif

			Inst.NpcDatas = Inst.NpcDatas.Add(new NpcData
			{
				NpcID = idChar,
				StartDialogs = new int[0]
			});
			Reset();
		}

		return Inst._dialogsByChars[idChar];
	}

	public static void AddDialogToChar(int charId, int dialogId)
	{
		for (int i = 0; i < Inst.NpcDatas.Length; i++)
		{
			if (Inst.NpcDatas[i].NpcID == charId)
			{
				Inst.NpcDatas[i].StartDialogs = Inst.NpcDatas[i].StartDialogs.Add(dialogId);
			}
		}
		Reset();
	}
	
	private static DialogData CreateNewDialog(int id)
	{
		var dlg = new DialogData
		{
			HideIfNoCondition = false,
			StartType = DialogData.StartTypes.manualyOnce,
			answers = new DialogData.Answer[0],
			conditionToStart = new DialogData.Conditions()
			{
				activeQueste = -1,
				completedQueste = -1,
				items = new DialogData.CharItem[0],
				CharCollectebelItemChanges = new DialogData.CharCollectebleItem[0],
				AreasType = AreasTypes.everywhere,
				MaxClothPrice = -1,
				MinClothPrice = -1,
			},
			dialog = new[] {CreateNewReplica()},
			actions = new DialogData.Actions()
			{
				trading = false,
				fighting = false,
				activateQuest = -1,
				completeQuest = -1,
				CharSpecParamChanges = new DialogData.CharSpecParamValue[0],
				CharCollectebleItemChanges = new DialogData.CharCollectebleItem[0],
				CharItemChanges = new DialogData.CharItem[0],
			},
			id = id,
		};


		return dlg;
	}
	
	

	public static DialogData.ReplicDate CreateNewReplica()
	{
		return new DialogData.ReplicDate()
		{
			isPlayersReplic = false,
			replicKey = "dialogFailedTextKey",
		};
	}

	private static void SetDirty()
	{
#if UNITY_EDITOR
		EditorUtility.SetDirty(Inst);
#endif
	}
}
