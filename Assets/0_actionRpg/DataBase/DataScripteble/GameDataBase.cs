﻿using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using baseLogic.Asset;
using DZTPC.UI;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;

#endif
namespace DZTPC
{
    public partial class GameDataBase : ScriptableObject
    {
        static GameDataBase _instance = null;

        private System.Type[] _allTypes = null;

        public static Type[] GetAllTypes()
        {
            return Instance._allTypes;
        }

        public static GameDataBase Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = Resources.Load<GameDataBase>("GameDataBase");
                    if (_instance == null)
                    {
                        _instance = ScriptableObject.CreateInstance<GameDataBase>();

#if UNITY_EDITOR
                        AssetDatabase.CreateAsset(_instance, Path.Combine(Path.Combine("Assets", "Resources"),
                            "GameDataBase" + ".asset"
                        ));
#endif
                    }


                    System.Type myType = typeof(DataForRPGObject);
                    List<System.Type> types = new List<Type>();
                    foreach (System.Type t in Assembly.GetAssembly(myType).GetTypes())
                    {
                        if (t.IsSubclassOf(myType))
                            types.Add(t);
                    }

                    _instance._allTypes = types.ToArray();
                    _instance._allPrefubs = CollectAll();
                    InitAllPivotSystems();
                }

                return _instance;
            }
        }

        private static DataForRPGObject[] CollectAll()
        {
            return AddSubCollectToAll(new DataForRPGObject[][]
            {
                _instance._chars,
                _instance._collecteble,
                _instance._weapons,
                _instance._animals,
                _instance._foods,
                _instance._intrObjs,
                _instance._armoredMonsters,
                _instance._noTradebleObjs,
                _instance._housesVolumes,
            });
        }

        private static DataForRPGObject[] AddSubCollectToAll(DataForRPGObject[][] subListes)
        {
            List<DataForRPGObject> all = new List<DataForRPGObject>();
            int startID = 0;
            foreach (DataForRPGObject[] subSub in subListes)
            {
                int subIDs = 0;
                foreach (DataForRPGObject v in subSub)
                {
                    v.id = startID + subIDs;
                    subIDs++;
                    all.Add(v);
                }

                startID += 1000;
            }

            return all.ToArray();
        }

        private static void InitAllPivotSystems()
        {
            #if UNITY_EDITOR

            foreach (var humChar in _instance._chars)
            {
                if (humChar.prefub.GetComponent<PivotPoints>() != null)
                {
                    continue;
                }
                
                humChar.prefub.AddComponent<PivotPoints>().InitBySample(_instance._pivotPointsSample);
            }
            
            #endif
        }
        
        [NonSerialized] private DataForRPGObject[] _allPrefubs = new DataForRPGObject[0];
        [SerializeField] private PhysicMaterial _charsPhisMat;
        [SerializeField] private Material _arrowTrail;
        [SerializeField] private PivotPoints _pivotPointsSample;
        [SerializeField] private Sprite _goldsSprite;
        [SerializeField] private Sprite _arrowsSprite;
        [SerializeField] private Sprite _boltsSprite;
        [SerializeField] private BaseSoundEffects _baseSoundEffects = new BaseSoundEffects();
        [SerializeField] private BaseWeaponPrefub[] _baseWeaponPrefubs = new BaseWeaponPrefub[0];
        [SerializeField] private Collecteble[] _collecteble = new Collecteble[0];
        [SerializeField] private Characters[] _chars = new Characters[0];

        public static Characters[] Chars => Instance._chars;

        [SerializeField] private Animals[] _animals = new Animals[0];
        [SerializeField] private ArmoredMonsters[] _armoredMonsters = new ArmoredMonsters[0];
        [SerializeField] private Weapons[] _weapons = new Weapons[0];
        [SerializeField] private Food[] _foods = new Food[0];
        [SerializeField] private Interectebel[] _intrObjs = new Interectebel[0];
        [SerializeField] private NoTradeble[] _noTradebleObjs = new NoTradeble[0];
        [SerializeField] private HouseVolume[] _housesVolumes = new HouseVolume[0];

        [SerializeField] private BaseVoiceForLife[] _baseVoices = new BaseVoiceForLife[0];

        [SerializeField] private Comix[] _comixes = new Comix[0];
        
        [SerializeField] private QuestData[] _questes = new QuestData[0];
        
        public static QuestData[] Questes => Instance == null?null:_instance._questes;

        public static Comix GetComix(int index)
        {
            if (Instance == null || index < 0 || index >= Instance._comixes.Length)
                return null;

            return Instance._comixes[index];
        }

        public static DataForRPGObject Get(int id)
        {
            if (Instance == null)
                return null;

            return Instance.OldGet(id);
        }

        DataForRPGObject OldGet(int id)
        {
            foreach (DataForRPGObject v in _allPrefubs)
            {
                if (v.id == id)
                    return v;
            }

            return null;
        }

        public static BaseSoundEffects BaseSounds
        {
            get
            {
                if(Instance == null)
                    return null;

                return Instance._baseSoundEffects;
            }
        }
        
        public static DataForRPGObject[] GetAll()
        {
            return GetAll<DataForRPGObject>(true);
        }

        public static T[] GetAll<T>(bool includeInheritos = false) where T : DataForRPGObject
        {
            List<T> all = new List<T>();
            DataForRPGObject[] allBase;
            
            if (Instance == null)
            {
                allBase = CollectAll();
            }
            else
            {
                allBase = Instance._allPrefubs;
            }


            foreach (DataForRPGObject v in allBase)
            {
                if (includeInheritos)
                {
                    if (v is T)
                    {
                        all.Add(v as T);
                    }
                }
                else
                {
                    if (v.GetType() == typeof(T))
                        all.Add(v as T);
                }
                
            }

            return all.ToArray();
        }

        public static DataForRPGObject[] GetAll(System.Type type)
        {
            List<DataForRPGObject> all = new List<DataForRPGObject>();
            DataForRPGObject[] allBase;
            
            if (Instance == null)
            {
                allBase = CollectAll();
            }
            else
            {
                allBase = Instance._allPrefubs;
            }


            foreach (DataForRPGObject v in allBase)
            {
                if (v.GetType() == type)
                    all.Add(v);
            }

            return all.ToArray();
        }
        
        public static PhysicMaterial GetCharsPhysMaterial()
        {
            if (Instance == null)
                return null;
            return Instance._charsPhisMat;
        }
        
        public static Material GetArrowTrailMaterial()
        {
            if (Instance == null)
                return null;
            return Instance._arrowTrail;
        }

        public static AudioClip GetAudioEffectForAction(LifeObject life, CCSingleActions actionType)
        {
            return _instance._baseVoices[life.GetVoiceIndex()].GetPhrase(actionType);
        }

        public static Sprite GetCollectebleSprite(CollectebleObjects.Types type)
        {
            switch (type)
            {
                case CollectebleObjects.Types.golds:
                    return _instance._goldsSprite;
                    break;
                case CollectebleObjects.Types.arrows:
                    return _instance._arrowsSprite;
                    break;
                case CollectebleObjects.Types.bolts:
                    return _instance._boltsSprite;
                    break;
            }
            
            return _instance._goldsSprite;
        }
    }
}