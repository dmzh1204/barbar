﻿using System;
using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

[CreateAssetMenu(fileName = "GlobalSettings", menuName = "Barbarian/GlobalSettings", order = 1)]
public class GlobalSettings : ScriptableObject {
	public class InAppsModifications
	{
		public float StrengthBonus = 0;
		public float DexterityBonus = 0;
		public float DefenseBonus = 0;
		public float ExpirenceMultiplier = 1;
		public bool ParkurMode;
		public bool DontNeedSleepAndEat;
	}
	
	[Serializable]
	public class GraphicsSettings
	{
		public static bool UseCameraOffsetInMeleeFighting = true;
		public Texture2D[] BloodTextures;
	}
	
	[Serializable]
	public class GameplaySettings
	{		
		[Serializable]
		public class CostCoeficents
		{
			[Tooltip("цена = (ParamBonus^WeaponCostProgressPov) * WeaponCostMultiplier")]

			public WeaponObject.WeaponTypes Type = WeaponObject.WeaponTypes.oneHandMelee;
			public int WeaponCostMultiplier = 50;
			public float WeaponCostProgressPov = 1;
		}
		
		
		public float BaseTraderBuyMultiplier = 0.5f;

		[Tooltip("цена = (ParamBonus^WeaponCostProgressPov) * WeaponCostMultiplier")]
		public CostCoeficents[] Coeficents;

		public int RewardedAdPrice = 200;
		public int ArrowsCost = 5;
		public int BoltsCost = 20;

		public int GetPrice(int prefabID)
		{
			var obj = GameDataBase.Get(prefabID);
			var weapon = obj as GameDataBase.Weapons;
			
			if(weapon != null)
				foreach (var c in Coeficents)
					if (c.Type == weapon.type)
						return (int)Mathf.Pow(obj.paramBonus, c.WeaponCostProgressPov) * c.WeaponCostMultiplier;
			
			return obj.basePrice;
		}

		public float CriticalAttackChance = 0.05f;
		public float CriticalAttackMultiplier = 2f;
		public float DelayOnMissChance = 0.9f;
		public float DelayOnMiss = 1;
	}

	public const int PlayerUniqID = 0;
	public static InAppsModifications PlayerModifications = new InAppsModifications();
	
	private static GlobalSettings inst;

	public static GlobalSettings Inst
	{
		get
		{
			if (inst == null)
			{
				inst = Resources.LoadAll<GlobalSettings>("")[0];
			}
			return inst;
		}
	}

	public GameplaySettings GPSettings;
	public GraphicsSettings Graphics;
}
