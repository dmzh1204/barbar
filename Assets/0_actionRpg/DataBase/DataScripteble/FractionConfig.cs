﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DZTPC;
using UnityEngine;

[CreateAssetMenu(fileName = "FractionConfig", menuName = "Barbarian/FractionConfig", order = 100)]
public class FractionConfig : ScriptableObject
{
	public enum Attitudes
	{
		none,
		defendAlways,
		defendIfAttacked,
		aggressive,
		avoid,
	}
	
	[Serializable]
	public class AttitudeContainer
	{
		public FractionConfig FractionConfig;
		public Attitudes Attitude;
	}
	
	public AttitudeContainer[] AttitudesToFraction;

	
}

public static class FExt
{
	public static FractionConfig.Attitudes GetAttitudeTo(this FractionConfig myFraction, FractionConfig fraction) =>
		(from a in myFraction.AttitudesToFraction where a.FractionConfig == fraction select a.Attitude).FirstOrDefault();
	
	public static LifeObject CheckAttackTarget(this LifeObject myLObj, DamagParameters damag)
	{
		if(damag.Target == myLObj 
		   || Vector3.Distance(myLObj.Position, damag.Target.Position) > 15
		   || !TPCObjectsManager.IsVisible(myLObj.UniqID, damag.Target.UniqID))
			return null;

		var attitude = myLObj.LifeDataBase.FractionConfig.GetAttitudeTo(damag.Target.LifeDataBase.FractionConfig);

		if (attitude == FractionConfig.Attitudes.defendAlways)
			if(myLObj.LifeDataBase.FractionConfig.GetAttitudeTo(damag.Source.LifeDataBase.FractionConfig) 
			   != FractionConfig.Attitudes.defendAlways)
				return damag.Source;

		if (attitude == FractionConfig.Attitudes.defendIfAttacked
		    && damag.Target.CurrentFightState == LifeObject.FightingStates.none)
			if(myLObj.LifeDataBase.FractionConfig.GetAttitudeTo(damag.Source.LifeDataBase.FractionConfig) 
			   != FractionConfig.Attitudes.defendAlways)
				return damag.Source;
		
		return null;
	}
}
