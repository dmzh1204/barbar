﻿using System;
using System.Linq;
using UnityEngine;

namespace DZTPC
{
    public partial class GameDataBase : ScriptableObject
    {
        [System.Serializable]
        public class BaseSoundEffects
        {
            public AudioClip[] bowShoot = new AudioClip[0];
            public AudioClip[] crosbowShoot = new AudioClip[0];
            public AudioClip[] attackMeleeWeapon = new AudioClip[0];

            public AudioClip[] waterSplash = new AudioClip[0];
            public AudioClip[] landing = new AudioClip[0];
        }

        [System.Serializable]
        public class DataForRPGObject : baseLogic.Asset.ObjectsAsset.ObjectData
        {
            public int basePrice = 1;
            public float paramBonus = 1;

            public virtual Transform GetBasePrefub()
            {
                return null;
            }


            public virtual BaseTPCGameObject GenerateNew(Vector3 pos, Quaternion rot)
            {
                return null;
            }

            public virtual string GetPrefubDescription()
            {
                return " id=" + id;
            }
        }

        [System.Serializable]
        public class BaseWeaponPrefub
        {
            public WeaponObject.WeaponTypes Type = WeaponObject.WeaponTypes.oneHandMelee;
            public Transform Prefub = null;
        }

        [System.Serializable]
        public class Life : DataForRPGObject
        {
            public int VoiceIndex = 0;
            
            public float Dexterity = 0;
            public float Strength = 0;
            public float DefenseBonus = 0;

            public FractionConfig FractionConfig;
        }

        [System.Serializable]
        public class Characters : Life
        {
            public override BaseTPCGameObject GenerateNew(Vector3 pos, Quaternion rot)
            {
                return TPCObjectsManager.CreatNewHuman(pos, rot.eulerAngles.y, id);
            }
        }

        [System.Serializable]
        public class Animals : Life
        {
            public float AttackMeleeDistBonus = 0;
            public float AttackTimeBonus = 0;

            
            public override BaseTPCGameObject GenerateNew(Vector3 pos, Quaternion rot)
            {
                return TPCObjectsManager.CreatNewAnimal(pos, rot.eulerAngles.y, id);
                ;
            }
        }

        [System.Serializable]
        public class ArmoredMonsters : Animals
        {
            public override BaseTPCGameObject GenerateNew(Vector3 pos, Quaternion rot)
            {
                return TPCObjectsManager.CreatNewArmoredMonster(pos, rot.eulerAngles.y, id);
            }
        }


        [System.Serializable]
        public class Weapons : DataForRPGObject
        {
            public WeaponObject.WeaponTypes type = WeaponObject.WeaponTypes.oneHandMelee;

            public override BaseTPCGameObject GenerateNew(Vector3 pos, Quaternion rot)
            {
                return TPCObjectsManager.CreatNewWeapon(pos, rot, id);
                ;
            }

            public override Transform GetBasePrefub()
            {
                foreach (var basePrefub in _instance._baseWeaponPrefubs)
                {
                    if (basePrefub.Type == type)
                    {
                        return basePrefub.Prefub;
                    }
                }

                return null;
            }
        }

        [System.Serializable]
        public class Food : DataForRPGObject
        {
            public override BaseTPCGameObject GenerateNew(Vector3 pos, Quaternion rot)
            {
                return TPCObjectsManager.CreatNewFood(pos, rot, id);
                ;
            }
        }

        [System.Serializable]
        public class Collecteble : DataForRPGObject
        {
            public CollectebleObjects.Types type = CollectebleObjects.Types.golds;
            public int Count = 10;

            public override BaseTPCGameObject GenerateNew(Vector3 pos, Quaternion rot)
            {
                return TPCObjectsManager.CreatNewCollectebleObj(pos, rot, type, Count, id);
                ;
            }
        }

        [System.Serializable]
        public class Interectebel : DataForRPGObject
        {
            public InteractiveObject.InterObjectTypes InterObjectTypeOfObj = InteractiveObject.InterObjectTypes.bed;

            public override BaseTPCGameObject GenerateNew(Vector3 pos, Quaternion rot)
            {
                return TPCObjectsManager.CreatNewInteractObject(InterObjectTypeOfObj, pos, rot, id);
            }
        }

        [System.Serializable]
        public class NoTradeble : DataForRPGObject
        {
            public override BaseTPCGameObject GenerateNew(Vector3 pos, Quaternion rot)
            {
                return TPCObjectsManager.CreatNewNoTradeble(pos, rot, id);
            }
        }

        [Serializable]
        public class HouseVolume : DataForRPGObject
        {
            public override BaseTPCGameObject GenerateNew(Vector3 pos, Quaternion rot)
            {
                return TPCObjectsManager.CreatNewHouseVolume(id);
            }
        }

        [Serializable]
        public class QuestData
        {
            [Serializable]
            public class QuestsCondition
            {
                public int[] quests;
            }

            public string name = "";
            public bool startOnWorldInit = false;
            public string description;
            public Sprite icon;
            public QuestsCondition[] autoCompletConditions;

            public static void RecalcQuestes(HumAIKnowelegeBase memory)
            {
                for (var i = 0; i < Questes.Length; i++)
                {
                    if(memory.IsQuestCompleted(i))
                        continue;
                    
                    foreach (var acc in Questes[i].autoCompletConditions)
                    {
                        if (acc.quests.Any(q => !memory.IsQuestCompleted(q)))
                            continue;

                        memory.CompleteQuest(i, true);
                    }
                }
            }
        }


        [System.Serializable]
        public class BaseVoiceForLife
        {
            [Serializable]
            public class SingleVoice
            {
                public CCSingleActions ActionType;
                public AudioClip[] Clip;
            }

            public SingleVoice[] Prases;

            public AudioClip GetPhrase(CCSingleActions actionType)
            {
                foreach (var phraseVariants in Prases)
                {
                    if (phraseVariants.ActionType == actionType && phraseVariants.Clip.Length > 0)
                    {
                        return phraseVariants.Clip[UnityEngine.Random.Range(0, phraseVariants.Clip.Length)];
                    }
                }

                return null;
            }
        }
    }
}