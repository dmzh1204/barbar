﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public static class DataExtensions  {

    public static DialogsData.DialogData[] GetDialogs(this GameDataBase.Characters character)
    {
        return DialogsData.GetCharDialogs(character.id);
    }
}
