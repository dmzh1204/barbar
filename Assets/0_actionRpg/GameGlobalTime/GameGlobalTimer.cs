﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameGlobalTimer : MonoBehaviour {

    private static  DateTime _currentData = new DateTime(856, 5, 2, 8, 0, 0);

    public static DateTime CurrentData
    {
        get
        {
            return _currentData;
        }
	    set { _currentData = value; }
    }

	// Use this for initialization
	/*void Start () {
        _currentData = new DateTime(856, 5, 2, 8, 0, 0);
	}*/
	
	// Update is called once per frame
	/*void FixedUpdate () {
        _currentData = _currentData.AddSeconds((double)(Time.fixedDeltaTime * 50));
   	}*/
}
