﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyController : MonoBehaviour {

    Material _mat;

	// Use this for initialization
	void OnEnable () {
        LightingByGlobalTime.OnSunMultiplierChanged += RecalcColor;
        _mat = GetComponent<Renderer>().material;
	}
	
    void OnDisable()
    {
        LightingByGlobalTime.OnSunMultiplierChanged -= RecalcColor;
    }

    void RecalcColor(float multiplier)
    {
        multiplier = Mathf.Clamp01(multiplier);
        _mat.color = Color.white * multiplier;
    }

    Vector2 _textOffset = Vector2.zero;

	// Update is called once per frame
	void FixedUpdate () {
        transform.position = Camera.main.transform.position + Vector3.up * 25;

        _textOffset += Vector2.one * Time.fixedDeltaTime * 0.005f;

        if (_textOffset.x > 1)
            _textOffset.x -= 1;
        if (_textOffset.y > 1)
            _textOffset.y -= 1;

        _mat.mainTextureOffset = _textOffset;
	}
}
