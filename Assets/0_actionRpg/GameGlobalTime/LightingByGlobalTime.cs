﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingByGlobalTime : MonoBehaviour {

    private const float _angOneHour = 15;
    private const float _angOneMinut = 0.25f;

    private Light _sunLight = null;
    private float _sunMaxIntensy = 1;

    [SerializeField]
    private Color _fogColorMax;
    

    public static System.Action<float> OnSunMultiplierChanged;

	private static bool IsGISupported =>
		SystemInfo.graphicsDeviceType != UnityEngine.Rendering.GraphicsDeviceType.OpenGLES2 &&
		Application.platform != RuntimePlatform.WebGLPlayer;
	
	// Use this for initialization
	void Start () {

        

        _sunLight = GetComponentInChildren<Light>();
		_sunLight.shadowBias = 0.33f;
		_sunLight.shadowNormalBias = 0.9f;
		
		if (!IsGISupported)
		{
			_sunLight.shadowStrength = 0.5f;
			RenderSettings.ambientLight = new Color(0.5f, 0.5f, 0.5f);
		}
		
		_sunMaxIntensy = _sunLight.intensity;
        //_fogColorMax = RenderSettings.fogColor;
	    RenderSettings.fogMode = FogMode.Linear;
	    RenderSettings.fogStartDistance = 0;
	    RenderSettings.fogEndDistance = 150;
        InvokeRepeating("Work", 0, 0.2f);

    }
	
	void Work () {


        float angle = GameGlobalTimer.CurrentData.Hour * _angOneHour + GameGlobalTimer.CurrentData.Minute * _angOneMinut - 180;

		//transform.eulerAngles = new Vector3(angle, transform.eulerAngles.y, transform.eulerAngles.z);
		
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.right);
	    
        float sunMultiplier = Mathf.Clamp01(transform.up.y * 2 + 0.7f);
        //Debug.Log(GameGlobalTimer.CurrentData.Hour);
	    
		
        //float sunMultiplier = 1f - ((float)(Mathf.Abs(GameGlobalTimer.CurrentData.Hour - 12)) / 12);

        if (OnSunMultiplierChanged != null)
        {
            OnSunMultiplierChanged(sunMultiplier);
        }

        _sunLight.intensity = _sunMaxIntensy * sunMultiplier;



        RenderSettings.fogColor = 
            new Color(_fogColorMax.r * Mathf.Sqrt(sunMultiplier), _fogColorMax.g * sunMultiplier, _fogColorMax.b * sunMultiplier * sunMultiplier);

        Camera.main.backgroundColor = RenderSettings.fogColor;
    }
}
