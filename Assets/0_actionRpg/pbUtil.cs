﻿// Decompiled with JetBrains decompiler
// Type: ProBuilder2.Common.pbUtil
// Assembly: ProBuilderCore-Unity5, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 18AF90A4-3FCA-47DD-B60C-356C71CEC131
// Assembly location: D:\work\repositories\barbar\barbar_ios\Assets\assets\ProCore\ProBuilder\Classes\ProBuilderCore-Unity5.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ProBuilder2.Common
{
  public static class pbUtil
  {
    public static T[] GetComponents<T>(this IEnumerable<GameObject> gameObjects) where T : Component
    {
      List<T> objList = new List<T>();
      foreach (GameObject gameObject in gameObjects)
        objList.AddRange((IEnumerable<T>) gameObject.transform.GetComponentsInChildren<T>());
      return objList.ToArray();
    }

    public static T[] GetComponents<T>(GameObject go) where T : Component
    {
      return go.transform.GetComponentsInChildren<T>();
    }

    public static T[] GetComponents<T>(this IEnumerable<Transform> transforms) where T : Component
    {
      List<T> objList = new List<T>();
      foreach (Transform transform in transforms)
        objList.AddRange((IEnumerable<T>) transform.GetComponentsInChildren<T>());
      return objList.ToArray();
    }

    public static Vector3[] ToWorldSpace(this Transform t, Vector3[] v)
    {
      Vector3[] vector3Array = new Vector3[v.Length];
      for (int index = 0; index < vector3Array.Length; ++index)
        vector3Array[index] = t.TransformPoint(v[index]);
      return vector3Array;
    }

    public static GameObject EmptyGameObjectWithTransform(Transform t)
    {
      return new GameObject()
      {
        transform = {
          position = t.position,
          localRotation = t.localRotation,
          localScale = t.localScale
        }
      };
    }

    public static T[] ValuesWithIndices<T>(this T[] arr, int[] indices)
    {
      T[] objArray = new T[indices.Length];
      for (int index = 0; index < indices.Length; ++index)
        objArray[index] = arr[indices[index]];
      return objArray;
    }

    public static List<T> ValuesWithIndices<T>(this IList<T> arr, IList<int> indices)
    {
      List<T> objList = new List<T>();
      foreach (int index in (IEnumerable<int>) indices)
        objList.Add(arr[index]);
      return objList;
    }

    public static int[] AllIndexesOf<T>(T[] arr, T instance)
    {
      List<int> intList = new List<int>();
      for (int index = 0; index < arr.Length; ++index)
      {
        if (arr[index].Equals((object) instance))
          intList.Add(index);
      }
      return intList.ToArray();
    }

    public static bool IsEqual<T>(T[] a, T[] b)
    {
      if (a == null && b == null)
        return true;
      if (a == null && b != null || a != null && b == null || a.Length != b.Length)
        return false;
      for (int index = 0; index < a.Length; ++index)
      {
        if (!a[index].Equals((object) b[index]))
          return false;
      }
      return true;
    }

    public static T[] Add<T>(this T[] arr, T val)
    {
      T[] objArray = new T[arr.Length + 1];
      Array.ConstrainedCopy((Array) arr, 0, (Array) objArray, 0, arr.Length);
      objArray[arr.Length] = val;
      return objArray;
    }

    public static T[] AddRange<T>(this T[] arr, T[] val)
    {
      T[] objArray = new T[arr.Length + val.Length];
      Array.ConstrainedCopy((Array) arr, 0, (Array) objArray, 0, arr.Length);
      Array.ConstrainedCopy((Array) val, 0, (Array) objArray, arr.Length, val.Length);
      return objArray;
    }

    public static T[] Remove<T>(this T[] arr, T val)
    {
      List<T> objList = new List<T>((IEnumerable<T>) arr);
      objList.Remove(val);
      return objList.ToArray();
    }

    public static T[] Remove<T>(this T[] arr, IEnumerable<T> val)
    {
      return ((IEnumerable<T>) arr).Except<T>(val).ToArray<T>();
    }

    public static T[] RemoveAt<T>(this T[] arr, int index)
    {
      T[] objArray = new T[arr.Length - 1];
      int index1 = 0;
      for (int index2 = 0; index2 < arr.Length; ++index2)
      {
        if (index2 != index)
        {
          objArray[index1] = arr[index2];
          ++index1;
        }
      }
      return objArray;
    }

    public static T[] RemoveAt<T>(this IList<T> list, IEnumerable<int> indices)
    {
      List<int> intList = new List<int>(indices);
      intList.Sort();
      return list.SortedRemoveAt<T>((IList<int>) intList);
    }

    public static T[] SortedRemoveAt<T>(this IList<T> list, IList<int> sorted_indices)
    {
      int count1 = sorted_indices.Count;
      int count2 = list.Count;
      T[] objArray = new T[count2 - count1];
      int index1 = 0;
      for (int index2 = 0; index2 < count2; ++index2)
      {
        if (index1 < count1 && sorted_indices[index1] == index2)
        {
          while (index1 < count1 && sorted_indices[index1] == index2)
            ++index1;
        }
        else
          objArray[index2 - index1] = list[index2];
      }
      return objArray;
    }

    public static int NearestIndexPriorToValue<T>(IList<T> sorted_list, T value) where T : IComparable<T>
    {
      int count = sorted_list.Count;
      if (count < 1)
        return -1;
      pbUtil.SearchRange searchRange = new pbUtil.SearchRange(0, count - 1);
      if (value.CompareTo(sorted_list[0]) < 0)
        return -1;
      if (value.CompareTo(sorted_list[count - 1]) > 0)
        return count - 1;
      while (searchRange.Valid())
      {
        if (sorted_list[searchRange.Center()].CompareTo(value) > 0)
        {
          searchRange.end = searchRange.Center();
        }
        else
        {
          searchRange.begin = searchRange.Center();
          if (sorted_list[searchRange.begin + 1].CompareTo(value) >= 0)
            return searchRange.begin;
        }
      }
      return 0;
    }

    public static T[] Fill<T>(T val, int length)
    {
      return pbUtil.FilledArray<T>(val, length);
    }

    public static List<T> Fill<T>(Func<int, T> ctor, int length)
    {
      List<T> objList = new List<T>(length);
      for (int index = 0; index < length; ++index)
        objList.Add(ctor(index));
      return objList;
    }

    public static T[] FilledArray<T>(T val, int length)
    {
      T[] objArray = new T[length];
      for (int index = 0; index < length; ++index)
        objArray[index] = val;
      return objArray;
    }

    public static bool ContainsMatch<T>(this T[] a, T[] b)
    {
      for (int index = 0; index < a.Length; ++index)
      {
        if (Array.IndexOf<T>(b, a[index]) > -1)
          return true;
      }
      return false;
    }

    public static bool ContainsMatch<T>(this T[] a, T[] b, out int index_a, out int index_b)
    {
      index_b = -1;
      index_a = 0;
      while (index_a < a.Length)
      {
        index_b = Array.IndexOf<T>(b, a[index_a]);
        if (index_b > -1)
          return true;
        ++index_a;
      }
      return false;
    }

    public static T[] Concat<T>(this T[] x, T[] y)
    {
      if (x == null)
        throw new ArgumentNullException(nameof (x));
      if (y == null)
        throw new ArgumentNullException(nameof (y));
      int length = x.Length;
      Array.Resize<T>(ref x, x.Length + y.Length);
      Array.Copy((Array) y, 0, (Array) x, length, y.Length);
      return x;
    }

    public static int IndexOf<T>(this List<List<T>> InList, T InValue)
    {
      for (int index1 = 0; index1 < InList.Count; ++index1)
      {
        for (int index2 = 0; index2 < InList[index1].Count; ++index2)
        {
          if (InList[index1][index2].Equals((object) InValue))
            return index1;
        }
      }
      return -1;
    }

    public static T[] Fill<T>(int count, Func<int, T> ctor)
    {
      T[] objArray = new T[count];
      for (int index = 0; index < count; ++index)
        objArray[index] = ctor(index);
      return objArray;
    }

    public static void AddOrAppend<T, K>(this Dictionary<T, List<K>> dictionary, T key, K value)
    {
      List<K> kList;
      if (dictionary.TryGetValue(key, out kList))
        kList.Add(value);
      else
        dictionary.Add(key, new List<K>() { value });
    }

    public static void AddOrAppendRange<T, K>(this Dictionary<T, List<K>> dictionary, T key, List<K> value)
    {
      List<K> kList;
      if (dictionary.TryGetValue(key, out kList))
        kList.AddRange((IEnumerable<K>) value);
      else
        dictionary.Add(key, value);
    }

    public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
    {
      HashSet<TKey> knownKeys = new HashSet<TKey>();
      return source.Where<TSource>((Func<TSource, bool>) (x => knownKeys.Add(keySelector(x))));
    }

    [Obsolete]
    public static string ToFormattedString<T>(this T[] t, string _delimiter)
    {
      return t.ToFormattedString<T>(_delimiter, 0, -1);
    }

    [Obsolete]
    public static string ToFormattedString<T>(this T[] t, string _delimiter, int entriesPerLine, int maxEntries)
    {
      int num = maxEntries <= 0 ? t.Length : Mathf.Min(t.Length, maxEntries);
      if (t == null || num < 1)
        return "Empty Array.";
      StringBuilder stringBuilder = new StringBuilder();
      for (int index = 0; index < num - 1; ++index)
      {
        if (entriesPerLine > 0 && (index + 1) % entriesPerLine == 0)
          stringBuilder.AppendLine(((object) t[index] != null ? t[index].ToString() : "null") + _delimiter);
        else
          stringBuilder.Append(((object) t[index] != null ? t[index].ToString() : "null") + _delimiter);
      }
      stringBuilder.Append((object) t[num - 1] != null ? t[num - 1].ToString() : "null");
      return stringBuilder.ToString();
    }

    [Obsolete]
    public static string ToFormattedString<T>(this List<T> t, string _delimiter)
    {
      return t.ToArray().ToFormattedString<T>(_delimiter);
    }

    [Obsolete]
    public static string ToFormattedString<T>(this HashSet<T> t, string _delimiter)
    {
      return t.ToArray<T>().ToFormattedString<T>(_delimiter);
    }

    public static string ToString<TKey, TValue>(this Dictionary<TKey, TValue> dict)
    {
      StringBuilder stringBuilder = new StringBuilder();
      foreach (KeyValuePair<TKey, TValue> keyValuePair in dict)
        stringBuilder.AppendLine(string.Format("Key: {0}  Value: {1}", (object) keyValuePair.Key, (object) keyValuePair.Value));
      return stringBuilder.ToString();
    }

    public static string ToString<T>(this IEnumerable<T> arr, string separator = ", ")
    {
      return string.Join(separator, arr.Select<T, string>((Func<T, string>) (x => x.ToString())).ToArray<string>());
    }

    public static string ControlKeyString(char character)
    {
      switch (character)
      {
        case '⇧':
          return "Shift";
        case '⌘':
          return "Control";
        case '⌥':
          return "Alt";
        case '⌫':
          return "Delete";
        case '⎇':
          return "Alt";
        default:
          return character.ToString();
      }
    }

    [Obsolete("ColorWithString is deprecated. Use TryParseColor.")]
    public static bool ColorWithString(string value, out Color col)
    {
      col = Color.white;
      return pbUtil.TryParseColor(value, ref col);
    }

    public static bool TryParseColor(string value, ref Color col)
    {
      string valid = "01234567890.,";
      value = new string(value.Where<char>((Func<char, bool>) (c => valid.Contains<char>(c))).ToArray<char>());
      string[] strArray = value.Split(',');
      if (strArray.Length < 4)
        return false;
      try
      {
        float num1 = float.Parse(strArray[0]);
        float num2 = float.Parse(strArray[1]);
        float num3 = float.Parse(strArray[2]);
        float num4 = float.Parse(strArray[3]);
        col.r = num1;
        col.g = num2;
        col.b = num3;
        col.a = num4;
      }
      catch
      {
        return false;
      }
      return true;
    }

    public static Vector3[] StringToVector3Array(string str)
    {
      List<Vector3> vector3List = new List<Vector3>();
      str = str.Replace(" ", string.Empty);
      string str1 = str;
      char[] chArray = new char[1]{ '\n' };
      foreach (string str2 in str1.Split(chArray))
      {
        if (!str2.Contains("//"))
        {
          string[] strArray = str2.Split(',');
          float result1;
          float result2;
          float result3;
          if (strArray.Length >= 3 && float.TryParse(strArray[0], NumberStyles.Any, (IFormatProvider) CultureInfo.InvariantCulture, out result1) && (float.TryParse(strArray[1], NumberStyles.Any, (IFormatProvider) CultureInfo.InvariantCulture, out result2) && float.TryParse(strArray[2], NumberStyles.Any, (IFormatProvider) CultureInfo.InvariantCulture, out result3)))
            vector3List.Add(new Vector3(result1, result2, result3));
        }
      }
      return vector3List.ToArray();
    }

    public static Vector2 DivideBy(this Vector2 v, Vector2 o)
    {
      return new Vector2(v.x / o.x, v.y / o.y);
    }

    public static Vector3 DivideBy(this Vector3 v, Vector3 o)
    {
      return new Vector3(v.x / o.x, v.y / o.y, v.z / o.z);
    }

    private struct SearchRange
    {
      public int begin;
      public int end;

      public SearchRange(int begin, int end)
      {
        this.begin = begin;
        this.end = end;
      }

      public bool Valid()
      {
        return this.end - this.begin > 1;
      }

      public int Center()
      {
        return this.begin + (this.end - this.begin) / 2;
      }

      public override string ToString()
      {
        return "{" + (object) this.begin + ", " + (object) this.end + "} : " + (object) this.Center();
      }
    }
  }
}
