﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StreamRiverEffectController : WaterSurfaceEffectsController {
	protected override void InitBeforeGen()
	{
		base.InitBeforeGen();
		stepWave = 1;
		lifeTime = 1;
		spawnByFrame = 20;
		spawnDist = 50;
	}

	protected override void HandleRHPoint(RaycastHit rh, List<PointData> pd)
	{
		if(rh.normal.y >= 1)
			return;

		Vector3 dir = rh.normal;

		dir.y = 0;
			
		float speed = Mathf.Clamp(dir.magnitude * 50, 0, 5);
		
		if(speed <= 0)
			return;
		
		dir.Normalize();
		dir = Vector3.ProjectOnPlane(dir, rh.normal);
		dir.Normalize();

		pd.Add(new PointData()
		{
			startPosition = rh.point + rh.normal * 0.1f,
			direction = dir,
			speed = speed,
		});
	}
}
