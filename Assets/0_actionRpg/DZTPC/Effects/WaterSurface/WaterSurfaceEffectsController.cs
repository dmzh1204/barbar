﻿using System.Collections.Generic;
#if !UNITY_WEBGL
using System.Threading.Tasks;
    #endif
using UnityEngine;
using Random = UnityEngine.Random;

public class WaterSurfaceEffectsController : MonoBehaviour
{
    [SerializeField] private MeshFilter _waterMesh = null;

    [SerializeField] private ParticleSystem _bigWavesParticles = null;

    [SerializeField] private ParticleSystem _streamParticles = null;

    [System.Serializable]
    public class PointData
    {
        public Vector3 startPosition;
        public Vector3 direction;
        public Vector3 rotation3d;
        public float speed = 1;
    }

    [SerializeField] PointData[] _bigWavePoints = new PointData[0];


    protected float stepWave = 5;
    protected float lifeTime = 10;
    private float waveMinSpace = 15;
    private float waveDistFromEdge = 10;
    
    const int layerMask = 16;

    [ContextMenu("GeneratEffect")]
    public void GeneratEffect()
    {
        InitBeforeGen();
        //layerMask = 1 >> _waterMesh.gameObject.layer;
        Vector3 center;
        Vector3 halfSize;

        center = _waterMesh.transform.position + _waterMesh.sharedMesh.bounds.center;

        halfSize = _waterMesh.sharedMesh.bounds.extents;

        GenerateBigWaves(center, halfSize);
    }

    protected virtual void InitBeforeGen()
    {
        
    }

    void GenerateBigWaves(Vector3 center, Vector3 halfSize)
    {
        List<PointData> pd = new List<PointData>();
        
        for (float x = center.x - halfSize.x; x < center.x + halfSize.x; x += stepWave)
        for (float y = center.y - halfSize.y; y < center.y + halfSize.y; y += stepWave)
        for (float z = center.z - halfSize.z; z < center.z + halfSize.z; z += stepWave)
        {
            Vector3 pos = new Vector3(x,y,z);

            RaycastHit rh;
            
            if(Physics.Raycast(pos, Vector3.down, out rh, stepWave, layerMask))
            {
                HandleRHPoint(rh, pd);
            }

        }

        _bigWavePoints = pd.ToArray();
    }

    protected virtual void HandleRHPoint(RaycastHit rh, List<PointData> pd)
    {
        if (rh.normal.y == 1)
        {
            for (float a = 0; a < 360; a += 45)
            {
                TryCreateWaveEffect(rh.point + Vector3.up * 0.15f, Quaternion.AngleAxis(a, Vector3.up) * Vector3.forward, pd);
            }
        }
    }

    void TryCreateWaveEffect(Vector3 pos, Vector3 dir, List<PointData> pd)
    {
        RaycastHit rh;
        if (Physics.Raycast(pos, dir, out rh, stepWave * 2))
        {
            Vector3 normal = rh.normal;
            
            if(normal.y > 0.97f)
                return;

            normal.y = 0;
            normal.Normalize();

            if (Physics.Raycast(rh.point, normal, waveMinSpace))
            {
                return;
            }
            
            pd.Add(new PointData()
            {
                startPosition = rh.point + normal * waveDistFromEdge,
                direction = -normal,
                speed = 1,
                rotation3d = new Vector3(90,Vector3.SignedAngle(Vector3.forward, -normal, Vector3.down) + 90,0),
            });
        }
    }

    // Use this for initialization
    void Start()
    {
        InitBeforeGen();
    }

    private bool needSpawn = false;
    
    private void OnEnable()
    {
        needSpawn = true;
    }

    private void OnDisable()
    {
        needSpawn = false;
    }

    private int currentIndex = 0;

    private PointData[] points = new PointData[0];

    private float spawnInterval = 0.1f;
    
    protected int spawnByFrame = 1;
    protected float spawnDist = 150;

    private float nextTimeSpawn = 0;
// Update is called once per frame
    async void Update()
    {
        if (Time.time < nextTimeSpawn)
            return;

        nextTimeSpawn = Time.time + spawnInterval;
        
        for (int i = 0; i < spawnByFrame; i++)
        {
            if (float.IsPositiveInfinity(nextTimeSpawn))
            {
                return;
            }

            if (currentIndex >= points.Length)
            {
    #if !UNITY_WEBGL
                points = await GetPointsForSpawn(Camera.main.transform.position, spawnDist);
    #endif 
                return;
            }

            

            var point = points[currentIndex];

            _bigWavesParticles.Emit(new ParticleSystem.EmitParams()
            {
                position = point.startPosition,
                velocity = point.direction * point.speed,
                rotation3D = point.rotation3d,
                startLifetime = lifeTime,
            }, 1);


            currentIndex++;
        }
        
    }
#if !UNITY_WEBGL
    async Task<PointData[]> GetPointsForSpawn(Vector3 camPos, float radius = 100, int maxCount = 100)
        #endif 
#if UNITY_WEBGL
    PointData[] GetPointsForSpawn(Vector3 camPos, float radius = 100, int maxCount = 100)
        #endif 
    {
        nextTimeSpawn = float.PositiveInfinity;
        
        currentIndex = 0;
        
        List<PointData> points = new List<PointData>();

        int iteractionCount = 0;

        while (points.Count < maxCount && iteractionCount < maxCount * 10)
        {
            iteractionCount++;
            var point = _bigWavePoints[Random.Range(0, _bigWavePoints.Length - 1)];

            if (Vector3.Distance(point.startPosition, camPos) < radius)
            {
                points.Add(point);
            }
        }
        
        nextTimeSpawn = float.NegativeInfinity;

        return points.ToArray();
    }
    
    
}