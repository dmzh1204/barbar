﻿using System;
#if !UNITY_WEBGL
using System.Threading.Tasks;
#endif
using baseLogic.Audio;
using DZTPC;
using UnityEngine;
using Random = UnityEngine.Random;

public class BlockEffectController : MonoBehaviour
{
    [SerializeField] Transform[] _prefubs = new Transform[1];

    [SerializeField] private AudioClip[] _audioClips = new AudioClip[3];

    private void OnEnable()
    {
        GlobalTPCEvents.OnAttackBlocked += OnBlock;
    }

    private void OnDisable()
    {
        GlobalTPCEvents.OnAttackBlocked -= OnBlock;
    }

#if !UNITY_WEBGL
        private async void OnBlock(Vector3 pos)
    #else
    private void OnBlock(Vector3 pos)
#endif
    {
#if !UNITY_WEBGL
		await Task.Delay(TimeSpan.FromMilliseconds(200));
                        #endif
        Transform prefub = _prefubs[Random.Range(0, _prefubs.Length - 1)];

        Destroy(Instantiate(prefub, pos, new Quaternion()).gameObject, 3);

        AudioSystem.PlayEffect(_audioClips[Random.Range(0, _audioClips.Length - 1)], pos);
    }
}