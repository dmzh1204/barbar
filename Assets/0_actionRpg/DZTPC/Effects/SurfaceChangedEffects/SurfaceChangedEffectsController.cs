﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class SurfaceChangedEffectsController : MonoBehaviour
	{
		[SerializeField] private Transform _landingPrefub = null;
		[SerializeField] private Transform _waterSplashPrefub = null;


		private void OnEnable()
		{
			GlobalTPCEvents.OnLanding += OnLanding;
			GlobalTPCEvents.OnWaterSplash += OnWaterSplash;
		}

		private void OnDisable()
		{
			GlobalTPCEvents.OnLanding -= OnLanding;
			GlobalTPCEvents.OnWaterSplash -= OnWaterSplash;        
		}

		void OnLanding(Vector3 pos)
		{
			DoEffect(pos, _landingPrefub);
		}

		void OnWaterSplash(Vector3 pos)
		{
			DoEffect(pos, _waterSplashPrefub);
		}

		void DoEffect(Vector3 pos, Transform prefub)
		{
			if(prefub == null)
				return;
			
			Destroy(Instantiate(prefub, pos, Quaternion.AngleAxis(Random.value * 360, Vector3.up)).gameObject, 5);
		}
	}
	
}