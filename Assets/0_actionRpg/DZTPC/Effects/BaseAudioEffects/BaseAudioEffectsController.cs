﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using baseLogic.Audio;

namespace DZTPC
{
    public class BaseAudioEffectsController : MonoBehaviour
    {
        private void OnEnable()
        {
            GlobalTPCEvents.OnShootBow += OnShootBow;
            GlobalTPCEvents.OnShootCrossbow += OnShootCrossbow;
            GlobalTPCEvents.OnAttackMellee += OnMeleeAttack;
            GlobalTPCEvents.OnLanding += OnLanding;
            GlobalTPCEvents.OnWaterSplash += OnWaterSplash;
        }

        private void OnDisable()
        {
            GlobalTPCEvents.OnShootBow -= OnShootBow;
            GlobalTPCEvents.OnShootCrossbow -= OnShootCrossbow;
            GlobalTPCEvents.OnAttackMellee -= OnMeleeAttack;
            GlobalTPCEvents.OnLanding -= OnLanding;
            GlobalTPCEvents.OnWaterSplash -= OnWaterSplash;        
        }

        AudioClip GetRandom(AudioClip[] clips)
        {
            if (clips == null || clips.Length == 0)
                return null;
            
            return clips[Random.Range(0, clips.Length - 1)];
        }
        
        void OnShootBow(Vector3 pos)
        {
            AudioSystem.PlayEffect(GetRandom(GameDataBase.BaseSounds.bowShoot));
        }
        void OnShootCrossbow(Vector3 pos)
        {
            AudioSystem.PlayEffect(GetRandom(GameDataBase.BaseSounds.bowShoot));
        }
        void OnMeleeAttack(Vector3 pos)
        {
            AudioSystem.PlayEffect(GetRandom(GameDataBase.BaseSounds.attackMeleeWeapon));

        }
        void OnLanding(Vector3 pos)
        {
            AudioSystem.PlayEffect(GetRandom(GameDataBase.BaseSounds.landing));

        }
        void OnWaterSplash(Vector3 pos)
        {
            AudioSystem.PlayEffect(GetRandom(GameDataBase.BaseSounds.waterSplash));

        }
    }
}