﻿using System.Collections;
using System.Collections.Generic;
using baseLogic.Audio;
using UnityEngine;

namespace DZTPC
{
	public class BloodEffectController : MonoBehaviour
	{
		[SerializeField] private Transform _prefubSmall = null;
		[SerializeField] private Transform _prefubMedium = null;
		[SerializeField] private Transform _prefubBig = null;
		[SerializeField] private Transform _prefubVeryBig = null;
		
		public AudioClip[] AudioClips = new AudioClip[0];

		class Job
		{
			public DamagParameters Damag;
			public Vector3 pos;
			public Vector3 dir;
		}
		
		Queue<Job> _jobs = new Queue<Job>();
		
		// Use this for initialization
		void OnEnable()
		{
			CharacterController.GlobalOnDamageEffect += OnDamageEffect;
		}

		// Update is called once per frame
		void OnDisable()
		{
			CharacterController.GlobalOnDamageEffect -= OnDamageEffect;
		}

		void OnDamageEffect(DamagParameters damag, Vector3 pos, Vector3 dir)
		{
			_jobs.Enqueue(new Job{Damag = damag, dir = dir, pos = pos});
		}

		void Update()
		{
			if(_jobs.Count == 0)
				return;
			
			CreateEffect(_jobs.Dequeue());
		}

		void CreateEffect(Job job)
		{
			Transform prefub = _prefubSmall;

			var percent = job.Damag.CutDamage / job.Damag.Target.GetParametr(LifeObject.DependetParams.maxHealth);
			
			if(percent > 0.05)
				prefub = _prefubMedium;
			if(percent > 0.1)
				prefub = _prefubBig;
			if(percent > 0.3)
				prefub = _prefubVeryBig;

			Destroy(Instantiate(prefub, job.pos, Quaternion.AngleAxis(360 * Random.value, job.dir)).gameObject, 3);
			AudioSystem.PlayEffect(AudioClips[Random.Range(0, AudioClips.Length - 1)], job.pos);
		}
		
	}
}