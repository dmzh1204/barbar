﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DecalDropper : MonoBehaviour
{
    private const int DecalMaxCount = 30;
    
    private static Queue<GameObject> spawnedDecals = new Queue<GameObject>();
    
    public GameObject[] Prefubs;

    public int Count = 5;

    private void Start()
    {
        StartCoroutine(Emmit());
    }

    // Use this for initialization
    IEnumerator Emmit()
    {
        for (var i = 0; i < Count; i++)
        {
            yield return new WaitForEndOfFrame();
            RaycastHit rh;
            NavMeshHit nvh;

            float range = 0.5f * Count;
            var pos = transform.position + new Vector3(Random.Range(-range, range), 0, Random.Range(-range, range));

            if (NavMesh.SamplePosition(pos, out nvh, 3, 1) && Physics.Raycast(nvh.position, Vector3.down, out rh, 1, 1))
                spawnedDecals.Enqueue(Instantiate(
                    Prefubs[Random.Range(0, Prefubs.Length)], 
                    nvh.position, 
                    Quaternion.AngleAxis(360 * Random.value, rh.normal)));
            
            if(spawnedDecals.Count > DecalMaxCount)
                Destroy(spawnedDecals.Dequeue());
        }

        yield return null;
    }
}