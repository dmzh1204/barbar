﻿#if !UNITY_IOS && !UNITY_WEBGL
using iRobi;
#endif
using UnityEngine;

public class CharacterUVPaintController : MonoBehaviour
{
	private float restoreDelay = 120;
	public int Count = 0;
	public void OnPaint()
	{
		restoreDelay = 120;
		Count++;
	}

	private void Update()
	{
		restoreDelay -= Time.deltaTime;
		if(restoreDelay > 0)
			return;
		#if !UNITY_IOS && !UNITY_WEBGL
		UVPaint.FullRestore(gameObject);
		#endif
		Destroy(this);
	}
}
