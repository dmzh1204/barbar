﻿using System.Collections;
#if !UNITY_IOS && !UNITY_WEBGL
using iRobi;
#endif
using UnityEngine;
using CharacterController = DZTPC.CharacterController;

public class CharacterPainter : MonoBehaviour
{

	private Texture2D[] Textures => GlobalSettings.Inst.Graphics.BloodTextures;
	// Use this for initialization
	void Start ()
	{
#if !UNITY_IOS && !UNITY_WEBGL
		StartCoroutine(Paint());
#endif
	}

	IEnumerator Paint()
	{
		yield return new WaitForSeconds(0.2f);

		for (int i = 0; i < 2; i++)
		{
			
		
		RaycastHit rh;
		
		var pos = transform.position + 
		          transform.right * (Random.value * 2 - 1) * 0.5f +
		          transform.up * (Random.value * 2 - 1) * 0.5f;

			var dir = transform.forward;

			if (i == 1)
			{
				pos += dir;
				dir = -dir;
			}
			
		if (Physics.Raycast(pos, dir, out rh, 3) && rh.collider.GetComponent<CharacterController>() != null)
		{
			yield return new WaitForFixedUpdate();
			if (rh.collider == null)
				yield return null;

			var p = rh.collider.GetComponent<CharacterUVPaintController>();

			if (p == null)
				p = rh.collider.gameObject.AddComponent<CharacterUVPaintController>();
			
#if !UNITY_IOS && !UNITY_WEBGL
			if(p.Count < 10)
				UVPaint.Create(rh.collider.gameObject, pos, Quaternion.AngleAxis(360 * Random.value, dir), Textures[Random.Range(0, Textures.Length)]);
			#endif
			p.OnPaint();
		}
			
		}

		
		yield return null;
	}
}
