﻿using System;
using System.Collections;
using System.Collections.Generic;
using baseLogic;
using Cinemachine;
using DZTPC;
using UnityEngine;
using CharacterController = DZTPC.CharacterController;

public class DialogController : MonoBehaviour
{
    public CinemachineVirtualCamera CameraDlg;

    public static bool TalkingNow = false;
    public static bool DialogInProgress = false;

    private static int _currentStep = 0;

    public static int CurrentStep => _currentStep;

    public static HumanoidObject DialogebleNpc => _dialogebleNpc;

    public static float DelayToNextStep = 0;

    private static HumanoidObject _dialogebleNpc = null;

    public static Action OnNextStep;

    private Animator currentAnimator = null;

    private Vector3 defCamOffset = new Vector3(0.6f, 0, 0.6f);

    private Vector3 vecToNpc;

    Transform npcTransform;
    Transform playerTransform;

    // Use this for initialization
    void Start()
    {
        HumanoidObject.OnDialogStart += OnDialogStart;
        InvokeRepeating(nameof(Init), 0.1f, 0.1f);
    }

    void Init()
    {
        if (UserController.PlayerCharacter == null || UserController.PlayerCharacter.HumanObj == null)
            return;

        CancelInvoke(nameof(Init));

        UserController.PlayerCharacter.HumanObj.OnInteractionFailure += OnInteractionFailure;

        UserController.PlayerCharacter.HumanObj.OnInteractionStateChanged += OnPlayerInteractionObjectChange;

        OnPlayerInteractionObjectChange();
    }

    private void OnDestroy()
    {
        HumanoidObject.OnDialogStart -= OnDialogStart;

        TalkingNow = false;
        DialogInProgress = false;
        _currentStep = 0;
        DelayToNextStep = 0;
        _dialogebleNpc = null;

        if (UserController.PlayerCharacter == null)
            return;

        UserController.PlayerCharacter.HumanObj.OnInteractionStateChanged -= OnPlayerInteractionObjectChange;
        UserController.PlayerCharacter.HumanObj.OnInteractionFailure -= OnInteractionFailure;
    }

    void OnPlayerInteractionObjectChange()
    {
    }

    void OnInteractionFailure(int idIntrObj, InteractiveObject.FailureTypes failType, int count)
    {
        DialogInProgress = false;
        TalkingNow = false;
        CameraDlg.gameObject.SetActive(false);
        UserController.PlayerCharacter.AvalibleActions.DoActions(CCSingleActions.StopUseObject);
    }

    void OnDialogStart(HumanoidObject npc)
    {
        if (UserController.PlayerLifeObj.UniqID != npc.CurrentUserId)
            return;

        npcTransform = CharacterController.GetController(npc).transform;
        playerTransform = UserController.PlayerCharacter.transform;

        vecToNpc = npcTransform.position - playerTransform.position;
        vecToNpc.y = 0;
        vecToNpc.Normalize();


        if (npc.CurrentDialog == null)
        {
            CameraDlg.gameObject.SetActive(false);
            TalkingNow = false;
            //DialogInProgress = false;
            return;
        }

        _dialogebleNpc = npc;

        CameraDlg.gameObject.SetActive(true);
        TalkingNow = true;
        DialogInProgress = true;

        _currentStep = -1;
        NextStep();
    }

    private void NextStep()
    {
        playerTransform.forward = vecToNpc;
        npcTransform.forward = -vecToNpc;

        const string talkingAntr = "Talking";
        if (currentAnimator != null)
            currentAnimator.SetBool(talkingAntr, false);
        _currentStep++;

        if (_currentStep >= _dialogebleNpc.CurrentDialog.dialog.Length)
        {
            _dialogebleNpc.CurrentDialog.ApplyActions(UserController.PlayerCharacter.HumanObj, _dialogebleNpc);
            
            TalkingNow = false;

            if (_dialogebleNpc.CurrentDialog.answers.Length == 0)
            {
                CameraDlg.gameObject.SetActive(false);

                if (UserController.PlayerCharacter.HumanObj.CurrentInterInteractionHum !=
                    InteractiveObject.InteractionType.tradeContainer)
                {
                    DialogInProgress = false;

                    if (_dialogebleNpc.GetAvailableDialogs(UserController.PlayerCharacter.HumanObj).Count > 0 && _dialogebleNpc.CurrentUserId >= 0)
                    {
                        _dialogebleNpc.StopUse(UserController.PlayerLifeObj.UniqID);
                        _dialogebleNpc.StartUse(UserController.PlayerLifeObj.UniqID);
                    }
                    else
                    {
                        UserController.PlayerCharacter.AvalibleActions.DoActions(CCSingleActions.StopUseObject);
                    }
                }
            }
            else
            {
                SetCamera(UserController.PlayerLifeObj);
            }

            return;
        }

        LifeObject lo = _dialogebleNpc.CurrentDialog.dialog[_currentStep].isPlayersReplic
            ? UserController.PlayerLifeObj
            : _dialogebleNpc;

        currentAnimator = CharacterController.GetController(lo).GetComponentInChildren<Animator>();
        if (currentAnimator != null)
            currentAnimator.SetBool(talkingAntr, true);

        SetCamera(lo);

        DelayToNextStep = GetReplicaDuration(_dialogebleNpc.CurrentDialog.dialog[_currentStep].replicKey);

        currentAnimator.GetComponentInChildren<CharacterFace>()?.StartTalk(DelayToNextStep);

        if (_dialogebleNpc.CurrentDialog.dialog[_currentStep].addToLog)
        {
            UserController.PlayerCharacter.HumanObj.HumAiKnowelegeBase.AddToLog(lo.PrefubId,
                _dialogebleNpc.CurrentDialog.dialog[_currentStep].replicKey);
            CenterNotificationController.Show("LogEntry".Localize());
        }

        OnNextStep?.Invoke();
    }

    private void SetCamera(LifeObject obj)
    {
        CharacterController cc = CharacterController.GetController(obj);


        if (cc == null)
            return;

        Vector3 camPos;
        Vector3 up = Vector3.up * cc.Params.GetParametr(LifeObject.SpecifedParams.height) * 0.9f;
        //camPos = cc.transform.position + cc.transform.forward * 2 + Vector3.up + cc.transform.right;
        camPos = cc.transform.position + cc.transform.TransformVector(defCamOffset) + up;
        CameraDlg.transform.position = camPos;

        //CameraDlg.transform.LookAt(cc.transform.position + Vector3.up + cc.transform.forward);
        CameraDlg.transform.LookAt(cc.transform.position + up);
    }

    public static float GetReplicaDuration(string key)
    {
        return Mathf.Clamp((float) key.Localize().Length / 10, 2, float.MaxValue);
    }

    private void Update()
    {
        if (TalkingNow == false)
            return;

        if (DelayToNextStep > 0)
        {
            DelayToNextStep -= Time.deltaTime;
            return;
        }

        NextStep();
    }
}