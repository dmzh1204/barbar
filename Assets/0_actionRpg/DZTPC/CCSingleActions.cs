﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public enum CCSingleActions
    {

        none = 0,
        AttackToBlock = 3,
        SimpleAttack = 5,
        CounterBlock = 17,
        BreackedAttack = 18,

        Revive = 6,
        StartUseObject = 7,
        StopUseObject = 16,
        Drop = 11,
        Activate = 8,
        Eating = 15,

        ToMeleeFight = 12,
        ToNoFight = 13,
        ToShooting = 14,
        
        DodgeForward = 21,
        DodgeBack = 22,
        DodgeRight = 23,
        DodgeLeft = 24,

        Damage = 9,
        Death = 10,
    }
}