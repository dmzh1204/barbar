﻿using System;
using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

[Serializable]
public class LockedContainerObj : ContainerObject
{
    private int _keyId = -1;
    
    private bool _consumeKey = false;

    public bool ConsumeKey
    {
        get { return _consumeKey; }
        set { _consumeKey = value; }
    }

    public int KeyId
    {
        get { return _keyId; }
        set { _keyId = value; }
    }

    

    public LockedContainerObj(int id, int prefubID, Vector3 position, Quaternion rotation) : base(id, prefubID,
        position, rotation)
    {
    }

    public override bool CanBeUsed(int userID)
    {
        if (base.CanBeUsed(userID) == false)
            return false;

        if (_keyId < 0)
        {
            return true;
        }

        foreach (var itemID in Inventory)
        {
            if (itemID == _keyId)
            {
                return true;
            }
        }

        HumanoidObject humUser = TPCObjectsManager.Get(userID) as HumanoidObject;
        
        if (humUser.Contain(_keyId))
        {
            return true;
        }
        
        humUser.OnInteractionFailureEvent(UniqID, FailureTypes.needKey, 0);
        
        return false;
    }

    public override void StartUse(int userID)
    {
        base.StartUse(userID);

        if (_currentUserID < 0)
            return;

        HumanoidObject humUser = TPCObjectsManager.Get(_currentUserID) as HumanoidObject;
        
        if (_consumeKey && _keyId >= 0)
        {
            humUser.RemoveFromInventar(_keyId);
            TPCObjectsManager.Remove(_keyId);
            _keyId = -1;
        }
        
        GeneratePosibleTradeElements();
        
        humUser.GeneratePosibleTradeElements();
        
        humUser.StartActionOnActiveObject(InteractionType.tradeContainer);
        
        
    }
    
}