﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    [Serializable]
    public class BedRepairIntrObj : InteractiveObject
    {
        private const float _repairPerSecond = 0.6f;
        private const LifeObject.SpecifedParams _targetParam = LifeObject.SpecifedParams.energy;
        private const LifeObject.DependetParams _maxLimit = LifeObject.DependetParams.maxEnergy;


        public BedRepairIntrObj(int id, int prefubID, Vector3 position, Quaternion rotation) : base(id, prefubID,
            position, rotation)
        {
            _type = InterObjectTypes.bed;
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);


            if (_currentUserID < 0 || (_currentInteractionIo != InteractionType.sleepToDay
                                       && _currentInteractionIo != InteractionType.sleepToNight
                                       && _currentInteractionIo != InteractionType.sleepToRepair))
                return;

            HumanoidObject humanoidObject = TPCObjectsManager.Get(_currentUserID) as HumanoidObject;

            if (humanoidObject == null)
                return;

            humanoidObject.SetParametr(_targetParam, humanoidObject.GetParametr(_targetParam) + _repairPerSecond * deltaTime);

            switch (_currentInteractionIo)
            {
                case InteractionType.sleepToRepair:
                    if (humanoidObject.GetParametr(_targetParam) >= humanoidObject.GetParametr(_maxLimit))
                    {
                        StopUse(humanoidObject.UniqID);
                    }

                    break;

                case InteractionType.sleepToDay:
                    if (GameGlobalTimer.CurrentData.Hour > 5 && GameGlobalTimer.CurrentData.Hour < 12)
                    {
                        StopUse(humanoidObject.UniqID);
                    }
                    break;

                case InteractionType.sleepToNight:
                    if (GameGlobalTimer.CurrentData.Hour > 17)
                    {
                        StopUse(humanoidObject.UniqID);
                    }
                    break;
            }
        }

        protected override ActionAndDescription[] GenerateActionsForUser(int userID)
        {
            List<ActionAndDescription> _actions = new List<ActionAndDescription>();

            if (_currentInteractionIo == InteractionType.none)
            {
                _actions.Add(new ActionAndDescription(InteractionType.sleepToRepair, "sleepToRepair", userID, UniqID));

                if (GameGlobalTimer.CurrentData.Hour > 12)
                    _actions.Add(new ActionAndDescription(InteractionType.sleepToDay, "sleepToDay", userID, UniqID));

                if (GameGlobalTimer.CurrentData.Hour < 17)
                    _actions.Add(new ActionAndDescription(InteractionType.sleepToNight, "sleepToNight", userID, UniqID));
            }

            _actions.AddRange(base.GenerateActionsForUser(userID));

            return _actions.ToArray();
        }

        public override void InitAfterDeserialize()
        {
            base.InitAfterDeserialize();

            _type = InterObjectTypes.bed;
        }

        public override bool CanBeUsed(int userID)
        {
            if (base.CanBeUsed(userID) == false)
            {
                StopUse(_currentUserID);
                return false;
            }

            return true;
        }

        public override void StartUse(int userID)
        {
            var hum = (TPCObjectsManager.Get(userID) as HumanoidObject);

            if(!TPCObjectsManager.CanBeUsedByDist(hum, this))
                return;
            
            hum.Position = Position;
            hum.Rotation = Rotation;
            base.StartUse(userID);
        }

        public override void StopUse(int userID)
        {
            HumanoidObject hum = (TPCObjectsManager.Get(userID) as HumanoidObject);
            
            hum.Position = Position + Rotation * (Vector3.right * 1.5f) + (Vector3.down / 2);
            hum.Rotation = Quaternion.Euler(0 ,Rotation.eulerAngles.y + 90 ,0);
            base.StopUse(userID);
            
            /*if(hum.PrefubId == 9)
                throw new Exception("hadan wakeup");*/
        }
    }
}