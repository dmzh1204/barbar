﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DZTPC.Utill
{
    public static class HumanoidInterGenerator
    {
        public static InteractiveObject.ActionAndDescription[] Generate(HumanoidObject human, HumanoidObject humanUser)
        {
            List<InteractiveObject.ActionAndDescription> actions = new List<InteractiveObject.ActionAndDescription>();

            if (human.CurrentInteractionIo == InteractiveObject.InteractionType.dialogInteraction)
            {
                if (human.CurrentDialog == null)
                {
                    GameDataBase.Characters character = GameDataBase.Get(human.PrefubId) as GameDataBase.Characters;

                    int i = 0;
                    foreach (var dialog in character.GetDialogs())
                    {
                        if (dialog.CanBeStartedBy(humanUser, human))
                        {
                            actions.Add(new InteractiveObject.ActionAndDescription(
                                (InteractiveObject.InteractionType) i,
                                dialog.dialog[0].replicKey,
                                humanUser.UniqID, human.UniqID));
                            i++;
                        }
                    }

                    return actions.ToArray();
                }

                for (int i = 0; i < human.CurrentDialog.answers.Length; i++)
                {
                    if (human.CurrentDialog.answers[i].nextDialog.CanBeStartedBy(humanUser, human, true))
                        actions.Add(new InteractiveObject.ActionAndDescription((InteractiveObject.InteractionType) actions.Count
                            , human.CurrentDialog.answers[i].replicKey
                            , humanUser.UniqID, human.UniqID));
                }

                return actions.ToArray();
            }

            foreach (var aiTemp in human.HumAiKnowelegeBase.ActiveAiTemplates)
            {
                AddActionsByAITemplate(actions, aiTemp, human.CurrentInteractionIo, human, humanUser);
            }

            if (human.CurrentInteractionIo == InteractiveObject.InteractionType.tradeContainer)
            {
                int priceDelta = 0;

                foreach (var elem in human.SelectedTradeElements)
                {
                    priceDelta -= elem.GetPrice();
                }

                foreach (var elem in humanUser.SelectedTradeElements)
                {
                    priceDelta += elem.GetPrice(GlobalSettings.Inst.GPSettings.BaseTraderBuyMultiplier);
                }

                if (priceDelta >= 0)
                    actions.Add(new InteractiveObject.ActionAndDescription(
                        InteractiveObject.InteractionType.tradeConfirm, "tradeConfirm"
                        , humanUser.UniqID, human.UniqID
                    ));
            }

            return actions.ToArray();
        }


        static void AddActionsByAITemplate(List<InteractiveObject.ActionAndDescription> actions
            , HumAIKnowelegeBase.HumAITemplates aiTemp, InteractiveObject.InteractionType currentIntrType
            , HumanoidObject human, HumanoidObject humanUser)
        {
            switch (currentIntrType)
            {
                case InteractiveObject.InteractionType.none:
                case InteractiveObject.InteractionType.askToLearningDexterity:
                case InteractiveObject.InteractionType.askToLearningStrength:
                    switch (aiTemp)
                    {
                        case HumAIKnowelegeBase.HumAITemplates.strengthTeacher:
                        case HumAIKnowelegeBase.HumAITemplates.dexterityTeacher:
                            AddLearnAction(actions, human, humanUser);
                            break;

                        case HumAIKnowelegeBase.HumAITemplates.trading:
                            actions.Add(new InteractiveObject.ActionAndDescription(
                                InteractiveObject.InteractionType.tradeContainer, "tradeSeller", humanUser.UniqID,
                                human.UniqID
                            ));
                            break;
                    }

                    break;

                case InteractiveObject.InteractionType.askToLearning:
                    switch (aiTemp)
                    {
                        case HumAIKnowelegeBase.HumAITemplates.strengthTeacher:
                            actions.Add(new InteractiveObject.ActionAndDescription(
                                InteractiveObject.InteractionType.askToLearningStrength, "askToLearningStrength",
                                humanUser.UniqID, human.UniqID
                            ));
                            break;

                        case HumAIKnowelegeBase.HumAITemplates.dexterityTeacher:
                            actions.Add(new InteractiveObject.ActionAndDescription(
                                InteractiveObject.InteractionType.askToLearningDexterity, "askToLearningDexterity",
                                humanUser.UniqID, human.UniqID
                            ));
                            break;
                    }

                    break;
            }
        }

        static void AddLearnAction(List<InteractiveObject.ActionAndDescription> actions
            , HumanoidObject human, HumanoidObject humanUser)
        {
            actions.Add(new InteractiveObject.ActionAndDescription(
                InteractiveObject.InteractionType.askToLearning, "askToLearning", humanUser.UniqID, human.UniqID
            ));
        }

        /*public static void HandleAction(InteractiveObject.InteractionType actionType, HumanoidObject user,
            HumanoidObject target)
        {
            switch (actionType)
            {
                case InteractiveObject.InteractionType.askToLearningDexterity:
                    if (user.GetParametr(LifeObject.SpecifedParams.expPoints) < 1)
                    {
                        user.OnInteractionFailure(target.UniqID, InteractiveObject.FailureTypes.needExpPoint);
                        break;
                    }

                    user.AddToParametr(LifeObject.SpecifedParams.dexterity, 1);
                    user.AddToParametr(LifeObject.SpecifedParams.expPoints, -1);
                    break;

                case InteractiveObject.InteractionType.askToLearningStrength:
                    if (user.GetParametr(LifeObject.SpecifedParams.expPoints) < 1)
                    {
                        user.OnInteractionFailure(target.UniqID, InteractiveObject.FailureTypes.needExpPoint);
                        break;
                    }

                    user.AddToParametr(LifeObject.SpecifedParams.strength, 1);
                    user.AddToParametr(LifeObject.SpecifedParams.expPoints, -1);
                    break;
            }
        }*/
    }
}