﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Xml.Serialization;
using UnityEngine;

namespace DZTPC
{
    [Serializable]
    public abstract class InteractiveObject : BaseTPCGameObject
    {
        [NonSerialized] protected int _currentUserID = -1;

        public int CurrentUserId => _currentUserID;

        [NonSerialized] protected InteractionType _currentInteractionIo = InteractionType.none;

        public InteractionType CurrentInteractionIo => _currentInteractionIo;


        public enum InterObjectTypes
        {
            human,
            animal,
            bed,
            food,
            weapon,
            activator,
            door,
            container,
        }

        public enum InteractionType
        {
            none = 101,
            stopInteraction = 102,
            sleepToNight = 103,
            sleepToDay = 104,
            sleepToRepair = 105,

            tradeContainer = 106,
            tradeConfirm = 107,

            askToLearning = 108,
            askToLearningStrength = 109,
            askToLearningDexterity = 110,

            dialogInteraction = 111,
            dialogAnswer01 = 0,
            dialogAnswer02 = 1,
            dialogAnswer03 = 2,
            dialogAnswer04 = 3,
            dialogAnswer05 = 4,
        }

        public enum FailureTypes
        {
            needKey = 0,
            busy = 1,
            needExpPoint = 2,
            needGold = 3,
            needItem = 4,
        }

        public class ActionAndDescription
        {
            private InteractionType _type;

            public InteractionType Type => _type;

            public string Description => _description;

            private string _description;

            private int _userID = -1;
            private int _targetID = -1;

            public ActionAndDescription(InteractionType type, string descr, int userID, int targetID)
            {
                _type = type;
                _description = descr;
                _userID = userID;
                _targetID = targetID;
            }

            public void Execute()
            {
                (TPCObjectsManager.Get(_targetID) as InteractiveObject).StartActionByUser(_type);
            }
        }

        [NonSerialized] protected InterObjectTypes _type = InterObjectTypes.human;

        public InterObjectTypes Type => _type;


        public InteractiveObject(int id, int prefubID, Vector3 position, Quaternion rotation) : base(id, prefubID,
            position, rotation)
        {
        }


        public virtual bool CanBeUsed(int userID)
        {
            var humUser = TPCObjectsManager.Get(userID) as HumanoidObject;

            if (!TPCObjectsManager.CanBeUsedByDist(humUser, this))
                return false;

            if (_currentUserID < 0 || _currentUserID == userID)
                return true;

            humUser.OnInteractionFailureEvent(UniqID, FailureTypes.busy, 0);
            return false;
        }

        public virtual void StartUse(int userID)
        {
            if (CanBeUsed(userID) == false)
                return;
            var hum = (TPCObjectsManager.Get(userID) as HumanoidObject);


            hum.SetAvalibleInteractionOptions(UniqID, GenerateActionsForUser(userID));


            _currentUserID = userID;
        }


        protected virtual ActionAndDescription[] GenerateActionsForUser(int userID)
        {
            return new ActionAndDescription[0];
        }

        public virtual void StopUse(int userID)
        {
            if(_currentUserID < 0)
                return;
            
            if (_currentUserID == userID)
                _currentUserID = -1;

            HumanoidObject hum = (TPCObjectsManager.Get(userID) as HumanoidObject);

            hum.SetAvalibleInteractionOptions(-1);
            _currentUserID = -1;
            _currentInteractionIo = InteractionType.none;
        }

        public virtual void StartActionByUser(InteractionType interactionType)
        {
            _currentInteractionIo = interactionType;

            (TPCObjectsManager.Get(_currentUserID) as HumanoidObject).SetAvalibleInteractionOptions(UniqID,
                GenerateActionsForUser(_currentUserID));
        }

        public override void InitAfterDeserialize()
        {
            base.InitAfterDeserialize();


            _currentUserID = -1;
            _currentInteractionIo = InteractionType.none;

            foreach (var humanoid in TPCObjectsManager.GetAll<HumanoidObject>())
            {
                if (humanoid.CurrentInterObject == UniqID)
                {
                    _currentUserID = humanoid.UniqID;
                    StartActionByUser(humanoid.CurrentInterInteractionHum);
                    break;
                }
            }
        }
    }
}