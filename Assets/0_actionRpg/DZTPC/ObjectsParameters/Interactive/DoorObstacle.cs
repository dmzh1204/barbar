﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public class DoorObstacle : MonoBehaviour
{

	public Vector3 OpenPosition;
	public Vector3 ClosePosition;

	private bool isOpened;

	public int QuestsCondition;
	
	// Use this for initialization
	private void Start () {
		InvokeRepeating("Check", Random.value * 3, 3);
	}
	
	// Update is called once per frame
	private void Check ()
	{
		if(UserController.PlayerCharacter == null)
			return;

		isOpened = UserController.PlayerCharacter.HumanObj.HumAiKnowelegeBase.IsQuestCompleted(QuestsCondition);

		transform.localPosition = isOpened ? OpenPosition : ClosePosition;
	}
}
