﻿using System.Collections.Generic;
using System.Linq;
#if !UNITY_WEBGL
	using System.Threading.Tasks;
	#endif
using DZTPC;
using UnityEngine;
using UnityEngine.AI;

public class ObjectsReplenisher : MonoBehaviour
{
	private float _interval = 120;

	private float _radius = 15;
	
	private int _maxCountLimit = 4;

	private int[] _prefubVariants = new[]
	{
		3000,
	};

	public enum ObjTypes
	{
		boars,
		mashroms,
		corn,
	}

	[SerializeField] private ObjTypes _type = ObjTypes.boars;

	public ObjTypes Type => _type;

	static List<ObjectsReplenisher> GeneratePoints = new List<ObjectsReplenisher>();

	public static List<Vector3> WildAnimals = new List<Vector3>();
	public static List<Vector3> FoodPlants = new List<Vector3>();
	
	// Use this for initialization
	void Start () {

		switch (_type)
		{
				case ObjTypes.boars:
					_prefubVariants = new[]
					{
						3000,
					};
					
					_radius = 15;
					WildAnimals.Add(transform.position);
					break;
				
				case ObjTypes.mashroms:
					_prefubVariants = new[]
					{
						4000,
					};
					_radius = 5;
					FoodPlants.Add(transform.position);
					break;
				
				case ObjTypes.corn:
					_prefubVariants = new[]
					{
						4002,
					};
					_radius = 5;
					FoodPlants.Add(transform.position);

					break;
		}
		
		InvokeRepeating("TryReplenish", 20 * Random.value + 1, _interval);
		
		GeneratePoints.Add(this);
	}

	void OnDestroy ()
	{
		CancelInvoke();
		GeneratePoints.Remove(this);
		if (GeneratePoints.Count == 0)
		{
			WildAnimals.Clear();
			FoodPlants.Clear();
		}
	}

#if !UNITY_WEBGL
	async void TryReplenish()
	#endif
#if UNITY_WEBGL
	void TryReplenish()
	#endif
	{
		int objsCount = 0;
#if !UNITY_WEBGL
		objsCount = await CheckObjsCount();
		#endif
#if UNITY_WEBGL
		objsCount = CheckObjsCount();
#endif
		if (objsCount < _maxCountLimit)
		{
			GenerateObj();
		}
	}

#if !UNITY_WEBGL
	async Task<int> CheckObjsCount()
				#endif
#if UNITY_WEBGL
	int CheckObjsCount()
#endif
	{
		int count = 0;

		foreach (var obj in GetAll())
		{			
			if(!_prefubVariants.Contains(obj.PrefubId))
				continue;
			
			var tobj = obj as TakebleObject;
			if(tobj != null && tobj.TakedBy >=0)
				continue;

			float dist = Vector3.Distance(obj.Position, transform.position);
			if (dist < _radius)
			{
				count++;
			}
		}
		
		return count;
	}

	BaseTPCGameObject[] GetAll()
	{
		TakebleObject[] takable = null;
			
		switch (_type)
		{
				case ObjTypes.boars:
					return TPCObjectsManager.GetAll<AllEatAnimalObj>();
				
			case ObjTypes.mashroms:
				takable = TPCObjectsManager.GetAll<FoodObject>();
				break;
			
			case ObjTypes.corn:
				takable = TPCObjectsManager.GetAll<FoodObject>();
				break;
		}

		if (takable != null)
		{
			List<BaseTPCGameObject> listTakeble= new List<BaseTPCGameObject>();
			foreach (var obj in takable)
			{
				if (obj.TakedBy < 0)
				{
					listTakeble.Add(obj);
				}
			}

			return listTakeble.ToArray();
		}

		return new BaseTPCGameObject[0];
	}

	void GenerateObj()
	{
		Vector3 pos = transform.position + Random.insideUnitSphere * _radius;

		NavMeshHit hit;
		if (NavMesh.SamplePosition(pos, out hit, _radius, 1))
		{
			pos = hit.position;
		}
		else
		{
			return;
		}

		BaseTPCGameObject go = GameDataBase.Get(_prefubVariants[Random.Range(0, _prefubVariants.Length - 1)])
			.GenerateNew(pos, Quaternion.AngleAxis(Random.value * 360, Vector3.up));
		
		Debug.Log("Object Replenished - " + go.GetName());
	}
}
