﻿using System.Collections;
using System.Collections.Generic;

namespace DZTPC
{
    
    public interface IInitPoint
    {

        void Init();
        void InitSecond();

    }
}