﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public class AdderAIOwnObjs : MonoBehaviour, IInitPoint
{
    [SerializeField] private ObjectCreator[] _objCreators = new ObjectCreator[0];
    public float RandomHomePosition = 5;

    public void Init()
    {
    }

    public void InitSecond()
    {
        HumanoidObject parHum =
            TPCObjectsManager.Get(transform.parent.GetComponent<ObjectCreator>().GeneratedObjectId) as HumanoidObject;
        
        parHum.HumAiKnowelegeBase.HomePositionRandom = RandomHomePosition;

        foreach (var obj in _objCreators)
        {
            parHum.HumAiKnowelegeBase.OwnObjects.Add(obj.GeneratedObjectId);

            AddContainerInventar(parHum, obj.GeneratedObjectId);
            AddObjectsInHouse(parHum, obj.GeneratedObjectId);
        }

        parHum.HumAiKnowelegeBase.Init(parHum);
    }

    void AddContainerInventar(HumanoidObject parHum, int idCont)
    {
        ContainerObject container = TPCObjectsManager.Get(idCont) as ContainerObject;

        if (container == null)
            return;

        foreach (var id in container.Inventory)
        {
            parHum.HumAiKnowelegeBase.OwnObjects.Add(id);
        }
    }

    void AddObjectsInHouse(HumanoidObject parHum, int idCont)
    {
        HouseObject house = TPCObjectsManager.Get(idCont) as HouseObject;

        if (house == null)
            return;

        foreach (var obj in house.GetObjectsInVolume())
        {
            parHum.HumAiKnowelegeBase.OwnObjects.Add(obj.UniqID);
            //AddContainerInventar(parHum, obj.UniqID);
        }

    }
}