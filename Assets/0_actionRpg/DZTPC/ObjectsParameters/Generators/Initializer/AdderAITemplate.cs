﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public class AdderAITemplate : MonoBehaviour, IInitPoint
{
	[SerializeField]
	private HumAIKnowelegeBase.HumAITemplates[] _templates = new HumAIKnowelegeBase.HumAITemplates[]{HumAIKnowelegeBase.HumAITemplates.trading};
	
	public void Init()
	{
		HumanoidObject parHum =
			TPCObjectsManager.Get(transform.parent.GetComponent<ObjectCreator>().GeneratedObjectId) as HumanoidObject;
		
		foreach (var t in _templates)
		{
			parHum.HumAiKnowelegeBase.AddAITemplate(t);
		}
	}
	
	public void InitSecond()
	{
			
	}
}
