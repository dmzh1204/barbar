﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using baseLogic;
using baseLogic.uiScreens;
using UnityEngine;

namespace DZTPC
{
    public class RootInitializer : MonoBehaviour
    {
        public bool TutorialMode;

        private static RootInitializer inst;
        public static RootInitializer Inst => inst ?? (inst = GameObject.FindObjectOfType<RootInitializer>());
        
        BaseTPCGameObject[] _updateList = new BaseTPCGameObject[0];

        private const float _updateInterval = 1;
        private float _updateDelay = 0;
        private int _missUpdateCount = 0;

        private int _index = 0;

        private static int savingGamesCounter = 0;

        private static int saveSlotsCount = 1;
        private float defaultAdInterval = 600;
        private float lastDefaultAdTime = 0;

        public static int SaveSlotsCount
        {
            get
            {
#if UNITY_EDITOR
                return 10;
#endif
                return saveSlotsCount;
            }
            set { saveSlotsCount = value; }
        }

        private void OnDestroy()
        {
            inst = null;
        }

        // Use this for initialization
        void Start()
        {
            inst = this;
            Debug.Log("Init GDB -> " + GameDataBase.Instance);
            TPCObjectsManager.AllObjects = new BaseTPCGameObject[] { };

            if(!TutorialMode)
                Load(CurrentSaveIndex);

            if (TPCObjectsManager.Get(0) == null)
                Init();
            
            if (!TutorialMode)
            {
                InvokeRepeating("AutoSave", 40, 40);
                InvokeRepeating("CheckAd", 2, 2);

                defaultAdInterval = RemoteSettings.GetInt("adMinutInterval", 10) * 60;
                lastDefaultAdTime = Time.unscaledTime;
            }
#if UNITY_WEBGL
                StartCoroutine(TPCObjectsManager.RecalcObjectsFoTakeCorutine());
            #endif
        }


        private void Init()
        {
            IInitPoint[] points = GetComponentsInChildren<IInitPoint>();

            foreach (IInitPoint ip in points)
            {
                ip.Init();
            }

            foreach (IInitPoint ip in points)
            {
                ip.InitSecond();
            }
        }


        void AutoSave()
        {
            Save(0);
        }

        private const string _saveFileName = "/BarbarianSaveFile_";
        private const string _saveFileVer = "_v0";

        //private static string GetVersion

        private void CheckAd()
        {
            defaultAdInterval = RemoteSettings.GetInt("adMinutInterval", 10) * 60;

            if (!baseLogic.monetization.AdsManager.IsRewardAdAvailable ||
                Time.unscaledTime - lastDefaultAdTime < defaultAdInterval || 
                !IsSaveAvailable())
                return;

            baseLogic.monetization.AdsManager.ShowDefaultAd();
            lastDefaultAdTime = Time.unscaledTime;
        }

        public static bool IsSaveAvailable()
        {
            if (UIScreensManager.CurrentScreen.ScreenType != UIScreen.ScreenTypes.gameController
                && UIScreensManager.CurrentScreen.ScreenType != UIScreen.ScreenTypes.pauseGame)
                return false;

            if (DialogController.DialogInProgress || DialogController.TalkingNow)
                return false;

            if (UserController.PlayerCharacter == null || UserController.PlayerCharacter.AvalibleActions.InProcess)
                return false;

            if (UserController.PlayerCharacter.Params == null)
                return false;

            if (UserController.PlayerCharacter.Params.IsDead())
                return false;

            if (UserController.PlayerCharacter.CurSurfState != SurfaseStates.onGround)
                return false;

            if (UserController.PlayerCharacter.Params.CurrentFightState != LifeObject.FightingStates.none)
                return false;

            if (UserController.PlayerCharacter.HumanObj.CurrentInterObject >= 0)
                return false;

            return true;
        }

        public static void Save(int slotIndex)
        {
            if (!IsSaveAvailable() || inst.TutorialMode)
                return;


            IFormatter binaryFormater = new BinaryFormatter();

            Stream stream = new FileStream(FullFileNameVersion(slotIndex), FileMode.OpenOrCreate, FileAccess.Write);

            binaryFormater.Serialize(stream, TPCObjectsManager.AllObjects);

            stream.Close();

            CenterNotificationController.Show(LocalizeAdapter.GetText("Game Saved"));
        }

        public static int CurrentSaveIndex = 0;

        public static bool IsSlotSaved(int slotIndex = 0)
        {
            return File.Exists(FullFileName(slotIndex));
        }

        public static int GetLastSavedSlot()
        {
            int slot = -1;
            var lastest = DateTime.MinValue;

            for (int i = 0; i <= SaveSlotsCount; i++)
            {
                if (!IsSlotSaved(i))
                    continue;

                var date = File.GetLastWriteTime(FullFileName(i));

                if (lastest >= date)
                    continue;

                lastest = date;
                slot = i;
            }

            return slot;
        }

        public static string GetSaveLabel(int slotIndex = 0) => (slotIndex == 0 ? $"AUTO" : $"{slotIndex}")
                                                                + (IsSlotSaved(slotIndex)
                                                                    ? " :" + File.GetLastWriteTime(
                                                                          FullFileName(slotIndex))
                                                                    : "");

        private static void Load(int slotIndex)
        {
            if (File.Exists(FullFileName(slotIndex)) == false || inst.TutorialMode)
                return;

            IFormatter binaryFormater = new BinaryFormatter();

            Stream stream = new FileStream(FullFileName(slotIndex), FileMode.OpenOrCreate, FileAccess.Read);

            TPCObjectsManager.AllObjects = (BaseTPCGameObject[]) binaryFormater.Deserialize(stream);

            stream.Close();
        }

        public static bool IsSaveSlotCorrect(int index) => FullFileName(index) == FullFileNameVersion(index);

        private static string FullFileName(int index)
        {
            if (File.Exists(FullFileNameVersion(index)))
                return FullFileNameVersion(index);
            string fileName = Application.persistentDataPath + _saveFileName + index + "_vError";
            foreach (var file in Directory.GetFiles(Application.persistentDataPath))
            {
                if (file.Contains(_saveFileName.Split('/')[1] + index))
                    fileName = file;
            }

            return fileName;
        }

        private static string FullFileNameVersion(int index) =>
            Application.persistentDataPath + _saveFileName + index + "_v" + Application.version;

        public static bool IsSaveFileExist(int index = 0)
        {
            return File.Exists(FullFileName(index));
        }
    }
}