﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace DZTPC
{
	public class ObjectCreator : MonoBehaviour, IInitPoint
	{
		
		public int id = 0;

		private int _generatedObjectID = -1;

		public int GeneratedObjectId => _generatedObjectID;

		public ObjectCreator linkToOtherObject = null;
		public bool consumeKey = true;
		
		
		public void Init()
		{
			
			BaseTPCGameObject tpcGo = GameDataBase.Get(id).GenerateNew(transform.position, transform.rotation);

			_generatedObjectID = tpcGo.UniqID;
			
			ObjectCreator parentObCr = transform.parent.GetComponent<ObjectCreator>();

			if (parentObCr != null && tpcGo is TakebleObject)
			{
				(tpcGo as TakebleObject).StartUse(parentObCr._generatedObjectID);
			}
			
			//TPCObjectsManager.CreatNewWeapon(transform.position, transform.rotation, 0);
		}

		public void InitSecond()
		{
			LockedContainerObj lockedContainer = TPCObjectsManager.Get(_generatedObjectID) as LockedContainerObj;

			if (lockedContainer != null && linkToOtherObject != null)
			{
				lockedContainer.KeyId = linkToOtherObject._generatedObjectID;
				lockedContainer.ConsumeKey = consumeKey;
			}
		}
		
		#if UNITY_EDITOR
		private void OnDrawGizmos()
		{
			if(Application.isPlaying || transform.parent.GetComponent<ObjectCreator>() != null)
				return;
			Gizmos.color = new Color(0.2f, 0.2f, 1, 0.4f);
			
			Gizmos.DrawCube(transform.position + Vector3.up, new Vector3(1,2,1));

			if (GameDataBase.Get(id) == null)
			{
				if (id >= 0)
				{
					foreach (var prefubObj in GameDataBase.GetAll())
					{
						if (prefubObj.name == transform.name)
						{
							id = prefubObj.id;
							return;
						}
					}
				}

				return;
			}

			if(GameDataBase.Get(id).prefub == null)
				return;
			
			MeshFilter mf = GameDataBase.Get(id).prefub.GetComponentInChildren<MeshFilter>();
			SkinnedMeshRenderer skm = GameDataBase.Get(id).prefub.GetComponentInChildren<SkinnedMeshRenderer>();

			Mesh mesh = null;
			Transform meshT = null;
			if (mf != null)
			{
				meshT = mf.transform;
				mesh = mf.sharedMesh;
			}

			if (skm != null)
			{
				mesh = skm.sharedMesh;
				meshT = skm.transform;
			}

			if(mesh == null)
				return;
			var curMat = Gizmos.matrix;
			
			//Gizmos.matrix = meshT.localToWorldMatrix;
			Gizmos.color = new Color(1f, 0.2f, 0.2f, 0.4f);
			Gizmos.DrawMesh(mesh, transform.position + meshT.position, transform.rotation * meshT.rotation, meshT.lossyScale);
			//Gizmos.matrix = curMat;
		}
		#endif
	}
}