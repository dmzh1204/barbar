﻿using System;
using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

namespace DZTPC
{

	public class WeatherCreator : MonoBehaviour, IInitPoint
	{

		[SerializeField] private int year = 856;
		[SerializeField] private int month = 5;
		[SerializeField] private int day = 2;
		[SerializeField] private int hour = 8;

		public void Init()
		{
			TPCObjectsManager.CreateWeather(new DateTime(year, month, day, hour, 0, 0));
		}

		public void InitSecond()
		{
			
		}
	}
}