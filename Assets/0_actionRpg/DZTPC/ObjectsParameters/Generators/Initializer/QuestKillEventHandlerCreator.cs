﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DZTPC
{
	public class QuestKillEventHandlerCreator : MonoBehaviour, IInitPoint
	{

		public ObjectCreator[] Targets = new ObjectCreator[0];

		public int QuestIndexForComplite = -1;
		
		public void Init()
		{
		}

		public void InitSecond()
		{
			TPCObjectsManager.CreateKillQuestHandler(Targets.Select(x => x.GeneratedObjectId).ToArray(), QuestIndexForComplite);
		}
	}
}