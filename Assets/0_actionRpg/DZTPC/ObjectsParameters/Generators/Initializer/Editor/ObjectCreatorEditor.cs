﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using baseLogic;
using NUnit.Framework.Internal.Filters;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Assertions.Must;

namespace DZTPC
{
	
	[CustomEditor(typeof(ObjectCreator))]
	public class ObjectCreatorEditor : Editor
	{
		private int _index = -1;
		private int _typeIndex = -1;
		
		override public void OnInspectorGUI()
		{
			if(Application.isPlaying)
				return;
			
			ObjectCreator objCreator = target as ObjectCreator;
			
			List<string> names = new List<string>();

			foreach (var t in GameDataBase.GetAllTypes())
			{
				names.Add(t.Name);
			}
			
			if (_typeIndex < 0)
			{
				if(GameDataBase.Get(objCreator.id) == null)
					return;
				_typeIndex = names.IndexOf((GameDataBase.Get(objCreator.id).GetType().Name));
			}
			
			_typeIndex = EditorGUILayout.Popup(_typeIndex, names.ToArray());
			
			names = new List<string>();
			
			GameDataBase.DataForRPGObject[] elements = GameDataBase.GetAll(GameDataBase.GetAllTypes()[_typeIndex]);

			foreach (var el in elements)
			{
				if (_index < 0 && el.id == objCreator.id)
				{
					_index = names.Count;
				}
				names.Add($"{el.name.Localize()} (bonus = {el.paramBonus}; price = {el.basePrice})");
				
			}

			if (names.Count == 0)
			{
				objCreator.id = -1;
				target.name = "null";
				EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
			}
			else
			{
				_index = Mathf.Clamp(_index, 0, names.Count - 1);

				_index = EditorGUILayout.Popup(_index, names.ToArray());

				objCreator.id = elements[_index].id;
				target.name = elements[_index].name;
				
				EditorGUILayout.TextField("" + elements[_index].GetPrefubDescription());

			}

			ExtensionElements();
			
			EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
		}


		void ExtensionElements()
		{
			ObjectCreator objCreator = target as ObjectCreator;

			GameDataBase.Interectebel interacteble = GameDataBase.Get(objCreator.id) as GameDataBase.Interectebel;
			
			if (interacteble != null)
			{
				if (interacteble.InterObjectTypeOfObj == InteractiveObject.InterObjectTypes.container)
				{
					objCreator.linkToOtherObject = EditorGUILayout.ObjectField("key "
						, objCreator.linkToOtherObject
						, objCreator.GetType()
						, true) as ObjectCreator;

					objCreator.consumeKey = EditorGUILayout.IntField("consumeKey(1-consume)", objCreator.consumeKey ? 1 : 0) == 1;
				}
			}
		}
		
	}
}