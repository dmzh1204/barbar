﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class TPCObjectsPreHandler : MonoBehaviour
    {
        const float _distForVisibleObj = 30;
        const float _updateInterval = 1;
        private const int PlayerID = 0;

        private HumanoidObject _playerHum;

        private static bool _sleepMode = false;

        // Use this for initialization
        void Start()
        {
            
            Invoke("Init", 0.5f);
        }

        void Init()
        {
            ObjectsHandlers.RemoveAll();

            TPCObjectsManager.OnCreated += OnCreated;
            
            foreach (var id in TPCObjectsManager.GetAll())
            {
                OnCreated(id);
            }
            
            TPCObjectsManager.OnRemoved += OnRemoved;
            InvokeRepeating("ManualUpdate", 0, _updateInterval);
        }

        private void Update()
        {
            ObjectsHandlers.Update();

            if (_playerHum != null)
                return;
            
            _playerHum = (TPCObjectsManager.Get(PlayerID) as HumanoidObject);

            if (_playerHum == null)
                return;
            
            transform.position = _playerHum.Position;

            _playerHum.OnInteractionStateChanged += OnPlayerInteraction;
            OnPlayerInteraction();
        }

        void OnPlayerInteraction()
        {
            _sleepMode = (_playerHum.CurrentInterInteractionHum == InteractiveObject.InteractionType.sleepToDay
                          || _playerHum.CurrentInterInteractionHum == InteractiveObject.InteractionType.sleepToNight
                          || _playerHum.CurrentInterInteractionHum == InteractiveObject.InteractionType.sleepToRepair
                );

            if (_sleepMode)
            {
                foreach (var id in TPCObjectsManager.GetClosest(PlayerID))
                {
                    ObjectsHandlers.RemoveVisibleHandler(id);
                }

                foreach (var id in TPCObjectsManager.GetFarest(PlayerID))
                {
                    ObjectsHandlers.RemoveVisibleHandler(id);
                }

                BaseObjectHandler.UpdateTimeMultiplier = 100;
            }
            else
            {
                BaseObjectHandler.UpdateTimeMultiplier = 1;
            }
        }

        void OnDestroy()
        {
            TPCObjectsManager.OnCreated -= OnCreated;
            TPCObjectsManager.OnRemoved -= OnRemoved;
            _playerHum.OnInteractionStateChanged -= OnPlayerInteraction;

            ObjectsHandlers.RemoveAll();
        }

        // Update is called once per frame
        void ManualUpdate()
        {

            if (_sleepMode)
                return;

            foreach (var id in TPCObjectsManager.GetClosest(PlayerID))
            {
                ObjectsHandlers.AddVisibleHandler(id);
            }

            foreach (var id in TPCObjectsManager.GetFarest(PlayerID))
            {
                ObjectsHandlers.RemoveVisibleHandler(id);
            }
        }

        void OnRemoved(int id)
        {
            ObjectsHandlers.OnDestroy(id);
        }

        void OnCreated(int id)
        {
            ObjectsHandlers.OnCreated(id);

            ChooseHandlerForObject(id);
        }

        void ChooseHandlerForObject(int id)
        {
            if (_sleepMode && id != PlayerID)
            {
                ObjectsHandlers.RemoveVisibleHandler(id);
                return;
            }

            if (id == PlayerID || Vector3.Distance(transform.position, TPCObjectsManager.Get(id).Position) <
                _distForVisibleObj)
            {
                ObjectsHandlers.AddVisibleHandler(id);
            }
            else
            {
                ObjectsHandlers.RemoveVisibleHandler(id);
            }
        }
    }
}