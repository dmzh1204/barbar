﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class ObjectsHandlers
    {
        static Dictionary<int, GameObject> _visibleHandlers = new Dictionary<int, GameObject>();
        
        static Dictionary<int, BaseObjectHandler> _baseHandlers = new Dictionary<int, BaseObjectHandler>();
        static List<BaseObjectHandler> _baseHandlersList = new List<BaseObjectHandler>();

        private static int _currentUpdIndex = 0;

        public static GameObject Get(int id)
        {
            if (_visibleHandlers.ContainsKey(id) == false)
                return null;

            return _visibleHandlers[id];
        }
        
        public static int GetID(GameObject gObject)
        {
            foreach (var handler in _visibleHandlers)
            {
                if (handler.Value == gObject)
                    return handler.Key;
            }
            
            return -1;
        }


        public static void AddVisibleHandler(int id)
        {
            if (_visibleHandlers.ContainsKey(id) || _baseHandlers.ContainsKey(id) == false)
                return;
            _visibleHandlers.Add(id, GenerateVisibleObject(id));
            
            _baseHandlers[id].IsVisible = true;

        }

        public static void OnCreated(int id)
        {
            if (TPCObjectsManager.Get(id) is HumanoidObject)
            {
                _baseHandlers.Add(id, new HumanoidObjectHandler(id, Time.time, false));
            }
            else
            {
                _baseHandlers.Add(id, new BaseObjectHandler(id, Time.time, false));
            }
            _baseHandlersList.Add(_baseHandlers[id]);
        }
        public static void OnDestroy(int id)
        {
            RemoveVisibleHandler(id);

            _baseHandlersList.Remove(_baseHandlers[id]);
            _baseHandlers.Remove(id);
        }

        public static void RemoveAll()
        {
            int[] keys = new int[_visibleHandlers.Count];
            
            _visibleHandlers.Keys.CopyTo(keys, 0);

            foreach (int key in keys)
            {
                RemoveVisibleHandler(key);
            }

            foreach (var h in _baseHandlersList)
            {
                h.OnDestroy();
            }
            
            _baseHandlersList.Clear();
            _baseHandlers.Clear();
        }

        public static void RemoveVisibleHandler(int id)
        {
            if (_visibleHandlers.ContainsKey(id))
            {
                GameObject.Destroy(_visibleHandlers[id]);
                _visibleHandlers.Remove(id);
            }
            
            if(_baseHandlers.ContainsKey(id))
                _baseHandlers[id].IsVisible = false;
        }


        public static void Update()
        {
            if(_baseHandlersList.Count == 0)
                return;
            
            for (int i = 0; i < 5; i++)
            {
                if (_currentUpdIndex >= _baseHandlersList.Count)
                    _currentUpdIndex = 0;
            
                _baseHandlersList[_currentUpdIndex].SetCurrentTime(Time.time);

                _currentUpdIndex++;
            }
        }
        
        static GameObject GenerateVisibleObject(int id)
        {
            GameObject new_go = null;
            BaseTPCGameObject go = TPCObjectsManager.Get(id);

            LifeObject lifeObj = go as LifeObject;

            if (lifeObj != null)
            {
                new_go = new GameObject("Character");
                
                new_go.transform.position = go.Position;
                new_go.transform.rotation = go.Rotation;
                
                GameObject body = (GameObject)GameObject.Instantiate(go.GetPrefub(), new_go.transform);
                body.transform.localPosition = go.GetPrefub().transform.localPosition;
                body.transform.localRotation = go.GetPrefub().transform.localRotation;

                //body.AddComponent<Animator>().runtimeAnimatorController = ModelsAsset.Instance.Animators[0];


                new_go.AddComponent<CharacterController>().Params = lifeObj;

                body.AddComponent<CharAnimation>();

                if (lifeObj.UniqID == 0)
                {
                    new_go.AddComponent<UserController>();
                }
                else
                {
                    if (lifeObj is HumanoidObject)
                    {
                        new_go.AddComponent<CharacterControllerAI>();
                    }
                    else
                    {
                        new_go.AddComponent<AnimalAI>();
                    }
                }
                
                return new_go;
            }
            
            TakebleObject to = go as TakebleObject;
            if (to != null && to.TakedBy < 0)
            {

                new_go = go.Instantiate();
                new_go.name = "MWeapon";


                new_go.transform.position = go.Position;
                new_go.transform.rotation = go.Rotation;

                new_go.AddComponent<TakebleController>().Params = to;

                return new_go;
            }
            
            if(go != null && go.GetPrefub() != null)
            {

                new_go = go.Instantiate();
                new_go.name = go.GetName();

                new_go.transform.position = go.Position;
                new_go.transform.rotation = go.Rotation;

                return new_go;
            }

            return new_go;
        }


    }

}
