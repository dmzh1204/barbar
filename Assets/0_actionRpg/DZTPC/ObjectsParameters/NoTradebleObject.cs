﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	[System.Serializable]
	public class NoTradebleObject : TakebleObject
	{
		public NoTradebleObject(int id, int prefubID, Vector3 position, Quaternion rotation) : base(id, prefubID, position, rotation)
		{
			
		}
	}
}