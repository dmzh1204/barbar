﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    [System.Serializable]
    public class FoodObject : TakebleObject
    {

        public FoodObject(int id, int prefubID, Vector3 position, Quaternion rotation)
            : base(id, prefubID, position, rotation)
        {
            
        }

        public void Eat()
        {
            LifeObject lo = TPCObjectsManager.Get(TakedBy) as LifeObject;
            var bonus = GameDataBase.Get(_prefubID).paramBonus;
            lo.AddToParametr(LifeObject.SpecifedParams.hunger, -bonus);
            lo.AddToParametr(LifeObject.SpecifedParams.currentHealth, bonus);
            TPCObjectsManager.Remove(UniqID);
        }
        
    }
}