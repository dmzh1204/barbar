﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
	[Serializable]
	public class ObjSerializeblePosition
	{
		
		public float _posX, _posY, _posZ;
		public float _rotX, _rotY, _rotZ;

		public Vector3 GetPos()
		{
			return new Vector3(_posX, _posY, _posZ);
		}
		
		public Quaternion GetRot()
		{
			return Quaternion.Euler(_rotX, _rotY, _rotZ);
		}

		public void SetPos(Vector3 pos)
		{
			_posX = pos.x;
			_posY = pos.y;
			_posZ = pos.z;
		}

		public void SetRot(Quaternion rot)
		{
			Vector3 angles = rot.eulerAngles;
			_rotX = angles.x;
			_rotY = angles.y;
			_rotZ = angles.z;
		}

		public static void SerializePos(ObjSerializeblePosition osp, Vector3 pos, Quaternion rot)
		{
			if (osp == null)
			{
				return;
			}
			
			osp.SetPos(pos);
			osp.SetRot(rot);
		}

		public static ObjSerializeblePosition SerializePos(Vector3 pos, Quaternion rot)
		{
			ObjSerializeblePosition osp = new ObjSerializeblePosition();
			
			osp.SetPos(pos);
			osp.SetRot(rot);
			
			return osp;
		}

		public static void DeserializePos(ObjSerializeblePosition osp, out Vector3 pos, out Quaternion rot)
		{
			if (osp == null)
			{
				pos = Vector3.zero;
				rot = new Quaternion();
				return;
			}

			pos = osp.GetPos();
			rot = osp.GetRot();
		}
	}
}