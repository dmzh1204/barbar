﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if !UNITY_WEBGL
using System.Threading;
#endif
namespace DZTPC
{
    public class TPCObjectsManager
    {
        static Dictionary<int, BaseTPCGameObject> _allObjectsDict = new Dictionary<int, BaseTPCGameObject>();

        private const int _maxObjsCount = 10000;

        public static BaseTPCGameObject[] AllObjects
        {
            get
            {
                BaseTPCGameObject[] allArray;
                lock (_allObjectsDict)
                {
                    allArray = new BaseTPCGameObject[_allObjectsDict.Count];
                    _allObjectsDict.Values.CopyTo(allArray, 0);
                }

                foreach (var gObj in allArray)
                {
                    gObj.InitBeforeSerialize();
                }

                return allArray;
            }
            set
            {
                lock (_allObjectsDict)
                {
                    _allObjectsDict.Clear();

                    List<int> listNextId = new List<int>();
                    for (int i = 0; i < _maxObjsCount; i++)
                    {
                        listNextId.Add(i);
                    }

                    foreach (var obj in value)
                    {
                        //_allObjectsDict.Add(obj.UniqID, obj);
                        HandleNewObject(obj);
                        listNextId.Remove(obj.UniqID);
                    }


                    _nextUniqIDs = new Queue<int>();

                    foreach (var id in listNextId)
                    {
                        _nextUniqIDs.Enqueue(id);
                    }

                    foreach (var obj in value)
                    {
                        obj.InitAfterDeserialize();
                    }
                }
            }
        }

        static Queue<int> _nextUniqIDs = null;

        static int TakeUniqID()
        {
            if (_nextUniqIDs == null)
            {
                _nextUniqIDs = new Queue<int>();

                for (int i = 0; i < _maxObjsCount; i++)
                {
                    _nextUniqIDs.Enqueue(i);
                }
            }

            return _nextUniqIDs.Dequeue();
        }

        static void HandleNewObject(BaseTPCGameObject go)
        {
            RaycastHit rh;

            if (Physics.Raycast(go.Position + Vector3.up, Vector3.down, out rh, 3, 1))
            {
                go.Position = rh.point;
            }

            lock (_allObjectsDict)
            {
                _allObjectsDict.Add(go.UniqID, go);
            }

            if (OnCreated != null)
                OnCreated(go.UniqID);
        }

        public static KillQuestHandlerObject CreateKillQuestHandler(int[] targetIds, int targetQuest)
        {
            KillQuestHandlerObject wo = new KillQuestHandlerObject(TakeUniqID(), targetIds, targetQuest);
            HandleNewObject(wo);
            return wo;
        }

        public static WeatherObject CreateWeather(DateTime dateTime)
        {
            WeatherObject wo = new WeatherObject(TakeUniqID(), dateTime);
            HandleNewObject(wo);
            return wo;
        }

        public static HumanoidObject CreatNewHuman(Vector3 position, float yAxis, int prefub)
        {
            HumanoidObject cp =
                new HumanoidObject(TakeUniqID(), prefub, position, Quaternion.AngleAxis(yAxis, Vector3.up));

            var lp = GameDataBase.Get(prefub) as GameDataBase.Life;
            cp.AddToParametr(LifeObject.SpecifedParams.strength, lp.Strength);
            cp.AddToParametr(LifeObject.SpecifedParams.dexterity, lp.Dexterity);
            
            HandleNewObject(cp);
            return cp;
        }

        public static LifeObject CreatNewAnimal(Vector3 position, float yAxis, int prefub)
        {
            AllEatAnimalObj cp =
                new AllEatAnimalObj(TakeUniqID(), prefub, position, Quaternion.AngleAxis(yAxis, Vector3.up));
            var lp = GameDataBase.Get(prefub) as GameDataBase.Animals;
            cp.AddToParametr(LifeObject.SpecifedParams.strength, lp.Strength);
            cp.AddToParametr(LifeObject.SpecifedParams.dexterity, lp.Dexterity);

            HandleNewObject(cp);
            return cp;
        }

        public static LifeObject CreatNewArmoredMonster(Vector3 position, float yAxis, int prefub)
        {
            ArmoredMonsterObject cp =
                new ArmoredMonsterObject(TakeUniqID(), prefub, position, Quaternion.AngleAxis(yAxis, Vector3.up));
            var lp = GameDataBase.Get(prefub) as GameDataBase.Animals;
            cp.AddToParametr(LifeObject.SpecifedParams.strength, lp.Strength);
            cp.AddToParametr(LifeObject.SpecifedParams.dexterity, lp.Dexterity);

            HandleNewObject(cp);
            return cp;
        }

        public static WeaponObject CreatNewWeapon(Vector3 position, Quaternion rotation, int prefub)
        {
            WeaponObject wo = new WeaponObject(TakeUniqID(), prefub, position, rotation);

            HandleNewObject(wo);
            return wo;
        }

        public static FoodObject CreatNewFood(Vector3 position, Quaternion rotation, int prefub)
        {
            FoodObject wo = new FoodObject(TakeUniqID(), prefub, position, rotation);

            HandleNewObject(wo);
            return wo;
        }

        public static InteractiveObject CreatNewInteractObject(InteractiveObject.InterObjectTypes type,
            Vector3 position,
            Quaternion rotation, int prefub)
        {
            InteractiveObject intrObj = null;

            switch (type)
            {
                case InteractiveObject.InterObjectTypes.bed:
                    intrObj = new BedRepairIntrObj(TakeUniqID(), prefub, position, rotation);
                    break;

                case InteractiveObject.InterObjectTypes.container:
                    intrObj = new LockedContainerObj(TakeUniqID(), prefub, position, rotation);
                    break;

                default:
                    intrObj = new LockedContainerObj(TakeUniqID(), prefub, position, rotation);
                    break;
            }

            HandleNewObject(intrObj);
            return intrObj;
        }

        public static TakebleObject CreatNewNoTradeble(Vector3 position, Quaternion rotation, int prefub)
        {
            NoTradebleObject wo = new NoTradebleObject(TakeUniqID(), prefub, position, rotation);

            HandleNewObject(wo);
            return wo;
        }

        public static HouseObject CreatNewHouseVolume(int prefub)
        {
            HouseObject wo = new HouseObject(TakeUniqID(), prefub);

            HandleNewObject(wo);
            return wo;
        }


        public static CollectebleObjects CreatNewCollectebleObj(Vector3 position, Quaternion rotation
            , CollectebleObjects.Types type
            , int count
            , int prefub)
        {
            CollectebleObjects co = new CollectebleObjects(TakeUniqID(), prefub
                , type
                , count
                , position, rotation);

            HandleNewObject(co);
            return co;
        }

        public static void ClearAll()
        {
            lock (_allObjectsDict)
            {
                _allObjectsDict.Clear();
            }

            _nextUniqIDs = null;
        }

        public static void Remove(int id)
        {
            lock (_allObjectsDict)
            {
                _allObjectsDict[id].OnDestroy();
                _allObjectsDict.Remove(id);
            }

            _nextUniqIDs.Enqueue((short) id);

            lock (_objectsForUse)
            {
                if (_objectsForUse.ContainsKey(id))
                    _objectsForUse.Remove(id);
            }

            if (OnRemoved != null)
                OnRemoved(id);
        }

        public static BaseTPCGameObject Get(int id)
        {
            BaseTPCGameObject go = null;
            lock (_allObjectsDict)
            {
                if (_allObjectsDict.TryGetValue(id, out go))
                    return go;
                else
                    return null;
            }
        }

        public static bool CanBeUsedByDist(HumanoidObject user, BaseTPCGameObject obj)
        {
            return Vector3.Distance(user.Position, obj.Position) < UseDistance;
        }

        public static int[] GetAll()
        {
            int[] all;
            lock (_allObjectsDict)
            {
                all = new int[_allObjectsDict.Count];

                _allObjectsDict.Keys.CopyTo(all, 0);
            }

            return all;
        }

        public static T[] GetAll<T>(bool inheritance = true) where T : BaseTPCGameObject
        {
            List<T> all = new List<T>();
            lock (_allObjectsDict)
            {
                foreach (var obj in _allObjectsDict)
                {
                    if (inheritance)
                    {
                        if (obj.Value is T)
                        {
                            all.Add(obj.Value as T);
                        }
                    }
                    else
                    {
                        if (obj.Value.GetType() == typeof(T))
                        {
                            all.Add(obj.Value as T);
                        }
                    }
                }
            }

            return all.ToArray();
        }

        public static BaseTPCGameObject[] GetAll(System.Type type)
        {
            List<BaseTPCGameObject> all = new List<BaseTPCGameObject>();

            lock (_allObjectsDict)
            {
                foreach (var obj in _allObjectsDict)
                {
                    if (obj.Value.GetType() == type)
                    {
                        all.Add(obj.Value);
                    }
                }
            }

            return all.ToArray();
        }

        public static Action<int> OnCreated;
        public static Action<int> OnRemoved;


#if !UNITY_WEBGL
        static Thread _takeObjectCalculator = null;
#endif
        static DateTime _threadStopTime = DateTime.Now;

        public static int[] GetObjectsForUse(int characterID)
        {
            InitThread();

            lock (_objectsForUse)
            {
                if (_objectsForUse.ContainsKey(characterID) == false)
                    return _zeroIdsArray;

                objsBuffer.Clear();
                foreach (var obj in _objectsForUse[characterID])
                    if (IsVisible(characterID, obj))
                        objsBuffer.Add(obj);

                return objsBuffer.ToArray();
            }
        }

        static void InitThread()
        {
            #if !UNITY_WEBGL
            _threadStopTime = DateTime.Now.AddSeconds(5);

            if (_takeObjectCalculator == null || _takeObjectCalculator.IsAlive == false)
            {
                _takeObjectCalculator = new Thread(RecalcObjectsFoTake);
                _takeObjectCalculator.Start();
            }
            #endif
        }

        private static List<int> objsBuffer = new List<int>();

        public static int[] GetVisible(int characterID, int maxIteractions = 5)
        {
            InitThread();

            if (_closetToCharacterObjects.ContainsKey(characterID) == false)
                return _zeroIdsArray;


            objsBuffer.Clear();
            lock (_closetToCharacterObjects)
            {
                var ids = _closetToCharacterObjects[characterID];
                for (var i = 0; i < maxIteractions && i < ids.Length; i++)
                    if (IsVisible(characterID, ids[i]))
                        objsBuffer.Add(ids[i]);
            }

            return objsBuffer.ToArray();
        }

        public static bool IsVisible(int characterID, int objID)
        {
            var character = Get(characterID);
            var obj = Get(objID);
            
            if (obj == null)
                return false;
            
            return Physics.Raycast(character.Position + Vector3.up, obj.Position - character.Position,
                       Vector3.Distance(obj.Position, character.Position), 1) == false;
        }


        public static int[] GetClosest(int characterID)
        {
            InitThread();


            lock (_closetToCharacterObjects)
            {
                if (_closetToCharacterObjects.ContainsKey(characterID) == false)
                    return _zeroIdsArray;

                return _closetToCharacterObjects[characterID];
            }
        }

        public static int[] GetFarest(int characterID)
        {
            InitThread();


            lock (_farestToCharacterObjects)
            {
                if (_farestToCharacterObjects.ContainsKey(characterID) == false)
                    return _zeroIdsArray;

                return _farestToCharacterObjects[characterID];
            }
        }

        static Dictionary<int, int[]> _objectsForUse = new Dictionary<int, int[]>();
        static Dictionary<int, int[]> _closetToCharacterObjects = new Dictionary<int, int[]>();
        static Dictionary<int, int[]> _farestToCharacterObjects = new Dictionary<int, int[]>();
        static int[] _zeroIdsArray = new int[0];
        const float UseDistance = 3;

        #if UNITY_WEBGL
        public static IEnumerator RecalcObjectsFoTakeCorutine()
        #endif
        #if !UNITY_WEBGL
        static void RecalcObjectsFoTake()
        #endif
        {
            Debug.Log("RPGObjectManeger Calculator Started");
        #if UNITY_WEBGL
            while (true)
            {
            yield return new WaitForSecondsRealtime(.3f);
        #endif
        #if !UNITY_WEBGL
            while (DateTime.Now < _threadStopTime)
            {Thread.Sleep(200);
        #endif


                BaseTPCGameObject[] allObjs;

                Dictionary<int, int[]> objTakes = new Dictionary<int, int[]>();

                lock (_allObjectsDict)
                {
                    allObjs = new BaseTPCGameObject[_allObjectsDict.Count];

                    _allObjectsDict.Values.CopyTo(allObjs, 0);
                }


                //List<BaseTPCGameObject> takebleObjects = new List<BaseTPCGameObject>();
                List<BaseTPCGameObject> characters = new List<BaseTPCGameObject>();
                List<BaseTPCGameObject> lifeObjects = new List<BaseTPCGameObject>();

                foreach (BaseTPCGameObject go in allObjs)
                {
                    /*TakebleObject to = go as TakebleObject;
                    if (to != null && to.TakedBy < 0)
                    {
                        takebleObjects.Add(go);
                        continue;
                    }*/

                    if (go is HumanoidObject)
                    {
                        characters.Add(go);
                    }

                    if (go is LifeObject)
                    {
                        lifeObjects.Add(go);
                    }
                }

                Dictionary<int, int[]> closestObjectsByCharacter = new Dictionary<int, int[]>();
                const float closestDistance = 30;

                foreach (LifeObject character in lifeObjects)
                {
                    Dictionary<int, float> objDist = new Dictionary<int, float>();
                    foreach (var obj in allObjs)
                    {
                        TakebleObject takrbleObject = obj as TakebleObject;
                        if (takrbleObject != null && takrbleObject.TakedBy >= 0)
                            continue;

                        float dist = Vector3.Distance(obj.Position, character.Position);
                        if (dist < closestDistance && obj.UniqID != character.UniqID)
                            objDist.Add(obj.UniqID, dist);
                    }

                    List<int> objs = new List<int>();
                    var ordered = objDist.OrderBy(value => value.Value);

                    foreach (var keyVal in ordered)
                    {
                        objs.Add(keyVal.Key);
                    }

                    closestObjectsByCharacter.Add(character.UniqID, objs.ToArray());
                }


                const float useAngle = 90;

                foreach (BaseTPCGameObject character in characters)
                {
                    List<int> objectsForChars = new List<int>();

                    foreach (int id in closestObjectsByCharacter[character.UniqID])
                    {
                        InteractiveObject interactiveObject = null;
                        foreach (var objInAll in allObjs)
                        {
                            if (objInAll.UniqID == id)
                            {
                                interactiveObject = objInAll as InteractiveObject;
                                break;
                            }
                        }


                        if (interactiveObject != null
                            //&& interactiveObject.CanBeUsed(character.UniqID) 
                            && Vector3.Distance(interactiveObject.Position, character.Position) < UseDistance
                            && Vector3.Angle(Vector3.forward, interactiveObject.Position - character.Position) -
                            character.Rotation.eulerAngles.y < useAngle
                        )
                        {
                            objectsForChars.Add(interactiveObject.UniqID);
                        }
                    }

                    objTakes.Add(character.UniqID, objectsForChars.ToArray());
                }


                ///Записываем объекты которые можно юзать для каждого персонажа
#if !UNITY_WEBGL
                lock (_objectsForUse)
    #endif
                {
                    _objectsForUse.Clear();

                    foreach (KeyValuePair<int, int[]> kvp in objTakes)
                    {
                        _objectsForUse.Add(kvp.Key, kvp.Value);
                    }
                }

                ///Записываем просчитнные ближайшие объекты для каждого персонажа
#if !UNITY_WEBGL
                lock (_closetToCharacterObjects)
                    #endif
                {
                    _closetToCharacterObjects.Clear();

                    foreach (KeyValuePair<int, int[]> kvp in closestObjectsByCharacter)
                    {
                        _closetToCharacterObjects.Add(kvp.Key, kvp.Value);
                    }
                }

                ///Вычисляем дальние объекты
                Dictionary<int, int[]> farestObjectsByCharacter = new Dictionary<int, int[]>();

                foreach (KeyValuePair<int, int[]> kvp in closestObjectsByCharacter)
                {
                    List<int> farestIds = new List<int>();
                    foreach (BaseTPCGameObject go in allObjs)
                    {
                        bool isFarest = true;

                        if (go.UniqID == kvp.Key)
                        {
                            isFarest = false;
                            continue;
                        }

                        foreach (var closestID in kvp.Value)
                        {
                            if (closestID == go.UniqID)
                            {
                                isFarest = false;
                                break;
                            }
                        }

                        if (isFarest)
                        {
                            farestIds.Add(go.UniqID);
                        }
                    }

                    farestObjectsByCharacter.Add(kvp.Key, farestIds.ToArray());
                }


                ///Записываем дальние обекты
                lock (_farestToCharacterObjects)
                {
                    _farestToCharacterObjects.Clear();


                    foreach (KeyValuePair<int, int[]> kvp in farestObjectsByCharacter)
                    {
                        _farestToCharacterObjects.Add(kvp.Key, kvp.Value);
                    }
                }
            }

            Debug.Log("RPGObjectManeger Calculator Stopped");
        }
    }
}