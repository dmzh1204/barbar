﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public class TradeElement {

	public enum TradeElemTypes
	{
		takebleItem,
		collecteble,
	}

	private TradeElemTypes _tradeElemType = TradeElemTypes.takebleItem;

	public TradeElemTypes TradeElType => _tradeElemType;

	private int _id = -1;

	public int Id => _id;

	private CollectebleObjects.Types _collectType = CollectebleObjects.Types.golds;

	public CollectebleObjects.Types CollectType => _collectType;

	private int _collectebleCount = 0;

	public int CollectebleCount
	{
		get { return _collectebleCount; }
		set { _collectebleCount = value; }
	}

	private int _containerID = -1;

	public TradeElement(int containerID, int uniqID)
	{
		_containerID = containerID;
		_tradeElemType = TradeElemTypes.takebleItem;
		_id = uniqID;
	}
	
	public TradeElement(int containerID, CollectebleObjects.Types type, int count)
	{
		_containerID = containerID;

		_tradeElemType = TradeElemTypes.collecteble;
		_collectType = type;
		_collectebleCount = count;
	}

	public void MoveTo(int targetContainerID)
	{
		switch (_tradeElemType)
		{
			case TradeElemTypes.takebleItem:
				
				TakebleObject to = TPCObjectsManager.Get(_id) as TakebleObject;
				
				to.StartUse(targetContainerID);
				
				break;
			
			case TradeElemTypes.collecteble:
				ContainerObject currentContainer = TPCObjectsManager.Get(_containerID) as ContainerObject;
				ContainerObject targetContainer = TPCObjectsManager.Get(targetContainerID) as ContainerObject;
				
				currentContainer.RemoveCollectebleCount(_collectType, _collectebleCount);
				targetContainer.AddCollectebleCount(_collectType, _collectebleCount);
				
				break;
		}
	}

	public int GetPrice(float multiplier = 1)
	{
		int price = 0;
		if (_tradeElemType == TradeElemTypes.takebleItem)
		{
			price = TPCObjectsManager.Get(_id).GetPrice();
		}

		if (_tradeElemType == TradeElemTypes.collecteble)
		{
			switch (_collectType)
			{
				case CollectebleObjects.Types.arrows:
					price = _collectebleCount * GlobalSettings.Inst.GPSettings.ArrowsCost;
					break;
				
				case CollectebleObjects.Types.bolts:
					price = _collectebleCount * GlobalSettings.Inst.GPSettings.BoltsCost;					
					break;
				default:
					multiplier = 1;
					price = _collectebleCount;
					break;
			}
		}

		price = (int)(price * multiplier);

		if (price < 1)
			price = 1;
		
		return price;
	}

	public static void MoveToOtherCollection(
		TradeElement tradeElement
		, List<TradeElement> currentCollection
		, List<TradeElement> targetCollection
		, int maxCount = 10)
	{
		if (tradeElement.TradeElType == TradeElemTypes.takebleItem)
		{
			currentCollection.Remove(tradeElement);
			targetCollection.Add(tradeElement);
			return;
		}

		int count = maxCount;

		if (tradeElement.CollectebleCount <= 10)
		{
			count = tradeElement.CollectebleCount;
			currentCollection.Remove(tradeElement);
		}
		else
		{
			tradeElement.CollectebleCount -= count;
		}

		TradeElement otherTradeElement = null;

		foreach (var el in targetCollection)
		{
			if (el.CollectType == tradeElement.CollectType)
			{
				otherTradeElement = el;
				break;
			}
		}

		if (otherTradeElement == null)
		{
			otherTradeElement = new TradeElement(tradeElement._containerID, tradeElement.CollectType, 0);
			
			targetCollection.Add(otherTradeElement);
		}

		otherTradeElement.CollectebleCount += count;

	}
	 
}
