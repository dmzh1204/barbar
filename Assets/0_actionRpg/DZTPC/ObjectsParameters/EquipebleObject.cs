﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    [System.Serializable]
    public abstract class EquipebleObject : TakebleObject
    {
        [NonSerialized] protected HumanoidObject.EquipTypes _euipType = HumanoidObject.EquipTypes.meleeOneHand;

        [NonSerialized] private int _euipedBy = -1;

        public int EuipedBy
        {
            get { return _euipedBy; }
            set { _euipedBy = value; }
        }

        public HumanoidObject.EquipTypes EuipType
        {
            get { return _euipType; }
        }

        public EquipebleObject(int id, int prefubID, Vector3 position, Quaternion rotation)
            : base(id, prefubID, position, rotation)
        {
        }


        public virtual float GetBonuce(LifeObject.DependetParams paramType, HumanoidObject human)
        {
            return 0;
        }

        public override void InitAfterDeserialize()
        {
            base.InitAfterDeserialize();

            foreach (var humna in TPCObjectsManager.GetAll<HumanoidObject>())
            {
                if (humna.GetCurrentEquipedID(_euipType) == UniqID)
                {
                    _euipedBy = humna.UniqID;
                    return;
                }
            }

            _euipedBy = -1;
        }


        protected override void OnTaked()
        {
            base.OnTaked();

            HumanoidObject humObj = TPCObjectsManager.Get(TakedBy) as HumanoidObject;

            if (humObj == null)
                return;

            if (UserController.PlayerLifeObj?.UniqID != TakedBy && _euipType == HumanoidObject.EquipTypes.armor)
                return;

            int conflictEquip = -1;

            conflictEquip = humObj.GetCurrentEquipedID(EuipType);

            if (conflictEquip < 0)
            {
                if (EuipType == HumanoidObject.EquipTypes.meleeTwoHand)
                {
                    conflictEquip = humObj.GetCurrentEquipedID(HumanoidObject.EquipTypes.meleeOneHand);
                }

                if (EuipType == HumanoidObject.EquipTypes.meleeOneHand)
                {
                    conflictEquip = humObj.GetCurrentEquipedID(HumanoidObject.EquipTypes.meleeTwoHand);
                }
            }

            if (conflictEquip < 0)
            {
                humObj.Equip(this);
                return;
            }

            EquipebleObject currentEquiped = TPCObjectsManager.Get(conflictEquip) as EquipebleObject;

            if (currentEquiped.GetPrefubBonus() < GetPrefubBonus())
            {
                humObj.Equip(this);
            }
        }

        protected override void OnDroped()
        {
            if (_euipedBy >= 0)
            {
                HumanoidObject humObj = TPCObjectsManager.Get(_euipedBy) as HumanoidObject;

                humObj.UnEquip(this);
            }
        }

        public float GetPrefubBonus()
        {
            return GameDataBase.Get(_prefubID).paramBonus;
        }
    }
}