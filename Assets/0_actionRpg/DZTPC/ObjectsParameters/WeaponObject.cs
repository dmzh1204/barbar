﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    [System.Serializable]
    public class WeaponObject : EquipebleObject
    {
        public enum WeaponTypes
        {
            oneHandMelee = 0,
            twoHandMelee = 1,
            bows = 2,
            crossBow = 3,
            shields = 4,
            armor = 5,
        }


        public WeaponTypes WeaponType
        {
            get { return (GameDataBase.Get(_prefubID) as GameDataBase.Weapons).type; }
        }

        public WeaponObject(int id, int prefubID, Vector3 position, Quaternion rotation)
            : base(id, prefubID, position, rotation)
        {
            _euipType = HumanoidObject.EquipTypes.meleeOneHand;

            switch (WeaponType)
            {
                case WeaponTypes.shields:
                    _euipType = HumanoidObject.EquipTypes.shield;
                    break;
                case WeaponTypes.bows:
                case WeaponTypes.crossBow:
                    _euipType = HumanoidObject.EquipTypes.bow;
                    break;
                case WeaponTypes.twoHandMelee:
                    _euipType = HumanoidObject.EquipTypes.meleeTwoHand;
                    break;
                case WeaponTypes.armor:
                    _euipType = HumanoidObject.EquipTypes.armor;
                    break;
            }
        }

        public override float GetBonuce(LifeObject.DependetParams paramType, HumanoidObject human)
        {
            switch (paramType)
            {
                case LifeObject.DependetParams.defenceMelee:
                    if (WeaponType == WeaponTypes.shields || WeaponType == WeaponTypes.armor)
                        return GetPrefubBonus();
                    break;

                case LifeObject.DependetParams.defenceShoot:
                    if (WeaponType == WeaponTypes.armor)
                        return GetPrefubBonus();
                    break;


                case LifeObject.DependetParams.attackMeele:
                    if (WeaponType == WeaponTypes.oneHandMelee || WeaponType == WeaponTypes.twoHandMelee)
                        return GetPrefubBonus();
                    break;

                case LifeObject.DependetParams.attackShoot:

                    if (WeaponType == WeaponTypes.crossBow)
                    {
                        return GetPrefubBonus();
                    }
                    
                    if (WeaponType == WeaponTypes.bows)
                    {
                        
                        return GetPrefubBonus()
                            + human.GetParametr(LifeObject.SpecifedParams.dexterity)
                            ;
                    }

                    break;

                case LifeObject.DependetParams.attackDistMeele:
                    if (WeaponType == WeaponTypes.oneHandMelee)
                        return 0.5f;
                    if (WeaponType == WeaponTypes.twoHandMelee)
                        return 1.5f;
                    break;
            }

            return 0;
        }

        public override void InitAfterDeserialize()
        {
            switch (WeaponType)
            {
                case WeaponTypes.shields:
                    _euipType = HumanoidObject.EquipTypes.shield;
                    break;
                case WeaponTypes.bows:
                case WeaponTypes.crossBow:
                    _euipType = HumanoidObject.EquipTypes.bow;
                    break;
                case WeaponTypes.twoHandMelee:
                    _euipType = HumanoidObject.EquipTypes.meleeTwoHand;
                    break;
                case WeaponTypes.armor:
                    _euipType = HumanoidObject.EquipTypes.armor;
                    break;
            }

            base.InitAfterDeserialize();
        }
    }
}