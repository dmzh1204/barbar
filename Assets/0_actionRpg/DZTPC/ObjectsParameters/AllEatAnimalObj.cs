﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
	[System.Serializable]
	public class AllEatAnimalObj : Animals
	{

		public AllEatAnimalObj(int id, int prefubID, Vector3 position, Quaternion rotation) : base(id, prefubID, position, rotation)
		{
			
		}


		public override void OnDeathLife()
		{
			base.OnDeathLife();
			(GameDataBase.GetAll<GameDataBase.Food>()[1].GenerateNew(Position, Rotation) as TakebleObject).StartUse(UniqID);

		}
	}
}