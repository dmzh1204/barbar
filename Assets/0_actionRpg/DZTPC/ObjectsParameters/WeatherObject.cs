﻿using System;
using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

namespace DZTPC
{

	[Serializable]
	public class WeatherObject : BaseTPCGameObject
	{
		private long _currentTime = 0;

		public WeatherObject(int id, DateTime dateTime) : base(id, -1, Vector3.zero, new Quaternion())
		{
			_serializeblePosition = null;
			_currentTime = dateTime.Ticks;
			GameGlobalTimer.CurrentData = new DateTime(_currentTime);
		}

		public override void InitBeforeSerialize()
		{
			base.InitBeforeSerialize();
			_currentTime = GameGlobalTimer.CurrentData.Ticks;
		}

		public override void InitAfterDeserialize()
		{
			GameGlobalTimer.CurrentData = new DateTime(_currentTime);
		}

		public override void Update(float deltaTime)
		{
			GameGlobalTimer.CurrentData = GameGlobalTimer.CurrentData.AddSeconds(deltaTime * 50);
		}
	}
}