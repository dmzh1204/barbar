﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	[Serializable]
	public class AIJobData
	{
		private readonly AIJobTypes _jobType = AIJobTypes.sleep;
		private readonly int _targetID = -1;

		private readonly bool _useTargetRot;

		public bool UseTargetRot => _useTargetRot;

		public AIJobTypes JobType => _jobType;

		public int TargetId => _targetID;

		private ObjSerializeblePosition _position;

		[NonSerialized] private bool errorFlag = false;

		public bool ErrorFlag => errorFlag;

		public Vector3 GetTargetPos(Vector3 myPos)
		{
			if (_targetID < 0)
				return _position.GetPos();
				
				BaseTPCGameObject obj = TPCObjectsManager.Get(_targetID);

				if (obj == null)
				{
					errorFlag = true;
					return myPos;
				}
				
				return obj.Position;
		}

		public Vector3 GetTargetForward()
		{
			return _position.GetRot() * Vector3.forward;
		}

		public AIJobData(AIJobTypes jobType)
		{
			_jobType = jobType;
		}
		public AIJobData(AIJobTypes jobType, int targetID)
		{
			_jobType = jobType;
			_targetID = targetID;
		}
		
		public AIJobData(AIJobTypes jobType, Vector3 pos)
		{
			_jobType = jobType;
			_targetID = -1;
			_position = ObjSerializeblePosition.SerializePos(pos, new Quaternion());

		}
		
		public AIJobData(AIJobTypes jobType, Vector3 pos, Quaternion rot)
		{
			_jobType = jobType;
			_targetID = -1;
			_position = ObjSerializeblePosition.SerializePos(pos, rot);
			_useTargetRot = true;
		}
		
	}
}