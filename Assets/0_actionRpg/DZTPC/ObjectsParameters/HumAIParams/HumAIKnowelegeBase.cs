using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using baseLogic;
using RPGDataBase;
using UnityEngine;

namespace DZTPC
{

    [Serializable]
    public class HumAIKnowelegeBase
    {
        public List<int> activeQuests = new List<int>();
        
        public List<int> completedQuests = new List<int>();
        
        public List<int> completedDialogs = new List<int>();
        
        public List<JournalLog> journalLogs = new List<JournalLog>();

        public static event Action<int> OnQuestComplete;
        public static event Action<int> OnQuestActivate;

        public void AddToLog(int idPrefubSource, string message)
        {
            journalLogs.Insert(0, new JournalLog()
            {
                target = idPrefubSource,
                message = message
            });
        }

        public bool IsQuestActive(int quest)
        {
            return activeQuests.Contains(quest);
        }
        public void ActivateQuest(int quest)
        {
            if(IsQuestActive(quest))
                return;
            activeQuests.Add(quest);
            AnalyticController.QuestActivated(GameDataBase.Questes[quest].name);
            OnQuestActivate?.Invoke(quest);
        }

        public bool IsQuestCompleted(int quest)
        {
            return completedQuests.Contains(quest);
        }
        
        public void CompleteQuest(int quest, bool forced = false)
        {
            if(IsQuestCompleted(quest))
                return;
            
            if(IsQuestActive(quest) == false && !forced)
                return;

            activeQuests.Remove(quest);
            completedQuests.Add(quest);
            
            GameDataBase.QuestData.RecalcQuestes(this);
            AnalyticController.QuestComleted(GameDataBase.Questes[quest].name);
            OnQuestComplete?.Invoke(quest);
        }
        
        [Serializable]
        public class JournalLog
        {
            public int target;

            public string message;

        }
        
        
        public enum HumAITemplates
        {
            strengthTeacher,
            dexterityTeacher,
            trading,
            
        }

        HumAITemplates[] _activeAiTemplates = new HumAITemplates[0];

        public HumAITemplates[] ActiveAiTemplates => _activeAiTemplates;

        public List<int> OwnObjects => _ownObjects;

        List<int> _ownObjects = new List<int>();


        private AIJobData _currentAIJob = null;

        public AIJobData CurrentAiJob => _currentAIJob;

        private List<AIJobData> _aiJobsList = null;

        private float _homePositionRandom = 5;

        public float HomePositionRandom
        {
            get { return _homePositionRandom; }
            set { _homePositionRandom = value; }
        }

        private readonly ObjSerializeblePosition _homePosition;

        public ObjSerializeblePosition HomePosition => _homePosition;        
        

        public HumAIKnowelegeBase(HumanoidObject human)
        {
            
            Init(human);
            _homePosition = ObjSerializeblePosition.SerializePos(human.Position, human.Rotation);
        }

        private List<int> _lastUsedObjects = new List<int>();

        [NonSerialized]
        private HumanoidObject _human;
            
        [NonSerialized]
        private List<BedRepairIntrObj> _myBeds;

        public BedRepairIntrObj MyBed => _myBeds.Count == 0? null : _myBeds.FirstOrDefault(x=>x.CurrentUserId < 0);

        public bool IsRecentlyUsed(int id)
        {
            return _lastUsedObjects.Contains(id);
        }

        public void OnUseObject(int id)
        {
            if(IsRecentlyUsed(id))
                return;
            
            _lastUsedObjects.Add(id);
            
            while (_lastUsedObjects.Count > 5)
            {
                _lastUsedObjects.RemoveAt(0);
            }
        }

        public void Init(HumanoidObject human)
        {
            _human = human;

            _myBeds = new List<BedRepairIntrObj>();
            
            foreach (var id in _ownObjects)
            {
                BaseTPCGameObject tpcObj = TPCObjectsManager.Get(id);
                BedRepairIntrObj bed = tpcObj as BedRepairIntrObj;
                if (bed != null)
                {
                    _myBeds.Add(bed);
                }
            }
            
            if(_currentAIJob == null)
                _currentAIJob = new AIJobData(AIJobTypes.none);
            
            if(_aiJobsList == null)
                _aiJobsList = new List<AIJobData>();

            if(_lastUsedObjects == null)
                _lastUsedObjects = new List<int>();
            
            RefreshCurrentAIJob();
        }

        public void AddAITemplate(HumAITemplates template)
        {
            List<HumAITemplates> list = new List<HumAITemplates>(_activeAiTemplates);
            list.Add(template);
            _activeAiTemplates = list.ToArray();
        }

        public void AddAIJob(AIJobData job)
        {
            _aiJobsList.Add(job);
            RefreshCurrentAIJob();
        }
        
        public void SetCurrentAIJob(AIJobData job)
        {
            if(_currentAIJob.JobType != AIJobTypes.none)
                _aiJobsList.Insert(0, _currentAIJob);
            
            _currentAIJob = job;
            RefreshCurrentAIJob();
        }

        public void CompleteCurrentAIJob()
        {
            _currentAIJob = new AIJobData(AIJobTypes.none);
            RefreshCurrentAIJob();
        }

        void RefreshCurrentAIJob()
        {
            if (_currentAIJob.JobType == AIJobTypes.none && _aiJobsList.Count > 0)
            {
                _currentAIJob = _aiJobsList[0];
                _aiJobsList.RemoveAt(0);
            }
        }
        
        
    }
}