﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
	public enum AIJobTypes
	{
		none,
		attackTarget,
		sleep,
		goToHunting,
		kill,
		takeItem,
		lootContainer,
		eat,
		moveTo,
		autoActivateDialog,
		
	}
}
