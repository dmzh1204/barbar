﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using baseLogic;
using DZTPC.Utill;
using RPGDataBase;
using UnityEngine;

namespace DZTPC
{
    [Serializable]
    public class HumanoidObject : LifeObject
    {
        [field: NonSerialized]
        public event System.Action<EquipTypes, int> OnEquipChanged;
        [field: NonSerialized]
        public event System.Action OnInteractionStateChanged;
        [field: NonSerialized]
        public event System.Action<int, FailureTypes, int> OnInteractionFailure;

        public void OnInteractionFailureEvent(int idIntrObj, InteractiveObject.FailureTypes failType, int count)
        {
            OnInteractionFailure?.Invoke(idIntrObj, failType, count);
        }

        /*public void OnInteractionStateChangedEvent()
        {
            OnInteractionStateChanged?.Invoke();
        }/*

        /*public void OnEquipChangedEvent(EquipTypes equipType, int id)
        {
            OnEquipChanged?.Invoke(equipType);
        }*/
        
        [NonSerialized] private DialogsData.DialogData _currentDialog;

        /// <summary>
        /// player user human, target human, dialog
        /// </summary>
        [field: NonSerialized]
        public static event System.Action<HumanoidObject> OnDialogStart;

        public DialogsData.DialogData CurrentDialog
        {
            get { return _currentDialog; }
            set
            {
                _currentDialog = value;

                if (_currentDialog != null)
                {
                    OnDialogStart?.Invoke(this);
                }
            }
        }

        public int CurrentInterObject => _currentInterObject;

        public InteractionType CurrentInterInteractionHum => _currentInterInteractionHum;

        public ActionAndDescription[] AvalibleInterAction => _avalibleInterAction;

        [NonSerialized] private ActionAndDescription[] _avalibleInterAction;

        public ActionAndDescription GetAvalibleAction(InteractionType intrType)
        {
            if (_avalibleInterAction == null)
                return null;

            foreach (var intr in _avalibleInterAction)
            {
                if (intr.Type == intrType)
                {
                    return intr;
                }
            }

            return null;
        }

        public HumAIKnowelegeBase HumAiKnowelegeBase => _humAiKnowelegeBase;

        #region Serializeble

        private int _currentInterObject = -1;

        private InteractionType _currentInterInteractionHum = InteractionType.none;

        Dictionary<EquipTypes, int> _equipedAmmos = new Dictionary<EquipTypes, int>();

        private HumAIKnowelegeBase _humAiKnowelegeBase;

        #endregion

        public int GetCurrentEquipedID(EquipTypes type)
        {
            if (_equipedAmmos.ContainsKey(type) == false)
                return -1;

            return _equipedAmmos[type];
        }

        public WeaponObject GetCurrentEquiped(EquipTypes type)
        {
            if (_equipedAmmos.ContainsKey(type) == false)
                return null;

            return TPCObjectsManager.Get(_equipedAmmos[type]) as WeaponObject;
        }

        public EquipTypes[] GetEquipedTypes()
        {
            EquipTypes[] array = new EquipTypes[_equipedAmmos.Count];
            _equipedAmmos.Keys.CopyTo(array, 0);
            return array;
        }

        public void UnEquip(EquipebleObject equipebleObject)
        {
            if (_equipedAmmos.ContainsKey(equipebleObject.EuipType) == false)
            {
                return;
            }

            _equipedAmmos.Remove(equipebleObject.EuipType);
            equipebleObject.EuipedBy = -1;

            if (OnEquipChanged != null)
                OnEquipChanged(EquipTypes.none, equipebleObject.UniqID);
        }

        public void UnEquip(EquipTypes type)
        {
            if (_equipedAmmos.ContainsKey(type) == false)
            {
                return;
            }

            var equipebleObject = TPCObjectsManager.Get(_equipedAmmos[type]) as EquipebleObject;

            _equipedAmmos.Remove(type);
            equipebleObject.EuipedBy = -1;

            if (OnEquipChanged != null)
                OnEquipChanged(EquipTypes.none, equipebleObject.UniqID);
        }

        public void Equip(EquipebleObject equipebleObject)
        {
            if (_equipedAmmos.ContainsKey(equipebleObject.EuipType) == false)
            {
                _equipedAmmos.Add(equipebleObject.EuipType, equipebleObject.UniqID);
                equipebleObject.EuipedBy = UniqID;
                if (OnEquipChanged != null)
                    OnEquipChanged(equipebleObject.EuipType, equipebleObject.UniqID);

                if (equipebleObject.EuipType == EquipTypes.shield
                    || equipebleObject.EuipType == EquipTypes.meleeOneHand)
                    UnEquip(EquipTypes.meleeTwoHand);

                if (equipebleObject.EuipType == EquipTypes.meleeTwoHand)
                {
                    UnEquip(EquipTypes.meleeOneHand);
                    UnEquip(EquipTypes.shield);
                }

                return;
            }

            if (_equipedAmmos[equipebleObject.EuipType] != equipebleObject.UniqID)
            {
                EquipebleObject prev =
                    (TPCObjectsManager.Get(_equipedAmmos[equipebleObject.EuipType]) as EquipebleObject);
                prev.EuipedBy = -1;
                if (OnEquipChanged != null)
                    OnEquipChanged(EquipTypes.none, prev.UniqID);

                _equipedAmmos[equipebleObject.EuipType] = equipebleObject.UniqID;
                equipebleObject.EuipedBy = UniqID;
                if (OnEquipChanged != null)
                    OnEquipChanged(equipebleObject.EuipType, equipebleObject.UniqID);
            }
        }

        public enum EquipTypes
        {
            none = -1,
            meleeOneHand = 0,
            meleeTwoHand = 1,
            shield = 2,
            bow = 3,
            armor = 4,
        }


        public HumanoidObject(int id, int prefubID
            , Vector3 position
            , Quaternion rotation
        )
            : base(id, prefubID, position, rotation)
        {
            _humAiKnowelegeBase = new HumAIKnowelegeBase(this);

            if (id == 0)
            {
                for (int i = 0; i < GameDataBase.Questes.Length; i++)
                {
                    if(GameDataBase.Questes[i].startOnWorldInit)
                        _humAiKnowelegeBase.ActivateQuest(i);
                }
            }
        }

        public override void InitAfterDeserialize()
        {
            base.InitAfterDeserialize();
            _humAiKnowelegeBase.Init(this);
        }

        public override bool HoldInHandMeleeWeapon() => CurrentFightState == FightingStates.melee && 
                                                        (GetCurrentEquiped(EquipTypes.meleeOneHand) != null ||
                                                         GetCurrentEquiped(EquipTypes.meleeTwoHand) != null);

        protected override float GetBonus(DependetParams paramType)
        {
            float v = 0;

            foreach (var ea in _equipedAmmos)
            {
                v += (TPCObjectsManager.Get(ea.Value) as EquipebleObject).GetBonuce(paramType, this);
            }

            if (paramType == DependetParams.defenceCurrent)
            {
                v += (GameDataBase.Get(_prefubID) as GameDataBase.Life).DefenseBonus;
            }
            
            return v;
        }

        public override float GetParametr(DependetParams type)
        {
            float v = base.GetParametr(type);

            if (type == DependetParams.maxMoveSpeed && CurrentFightState != FightingStates.none)
                return 5;
                

            if (type != DependetParams.attackTime)
                return v;

            switch (CurrentFightState)
            {
                case FightingStates.melee:

                    if (GetCurrentEquipedID(EquipTypes.meleeOneHand) >= 0)
                    {
                        return RPGMath.DefAttackTime(WeaponObject.WeaponTypes.oneHandMelee);
                    }

                    if (GetCurrentEquipedID(EquipTypes.meleeTwoHand) >= 0)
                    {
                        return RPGMath.DefAttackTime(WeaponObject.WeaponTypes.twoHandMelee);
                    }

                    return RPGMath.DefAttackTime(WeaponObject.WeaponTypes.oneHandMelee);

                    break;
                case FightingStates.shoot:
                    if (GetCurrentEquipedID(EquipTypes.bow) < 0)
                        return v;

                    if (GetCurrentEquiped(EquipTypes.bow).WeaponType == WeaponObject.WeaponTypes.bows)
                    {
                        return RPGMath.DefAttackTime(WeaponObject.WeaponTypes.bows);
                    }
                    else
                    {
                        return RPGMath.DefAttackTime(WeaponObject.WeaponTypes.crossBow);
                    }

                    break;
            }

            return v;
        }

        public void SetAvalibleInteractionOptions(int idInteractObj, ActionAndDescription[] options = null)
        {
            _currentInterObject = idInteractObj;

            _avalibleInterAction = options;

            if (_currentInterObject < 0)
                _currentInterInteractionHum = InteractionType.none;

            OnInteractionStateChanged?.Invoke();
            
/*            if (_currentInterObject < 0 && this == UserController.PlayerCharacter.HumanObj && DialogController.DialogInProgress)
                throw new Exception("BarbarianError. No currentIntrObj in Dialog (A)");*/
                
        }

        public void StartActionOnActiveObject(InteractionType interactionType)
        {
            if (_currentInterObject < 0)
                return;

            _humAiKnowelegeBase.OnUseObject(_currentInterObject);

            InteractiveObject intrObj = TPCObjectsManager.Get(_currentInterObject) as InteractiveObject;

            if (intrObj == null)
            {
                _currentInterObject = -1;

                _currentInterInteractionHum = InteractionType.none;
                intrObj.StartActionByUser(interactionType);

                OnInteractionStateChanged?.Invoke();
                
                if (_currentInterObject < 0 && this == UserController.PlayerCharacter.HumanObj && DialogController.DialogInProgress)
                    throw new Exception("BarbarianError. No currentIntrObj in Dialog (B)");
                
                return;
            }

            _currentInterInteractionHum = interactionType;

            if (_currentInterInteractionHum == InteractionType.tradeContainer)
            {
                GeneratePosibleTradeElements();
                (intrObj as ContainerObject).GeneratePosibleTradeElements();
            }

            intrObj.StartActionByUser(interactionType);

            if ((int) interactionType < 10)
            {
                interactionType = InteractionType.dialogInteraction;
            }

            OnInteractionStateChanged?.Invoke();
        }

        public List<DialogsData.DialogData> GetAvailableDialogs(HumanoidObject hum)
        {
            List<DialogsData.DialogData> avalibleDialogs = new List<DialogsData.DialogData>();
            GameDataBase.Characters character = GameDataBase.Get(_prefubID) as GameDataBase.Characters;

            foreach (var dialog in character.GetDialogs())
                if (dialog.CanBeStartedBy(hum, this))
                    avalibleDialogs.Add(dialog);

            return avalibleDialogs;
        }
        
        public override void StartUse(int userID)
        {
            if (IsDead() == false)
            {
                if (CanBeUsed(userID) == false)
                    return;

                GameDataBase.Characters character = GameDataBase.Get(_prefubID) as GameDataBase.Characters;

                HumanoidObject hum = (TPCObjectsManager.Get(userID) as HumanoidObject);


                List<DialogsData.DialogData> avalibleDialogs = new List<DialogsData.DialogData>();
                List<ActionAndDescription> dialogsActions = new List<ActionAndDescription>();

                int i = 0;
                foreach (var dialog in character.GetDialogs())
                {
                    if (dialog.CanBeStartedBy(hum, this))
                    {
                        avalibleDialogs.Add(dialog);
                        dialogsActions.Add(new ActionAndDescription((InteractionType) i, dialog.dialog[0].replicKey,
                            userID, UniqID));
                        i++;
                    }
                }

                if (avalibleDialogs.Count == 0)
                    return;

                _currentUserID = userID;

                CurrentDialog = null;

                foreach (var dialog in avalibleDialogs)
                {
                    if (dialog.StartType > DialogsData.DialogData.StartTypes.manualyRepiteble)
                    {
                        CurrentDialog = dialog;
                        //hum.SetAvalibleInteractionOptions(UniqID, GenerateActionsForUser(userID));
                        break;
                    }
                }

                /*if (CurrentDialog == null)
                {
                    hum.SetAvalibleInteractionOptions(UniqID, dialogsActions.ToArray());
                }*/
                hum.SetAvalibleInteractionOptions(UniqID, GenerateActionsForUser(userID));
                hum.StartActionOnActiveObject(InteractionType.dialogInteraction);
            }
            else
            {
                base.StartUse(userID);
            }
        }

        public override void StartActionByUser(InteractionType interactionType)
        {
            if (IsDead() == false && (int) interactionType < 10)
            {
                HumanoidObject hum = (TPCObjectsManager.Get(_currentUserID) as HumanoidObject);

                if (CurrentDialog == null)
                {
                    GameDataBase.Characters character = GameDataBase.Get(_prefubID) as GameDataBase.Characters;
                    List<DialogsData.DialogData> avalibleDialogs = new List<DialogsData.DialogData>();

                    foreach (var dialog in character.GetDialogs())
                    {
                        if (dialog.CanBeStartedBy(hum, this))
                        {
                            avalibleDialogs.Add(dialog);
                        }
                    }

                    CurrentDialog = avalibleDialogs[(int) interactionType];
                }
                else
                {
                    List<DialogsData.DialogData.Answer> avalibleAnswers = new List<DialogsData.DialogData.Answer>();

                    foreach (var answer in CurrentDialog.answers)
                    {
                        if(answer.nextDialog.CanBeStartedBy(hum, this, true))
                            avalibleAnswers.Add(answer);
                    }
                    
                    if (avalibleAnswers[(int) interactionType].nextDialog.CheckActionsPosobility(hum, this) == false)
                        CurrentDialog = null;
                    else
                        CurrentDialog = avalibleAnswers[(int) interactionType].nextDialog;
                }

                if (CurrentDialog == null)
                {
                    //interactionType = InteractionType.stopInteraction;
                    StopUse(_currentUserID);
                    return;
                }
                else
                {
                    interactionType = InteractionType.dialogInteraction;
                }
            }

            base.StartActionByUser(interactionType);
        }

        protected override ActionAndDescription[] GenerateActionsForUser(int userID)
        {
            if (IsDead() == false)
            {
                return HumanoidInterGenerator.Generate(this, TPCObjectsManager.Get(userID) as HumanoidObject);
            }
            else
            {
                return base.GenerateActionsForUser(userID);
            }
        }

        public override void GeneratePosibleTradeElements()
        {
            base.GeneratePosibleTradeElements();

            if (IsDead() == false)
            {
                if (_currentInterObject >= 0)
                {
                    LifeObject lo = TPCObjectsManager.Get(_currentInterObject) as LifeObject;

                    if (lo != null && lo.IsDead())
                    {
                        PosibleTradeElements.Clear();
                        SelectedTradeElements.Clear();
                    }
                }

                foreach (var elem in PosibleTradeElements.ToArray())
                {
                    if (elem.TradeElType == TradeElement.TradeElemTypes.takebleItem)
                    {
                        EquipebleObject eo = TPCObjectsManager.Get(elem.Id) as EquipebleObject;

                        if (eo != null && eo.EuipedBy >= 0)
                        {
                            PosibleTradeElements.Remove(elem);
                        }
                    }

                    if (elem.TradeElType == TradeElement.TradeElemTypes.collecteble)
                    {
                        if (elem.CollectType == CollectebleObjects.Types.golds)
                        {
                            PosibleTradeElements.Remove(elem);
                        }
                    }
                }
            }
        }

        public override bool IsShootAvalible()
        {
            if (GetCurrentEquiped(EquipTypes.bow) == null)
            {
                return false;
            }

            if (GetCurrentEquiped(EquipTypes.bow).WeaponType ==
                WeaponObject.WeaponTypes.bows && GetCollectebleCount(CollectebleObjects.Types.arrows) > 0)
            {
                return true;
            }

            if (GetCurrentEquiped(EquipTypes.bow).WeaponType ==
                WeaponObject.WeaponTypes.crossBow && GetCollectebleCount(CollectebleObjects.Types.bolts) > 0)
            {
                return true;
            }

            return false;
        }

        public override void SetDamage(DamagParameters damag)
        {
            base.SetDamage(damag);
            
            if(CurrentInterObject < 0 || DialogController.DialogInProgress)
                return;
            
            (TPCObjectsManager.Get(CurrentInterObject) as InteractiveObject)
                .StopUse(UniqID);
        }
    }
}