﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    [System.Serializable]
    public class CollectebleObjects : TakebleObject
    {

        public enum Types
        {
            golds = 0,
            arrows = 5,
            bolts = 6,
        }

        private Types _type = Types.golds;

        public Types Type
        {
            get { return _type; }
        }

        private int _count = 1;

        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }

        public CollectebleObjects(int id, int prefubID, Types type, int count, Vector3 position, Quaternion rotation)
            : base(id, prefubID, position, rotation)
        {
            _type = type;
            _count = count;
        }

        
        
    }
}