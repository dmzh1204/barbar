﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    [Serializable]
    public class BaseTPCGameObject
    {
        #region Serializeble

        int _uniqID = 0;
        protected int _prefubID = 0;

        public int PrefubId => _prefubID;

        protected ObjSerializeblePosition _serializeblePosition = null;
        
        #endregion
        
        public int UniqID
        {
            get { return _uniqID; }
        }

        [NonSerialized]
        private Vector3 position;
        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }

        [NonSerialized]
        private Quaternion rotation;
        public Quaternion Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }


        public BaseTPCGameObject(int id, int prefubID, Vector3 position, Quaternion rotation)
        {
            _uniqID = id;
            Position = position;
            Rotation = rotation;
            _prefubID = prefubID;
            _serializeblePosition = new ObjSerializeblePosition();
        }

        public int GetPrice()
        {
            return GlobalSettings.Inst.GPSettings.GetPrice(_prefubID);
        }
        
        public GameObject GetPrefub()
        {
            if (_prefubID < 0)
                return null;
            
            return GameDataBase.Get(_prefubID).prefub;
        }
        public GameObject GetBasePrefub()
        {
            if (_prefubID < 0)
                return null;
                
            Transform basePrefub = GameDataBase.Get(_prefubID).GetBasePrefub();
            
            if (basePrefub == null)
                return null;
            
            return basePrefub.gameObject;
        }

        public GameObject Instantiate()
        {
            
            GameObject basePrefub = GetBasePrefub();

            GameObject new_go;
            
            if (basePrefub != null)
            {
                new_go = (GameObject) GameObject.Instantiate(basePrefub);
            }
            else
            {
                new_go = new GameObject("NewObject");
            }

            if (GetPrefub() == null)
            {
                return new_go;
            }
            
            GameObject model = (GameObject)GameObject.Instantiate(GetPrefub(), new_go.transform);

            Transform modelTrans = new_go.transform.Find("model");

            if (modelTrans != null)
            {
                model.transform.position = modelTrans.position;
                model.transform.rotation = modelTrans.rotation;  
                GameObject.Destroy(modelTrans.gameObject);
            }
            else
            {
                model.transform.localPosition = Vector3.zero;
                model.transform.localRotation = new Quaternion();
            }

            return new_go;
        }

        public Sprite GetIcon()
        {
            return GameDataBase.Get(_prefubID).icon;
        }
        
        public string GetName()
        {
            return GameDataBase.Get(_prefubID).name;
        }

        
        
        public string GetDescription()
        {
            return GameDataBase.Get(_prefubID).description;
        }

        public virtual void OnDestroy()
        {
            
        }

        
        public virtual void InitAfterDeserialize()
        {
            ObjSerializeblePosition.DeserializePos(_serializeblePosition, out position, out rotation);
            /*if (_serializeblePosition == null)
            {
                rotation = new Quaternion();
                position = Vector3.zero;
            }
            else
            {
                rotation = Quaternion.Euler(_serializeblePosition._rotX, _serializeblePosition._rotY, _serializeblePosition._rotZ);
                position = new Vector3(_serializeblePosition._posX, _serializeblePosition._posY, _serializeblePosition._posZ);
            }*/
            
        }

        public virtual void InitBeforeSerialize()
        {
            ObjSerializeblePosition.SerializePos(_serializeblePosition, position, rotation);
                
            /*if(_serializeblePosition == null)
                return;
            
            Vector3 angs = rotation.eulerAngles;
            
            _serializeblePosition._rotX = angs.x;
            _serializeblePosition._rotY = angs.y;
            _serializeblePosition._rotZ = angs.z;
            
            _serializeblePosition._posX = position.x;
            _serializeblePosition._posY = position.y;
            _serializeblePosition._posZ = position.z;*/
        }

        public virtual void Update(float deltaTime)
        {
            
        }
    }
}