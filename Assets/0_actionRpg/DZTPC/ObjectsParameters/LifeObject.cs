﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RPGDataBase;
using UnityEngine;
using Random = System.Random;

namespace DZTPC
{
    [System.Serializable]
    public class LifeObject : ContainerObject
    {
        public enum DependetParams
        {
            defenceCurrent = 0,
            defenceMelee = 1,
            defenceShoot = 2,
            attackCurrent = 3,
            attackMeele = 8,
            attackShoot = 9,
            attackDistCurrent = 7,
            attackDistMeele = 10,
            attackDistShoot = 11,
            maxHealth = 4,
            level = 5,
            attackTime = 6,
            maxEnergy = 12,
            maxMoveSpeed = 13,
        }

        public enum SpecifedParams
        {
            hunger = 0,
            currentHealth = 1,
            expirence = 2,
            expPoints = 3,
            strength = 4,
            dexterity = 5,
            height = 6,
            energy = 8,
        }
        
        Dictionary<SpecifedParams, float> _specifidParams = new Dictionary<SpecifedParams, float>();

        public bool GodMode;

        public virtual bool HoldInHandMeleeWeapon() => false;

        [NonSerialized]
        private GameDataBase.Life lifeDataBase = null;

        public GameDataBase.Life LifeDataBase =>
            lifeDataBase ?? (lifeDataBase = GameDataBase.Get(_prefubID) as GameDataBase.Life);
        
        public float GetParametr(SpecifedParams type)
        {
            InitSpecParam(type);
            
            if(type == SpecifedParams.strength)
                return UniqID == GlobalSettings.PlayerUniqID ? 
                    _specifidParams[type] + GlobalSettings.PlayerModifications.StrengthBonus : _specifidParams[type];
            if(type == SpecifedParams.dexterity)
                return UniqID == GlobalSettings.PlayerUniqID ? 
                    _specifidParams[type] + GlobalSettings.PlayerModifications.DexterityBonus : _specifidParams[type];

            return _specifidParams[type];
        }

        private void InitSpecParam(SpecifedParams type)
        {
            if (_specifidParams.ContainsKey(type))
                return;

            switch (type)
            {
                case SpecifedParams.currentHealth:
                    _specifidParams.Add(type, GetParametr(DependetParams.maxHealth));
                    break;

                case SpecifedParams.hunger:
                    _specifidParams.Add(type, -100);
                    break;

                case SpecifedParams.height:
                    _specifidParams.Add(type, 1.8f);
                    break;
                
                case SpecifedParams.energy:
                    _specifidParams.Add(type, GetParametr(DependetParams.maxEnergy));
                    break;

                case SpecifedParams.expirence:
                case SpecifedParams.expPoints:
                    _specifidParams.Add(type, 0);
                    break;

                default:
                    _specifidParams.Add(type, 1);
                    break;
            }
        }

        public void SetParametr(SpecifedParams type, float value)
        {
            AddToParametr(type, value - GetParametr(type));
        } 
        
        public void AddToParametr(SpecifedParams type, float deltaValue)
        {
            if(deltaValue == 0)
                return;
            
            InitSpecParam(type);

            if (type == SpecifedParams.expirence)
            {
                float deltaLevel = GetParametr(DependetParams.level);
                _specifidParams[type] += UniqID == GlobalSettings.PlayerUniqID ? 
                    deltaValue * GlobalSettings.PlayerModifications.ExpirenceMultiplier : 
                    deltaValue;
                deltaLevel = GetParametr(DependetParams.level) - deltaLevel;
                
                AddToParametr(SpecifedParams.expPoints, deltaLevel);
                
                return;
            }
            
            _specifidParams[type] += deltaValue;
            ChecParam(type);
        }
        
        [System.NonSerialized]
        public System.Action<SpecifedParams> OnParamChanged;

        private void ChecParam(SpecifedParams type)
        {
            switch (type)
            {
                case SpecifedParams.currentHealth:
                    _specifidParams[type] =
                        Mathf.Clamp(_specifidParams[type], 0, GetParametr(DependetParams.maxHealth));
                    break;
                
                case SpecifedParams.energy:
                    _specifidParams[type] =
                        Mathf.Clamp(_specifidParams[type], 0, GetParametr(DependetParams.maxEnergy));
                    break;

                case SpecifedParams.hunger:
                    _specifidParams[type] =
                        Mathf.Clamp(_specifidParams[type], -10, 0.5f);
                    break;
            }

            if (OnParamChanged != null)
                OnParamChanged(type);
        }

        public virtual float GetParametr(DependetParams type)
        {
            float v = 1;

            switch (type)
            {
                case DependetParams.attackTime:
                    v = 1;
                    break;
                
                case DependetParams.maxEnergy:
                    return 100;

                case DependetParams.maxMoveSpeed:
                    return 7;

                case DependetParams.maxHealth:
                    v = RPGMath.GetMaxHealth(RPGMath.GetCurrentLevel((int)GetParametr(SpecifedParams.expirence)));
                    break;

                case DependetParams.level:
                    v = RPGMath.GetCurrentLevel((int)GetParametr(SpecifedParams.expirence));
                    break;

                case DependetParams.attackCurrent:
                    if (_currentFightState == FightingStates.shoot)
                        return GetParametr(DependetParams.attackShoot);

                    return GetParametr(DependetParams.attackMeele);
                    break;

                case DependetParams.attackMeele:
                    v = GetParametr(SpecifedParams.strength);
                    v += GetParametr(DependetParams.level) - 1;
                    break;


                case DependetParams.attackShoot:
                    //v = GetParametr(SpecifedParams.dexterity);
                    v += GetParametr(DependetParams.level) - 1;
                    break;


                
                case DependetParams.attackDistCurrent:

                    if (_currentFightState == FightingStates.shoot)
                        return GetParametr(DependetParams.attackDistShoot);

                    return GetParametr(DependetParams.attackDistMeele);
                    
                
                case DependetParams.attackDistShoot:
                    return 30;
                case DependetParams.attackDistMeele:
                    v = 1.5f;
                    break;
                
                case DependetParams.defenceCurrent:
                    if(_currentFightState == FightingStates.shoot)
                        v = GetParametr(DependetParams.defenceShoot) + 
                               (UniqID == GlobalSettings.PlayerUniqID ? GlobalSettings.PlayerModifications.DexterityBonus : 0);
                    else
                        v = UniqID == GlobalSettings.PlayerUniqID ? 
                               GetParametr(DependetParams.defenceMelee) + GlobalSettings.PlayerModifications.DefenseBonus : 
                               GetParametr(DependetParams.defenceMelee);
                    break;

                case DependetParams.defenceMelee:
                    v = 0;
                    break;
                case DependetParams.defenceShoot:
                    v = 0;
                    break;
            }

            v += GetBonus(type);
            return v;
        }

        [System.NonSerialized]
        public System.Action<FightingStates> OnFightingStateChanged;

        public FightingStates CurrentFightState
        {
            get { return _currentFightState; }

            set
            {
                if (_currentFightState == value)
                    return;

                _currentFightState = value;

                if (OnFightingStateChanged != null)
                    OnFightingStateChanged(_currentFightState);
            }
        }

        public enum FightingStates
        {
            none = 0,
            melee = 1,
            shoot = 2,
        }

        FightingStates _currentFightState = FightingStates.none;

        [System.NonSerialized]
        public System.Action<DamagParameters> OnDamage;
        
        [System.NonSerialized]
        public System.Action OnDeath;
        
        [System.NonSerialized]
        public static System.Action<LifeObject> GlobalOnDeath;
        [System.NonSerialized]
        public static System.Action<DamagParameters> GlobalOnDamage;


        public LifeObject(int id, int prefubID, Vector3 position, Quaternion rotation)
            : base(id, prefubID, position, rotation)
        {
            SetParametr(SpecifedParams.expirence, RPGMath.GetExpirence((int)GameDataBase.Get(prefubID).paramBonus));
        }

        protected virtual float GetBonus(DependetParams paramType)
        {
            return 0;
        }

        [System.NonSerialized]
        public System.Action<float> OnUpdate;
        
        public override void Update(float deltaTime)
        {
            if (IsDead())
            {
                return;
            }

            if (OnUpdate != null)
                OnUpdate(deltaTime);
            
            const float hungerSpeed = 0.01f;
            const float healthSpeed = 0.001f;
            const float energiSpeed = 0.1f;
            
            if(UniqID != GlobalSettings.PlayerUniqID || !GlobalSettings.PlayerModifications.DontNeedSleepAndEat)
                AddToParametr(SpecifedParams.hunger, deltaTime * hungerSpeed);

            float hunger = GetParametr(SpecifedParams.hunger);
            float energy = GetParametr(SpecifedParams.energy);

            float deltaHealth = deltaTime * healthSpeed * GetParametr(DependetParams.maxHealth);

            if (UniqID == GlobalSettings.PlayerUniqID && (hunger > 0 || energy <= 0))
            {
                deltaHealth *= -0.5f;
                
                if (GetParametr(SpecifedParams.currentHealth) + deltaHealth < GetParametr(DependetParams.maxHealth) * 0.3f)
                {
                    deltaHealth = 0;
                }
            }
            
            float targetHealth = Mathf.Clamp(GetParametr(SpecifedParams.currentHealth) + deltaHealth
                , 0, GetParametr(DependetParams.maxHealth));
            
            SetParametr(SpecifedParams.currentHealth, targetHealth);

            if(UniqID != GlobalSettings.PlayerUniqID || !GlobalSettings.PlayerModifications.DontNeedSleepAndEat)
                SetParametr(SpecifedParams.energy, GetParametr(SpecifedParams.energy) - deltaTime * energiSpeed);

        }

        public virtual void SetDamage(DamagParameters damag)
        {
            if(DialogController.DialogInProgress || GodMode)
                return;
            
            if (IsDead())
                return;

            if (damag.CutDamage != 0)
            {
                damag.CutDamage -= GetParametr(DependetParams.defenceCurrent);

                if (damag.CutDamage < 0)
                    damag.CutDamage = 0.1f;
            }

            AddToParametr(SpecifedParams.currentHealth, -damag.CutDamage);
            OnDamage?.Invoke(damag);
            GlobalOnDamage?.Invoke(damag);
            
            if (IsDead())
            {
                LifeObject life = (damag.Source as LifeObject);
                if (life != null)
                {
                    life.AddToParametr(SpecifedParams.expirence, RPGMath.GetExpirenceReward((int)GetParametr(DependetParams.level)));
                }

                OnDeathLife();
            }
        }

        public bool IsDead()
        {
            return GetParametr(SpecifedParams.currentHealth) <= 0;
        }
        
        public virtual void OnDeathLife()
        {
            if (OnDeath != null)
            {
                OnDeath();
            }
                
            GlobalOnDeath?.Invoke(this);
            
        }

        public virtual bool IsShootAvalible()
        {
            return false;
        }

        public int GetVoiceIndex()
        {
            return (GameDataBase.Get(_prefubID) as GameDataBase.Life).VoiceIndex;
        }
        
        public override void StartUse(int userID)
        {
            if(CanBeUsed(userID) == false)
                return;
            
            if (IsDead())
            {
                HumanoidObject hum = (TPCObjectsManager.Get(userID) as HumanoidObject);
                _currentUserID = userID;
                
                hum.SetAvalibleInteractionOptions(UniqID, GenerateActionsForUser(userID));

                hum.StartActionOnActiveObject(InteractionType.tradeContainer);

            }
        }

        public override void GeneratePosibleTradeElements()
        {
            base.GeneratePosibleTradeElements();

            if (IsDead())
            {
                

                foreach (var el in PosibleTradeElements)
                {
                    SelectedTradeElements.Add(el);
                }
                PosibleTradeElements.Clear();
            }
            else
            {
                foreach (var el in PosibleTradeElements.ToArray())
                {
                    if(el.TradeElType == TradeElement.TradeElemTypes.takebleItem 
                       && TPCObjectsManager.Get(el.Id) is NoTradebleObject)
                    {
                        PosibleTradeElements.Remove(el);
                    }
                }
            }
            
        }
    }
}