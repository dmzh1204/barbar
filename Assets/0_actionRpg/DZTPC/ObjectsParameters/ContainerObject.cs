﻿using System;
using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

namespace DZTPC
{
    [Serializable]
    public class ContainerObject : InteractiveObject
    {
        [System.NonSerialized] public System.Action<int> OnTake;
        [System.NonSerialized] public System.Action<int> OnDrop;


        [System.NonSerialized] List<TradeElement> _posibleTradeElements = new List<TradeElement>();

        public List<TradeElement> PosibleTradeElements => _posibleTradeElements;

        public List<TradeElement> SelectedTradeElements => _selectedTradeElements;

        [System.NonSerialized] List<TradeElement> _selectedTradeElements = new List<TradeElement>();


        List<int> _inventory = new List<int>();

        public int[] Inventory
        {
            get { return _inventory.ToArray(); }
        }

        public List<int> GetItemsByPrefubId(int prefubId)
        {
            List<int> list = new List<int>();

            foreach (var item in _inventory)
                if(TPCObjectsManager.Get(item).PrefubId == prefubId)
                    list.Add(item);
            
            return list;
        }
        
        public void AddToInventar(int id)
        {
            if (_inventory.Contains(id))
                return;

            CollectebleObjects collecteble = TPCObjectsManager.Get(id) as CollectebleObjects;
            if (collecteble != null)
            {
                TPCObjectsManager.Remove(id);
                AddCollectebleCount(collecteble.Type, collecteble.Count);
                return;
            }


            _inventory.Add(id);
            if (OnTake != null)
                OnTake(id);
        }

        public void RemoveFromInventar(int id)
        {
            if (_inventory.Contains(id) == false)
                return;

            _inventory.Remove(id);

            if (OnDrop != null)
                OnDrop(id);
        }

        public bool Contain(int id)
        {
            return _inventory.Contains(id);
        }

        Dictionary<CollectebleObjects.Types, int>
            _collectebleInventar = new Dictionary<CollectebleObjects.Types, int>();

        public int GetCollectebleCount(CollectebleObjects.Types type)
        {
            if (_collectebleInventar.ContainsKey(type) == false)
                _collectebleInventar.Add(type, 0);

            return _collectebleInventar[type];
        }

        public void RemoveCollectebleCount(CollectebleObjects.Types type, int count)
        {
            if (_collectebleInventar.ContainsKey(type) == false)
                _collectebleInventar.Add(type, 0);

            _collectebleInventar[type] -= count;
        }

        public void AddCollectebleCount(CollectebleObjects.Types type, int count)
        {
            if (_collectebleInventar.ContainsKey(type) == false)
                _collectebleInventar.Add(type, 0);

            _collectebleInventar[type] += count;
        }


        public ContainerObject(int id, int prefubID, Vector3 position, Quaternion rotation) : base(id, prefubID,
            position, rotation)
        {
        }

        public virtual void GeneratePosibleTradeElements()
        {
            _posibleTradeElements = new List<TradeElement>();
            _selectedTradeElements = new List<TradeElement>();

            CollectebleObjects.Types[] collTypesAll = new CollectebleObjects.Types[]
            {
                CollectebleObjects.Types.golds,
                CollectebleObjects.Types.arrows,
                CollectebleObjects.Types.bolts,
            };

            foreach (var item in collTypesAll)
            {
                int count = GetCollectebleCount(item);
                if (count > 0)
                {
                    _posibleTradeElements.Add(new TradeElement(UniqID, item, count));
                }
            }

            foreach (var item in _inventory)
            {
                _posibleTradeElements.Add(new TradeElement(UniqID, item));
            }
        }

        public bool IsConteinerEmpty()
        {
            if (_inventory.Count > 0
                || GetCollectebleCount(CollectebleObjects.Types.arrows) > 0
                || GetCollectebleCount(CollectebleObjects.Types.bolts) > 0
                || GetCollectebleCount(CollectebleObjects.Types.golds) > 0
                )
                return false;
            
            
            
            return true;
        }

        public override void StartActionByUser(InteractionType interactionType)
        {
            if (interactionType == InteractionType.tradeConfirm)
            {
                ContainerObject userContainer = TPCObjectsManager.Get(_currentUserID) as ContainerObject;


                foreach (var elem in userContainer.SelectedTradeElements)
                {
                    elem.MoveTo(UniqID);
                }

                userContainer.SelectedTradeElements.Clear();

                foreach (var elem in SelectedTradeElements)
                {
                    elem.MoveTo(_currentUserID);
                }

                SelectedTradeElements.Clear();

                StopUse(_currentUserID);
                return;
            }

            base.StartActionByUser(interactionType);
        }

        protected override ActionAndDescription[] GenerateActionsForUser(int userID)
        {
            if (_currentInteractionIo == InteractionType.tradeContainer)
            {
                return new ActionAndDescription[]
                {
                    new ActionAndDescription(InteractionType.tradeConfirm, InteractionType.tradeConfirm.ToString(), userID, UniqID),
                };
            }


            return new ActionAndDescription[]
            {
                new ActionAndDescription(InteractionType.tradeContainer, InteractionType.tradeContainer.ToString(), userID, UniqID),
            };
        }

        public override void InitAfterDeserialize()
        {
            base.InitAfterDeserialize();
            _posibleTradeElements = new List<TradeElement>();
            _selectedTradeElements = new List<TradeElement>();
        }
    }
}