﻿using System;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

[Serializable]
///Класс объекта "дом". Его задача обозначить дом в игровом мире. Наследуется от базового объекта игрового мира.
///Может назначаться в собственность персонажу. При этом все объекты в доме персонаж/владедец будет воспринимать как собственные
public class HouseObject : BaseTPCGameObject
{
    ///Внутренние пространство дома для удобства настройки задаеться объектами типа Transform.
    ///Какие Transform-ы используются для этого указано в базе данных, поэтому сохранять это поле не нужно.
    [NonSerialized] private Transform[] _houseVolumes;

    /// <summary>
    /// Конструктор вызывается в момент создания. При запуске игроком новой игры.
    /// </summary>
    /// <param name="id">Уникальный ID в игровом мире</param>
    /// <param name="prefubID">ID в базе данных</param>
    public HouseObject(int id, int prefubID) : base(id, prefubID, Vector3.zero, 
        new Quaternion())
    {
        ///Позиция не имеет значения поэтому обнулена
        _serializeblePosition = null;

        ///Получаем внутренние помещения дома из базы данных
        _houseVolumes = GameDataBase.Get(_prefubID).prefub.GetComponentsInChildren<Transform>();
    }

    /// <summary>
    /// Инициализация дома после загрузки из сохранения.
    /// </summary>
    public override void InitAfterDeserialize()
    {
        base.InitAfterDeserialize();
        _serializeblePosition = null;
        
        _houseVolumes = GameDataBase.Get(_prefubID).prefub.GetComponentsInChildren<Transform>();
    }


    /// <summary>
    /// Можно будет использовать для определения, находится ли определенный объект в этом доме.
    /// </summary>
    /// <param name="id">ID проверяемого объекта</param>
    /// <returns></returns>
    public bool IsObjectInVolume(int id)
    {
        return IsPointInVolume(TPCObjectsManager.Get(id).Position);
    }

    /// <summary>
    /// Получает все объекты в доме. Используется только на инициализации ИИ поэтому оптимизация не важна.
    /// </summary>
    /// <returns></returns>
    public List<BaseTPCGameObject> GetObjectsInVolume()
    {
        List<BaseTPCGameObject> objects = new List<BaseTPCGameObject>();
        
        ///Берем все объекты игрового мира
        foreach (var obj in TPCObjectsManager.GetAll<BaseTPCGameObject>())
        {
            ///еслм объект находится в доме то берем его
            if (obj != this && IsPointInVolume(obj.Position))
            {
                objects.Add(obj);
                
                ContainerObject container = obj as ContainerObject;

                ///Если объект является контейнером, то добавляем его содержимое
                if (container != null)
                {
                    foreach (var id in container.Inventory)
                    {
                        objects.Add(TPCObjectsManager.Get(id));
                    }
                }
                    
            }
        }

        return objects;
    }

    /// <summary>
    /// Просто вынесена часть внутренней логики. Определяет находится ли точка в доме.
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    private bool IsPointInVolume(Vector3 point)
    {
        foreach (var vol in _houseVolumes)
        {
            Vector3 locPos = vol.InverseTransformPoint(point);
            ///Проверяем попала ли точка в один из Transform-ов дома
            if (locPos.x < 0.5f && locPos.x > -0.5f
                && locPos.y < 0.5f && locPos.y > -0.5f
                && locPos.z < 0.5f && locPos.z > -0.5f)
            {
                return true;
            }
        }

        return false;
    }
}