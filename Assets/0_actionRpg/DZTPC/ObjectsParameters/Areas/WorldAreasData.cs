﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldAreasData : MonoBehaviour
{
    [Serializable]
    private struct AreaData
    {
        public AreasTypes Type;
        public WorldTrigger Zone;
    }

    [SerializeField] private AreaData[] _datas;


    private static Dictionary<AreasTypes, WorldTrigger> _areaTriggers = new Dictionary<AreasTypes, WorldTrigger>();

    private void Awake()
    {
        _areaTriggers.Clear();
        foreach (var areaData in _datas)
            if (_areaTriggers.ContainsKey(areaData.Type) == false && areaData.Zone != null)
                _areaTriggers.Add(areaData.Type, areaData.Zone);
    }

    public static bool IsPointInArea(AreasTypes areaType, Vector3 point) =>
        _areaTriggers.ContainsKey(areaType) && _areaTriggers[areaType].IsPointInArea(point);
}