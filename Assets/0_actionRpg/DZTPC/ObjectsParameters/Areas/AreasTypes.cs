﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AreasTypes {

	everywhere = 0,
	arlinTerritory = 10,
	townMainGate = 20,
	townLowLevel = 21,
	townElitGate = 25,
	townElitArea = 26,
}
