﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class WorldTrigger : MonoBehaviour
{

	public bool IsPointInArea(Vector3 point)
	{
		foreach (Transform subzone in GetComponentsInChildren<Transform>())
		{
			if (subzone.InverseTransformPoint(point).x > -0.5f 
			    && subzone.InverseTransformPoint(point).x < 0.5f &&
			    subzone.InverseTransformPoint(point).z > -0.5f 
			    && subzone.InverseTransformPoint(point).z < 0.5f &&
			    subzone.InverseTransformPoint(point).y > -0.5f 
			    && subzone.InverseTransformPoint(point).y < 0.5f)
				return true;
		}

		return false;
	}
	
#if UNITY_EDITOR

	private void OnDrawGizmos()
	{
		if(Application.isPlaying)
			return;
		if (Selection.activeTransform == null)
			return;
		if (Selection.activeTransform.root != transform.root)
			return;
		
		Gizmos.color = Selection.activeTransform == transform ? new Color(0.5f,0.3f,1,0.3f) : new Color(0.3f,0.3f,1,0.1f);

		DrawTransform(transform);
		
		foreach (Transform subzone in GetComponentsInChildren<Transform>())
		{
			DrawTransform(subzone);
		}
	}
	
	
	
	private void DrawTransform(Transform t)
	{
		Gizmos.matrix = Matrix4x4.TRS(t.position, t.rotation, t.lossyScale);
		Gizmos.DrawCube(Vector3.zero, Vector3.one);
	}

#endif
}
