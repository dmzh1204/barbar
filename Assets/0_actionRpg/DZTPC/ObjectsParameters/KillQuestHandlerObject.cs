﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DZTPC
{
	[Serializable]
	public class KillQuestHandlerObject : BaseTPCGameObject
	{
		private int[] _targetIds;
		private int _targetQuest;
		
		[NonSerialized]
		private float _delayToCheck = 3;
		
		public KillQuestHandlerObject(int id, int[] targetIds, int targetQuest) : base(id, -1, Vector3.zero, new Quaternion())
		{
			_targetIds = targetIds;
			_targetQuest = targetQuest;
			_delayToCheck = Random.value * 3;
		}
		
		public override void InitBeforeSerialize()
		{
			
		}

		public override void InitAfterDeserialize()
		{
			_delayToCheck = Random.value * 3;
		}
		
		public override void Update(float deltaTime)
		{
			_delayToCheck -= deltaTime;
			
			if(_delayToCheck > 0)
				return;

			bool allKilled = true;
			
			foreach (var target in _targetIds)
			{
				var obj = TPCObjectsManager.Get(target);
				if(obj == null) continue;
				
				LifeObject lo = obj as LifeObject;
				if(lo == null) break;
				
				if(lo.IsDead())
					continue;
				else
				{
					allKilled = false;
					break;
				}
			}
			
			_delayToCheck = 3;

			if (allKilled && UserController.PlayerCharacter != null)
			{
				UserController.PlayerCharacter.HumanObj.HumAiKnowelegeBase.CompleteQuest(_targetQuest, true);
				TPCObjectsManager.Remove(UniqID);
			}
		}
	}
}