﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
	[System.Serializable]
	public class ArmoredMonsterObject : Animals
	{

		public ArmoredMonsterObject(int id, int prefubID, Vector3 position, Quaternion rotation) : base(id, prefubID, position, rotation)
		{
			
		}

		public override bool HoldInHandMeleeWeapon()
		{
			return CurrentFightState == FightingStates.melee;
		}
	}
}