﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    [System.Serializable]
    public class TakebleObject : InteractiveObject
    {
        [System.NonSerialized] public System.Action<int> OnTakedBy;
        [System.NonSerialized] public System.Action<int> OnDropedBy;


        [System.NonSerialized] int _takedBy = -1;

        public int TakedBy
        {
            get { return _takedBy; }
        }


        public TakebleObject(int id, int prefubID, Vector3 position, Quaternion rotation)
            : base(id, prefubID, position, rotation)
        {
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            StartUse(-1);
        }

        protected virtual void OnTaked()
        {
        }
        
        protected virtual void OnDroped()
        {
            if (OnDropedBy != null)
                OnDropedBy(_takedBy);
        }

        public override void InitAfterDeserialize()
        {
            base.InitAfterDeserialize();

            foreach (var container in TPCObjectsManager.GetAll<ContainerObject>())
            {
                foreach (int id in container.Inventory)
                    if (id == UniqID)
                    {
                        _takedBy = container.UniqID;
                        _serializeblePosition = null;
                        return;
                    }
            }

            _serializeblePosition = new ObjSerializeblePosition();

            _takedBy = -1;
        }

        public override void StartUse(int userID)
        {
            if (userID >= 0)
            {
                if (_takedBy >= 0)
                {
                    (TPCObjectsManager.Get(_takedBy) as ContainerObject).RemoveFromInventar(UniqID);

                    OnDroped();
                }

                (TPCObjectsManager.Get(userID) as ContainerObject).AddToInventar(UniqID);

                if (OnTakedBy != null)
                    OnTakedBy(userID);

                _takedBy = userID;
                OnTaked();
            }
            else if (_takedBy >= 0)
            {
                ContainerObject co = TPCObjectsManager.Get(_takedBy) as ContainerObject;

                Position = co.Position;
                Rotation = co.Rotation;
                
                co.RemoveFromInventar(UniqID);
                
                OnDroped();
            }

            _takedBy = userID;

            if (_takedBy < 0)
            {
                _serializeblePosition = new ObjSerializeblePosition();
            }
            else
            {
                _serializeblePosition = null;
            }
            
            
        }

        
    }
}