﻿using System;
using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

namespace DZTPC
{
    [Serializable]
    public class Animals : LifeObject
    {
        [NonSerialized] Vector3 _homePosition = Vector3.zero;

        public Vector3 HomePosition => _homePosition;

        private float _homePosX = 0;
        private float _homePosY = 0;
        private float _homePosZ = 0;

        public Animals(int id, int prefubID, Vector3 position, Quaternion rotation) : base(id, prefubID, position,
            rotation)
        {
            _homePosition = position;
        }

        public override void InitBeforeSerialize()
        {
            base.InitBeforeSerialize();
            _homePosX = _homePosition.x;
            _homePosY = _homePosition.y;
            _homePosZ = _homePosition.z;
        }

        public override void InitAfterDeserialize()
        {
            base.InitAfterDeserialize();
            _homePosition = new Vector3(_homePosX, _homePosY, _homePosZ);
        }

        public override void Update(float deltaTime)
        {
        }

        protected override float GetBonus(DependetParams param)
        {
            switch (param)
            {
                case DependetParams.attackTime:
                    return (GameDataBase.Get(PrefubId) as GameDataBase.Animals).AttackTimeBonus;
                case DependetParams.attackDistMeele:
                    return (GameDataBase.Get(PrefubId) as GameDataBase.Animals).AttackMeleeDistBonus;
                case DependetParams.defenceCurrent:
                    return (GameDataBase.Get(PrefubId) as GameDataBase.Animals).DefenseBonus;
            }

            return 0;
        }
    }
}