﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public interface IWaterableObject
    {
        void OnWaterEnter(Collider waterTriggerCollider);

        void OnWaterExit(Collider waterTriggerCollider);

    }
}