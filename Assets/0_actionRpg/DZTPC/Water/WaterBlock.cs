﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class WaterBlock : MonoBehaviour
    {
        Collider _waterTriggerCollider = null;

        // Use this for initialization
        void Start()
        {
            _waterTriggerCollider = GetComponent<Collider>();
        }
	
        // Update is called once per frame
        void Update()
        {
		
        }

        void OnTriggerEnter(Collider c)
        {
            IWaterableObject iwObj = c.GetComponent<IWaterableObject>();

            if (iwObj == null)
                return;

            iwObj.OnWaterEnter(_waterTriggerCollider);
        }

        void OnTriggerExit(Collider c)
        {

        }

        void OnTriggerStay(Collider c)
        {

        }
    }
}