﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class EatingBlandShapeAnimator : MonoBehaviour
	{
		//[SerializeField] 
		private int _mouthUpDownIndex = 0;
		
		CharacterController _cc = null;

		private SkinnedMeshRenderer _skm = null;

		private Action<float> _updater = null;

		private float _timer = 0;
		
		// Use this for initialization
		private void OnEnable()
		{
			Invoke("Init", 2);
		}

		private void Init()
		{
			_skm = GetComponent<SkinnedMeshRenderer>();

			_cc = GetComponentInParent<CharacterController>();

			if(_cc == null)
				return;
			
			_cc.AvalibleActions.OnActions += OnDoAction;
		}

		private void OnDisable()
		{
			if(_cc == null)
				return;
			
			_cc.AvalibleActions.OnActions -= OnDoAction;
		}

		// Update is called once per frame
		void Update()
		{
			if(_updater == null)
				return;
			
			_updater(Time.deltaTime);
			_timer += Time.deltaTime;
		}

		void OnDoAction(CCSingleActions action)
		{
			if (action == CCSingleActions.Eating)
			{
				_timer = 0;
				_updater = EatUpdater;
			}
		}

		void EatUpdater(float deltaTime)
		{
			if(_timer < 0.5f)
				return;

			if (_timer > 3f)
			{
				_updater = null;
				_skm.SetBlendShapeWeight(_mouthUpDownIndex, 0);
				return;
			}
			
			_skm.SetBlendShapeWeight(_mouthUpDownIndex
				, (Mathf.Sin((_timer - 0.5f) * 360) + 1) * 50
				);
		}
	}
}