﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class CharAnimation : MonoBehaviour
    {
        Animator _antr = null;
        CharacterController _cc = null;

        static int _hashMoveF = -1;
        static int _hashMoveS = -1;
        static int _hashSurfType = -1;
        static int _hashPrevSurfType = -1;
        static int _hashSurfSwitch = -1;
        static int _hashCurrentWeapon = -1;
        static int _hashAttackMultiplier = -1;
        static int _dodgeRight = -1;
        static int _dodgeLeft = -1;
        static int _hashDeath = -1;
        static int _hashActionTrigger = -1;
        static int _hashActionType = -1;
        static int _hashSimpleAttackTrigger = -1;
        static int _hashBreackAttackTrigger = -1;
        static int _hashBlockAttackTrigger = -1;

        float _defMaxSpeed = 7;
        Vector3 _prevCCPos;
        private bool _needAlignSurf = false;

        // Use this for initialization
        void OnEnable()
        {
            if (_hashMoveF == -1)
            {
                _hashMoveF = Animator.StringToHash("MoveForward");
                _hashMoveS = Animator.StringToHash("MoveStrafe");
                _hashSurfType = Animator.StringToHash("SurfaceType");
                _hashPrevSurfType = Animator.StringToHash("PrevSurfaceType");
                _hashSurfSwitch = Animator.StringToHash("SurfaceTypeSwitch");
                _hashCurrentWeapon = Animator.StringToHash("CurrentWeapon");
                _hashAttackMultiplier = Animator.StringToHash("AttackMultiplier");
                _hashDeath = Animator.StringToHash("Death");
                _hashActionTrigger = Animator.StringToHash("ActionTrigger");
                _hashActionType = Animator.StringToHash("ActionType");
                _hashSimpleAttackTrigger = Animator.StringToHash("SimpleAttack");
                _hashBreackAttackTrigger = Animator.StringToHash("BreackAttack");
                _hashBlockAttackTrigger = Animator.StringToHash("CounterBlock");
                _dodgeRight = Animator.StringToHash("DodgeRight");
                _dodgeLeft = Animator.StringToHash("DodgeLeft");
            }

            _antr = GetComponent<Animator>();
            _cc = GetComponentInParent<CharacterController>();
            OnSurfStateChanged(_cc.CurSurfState);
            _cc.OnCurrentSurfStateChanged += OnSurfStateChanged;

            _prevCCPos = _cc.transform.position;
            _cc.OnClimbingUp += OnClimbingUp;
            _cc.AvalibleActions.OnActions += OnDoAction;

            _cc.Params.OnFightingStateChanged += OnFightingStateChanged;
            _cc.OnFixedUpdated += OnFixedUpdate;
            OnFightingStateChanged(_cc.Params.CurrentFightState);

            if (_cc.Params.IsDead())
                _antr.SetTrigger(_hashDeath);

            if (_cc.Params is AllEatAnimalObj)
                _needAlignSurf = true;

            Invoke("InitAnim", 0.01f);
        }

        void InitAnim()
        {
            _antr.Update(3);
        }

        public void OnRebindAnimator()
        {
            OnSurfStateChanged(_cc.CurSurfState);
            OnFightingStateChanged(_cc.Params.CurrentFightState);
        }

        void OnDisable()
        {
            _cc.OnCurrentSurfStateChanged -= OnSurfStateChanged;
            _cc.OnClimbingUp -= OnClimbingUp;
            _cc.AvalibleActions.OnActions -= OnDoAction;
            _cc.Params.OnFightingStateChanged -= OnFightingStateChanged;
            _cc.OnFixedUpdated -= OnFixedUpdate;
        }

        void OnFightingStateChanged(HumanoidObject.FightingStates fState)
        {
            if (_cc.HumanObj == null)
                return;

            if (fState == HumanoidObject.FightingStates.none)
            {
                _antr.SetFloat(_hashCurrentWeapon, -1);
                return;
            }

            if (fState == HumanoidObject.FightingStates.melee)
            {
                if (_cc.HumanObj.GetCurrentEquipedID(HumanoidObject.EquipTypes.meleeOneHand) >= 0)
                    _antr.SetFloat(_hashCurrentWeapon, 1);
                else if (_cc.HumanObj.GetCurrentEquipedID(HumanoidObject.EquipTypes.meleeTwoHand) >= 0)
                    _antr.SetFloat(_hashCurrentWeapon, 2);
                else
                    _antr.SetFloat(_hashCurrentWeapon, 0);
            }

            if (fState == HumanoidObject.FightingStates.shoot &&
                _cc.HumanObj.GetCurrentEquipedID(HumanoidObject.EquipTypes.bow) >= 0)
            {
                WeaponObject shootWeapon =
                    TPCObjectsManager.Get(_cc.HumanObj.GetCurrentEquipedID(HumanoidObject.EquipTypes.bow)) as
                        WeaponObject;
                _antr.SetFloat(_hashCurrentWeapon, shootWeapon.WeaponType == WeaponObject.WeaponTypes.bows ? 3 : 4);
            }
        }

        void OnFixedUpdate() => InternalUpdate(Time.fixedDeltaTime);

        private System.Action<float> updater;

        void InternalUpdate(float deltaTime)
        {
            if (updater != null)
                updater(deltaTime);

            _prevCCPos = _cc.transform.position;
        }

        SurfaseStates _prevSurfState = SurfaseStates.inAir;
        private bool _interactionExitin = false;

        void OnSurfStateChanged(SurfaseStates state)
        {
            if (_prevSurfState == state)
                return;

            transform.localEulerAngles = Vector3.zero;
            _angVelOffset = 0;
            _prevVelocity = Vector3.zero;

            switch (state)
            {
                case SurfaseStates.onGround:
                    updater = Walking;
                    break;

                case SurfaseStates.onConerClimb:
                    updater = ConerClimb;
                    break;

                case SurfaseStates.onWater:
                case SurfaseStates.underWater:
                    updater = Swiming;
                    break;

                case SurfaseStates.Interaction:
                    _interactionExitin = false;
                    updater = Interaction;
                    break;

                default:
                    updater = Walking;
                    break;
            }

            _antr.SetInteger(_hashPrevSurfType, (int) _prevSurfState);
            _antr.SetInteger(_hashSurfType, (int) state);
            _prevSurfState = state;
            _antr.SetTrigger(_hashSurfSwitch);
        }

        private Vector3 _prevVelocity = Vector3.zero;
        private float _angVelOffset = 0;
        
        private float currentLocalYAngle = 0;

        void TurnByVel(float deltaTime, Vector3 vel, bool useInverse = false)
        {
            var targetLocalYAngle = vel.sqrMagnitude < 0.1f ? 0 
                : Vector3.SignedAngle(_cc.transform.forward, vel.normalized, Vector3.up);
                
            if (useInverse && Mathf.Abs(targetLocalYAngle) > 90)
                targetLocalYAngle -= 180;

            currentLocalYAngle = Mathf.MoveTowardsAngle(currentLocalYAngle, targetLocalYAngle, deltaTime * 180);
            transform.localEulerAngles = new Vector3(0,currentLocalYAngle,0);        
        }
        
        void Walking(float deltaTime)
        {
            Vector3 velLoacl = transform.InverseTransformVector(_cc.RigBody.velocity) * transform.localScale.x;
            velLoacl /= _defMaxSpeed;

            if (_cc.HumanObj == null)
            {
                Vector3 vel = _cc.RigBody.velocity;
                vel /= _defMaxSpeed;
                vel.y = 0;

                _antr.SetFloat(_hashMoveF, vel.magnitude, 0.1f, deltaTime);
                TurnByVel(deltaTime, vel, true);
            }
            else
            {
                _antr.SetFloat(_hashMoveF, velLoacl.z, 0.1f, deltaTime);
                _antr.SetFloat(_hashMoveS, velLoacl.x, 0.1f, deltaTime);
                
                if (_cc.CurSurfState == SurfaseStates.onGround)
                {
                    float target = velLoacl.z > 0.5f
                        ? Mathf.Clamp(
                            Vector3.SignedAngle(_cc.RigBody.velocity, _prevVelocity, Vector3.up) / deltaTime / 5, -10,
                            10)
                        : 0;
                    _angVelOffset = Mathf.MoveTowards(_angVelOffset,
                        target, deltaTime * 50);
                }
                else
                {
                    _angVelOffset = 0;
                }

                transform.localEulerAngles = new Vector3(0, 0, _angVelOffset);
            }

            if (_needAlignSurf)
            {
                transform.rotation = Quaternion.FromToRotation(_cc.transform.up, _cc.SurfaceNormal) *
                                         transform.rotation;
            }

            _prevVelocity = _cc.RigBody.velocity;
        }

        void Swiming(float deltaTime)
        {
            Vector3 velByWater = _cc.RigBody.velocity - _cc.WaterStream;
            Vector3 velLoacl = transform.InverseTransformVector(velByWater) * transform.localScale.x;
            velLoacl /= (_defMaxSpeed / 2);

            _antr.SetFloat(_hashMoveF, velLoacl.z, 0.1f, deltaTime);
            _antr.SetFloat(_hashMoveS, velLoacl.x, 0.1f, deltaTime);
            TurnByVel(deltaTime, velByWater);
        }

        void ConerClimb(float deltaTime)
        {
            var x = _cc.transform.InverseTransformVector(_cc.RigBody.velocity).x * transform.localScale.x;
            _antr.SetFloat(_hashMoveF, 0, 0.1f, deltaTime);
            _antr.SetFloat(_hashMoveF, 0, 0.1f, deltaTime);
            _antr.SetFloat(_hashMoveS, x, 0.1f, deltaTime);
        }


        void Interaction(float deltaTime)
        {
            if (_interactionExitin != false || _cc.HumanObj.CurrentInterObject >= 0)
                return;
            _interactionExitin = true;
            _antr.SetTrigger(_hashActionTrigger);
        }

        void OnClimbingUp() => _antr.SetFloat(_hashMoveF, 1);

        int _prevSimpleAttack = 1;

        void OnDoAction(CCSingleActions action)
        {
            switch (action)
            {
                case CCSingleActions.SimpleAttack:
                    _antr.SetFloat(_hashAttackMultiplier, 1f / _cc.AvalibleActions.AttacksDuration);
                    _antr.SetTrigger(_hashSimpleAttackTrigger);
                    break;

                case CCSingleActions.CounterBlock:
                    _antr.SetTrigger(_hashBlockAttackTrigger);
                    break;

                case CCSingleActions.BreackedAttack:
                    _antr.SetTrigger(_hashBreackAttackTrigger);
                    break;

                case CCSingleActions.AttackToBlock:
                    _antr.SetTrigger(_hashBlockAttackTrigger);
                    break;

                case CCSingleActions.DodgeRight:
                    _antr.SetTrigger(_dodgeRight);
                    break;

                case CCSingleActions.DodgeLeft:
                    _antr.SetTrigger(_dodgeLeft);
                    break;

                case CCSingleActions.Revive:
                    _antr.SetFloat(_hashActionType, 0);
                    _antr.SetTrigger(_hashActionTrigger);
                    break;

                case CCSingleActions.StartUseObject:
                    if (TPCObjectsManager.Get(_cc.SelectedObjID) is TakebleObject)
                    {
                        _antr.SetFloat(_hashActionType, 1);
                        _antr.SetTrigger(_hashActionTrigger);
                    }

                    break;

                case CCSingleActions.ToMeleeFight:
                case CCSingleActions.ToShooting:

                    _antr.SetFloat(_hashActionType, 4);
                    _antr.SetTrigger(_hashActionTrigger);
                    break;

                case CCSingleActions.ToNoFight:

                    _antr.SetFloat(_hashActionType, 5);
                    _antr.SetTrigger(_hashActionTrigger);
                    break;

                case CCSingleActions.Activate:
                case CCSingleActions.Drop:

                    _antr.SetFloat(_hashActionType, 2);
                    _antr.SetTrigger(_hashActionTrigger);
                    break;

                case CCSingleActions.Damage:

                    _antr.SetFloat(_hashActionType, 3);
                    _antr.SetTrigger(_hashActionTrigger);
                    break;

                case CCSingleActions.Death:
                    _antr.SetTrigger(_hashDeath);
                    break;

                case CCSingleActions.Eating:
                    _antr.SetFloat(_hashActionType, 6);
                    _antr.SetTrigger(_hashActionTrigger);
                    break;
            }
        }

        #region AnimEvents

        void FootR()
        {
        }

        void FootL()
        {
        }

        void Land()
        {
        }

        void Hit()
        {
        }

        void WeaponSwitch()
        {
        }

        void Shoot()
        {
        }

        void OnDeath()
        {
        }

        #endregion
    }
}