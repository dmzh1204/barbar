﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAnimNumController : StateMachineBehaviour
{

    public float[] _limits = new float[0];

    static int _hashAnimNum = -1;
    static int _hashCurrentWeapon = -1;

    float targetAnimNum = 0;

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_hashAnimNum < 0)
        {
            _hashCurrentWeapon = Animator.StringToHash("CurrentWeapon");
            _hashAnimNum = Animator.StringToHash("AnimNum");
        }

        float animNum = animator.GetFloat(_hashAnimNum);
        animNum++;

        int index = (int)animator.GetFloat(_hashCurrentWeapon);
        index = Mathf.Clamp(index, 0, _limits.Length - 1);
        float maxLimit = _limits[index];

        if (animNum > maxLimit)
        {
            animator.SetFloat(_hashAnimNum, -1);
            animNum = 0;
        }

        targetAnimNum = animNum;
        animator.SetFloat(_hashAnimNum, targetAnimNum);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //animator.SetFloat(_hashAnimNum, Mathf.MoveTowards(animator.GetFloat(_hashAnimNum), targetAnimNum, Time.deltaTime * 10));
    }

}