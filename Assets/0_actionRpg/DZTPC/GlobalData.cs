﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DZTPC
{
	public static class GlobalData
	{
		static Vector3[] _foodGrowPoints = new Vector3[0];
		static Vector3[] _wildAnimalsPoints = new Vector3[0];

		private static bool _inited = false;
		
		static void Init()
		{
			if(_inited)
				return;

			_inited = true;
		}

		public static Vector3 GetClosestFoodGrow(Vector3 myPos, float minDist = -1)
		{
			Init();

			return GetClosest(myPos, ObjectsReplenisher.FoodPlants, minDist);
		}
		
		public static Vector3 GetClosestWildAnimals(Vector3 myPos, float minDist = -1)
		{
			Init();

			return GetClosest(myPos, ObjectsReplenisher.WildAnimals, minDist);
		}

		static Vector3 GetClosest(Vector3 myPos, List<Vector3> variants, float minDist = -1)
		{
			float dist = float.MaxValue;
			Vector3 pos = myPos;
				
			foreach (var point in variants)
			{
				float newDist = Vector3.Distance(myPos, point);
				if (newDist < dist && newDist > minDist)
				{
					dist = newDist;
					pos = point;
				}
			}
			
			return pos;
		}

		public static bool IsPointInArea(AreasTypes areaType, Vector3 point) =>
			WorldAreasData.IsPointInArea(areaType, point);
	}
}