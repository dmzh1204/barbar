﻿using System.Collections;
using System.Collections.Generic;
using baseLogic.uiScreens;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace DZTPC
{
    public class CameraController : MonoBehaviour
    {

        static Transform _target = null;
        static CharacterController _ccTarget = null;

        public static Transform Target
        {
            get
            {
                return _target;
            }
            set
            {
                _target = value;
                if (_target != null)
                {
                    _ccTarget = _target.GetComponent<CharacterController>();
                    _yWorldAxis = _target.eulerAngles.y;
                }
            }
        }

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            InterUpdate(Time.fixedDeltaTime);
        }

        static float _yWorldAxis = 0;
        static float _xWorldAxis = 0;
        const string _hAxis = "Mouse X";
        const string _vAxis = "Mouse Y";

        void Update()
        {
            float turnSpeed = 360;

            if (UserController.IsPCControl)
                turnSpeed /= 100;

            if (UIScreensManager.CurrentScreen.ScreenType != UIScreen.ScreenTypes.gameController)
                turnSpeed = 0;
            
            _xWorldAxis -= CrossPlatformInputManager.GetAxis(_vAxis) * turnSpeed;

            if (_ccTarget != null && _ccTarget.SelectedObject != null && _ccTarget.Params.CurrentFightState != HumanoidObject.FightingStates.none)
            {
                Vector3 locPos = transform.InverseTransformPoint(_ccTarget.SelectedObject.position + Vector3.up);
                _yWorldAxis += locPos.x * Time.deltaTime * 180;
                //_xWorldAxis -= locPos.y * Time.deltaTime * 180;
                _xWorldAxis = Mathf.MoveTowards(_xWorldAxis, 10, Time.deltaTime * 180);
            }
            else
            {
                _yWorldAxis += CrossPlatformInputManager.GetAxis(_hAxis) * turnSpeed;
            }

            _xWorldAxis = Mathf.Clamp(_xWorldAxis, -45, 45);
            
            //InterUpdate(Time.deltaTime);
        }

        public static void AddTurnAxis(float delta)
        {
            _yWorldAxis += delta;
        }

        private Vector3 _camOffsets = new Vector3(0, 1, -0.5f);
        private Vector3 lookVector = Vector3.forward;

        private Vector3 _prevPos = new Vector3(0, 1, -0.5f);
        private bool rightOffset = true;

        private float delayToSmothLookVector = 0;
        
        void InterUpdate(float deltaTime)
        {
            if (_target == null)
                return;
            
            if (_ccTarget.CurSurfState == SurfaseStates.Interaction)
            {
                return;
            }

            float speedMultiplier = 1;

            Vector3 locaVelocity = Vector3.zero;
            Vector3 locaVelocityNoNormalized = Vector3.zero;


            if (_ccTarget.CurSurfState == SurfaseStates.onGround)
            {
                Vector3 velocity = (_target.position - _prevPos) / deltaTime;

                locaVelocityNoNormalized = transform.InverseTransformVector(velocity);
                locaVelocity = locaVelocityNoNormalized.normalized;
            }
            
            _prevPos = _target.position;
            
            float charHeight = 2;

            Vector3 targetLookVector;

            targetLookVector = Quaternion.AngleAxis(_yWorldAxis, Vector3.up) * (Quaternion.AngleAxis(_xWorldAxis, Vector3.right) * Vector3.forward);
            targetLookVector.Normalize();
            
            
            Vector3 targetPosition;

            Vector3 targetOffsets;
            
            
            if (_ccTarget.Params.CurrentFightState == LifeObject.FightingStates.shoot)
            {
                targetOffsets = new Vector3(0.3f - locaVelocity.x, _ccTarget.Params.GetParametr(LifeObject.SpecifedParams.height), -1 - locaVelocity.z);
            }
            else
            {

                if (_ccTarget.Params.CurrentFightState == LifeObject.FightingStates.melee &&
                    _ccTarget.SelectedObject != null && GlobalSettings.GraphicsSettings.UseCameraOffsetInMeleeFighting)
                {
                    _yWorldAxis = _target.eulerAngles.y;
                    
                    delayToSmothLookVector = 1;
                    
                    if (locaVelocityNoNormalized.x * (rightOffset ? 1 : -1) > 6f)
                        rightOffset = !rightOffset;
                    
                    targetOffsets = new Vector3(
                        0 - locaVelocity.x + 0.4f * (rightOffset ? 1 : -1)
                        , charHeight / 1.2f
                        , -charHeight / 1.5f - locaVelocity.z);
                    
                    targetLookVector = (_target.forward 
                                  - _target.right / 4 * (rightOffset ? 1 : -1) 
                                  + Vector3.down / 5).normalized;
                    speedMultiplier = 3;
                }
                else
                {
                    targetOffsets = new Vector3(0 - locaVelocity.x, charHeight, -charHeight - locaVelocity.z);
                }
            }
            
            if (delayToSmothLookVector > 0 || _ccTarget.Params.CurrentFightState == LifeObject.FightingStates.melee &&
                _ccTarget.SelectedObject != null && GlobalSettings.GraphicsSettings.UseCameraOffsetInMeleeFighting)
            {
                delayToSmothLookVector -= deltaTime;
                lookVector = Vector3.RotateTowards(lookVector, targetLookVector, deltaTime * 3, deltaTime * 3);
            }
            else
                lookVector = targetLookVector;
            
            targetPosition = _target.position
                             + Vector3.up * _camOffsets.y
                             + _target.right * _camOffsets.x
                             + lookVector * _camOffsets.z;
            var headPos = _target.position
                          + Vector3.up * _ccTarget.Params.GetParametr(LifeObject.SpecifedParams.height) + lookVector.normalized * 0.2f;
            var vectorToCamera = targetPosition - headPos;
            
            if (Physics.Raycast(headPos, vectorToCamera, vectorToCamera.magnitude, 1))
            {
                targetPosition = headPos;
            }
            else
            {
                _camOffsets = Vector3.MoveTowards(_camOffsets, targetOffsets, deltaTime * speedMultiplier);
            }


            
            
            transform.position = targetPosition;

            transform.forward = lookVector;
        }
    }
}