﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class BaseAIStatesController
	{

		private EntityAIState _currentMoveState = null;
		private EntityAIState _currentActionState = null;
		protected HumanoidObject _human;
		
		public BaseAIStatesController(HumanoidObject human)
		{
			_human = human;
			_currentMoveState = new EntityAIState();
			_currentActionState = new EntityAIState();
		}

		public void Update(float deltaTime)
		{
			if(_human.IsDead())
				return;
			
			CheckStates();

			_currentMoveState.Update(deltaTime);
			_currentActionState.Update(deltaTime);
			
		}

		void StartMoveState(EntityAIState.AiStateTypes aiState)
		{
			if(_currentMoveState.AiStateType == aiState)
				return;
			_currentMoveState.Breack();

			_currentMoveState = CreateStates(aiState);
			
			_currentMoveState.Start();
		}
		
		void StartActionState(EntityAIState.AiStateTypes aiState)
		{
			if(_currentActionState.AiStateType == aiState)
				return;
			_currentActionState.Breack();

			_currentActionState = CreateStates(aiState);
			
			_currentActionState.Start();
		}

		protected virtual EntityAIState CreateStates(EntityAIState.AiStateTypes aiState)
		{
			return new EntityAIState();
		}

		void CheckStates()
		{
			switch (_human.HumAiKnowelegeBase.CurrentAiJob.JobType)
			{
				case AIJobTypes.none:
					StartMoveState(EntityAIState.AiStateTypes.entity);
					StartActionState(EntityAIState.AiStateTypes.entity);
					break;
				
				case AIJobTypes.sleep:
					
					StartMoveState(EntityAIState.AiStateTypes.walkTo);
					StartActionState(EntityAIState.AiStateTypes.sleepAction);

					break;
				
				case AIJobTypes.goToHunting:
					
					StartMoveState(EntityAIState.AiStateTypes.walkTo);
					StartActionState(EntityAIState.AiStateTypes.entity);

					break;
				
				case AIJobTypes.kill:
					StartMoveState(EntityAIState.AiStateTypes.fightingMove);
					StartActionState(EntityAIState.AiStateTypes.fighting);
					break;
				
				case AIJobTypes.lootContainer:
					StartMoveState(EntityAIState.AiStateTypes.walkTo);
					StartActionState(EntityAIState.AiStateTypes.lootAction);
					break;
				
				case AIJobTypes.moveTo:
					StartMoveState(EntityAIState.AiStateTypes.walkTo);
					StartActionState(EntityAIState.AiStateTypes.walkinToComleter);
					break;
				
				
				case AIJobTypes.eat:
					StartMoveState(EntityAIState.AiStateTypes.entity);
					StartActionState(EntityAIState.AiStateTypes.eat);
					break;
				
				case AIJobTypes.takeItem:
					StartMoveState(EntityAIState.AiStateTypes.walkTo);
					StartActionState(EntityAIState.AiStateTypes.take);
					break;
				
				case AIJobTypes.autoActivateDialog:
					StartMoveState(EntityAIState.AiStateTypes.walkTo);
					StartActionState(EntityAIState.AiStateTypes.autoUse);
					break;
			}
		}
	}
}