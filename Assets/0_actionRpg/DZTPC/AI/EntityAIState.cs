﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityAIState {
	public enum AiStateTypes
	{
		entity,
		walkTo,
		walkinToComleter,
		fightingMove,
		fighting,
		lootAction,
		sleepAction,
		eat,
		take,
		autoUse,
	}
		
	protected AiStateTypes _aiStateType = AiStateTypes.entity;

	public AiStateTypes AiStateType => _aiStateType;
	
	
	public virtual void Start()
	{
			
	}
		
	// Update is called once per frame
	public virtual void Update(float deltaTime)
	{

	}

	public virtual void Breack()
	{
			
	}
}
