namespace DZTPC
{
    public class VisibleAIStatesController : BaseAIStatesController
    {
        private CharacterControllerAI _ai;
        public VisibleAIStatesController(HumanoidObject human, CharacterControllerAI ai) : base(human)
        {
            _ai = ai;
        }

        protected override EntityAIState CreateStates(EntityAIState.AiStateTypes aiState)
        {
            switch (aiState)
            {
                case EntityCharAIState.AiStateTypes.sleepAction:
                    return new SleepingAIState(_ai);
                case EntityCharAIState.AiStateTypes.fighting:
                    return new FightingAIState(_ai);
                case EntityCharAIState.AiStateTypes.lootAction:
                    return new LootingAIState(_ai);
                case EntityCharAIState.AiStateTypes.walkinToComleter:
                    return new MoveToCompleterAIState(_ai);
                case EntityCharAIState.AiStateTypes.eat:
                    return new EatAIState(_ai);
                
                
				
                case EntityCharAIState.AiStateTypes.walkTo:
                    return new WalkToAIState(_ai);
                    break;
				
                case EntityCharAIState.AiStateTypes.fightingMove:
                    return new FightingMovingAIState(_ai);
                
                case EntityCharAIState.AiStateTypes.take:
                    return new TakeAIState(_ai);
                
                case EntityCharAIState.AiStateTypes.autoUse:
                    return new AutoUseAIState(_ai);
                
                default:
                    return new EntityCharAIState(_ai);
            }
            
        }
    }
}