﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
	public class CharacterControllerAI : MonoBehaviour
	{
		private PathFinder _pathFinder = null;

		public PathFinder PathFinder => _pathFinder;

		private CharacterController _cc = null;

		public CharacterController Cc => _cc;

		private VisibleAIStatesController _aiControler;
		
		// Use this for initialization
		void OnEnable()
		{
			_pathFinder = new PathFinder();

			_cc = GetComponent<CharacterController>();

			_aiControler = new VisibleAIStatesController(_cc.HumanObj, this);
			
		}
		
		// Update is called once per frame
		void FixedUpdate()
		{
			_aiControler.Update(Time.fixedDeltaTime);
		}
	}
}