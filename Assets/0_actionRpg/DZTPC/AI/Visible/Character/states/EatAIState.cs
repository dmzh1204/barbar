﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
	public class EatAIState : EntityCharAIState
	{
		public EatAIState(CharacterControllerAI ai) : base(ai)
		{
			_aiStateType = AiStateTypes.eat;

			
			
		}

		public override void Update(float deltaTime)
		{
			_ai.Cc.SelectedObjID = _ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.TargetId;

			_ai.Cc.AvalibleActions.DoActions(CCSingleActions.Eating);
			
			_ai.Cc.HumanObj.HumAiKnowelegeBase.CompleteCurrentAIJob();
		}
	}
}