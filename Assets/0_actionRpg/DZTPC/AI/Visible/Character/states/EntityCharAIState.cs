﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{



	public class EntityCharAIState : EntityAIState
	{
		
		
		protected CharacterControllerAI _ai;

		protected float _createTime = 0;

		public EntityCharAIState(CharacterControllerAI ai)
		{
			_ai = ai;
			_createTime = Time.time;
		}
		
		// Update is called once per frame
		public override void Update(float deltaTime)
		{
			_ai.Cc.AvalibleActions.DoActions(CCSingleActions.ToNoFight);

		}
	}
}