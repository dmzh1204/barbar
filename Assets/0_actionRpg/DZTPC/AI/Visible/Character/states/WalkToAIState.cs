﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class WalkToAIState : EntityCharAIState
    {
        public WalkToAIState(CharacterControllerAI ai) : base(ai)
        {
            _aiStateType = AiStateTypes.walkTo;
        }

        // Update is called once per frame
        public override void Update(float deltaTime)
        {
            _ai.Cc.AvalibleActions.DoActions(CCSingleActions.ToNoFight);

            _ai.PathFinder.Update(_ai.transform.position, _ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.GetTargetPos(
                _ai.Cc.HumanObj.Position
            ));

            if (_ai.PathFinder.GetDistance() < 1)
            {
                _ai.Cc.MoveVector = Vector3.zero;
                _ai.Cc.LookVector = _ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.UseTargetRot
                    ? _ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.GetTargetForward()
                    : _ai.Cc.LookVector;
            }
            else
            {
                
                var dist = _ai.PathFinder.GetDistance();

                var maxSpeed = _ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.JobType ==
                               AIJobTypes.autoActivateDialog && dist > 8
                    ? 1f
                    : 0.3f;

                _ai.Cc.MoveVector = _ai.PathFinder.GetDirection() * (dist < 2 ? 0.15f :
                                        maxSpeed);
                _ai.Cc.LookVector = dist < 2 && _ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.UseTargetRot
                    ? _ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.GetTargetForward()
                    : _ai.PathFinder.GetDirection();
            }
        }

        public override void Breack()
        {
            base.Breack();
            _ai.Cc.MoveVector = Vector3.zero;
        }
    }
}