﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
	public class SleepingAIState : EntityCharAIState
	{
		private bool _waitWakeUp = false;
		
		public SleepingAIState(CharacterControllerAI ai) : base(ai)
		{
			_aiStateType = AiStateTypes.sleepAction;
		}

		public override void Update(float deltaTime)
		{
			base.Update(deltaTime);
			
			_ai.Cc.AvalibleActions.DoActions(CCSingleActions.ToNoFight);

			BaseAIFunctions.SleepJobTryComplete(_ai.Cc.HumanObj);
		}
	}
}