﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
	public class FightingAIState : EntityCharAIState
	{
		static List<LifeObject> _playerAttackers = new List<LifeObject>();

		public static bool AnyAttackPlayer
		{
			get
			{
				if (_playerAttackers.Count > 0)
				{
					if (_playerAttackers[0].IsDead())
					{
						_playerAttackers.RemoveAt(0);
						return true;
					}

					return true;
				}
                
				return false;
			}
		}
		
		public FightingAIState(CharacterControllerAI ai) : base(ai)
		{
			_aiStateType = AiStateTypes.fighting;
			
			if (_ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.TargetId == UserController.PlayerLifeObj.UniqID)
			{
				_playerAttackers.Add(_ai.Cc.Params);
			}

		}

		private Vector3 targetLocalPos;
		private float distToTarget = 0;
		
		public override void Update(float deltaTime)
		{
			LifeObject lifeObject = TPCObjectsManager.Get(_ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.TargetId) as LifeObject;

			if (lifeObject.IsDead())
			{
				_ai.Cc.HumanObj.HumAiKnowelegeBase.CompleteCurrentAIJob();
				return;
			}
			
			targetLocalPos = _ai.transform.InverseTransformPoint(lifeObject.Position);
			distToTarget = targetLocalPos.magnitude;
			
			if (CanIShoot())
			{
				ShootingUpdate(deltaTime);
			}
			else
			{
				MeleeUpdate(deltaTime);
			}
			

		}

		bool CanIShoot()
		{
			if (distToTarget < 8)
				return false;

			if (Physics.Raycast(_ai.Cc.ShootPoint, _ai.Cc.LookVector, distToTarget - .6f))
				return false;

			return _ai.Cc.HumanObj.IsShootAvalible();
		}

		void ShootingUpdate(float deltaTime)
		{
			_ai.Cc.AvalibleActions.DoActions(CCSingleActions.ToShooting);

			foreach (var obj in GetClosestFriendly())
			{
				var targetPos = TPCObjectsManager.Get(_ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.TargetId).Position;
				var distToMe = Vector3.Distance(obj.Position, _ai.Cc.transform.position);
				var distToTarget = Vector3.Distance(obj.Position, targetPos);

				var localPos = _ai.Cc.transform.InverseTransformPoint(obj.Position);
				
				if(localPos.z < -1)
					continue;
				
				if(Vector3.Angle(Vector3.forward, localPos) < 20)
					return;
				
				if(distToTarget < 3 || distToMe < 3)
					return;
				
			}

			_ai.Cc.AvalibleActions.DoActions(CCSingleActions.SimpleAttack);
		}

		IEnumerable<BaseTPCGameObject> GetClosestFriendly()
		{
			foreach (var objID in TPCObjectsManager.GetClosest(_ai.Cc.Params.UniqID))
			{

				if (objID == _ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.TargetId)
					continue;
				var obj = TPCObjectsManager.Get(objID);
				if(obj is HumanoidObject == false)
					continue;

				yield return obj;
			}
		}


		void MeleeUpdate(float deltaTime)
		{
			_ai.Cc.AvalibleActions.DoActions(CCSingleActions.ToMeleeFight);

			if (distToTarget > _ai.Cc.Params.GetParametr(LifeObject.DependetParams.attackDistCurrent))
				return;
			foreach (var obj in GetClosestFriendly())
			{
				var localPos = _ai.Cc.transform.InverseTransformPoint(obj.Position);
				
				if(localPos.z < -1 || localPos.z > _ai.Cc.Params.GetParametr(LifeObject.DependetParams.attackDistCurrent) + 1)
					continue;
				
				if(Vector3.Angle(Vector3.forward, localPos) < 50)
					return;
			}

			_ai.Cc.AvalibleActions.DoActions(CCSingleActions.SimpleAttack);
		}

		public override void Breack()
		{
			base.Breack();
			
			_playerAttackers.Remove(_ai.Cc.Params);
		}
	}
}