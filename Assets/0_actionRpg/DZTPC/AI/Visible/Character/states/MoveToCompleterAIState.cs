﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
	public class MoveToCompleterAIState : EntityCharAIState
	{
		public MoveToCompleterAIState(CharacterControllerAI ai) : base(ai)
		{
			_aiStateType = AiStateTypes.walkinToComleter;
			_prevPos = _ai.transform.position;

		}

		private Vector3 _prevPos;

		private float timer = 0;
		private float dist = 0;
		
		public override void Update(float deltaTime)
		{
			dist += Vector3.Distance(_ai.transform.position, _prevPos);
			timer += deltaTime;

			if (timer > 1)
			{
				if (dist < 1)
				{
					_ai.Cc.HumanObj.HumAiKnowelegeBase.CompleteCurrentAIJob();
				}
				
				timer = 0;
				dist = 0;
			}
			
			
				
			_prevPos = _ai.transform.position;
		}

		public override void Breack()
		{
			
		}
	}
}