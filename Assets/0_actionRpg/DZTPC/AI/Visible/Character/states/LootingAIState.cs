﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DZTPC
{
	public class LootingAIState : EntityCharAIState
	{
		public LootingAIState(CharacterControllerAI ai) : base(ai)
		{
			_aiStateType = AiStateTypes.lootAction;
		}

		public override void Update(float deltaTime)
		{
			_ai.Cc.AvalibleActions.DoActions(CCSingleActions.ToNoFight);

			BaseAIFunctions.LootJobTryComlete(_ai.Cc.HumanObj);
			
		}
	}
}