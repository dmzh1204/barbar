﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
	public class FightingMovingAIState : EntityCharAIState
	{
		public FightingMovingAIState(CharacterControllerAI ai) : base(ai)
		{
			_aiStateType = AiStateTypes.fightingMove;

		}

		public override void Start()
		{
			base.Start();
			
			_ai.Cc.SelectedObjID = _ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.TargetId;

		}

		private Vector3 _targetPos; 
		
		public override void Update(float deltaTime)
		{
			_targetPos = TPCObjectsManager.Get(_ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.TargetId).Position;
			
			_ai.PathFinder.Update(_ai.transform.position, _targetPos);
			
			_ai.Cc.LookVector = (_targetPos - _ai.transform.position).normalized;

			switch (_ai.Cc.Params.CurrentFightState)
			{
				case LifeObject.FightingStates.melee:
					MoveMeele(deltaTime);
					break;
				
				case LifeObject.FightingStates.shoot:
					MoveShoot(deltaTime);
					break;
			}
		}

		void MoveMeele(float delta)
		{
			if (_ai.PathFinder.GetDistance() > _ai.Cc.Params.GetParametr(LifeObject.DependetParams.attackDistCurrent))
			{
				_ai.Cc.MoveVector = _ai.PathFinder.GetDirection();
			}
			else
			{
				_ai.Cc.MoveVector = Vector3.zero;
			}
		}

		void MoveShoot(float delta)
		{
			_ai.Cc.MoveVector = Vector3.zero;
		}

		public override void Breack()
		{
			base.Breack();
			_ai.Cc.SelectedObjID = -1;
			_ai.Cc.MoveVector = Vector3.zero;
		}
	}
}