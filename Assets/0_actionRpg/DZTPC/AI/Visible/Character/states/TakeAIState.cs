﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DZTPC
{
	public class TakeAIState : EntityCharAIState
	{
		public TakeAIState(CharacterControllerAI ai) : base(ai)
		{
			_aiStateType = AiStateTypes.take;
		}

		public override void Update(float deltaTime)
		{
			_ai.Cc.AvalibleActions.DoActions(CCSingleActions.ToNoFight);

			if (BaseAIFunctions.ObjectExistAndNoTaked(_ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.TargetId) ==
			    false)
			{
				_ai.Cc.HumanObj.HumAiKnowelegeBase.CompleteCurrentAIJob();
				return;
			}

			if (BaseAIFunctions.ObjectCanTakedNow(_ai.Cc.HumanObj.UniqID, _ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.TargetId) ==
			    false)
			{
				_ai.Cc.SelectedObjID = _ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.TargetId;
				_ai.Cc.AvalibleActions.DoActions(CCSingleActions.StartUseObject);
				_ai.Cc.HumanObj.HumAiKnowelegeBase.CompleteCurrentAIJob();
				return;
			}
		}
	}
}