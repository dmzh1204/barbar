﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DZTPC
{
	public class AutoUseAIState : EntityCharAIState
	{
		private HumanoidObject _humTarget = null;
		public AutoUseAIState(CharacterControllerAI ai) : base(ai)
		{
			_aiStateType = AiStateTypes.autoUse;
			
			
			_humTarget = TPCObjectsManager.Get(ai.Cc.HumanObj.HumAiKnowelegeBase.CurrentAiJob.TargetId) as HumanoidObject;
		}

		public override void Update(float deltaTime)
		{
			_ai.Cc.AvalibleActions.DoActions(CCSingleActions.ToNoFight);

			_ai.Cc.AvalibleActions.DoActions(CCSingleActions.StopUseObject);

			int[] objs = TPCObjectsManager.GetObjectsForUse(_ai.Cc.HumanObj.UniqID);

			foreach (var obj in objs)
			{
				if (obj == _humTarget.UniqID)
				{
					UserController.PlayerCharacter.AvalibleActions.DoActions(CCSingleActions.StopUseObject);
					
					UserController.PlayerCharacter.AvalibleActions.DoActions(CCSingleActions.ToNoFight);
	
					UserController.PlayerCharacter.SelectedObjID = _ai.Cc.HumanObj.UniqID;

					if (UserController.PlayerCharacter.AvalibleActions.DoActions(CCSingleActions.StartUseObject))
					{
						_ai.Cc.HumanObj.HumAiKnowelegeBase.CompleteCurrentAIJob();
					}
					
					return;
				}
			}

		}
	}
}