﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

namespace DZTPC
{
    public class AnimalAI : MonoBehaviour
    {
        CharacterController _cc = null;

        private PathFinder _pathFinder = null;


        private float comboIntervalMin = 3;
        private float comboIntervalMax = 7;
        private int attaksInComboCountMin = 2;
        private int attaksInComboCountMax = 3;

        static List<LifeObject> _playerAttackers = new List<LifeObject>();

        public static bool AnyAttackPlayer
        {
            get
            {
                if (_playerAttackers.Count > 0)
                {
                    if (_playerAttackers[0].IsDead())
                    {
                        _playerAttackers.RemoveAt(0);
                        return true;
                    }

                    return true;
                }
                
                return false;
            }
        }

        private LifeObject _objectTargetAttack = null;

        private LifeObject ObjectTargetAttack
        {
            get { return _objectTargetAttack; }
            set
            {
                if (_objectTargetAttack == value)
                    return;


                if (value == null)
                {
                    if (_objectTargetAttack == UserController.PlayerLifeObj)
                    {
                        _playerAttackers.Remove(_cc.Params);
                    }
                }
                else
                {
                    if (value == UserController.PlayerLifeObj)
                    {
                        _playerAttackers.Add(_cc.Params);
                    }
                }

                _objectTargetAttack = value;
            }
        }

        // Use this for initialization
        void OnEnable()
        {
            _cc = GetComponent<CharacterController>();

            _cc.Params.CurrentFightState = LifeObject.FightingStates.melee;

            _pathFinder = new PathFinder();
            _updater = FreeMovingUpdater;
            _cc.Params.OnDamage += OnDamage;
            LifeObject.GlobalOnDamage += OnDamage;

            if (_cc.Params.GetParametr(LifeObject.DependetParams.level) > 2)
            {
                attaksInComboCountMin = 4;
                attaksInComboCountMax = 5;
            }

            if (_cc.Params.GetParametr(LifeObject.DependetParams.level) > 4)
            {
                comboIntervalMin = 2;
                comboIntervalMax = 5;
            }
        }

        void OnDisable()
        {
            _cc.Params.OnDamage -= OnDamage;
            LifeObject.GlobalOnDamage -= OnDamage;

            _playerAttackers.Remove(_cc.Params);
        }

        private System.Action<float> _updater = null;

        // Update is called once per frame
        void FixedUpdate()
        {
            if (_cc.Params.GetParametr(LifeObject.SpecifedParams.currentHealth) <= 0)
            {
                ObjectTargetAttack = null;
                return;
            }

            DetectEnemie();

            _pathFinder.UpdateMyPos(transform.position);

            if (_updater != null)
                _updater(Time.fixedDeltaTime);
        }

        private void OnDamage(DamagParameters damag)
        {
            if (ObjectTargetAttack != null)
                return;

            LifeObject target = damag.Source;
            
            if (damag.Target != _cc.Params)
            {
                target = _cc.Params.CheckAttackTarget(damag);
                if(target == null)
                    return;
            }

            _nextTimeDetectEnemie = Time.fixedTime + 5;
            ObjectTargetAttack = target;
            _updater = AttackUpdate;
        }

        private float _nextTimeRandomMove = 0;

        void FreeMovingUpdater(float deltaTime)
        {
            if (ObjectTargetAttack != null && _updater != PreperingAttackUpdate)
            {
                _delayBeforeAttack = 2;
                _updater = PreperingAttackUpdate;
                return;
            }

            if (_pathFinder.GetDistance() > 1.5f)
            {
                float speed = 0.3f;

                _cc.MoveVector = _pathFinder.GetDirection() * 0.3f;
                if (_cc.MoveVector.magnitude > 0)
                    _cc.LookVector = _cc.MoveVector;
            }
            else
            {
                _cc.MoveVector = Vector3.zero;
            }

            if (Time.fixedTime < _nextTimeRandomMove)
                return;
            _nextTimeRandomMove = Time.fixedTime + 8 + Random.value * 4;
            //Debug.Log("xvxcbcxb");

            _pathFinder.SetTarget(PathFinder.RandomPoint((_cc.Params as Animals).HomePosition, 5));
        }


        private float _delayBeforeAttack = 5;

        void PreperingAttackUpdate(float deltaTime)
        {
            _delayBeforeAttack -= deltaTime;

            if (ObjectTargetAttack == null || ObjectTargetAttack.IsDead())
            {
                ObjectTargetAttack = null;
                _updater = FreeMovingUpdater;
                return;
            }

            _cc.LookVector = (ObjectTargetAttack.Position - _cc.Params.Position).normalized;

            _cc.MoveVector = Vector3.zero;

            if (_delayBeforeAttack < 0)
            {
                _cc.AvalibleActions.DoActions(CCSingleActions.ToMeleeFight);
                attckCount = Random.Range(attaksInComboCountMin, attaksInComboCountMax);
                _updater = AttackUpdate;
            }
        }

        private int attckCount = 0;
        private float delayToNextCombo = 0;

        void AttackUpdate(float deltaTime)
        {
            if (ObjectTargetAttack == null || ObjectTargetAttack.IsDead())
            {
                _cc.SelectedObjID = -1;

                ObjectTargetAttack = null;
                _updater = FreeMovingUpdater;
                return;
            }

            _cc.SelectedObjID = ObjectTargetAttack.UniqID;

            _cc.LookVector = _pathFinder.GetDirection();

            _pathFinder.SetTarget(ObjectTargetAttack.Position);

            float attackDist = _cc.Params.GetParametr(LifeObject.DependetParams.attackDistCurrent) - 0.1f;
            float distance = Vector3.Distance(ObjectTargetAttack.Position, _cc.Params.Position);

            if (attckCount <= 0)
            {
                delayToNextCombo -= deltaTime;

                if (distance < attackDist * 2
                    && _cc.Params.GetParametr(LifeObject.DependetParams.level) > 4)
                {
                    _cc.MoveVector = -_pathFinder.GetDirection();
                }
                else
                {
                    _cc.MoveVector = Vector3.zero;
                }

                if (delayToNextCombo < 0)
                {
                    attckCount = Random.Range(attaksInComboCountMin, attaksInComboCountMax);
                }

                return;
            }

            if (distance < attackDist)
            {
                _cc.MoveVector = Vector3.zero;
                if (_cc.AvalibleActions.IsAvalible(CCSingleActions.SimpleAttack))
                {
                    attckCount--;
                    _cc.AvalibleActions.DoActions(CCSingleActions.SimpleAttack);
                    delayToNextCombo = Random.Range(comboIntervalMin, comboIntervalMax);
                }
            }
            else
            {
                _cc.MoveVector = _pathFinder.GetDirection();
            }
        }

        const float distToPreperAttack = 10;

        private float _interval = 1;
        private float _nextTimeDetectEnemie = 1;

        void DetectEnemie()
        {
            if (Time.fixedTime < _nextTimeDetectEnemie)
                return;

            _nextTimeDetectEnemie = Time.fixedTime + _interval;

            const float distToAttack = 5;

            //ObjectTargetAttack = null;
            LifeObject targetAttack = null;
            foreach (int id in TPCObjectsManager.GetVisible(_cc.Params.UniqID))
            {
                BaseTPCGameObject obj = TPCObjectsManager.Get(id);

                if (obj == null)
                    continue;

                LifeObject lObj = obj as LifeObject;

                if (lObj == null || lObj.IsDead())
                    continue;

                float dist = Vector3.Distance(obj.Position, _cc.Params.Position);

                if (targetAttack != null && (targetAttack == obj
                                             || dist > Vector3.Distance(targetAttack.Position,
                                                 _cc.Params.Position)))
                {
                    continue;
                }

                if (dist > distToPreperAttack)
                    continue;

                if (_cc.Params.LifeDataBase.FractionConfig.GetAttitudeTo(lObj.LifeDataBase.FractionConfig)
                    != FractionConfig.Attitudes.aggressive)
                    continue;

                targetAttack = lObj;
                if (dist < distToAttack)
                {
                    _delayBeforeAttack = 0;
                    break;
                }

                break;
            }

            ObjectTargetAttack = targetAttack;
        }
    }
}