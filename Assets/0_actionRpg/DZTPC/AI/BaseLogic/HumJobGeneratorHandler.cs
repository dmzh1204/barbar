using ProBuilder2.Common;
using UnityEngine;
using UnityEngine.AI;

namespace DZTPC
{
    public class HumJobGeneratorHandler : BaseObjectHandler
    {
        protected HumanoidObject _humanoidObject;

        public HumanoidObject HumanoidObject => _humanoidObject;

        private float _timer = 0;

        public HumJobGeneratorHandler(int id, float currentTime, bool isVisible) : base(id, currentTime, isVisible)
        {
            _humanoidObject = _tpcGameObj as HumanoidObject;

            if (_humanoidObject != null)
            {
                _baseUpdater += HumanAiJobGenerate;
            }

            _humanoidObject.OnDamage += OnDamage;
            LifeObject.GlobalOnDamage += OnDamage;

        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            _humanoidObject.OnDamage -= OnDamage;
            LifeObject.GlobalOnDamage -= OnDamage;
        }

        private const float enemyDetectDist = 15;
        
        void OnDamage(DamagParameters damag)
        {
            if (damag.Source == null)
                return;

            if (_humanoidObject.HumAiKnowelegeBase.CurrentAiJob.JobType == AIJobTypes.kill)
                return;
            
            LifeObject target = damag.Source;
            
            if (damag.Target != _humanoidObject)
            {
                target = _humanoidObject.CheckAttackTarget(damag);
                if(target == null)
                    return;
            }
            
            _humanoidObject.HumAiKnowelegeBase.SetCurrentAIJob(new AIJobData(AIJobTypes.kill, target.UniqID));
        }


        void HumanAiJobGenerate(float deltaTime)
        {
            if (DialogController.DialogInProgress)
                return;

            ClearJobs();

            if (_humanoidObject.HumAiKnowelegeBase.CurrentAiJob.JobType != AIJobTypes.autoActivateDialog)
            {
                AutoStartDialogJob();
                SleepJob();
                EatJob();
                FindFoodJob();
                GoHomeJob();
                DetectEnemies();
            }

            _timer += deltaTime;
        }

        void ClearJobs()
        {
            if (_humanoidObject.HumAiKnowelegeBase.CurrentAiJob.ErrorFlag)
            {
                _humanoidObject.HumAiKnowelegeBase.CompleteCurrentAIJob();
                return;
            }

            if(_humanoidObject.HumAiKnowelegeBase.CurrentAiJob.JobType != AIJobTypes.sleep
               && TPCObjectsManager.Get(_humanoidObject.CurrentInterObject) is BedRepairIntrObj
               && _humanoidObject != UserController.PlayerLifeObj)
                (TPCObjectsManager.Get(_humanoidObject.CurrentInterObject) as BedRepairIntrObj).StopUse(_humanoidObject.UniqID);
            
            switch (_humanoidObject.HumAiKnowelegeBase.CurrentAiJob.JobType)
            {
                ///поиск жертвы
                case AIJobTypes.goToHunting:
                    foreach (var id in TPCObjectsManager.GetVisible(_humanoidObject.UniqID))
                    {
                        BaseTPCGameObject obj = TPCObjectsManager.Get(id);

                        if (obj == null)
                            continue;

                        /*if (Vector3.Distance(_humanoidObject.Position, obj.Position) > 20)
                            continue;*/

                        AllEatAnimalObj animal = obj as AllEatAnimalObj;
                        if (animal != null)
                        {
                            if (_humanoidObject.HumAiKnowelegeBase.IsRecentlyUsed(animal.UniqID))
                                continue;

                            _humanoidObject.HumAiKnowelegeBase.CompleteCurrentAIJob();

                            _humanoidObject.HumAiKnowelegeBase.SetCurrentAIJob(new AIJobData(AIJobTypes.lootContainer,
                                animal.UniqID));

                            if (!animal.IsDead())
                            {
                                _humanoidObject.HumAiKnowelegeBase.SetCurrentAIJob(new AIJobData(AIJobTypes.kill,
                                    animal.UniqID));
                            }

                            return;
                        }

                        FoodObject food = obj as FoodObject;
                        if (food != null)
                        {
                            _humanoidObject.HumAiKnowelegeBase.CompleteCurrentAIJob();

                            _humanoidObject.HumAiKnowelegeBase.SetCurrentAIJob(new AIJobData(AIJobTypes.takeItem,
                                food.UniqID));

                            return;
                        }
                    }

                    if (_timer > 120)
                        _humanoidObject.HumAiKnowelegeBase.CompleteCurrentAIJob();

                    break;
                
                case AIJobTypes.eat:
                    if (_timer > 10)
                        _humanoidObject.HumAiKnowelegeBase.CompleteCurrentAIJob();
                    break;
            }
        }

        void SleepJob()
        {
            if (_humanoidObject.HumAiKnowelegeBase.CurrentAiJob.JobType != AIJobTypes.none)
                return;

            if (_humanoidObject.GetParametr(LifeObject.SpecifedParams.energy) >
                _humanoidObject.GetParametr(LifeObject.DependetParams.maxEnergy) * 0.2f)
                return;

            if (_humanoidObject.HumAiKnowelegeBase.MyBed == null)
                return;

            _humanoidObject.HumAiKnowelegeBase.AddAIJob(
                new AIJobData(AIJobTypes.sleep, _humanoidObject.HumAiKnowelegeBase.MyBed.UniqID)
            );
        }

        void FindFoodJob()
        {
            if (_humanoidObject.HumAiKnowelegeBase.CurrentAiJob.JobType != AIJobTypes.none)
            {
                return;
            }

            int foodCount = 0;

            foreach (var id in _humanoidObject.Inventory)
            {
                if (TPCObjectsManager.Get(id) is FoodObject)
                {
                    foodCount++;
                }
            }

            if (foodCount > 2)
                return;

            Vector3 pos = Vector3.zero;

            if (_humanoidObject.GetCurrentEquiped(HumanoidObject.EquipTypes.meleeOneHand) != null
                || _humanoidObject.GetCurrentEquiped(HumanoidObject.EquipTypes.meleeTwoHand) != null)
            {
                pos = GlobalData.GetClosestWildAnimals(_humanoidObject.Position, 10);
                pos += Quaternion.AngleAxis(Random.value * 360, Vector3.up) * Vector3.forward * 5;
            }
            else
            {
                pos = GlobalData.GetClosestFoodGrow(_humanoidObject.Position, 10);
                pos += Quaternion.AngleAxis(Random.value * 360, Vector3.up) * Vector3.forward * 2;
            }

            _humanoidObject.HumAiKnowelegeBase.AddAIJob(
                new AIJobData(AIJobTypes.goToHunting,
                    pos)
            );

            _timer = 0;
        }

        void EatJob()
        {
            if (_humanoidObject.HumAiKnowelegeBase.CurrentAiJob.JobType != AIJobTypes.none)
                return;

            if (_humanoidObject.GetParametr(LifeObject.SpecifedParams.hunger) < -5)
                return;

            foreach (var id in _humanoidObject.Inventory)
            {
                if (TPCObjectsManager.Get(id) is FoodObject)
                {
                    _humanoidObject.HumAiKnowelegeBase.AddAIJob(
                        new AIJobData(AIJobTypes.eat, id));
                    return;
                }
            }
        }

        void GoHomeJob()
        {
            if (_humanoidObject.HumAiKnowelegeBase.CurrentAiJob.JobType != AIJobTypes.none)
                return;

            if (_humanoidObject.HumAiKnowelegeBase.HomePosition == null)
                return;

            NavMeshHit nmh;

            float randomRadius = _humanoidObject.HumAiKnowelegeBase.HomePositionRandom;

            Vector3 randomPosOffset = (Vector3.forward * (Random.Range(-randomRadius, randomRadius))
                                       + (Vector3.right * Random.Range(-randomRadius, randomRadius)));

            var posEnd = PathFinder.GetClosestNavPoint(_humanoidObject.HumAiKnowelegeBase.HomePosition.GetPos()
                                                    + randomPosOffset, 4);
            
            
            
            if (PathFinder.HasPath( PathFinder.GetClosestNavPoint(_humanoidObject.Position), posEnd))
            {
                if (randomRadius > 0)
                    _humanoidObject.HumAiKnowelegeBase.AddAIJob(new AIJobData(AIJobTypes.moveTo, posEnd));
                else
                    _humanoidObject.HumAiKnowelegeBase.AddAIJob(new AIJobData(AIJobTypes.moveTo, posEnd, _humanoidObject.HumAiKnowelegeBase.HomePosition.GetRot()));
            }
        }

        private void DetectEnemies()
        {
            if (_humanoidObject.HumAiKnowelegeBase.CurrentAiJob.JobType == AIJobTypes.kill)
                return;
            
            foreach (int id in TPCObjectsManager.GetVisible(_humanoidObject.UniqID))
            {
                var lObj = TPCObjectsManager.Get(id) as LifeObject;

                if (lObj == null 
                    || lObj.IsDead() 
                    || Vector3.Distance(lObj.Position, _humanoidObject.Position) > enemyDetectDist
                    || _humanoidObject.LifeDataBase.FractionConfig.GetAttitudeTo(lObj.LifeDataBase.FractionConfig)
                    != FractionConfig.Attitudes.aggressive)
                    continue;
                
                _humanoidObject.HumAiKnowelegeBase.SetCurrentAIJob(new AIJobData(AIJobTypes.kill, lObj.UniqID));
                break;
            }
        }

        private DialogsData.DialogData[] _dialogs = null;

        void AutoStartDialogJob()
        {
            if(UserController.PlayerCharacter == null)
                return;
            
            if (_dialogs == null)
            {
                _dialogs = new DialogsData.DialogData[0];

                GameDataBase.Characters c = GameDataBase.Get(_humanoidObject.PrefubId) as GameDataBase.Characters;

                foreach (var d in c.GetDialogs())
                {
                    if (d.StartType == DialogsData.DialogData.StartTypes.autoStartEvryware
                        || d.StartType == DialogsData.DialogData.StartTypes.autoStartIfVisible)
                    {
                        _dialogs = _dialogs.Add(d);
                    }
                }
            }

            foreach (var d in _dialogs)
            {
                if (d.StartType == DialogsData.DialogData.StartTypes.autoStartIfVisible)
                {
                    if (IsVisible == false)
                        return;
                }

                if (d.CanBeStartedBy(UserController.PlayerCharacter.HumanObj, _humanoidObject))
                {
                    while (_humanoidObject.HumAiKnowelegeBase.CurrentAiJob.JobType != AIJobTypes.none)
                    {
                        _humanoidObject.HumAiKnowelegeBase.CompleteCurrentAIJob();
                    }

                    _humanoidObject.HumAiKnowelegeBase.SetCurrentAIJob(new AIJobData(
                        AIJobTypes.autoActivateDialog,
                        UserController.PlayerLifeObj.UniqID
                    ));

                    break;
                }
            }
        }
    }
}