﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DZTPC
{
    public class BaseAIFunctions
    {
        public static bool ObjectCanTakedNow(int myId, int objId)
        {
            int[] all = TPCObjectsManager.GetObjectsForUse(myId);

            foreach (var id in all)
            {
                if (id == myId)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool ObjectExistAndNoTaked(int id)
        {
            TakebleObject obj = TPCObjectsManager.Get(id) as TakebleObject;

            if (obj == null || obj.TakedBy >= 0)
                return false;

            return true;
        }

        public static void LootJobTryComlete(HumanoidObject human)
        {
            foreach (var id in TPCObjectsManager.GetObjectsForUse(human.UniqID))
            {
                if (id == human.HumAiKnowelegeBase.CurrentAiJob.TargetId)
                {
                    ContainerObject container = TPCObjectsManager.Get(id) as ContainerObject;

                    if (container != null && container.CanBeUsed(human.UniqID))
                    {
                        container.StartUse(human.UniqID);

                        if (human.GetAvalibleAction(InteractiveObject.InteractionType.tradeConfirm) != null)
                        {
                            foreach (var el in container.SelectedTradeElements.ToArray())
                            {
                                if (el.TradeElType == TradeElement.TradeElemTypes.takebleItem &&
                                    TPCObjectsManager.Get(el.Id) is FoodObject)
                                    continue;

                                container.SelectedTradeElements.Remove(el);
                                container.PosibleTradeElements.Add(el);
                            }


                            human.GetAvalibleAction(InteractiveObject.InteractionType.tradeConfirm).Execute();
                        }

                        container.StopUse(human.UniqID);
                    }

                    human.HumAiKnowelegeBase.CompleteCurrentAIJob();
                }
            }
        }

        public static void SleepJobTryComplete(HumanoidObject hum)
        {
                if (hum.GetParametr(LifeObject.SpecifedParams.energy) >
                    hum.GetParametr(LifeObject.DependetParams.maxEnergy) * 0.9f)
                {
                    hum.HumAiKnowelegeBase.CompleteCurrentAIJob();

                    InteractiveObject io = TPCObjectsManager.Get(hum.CurrentInterObject) as InteractiveObject;
                    io?.StopUse(hum.UniqID);

                    return;
                }

            hum.GetAvalibleAction(InteractiveObject.InteractionType.sleepToRepair)?.Execute();

            var bed = hum.HumAiKnowelegeBase.MyBed;

            if (bed == null || bed.UniqID != hum.HumAiKnowelegeBase.CurrentAiJob.TargetId)
            {
                hum.HumAiKnowelegeBase.CompleteCurrentAIJob();
                return;
            }

            if (TPCObjectsManager.GetObjectsForUse(hum.UniqID).FirstOrDefault(x => x == bed.UniqID) != null)
                bed.StartUse(hum.UniqID);
        }
    }
}