using System;
using System.Collections.Generic;

namespace DZTPC
{
    public class BaseObjectHandler
    {
        public static float UpdateTimeMultiplier = 1;

        private int _id;

        protected BaseTPCGameObject _tpcGameObj = null;

        private float _lastTimeUpdate = 0;

        private bool _isVisible = false;

        protected Action<float> _unvisibleUpdater;
        private Action<float> _visibleUpdater;
        protected Action<float> _baseUpdater;

        public bool IsVisible
        {
            get { return _isVisible; }
            set { _isVisible = value; }
        }

        private LifeObject _lifeObject;

        public BaseObjectHandler(int id, float currentTime, bool isVisible)
        {
            _id = id;
            _tpcGameObj = TPCObjectsManager.Get(id);
            _lastTimeUpdate = currentTime;
            _isVisible = isVisible;

            _lifeObject = _tpcGameObj as LifeObject;

            if (_lifeObject != null)
            {
                _baseUpdater += LifeCleaner;
            }

            
        }

        public virtual void SetCurrentTime(float currentTime)
        {
            float deltaTime = (currentTime - _lastTimeUpdate) * UpdateTimeMultiplier;

            _tpcGameObj.Update(deltaTime);
            _lastTimeUpdate = currentTime;
            if (_isVisible)
            {
                UpdateVisible(deltaTime);
            }
            else
            {
                UpdateUnvisible(deltaTime);
            }

            UpdateBase(deltaTime);
        }

        protected virtual void UpdateUnvisible(float deltaTime)
        {
            _unvisibleUpdater?.Invoke(deltaTime);
        }

        protected virtual void UpdateVisible(float deltaTime)
        {
            _visibleUpdater?.Invoke(deltaTime);
        }

        protected virtual void UpdateBase(float deltaTime)
        {
            _baseUpdater?.Invoke(deltaTime);
        }

        private void LifeCleaner(float deltaTime)
        {
            if (_lifeObject.IsDead() && _lifeObject.CurrentUserId < 0 && _lifeObject.IsConteinerEmpty())
            {
                TPCObjectsManager.Remove(_id);
                OnDestroy();
            }
        }

        public virtual void OnDestroy()
        {
            
        }

        
    }
}