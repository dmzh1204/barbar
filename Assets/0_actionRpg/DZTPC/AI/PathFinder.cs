﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace DZTPC
{
    public class PathFinder
    {
        Vector3 _agentPos = Vector3.zero;
        Vector3 _targetPos = Vector3.zero;
        Vector3 _direction = Vector3.zero;
        float _distance = 0;
        private float _nextUdateTime = 0;
        private float _updateInterval = 1;

        private static int _areaMask = 1; //9 if with water

        private static int _waterAreaMask = 8;

        private const float _simplePosDistance = 30;

        private List<Vector3> _calculatedPath = new List<Vector3>();
        
        public PathFinder(float updateInterval = 1)
        {
            _updateInterval = updateInterval;
            //_nextUdateTime = Time.time + Random.value * updateInterval;
            _nextUdateTime = -1;
        }

        public void Reset()
        {
            _nextUdateTime = -1;
            _calculatedPath.Clear();
        }

        public void Update(Vector3 myPos, Vector3 targetPos)
        {
            _agentPos = myPos;
            _targetPos = targetPos;
            OnSomeChange();
        }
        
        public void UpdateMyPos(Vector3 pos)
        {
            /*if (_agentPos == pos)
                return;*/

            _agentPos = pos;
            OnSomeChange();
        }


        public void SetTarget(Vector3 pos)
        {
            /*if (_targetPos == pos)
                return;*/

            _targetPos = pos;
            OnSomeChange();
        }

        public Vector3 GetDirection()
        {
            return _direction;
        }

        public Vector3 GetPointOnPath(float dist)
        {
            Vector3 pos = _agentPos;

            if (dist <= 0 || _calculatedPath.Count == 0)
                return pos;

            int currentStep = 0;
            
            while (dist > 0 && currentStep < _calculatedPath.Count - 1)
            {
                dist -= Vector3.Distance(_calculatedPath[currentStep], _calculatedPath[currentStep + 1]);
                currentStep++;
            }

            if (dist < 0)
            {
                currentStep--;
                dist += Vector3.Distance(_calculatedPath[currentStep], _calculatedPath[currentStep + 1]);
                pos = _calculatedPath[currentStep] +
                      (_calculatedPath[currentStep + 1] - _calculatedPath[currentStep]).normalized * dist;
            }
            else
            {
                pos = _calculatedPath[_calculatedPath.Count - 1];
            }
            
            _nextUdateTime = -1;
            OnSomeChange();
            
            Debug.DrawRay(pos, Vector3.up, Color.green, _updateInterval);
            
            return pos;
        }

        public float GetDistance()
        {
            return _distance;
        }

        public static Vector3 RandomPoint(Vector3 center, float radius)
        {
            center += Quaternion.AngleAxis(Random.Range(0, 359), Vector3.up) * Vector3.forward * radius;

            NavMeshHit nmh;
            if (NavMesh.SamplePosition(center, out nmh, radius, 1))
            {
                return nmh.position;
            }

            return center;
        }

        void OnSomeChange()
        {
            if (Time.time > _nextUdateTime)
            {
                _nextUdateTime = Time.time + _updateInterval;
                RecalcPath();
            }

            PathCleaner();
        }

        private static readonly NavMeshPath emptyNavMeshPath = new NavMeshPath();
        public static bool HasPath(Vector3 start, Vector3 end)
        {
            return NavMesh.CalculatePath(start, end, _areaMask, emptyNavMeshPath);
        }
        
        public static Vector3 GetClosestNavPoint(Vector3 pos, int dist = 2)
        {
            NavMeshHit nmh;

            const float step = 1;
            const float searchRad = 1.5f;
                        
            for (int radius = 0; radius < dist; radius++)
            {
                for (int xOffset = -radius; xOffset <= radius; xOffset++)
                for (int yOffset = -radius; yOffset <= radius; yOffset++)
                for (int zOffset = -radius; zOffset <= radius; zOffset++)
                {
                    if (NavMesh.SamplePosition(pos + new Vector3(xOffset * step, yOffset * step, zOffset * step)
                        , out nmh, searchRad, _areaMask))
                    {
                        return nmh.position;
                    }
                }
            }

            return pos;
        }

        void PathCleaner()
        {
            if (_calculatedPath.Count > 1)
            {
                Vector3 vectorToFirstPoint = _calculatedPath[0] - _agentPos;
                vectorToFirstPoint.y = 0;

                if (vectorToFirstPoint.magnitude < 0.1f)
                {
                    _calculatedPath.RemoveAt(0);
                    RecalcDir();
                }
            }
        }

        void RecalcDist()
        {
            _distance = 0;

            for (int i = 0; i < _calculatedPath.Count; i++)
            {
                if (i == 0)
                {
                    _distance += Vector3.Distance(_agentPos, _calculatedPath[i]);
                    continue;
                }

                _distance += Vector3.Distance(_calculatedPath[i - 1], _calculatedPath[i]);
            }

            _distance += Vector3.Distance(_calculatedPath[_calculatedPath.Count - 1], _targetPos);

        }

        void RecalcDir()
        {
            if (_calculatedPath.Count == 0)
            {
                return;
            }
            
            _direction = (_calculatedPath[0] - _agentPos);
            _direction.y = 0;

            while (_calculatedPath.Count > 1 && _direction == Vector3.zero)
            {
                _calculatedPath.RemoveAt(0);
                
                _direction = (_calculatedPath[0] - _agentPos);
                _direction.y = 0;
            }
                        
            _direction = _direction.normalized;
            
            
            if (_direction == Vector3.zero && _distance > 0.5f)
            {
                _direction = Quaternion.AngleAxis(Random.Range(0, 359), Vector3.up) * Vector3.forward;
            }
        }
        
        void RecalcPath()
        {
            NavMeshHit nmh;
            Vector3 closestAgentPoint = _agentPos;
            Vector3 closestTargetPoint = _targetPos;

            closestAgentPoint = GetClosestNavPoint(_agentPos);
            closestTargetPoint = GetClosestNavPoint(_targetPos);

            _calculatedPath.Clear();
                
            NavMeshPath nmPath = new NavMeshPath();
            
            _calculatedPath.Add(_agentPos);
            _calculatedPath.Add(closestAgentPoint);

            if (NavMesh.CalculatePath(closestAgentPoint, closestTargetPoint, _areaMask, nmPath))
            {
                _calculatedPath.AddRange(nmPath.corners);
            }
            
            _calculatedPath.Add(closestTargetPoint);
            _calculatedPath.Add(_targetPos);
            
            RecalcDist();

            RecalcDir();
            
            
#if UNITY_EDITOR
            DrawDebugPath(_calculatedPath.ToArray());
#endif
        }

#if UNITY_EDITOR
        void DrawDebugPath(Vector3[] path)
        {
            //Debug.DrawRay(_agentPos, Vector3.up, Color.cyan, _updateInterval);

            if (path.Length < 1)
                return;
            
                
            /*Debug.DrawLine(_agentPos + Vector3.up, path[0] + Vector3.up, Color.red, _updateInterval);
            Debug.DrawLine(_agentPos, path[0], Color.red, _updateInterval);*/

            for (int i = 1; i < path.Length; i++)
            {
                Debug.DrawLine(path[i - 1] + Vector3.up, path[i] + Vector3.up, Color.red, _updateInterval);
                Debug.DrawLine(path[i - 1], path[i], Color.magenta, _updateInterval);
            }
        }
#endif
    }
}