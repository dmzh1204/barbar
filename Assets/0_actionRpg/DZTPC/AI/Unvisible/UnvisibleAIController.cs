namespace DZTPC
{
    public class UnvisibleAIController : BaseAIStatesController
    {
        private HumanoidObjectHandler _humanHandler;
        public UnvisibleAIController(HumanoidObjectHandler humanHandler) : base(humanHandler.HumanoidObject)
        {
            _humanHandler = humanHandler;
        }

        protected override EntityAIState CreateStates(EntityAIState.AiStateTypes aiState)
        {
            switch (aiState)
            {
                case EntityCharAIState.AiStateTypes.sleepAction:
                    return new SleepUnvisAIState(_humanHandler);
                case EntityCharAIState.AiStateTypes.fighting:
                    return new FightUnvisAIState(_humanHandler);
                case EntityCharAIState.AiStateTypes.lootAction:
                    return new LootUnvisAIState(_humanHandler);
                case EntityCharAIState.AiStateTypes.walkinToComleter:
                    return new WalkToCompleterUnvisAIState(_humanHandler);
                case EntityCharAIState.AiStateTypes.eat:
                    return new EatUnvisAIState(_humanHandler);
                
                
				
                case EntityCharAIState.AiStateTypes.walkTo:
                    return new WalkToUnvisAIState(_humanHandler);
				
                case EntityCharAIState.AiStateTypes.fightingMove:
                    return new WalkToUnvisAIState(_humanHandler);
                
                case EntityCharAIState.AiStateTypes.take:
                    return new TakeUnvisAIState(_humanHandler);
                
                default:
                    return new EntityUnvisAIState(_humanHandler);
            }
        }
    }
}