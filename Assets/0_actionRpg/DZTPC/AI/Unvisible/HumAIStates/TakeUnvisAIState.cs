﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class TakeUnvisAIState : EntityUnvisAIState
	{
		private bool _waitWakeUp = false;

		public TakeUnvisAIState(HumanoidObjectHandler humObjHandler) : base(humObjHandler)
		{
			_aiStateType = AiStateTypes.take;
		}
		
		public override void Update(float deltaTime)
		{
			if (BaseAIFunctions.ObjectExistAndNoTaked(_humObjHandler.HumanoidObject.HumAiKnowelegeBase.CurrentAiJob.TargetId) ==
			    false)
			{
				_humObjHandler.HumanoidObject.HumAiKnowelegeBase.CompleteCurrentAIJob();
				return;
			}

			if (BaseAIFunctions.ObjectCanTakedNow(_humObjHandler.HumanoidObject.UniqID, _humObjHandler.HumanoidObject.HumAiKnowelegeBase.CurrentAiJob.TargetId) ==
			    false)
			{

				(TPCObjectsManager.Get(_humObjHandler.HumanoidObject.HumAiKnowelegeBase.CurrentAiJob.TargetId)
					as TakebleObject).StartUse(_humObjHandler.HumanoidObject.UniqID);
				
				_humObjHandler.HumanoidObject.HumAiKnowelegeBase.CompleteCurrentAIJob();
				return;
			}
		}
	}
}