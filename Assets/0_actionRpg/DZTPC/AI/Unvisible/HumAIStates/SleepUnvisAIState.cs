﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class SleepUnvisAIState : EntityUnvisAIState
	{
		private bool _waitWakeUp = false;

		public SleepUnvisAIState(HumanoidObjectHandler humObjHandler) : base(humObjHandler)
		{
			_aiStateType = AiStateTypes.sleepAction;
		}
		
		public override void Update(float deltaTime)
		{
			BaseAIFunctions.SleepJobTryComplete(_humObjHandler.HumanoidObject);
		}
	}
}