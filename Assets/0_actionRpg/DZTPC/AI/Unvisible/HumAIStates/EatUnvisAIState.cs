﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class EatUnvisAIState : EntityUnvisAIState
	{
		public EatUnvisAIState(HumanoidObjectHandler humObjHandler) : base(humObjHandler)
		{
			_aiStateType = AiStateTypes.eat;
		}
		
		public override void Update(float deltaTime)
		{
			FoodObject food = TPCObjectsManager.Get(_humObjHandler.HumanoidObject.HumAiKnowelegeBase.CurrentAiJob.TargetId) as FoodObject;
			
			food?.Eat();
			
			_humObjHandler.HumanoidObject.HumAiKnowelegeBase.CompleteCurrentAIJob();

			
		}
	}
}