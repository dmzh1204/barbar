﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class WalkToUnvisAIState : EntityUnvisAIState
	{
		public WalkToUnvisAIState(HumanoidObjectHandler humObjHandler) : base(humObjHandler)
		{
			_aiStateType = AiStateTypes.walkTo;
			_humObjHandler.PathFinder.Reset();
		}

		public override void Update(float deltaTime)
		{
			if(_humObjHandler.HumanoidObject.CurrentInterObject > 0)
				return;
				
			_humObjHandler.PathFinder.Update(_humObjHandler.HumanoidObject.Position
				, _humObjHandler.HumanoidObject.HumAiKnowelegeBase.CurrentAiJob.GetTargetPos(
					_humObjHandler.HumanoidObject.Position)
				);

			float dist = Mathf.Clamp(deltaTime, deltaTime, 3);
			
			Vector3 nextPos;
			
			RaycastHit rh;

			for (float distMultiplier = 1; distMultiplier > 0; distMultiplier -= 0.25f)
			{
				nextPos = _humObjHandler.PathFinder.GetPointOnPath(dist * distMultiplier);
				
				if (Physics.Raycast(nextPos + Vector3.up, Vector3.down, out rh, 2, 1))
				{
					_humObjHandler.HumanoidObject.Position = rh.point;
					return;
				}
			}
		}
	}
}