﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class EntityUnvisAIState : EntityAIState
	{
		protected HumanoidObjectHandler _humObjHandler;
		
		public EntityUnvisAIState(HumanoidObjectHandler humObjHandler)
		{
			_humObjHandler = humObjHandler;
			_humObjHandler.HumanoidObject.CurrentFightState = LifeObject.FightingStates.none;
		}
		
	}
	
}