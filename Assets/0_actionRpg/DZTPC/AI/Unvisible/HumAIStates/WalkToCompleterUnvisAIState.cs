﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class WalkToCompleterUnvisAIState : EntityUnvisAIState
	{
		public WalkToCompleterUnvisAIState(HumanoidObjectHandler humObjHandler) : base(humObjHandler)
		{
			_aiStateType = AiStateTypes.walkinToComleter;
		}

		private Vector3 prevPos;

		private int staiCounter = 0;
		
		public override void Update(float deltaTime)
		{
			if (prevPos == _humObjHandler.HumanoidObject.Position)
			{
				staiCounter++;
			}
			else
			{
				staiCounter = 0;
			}
			
			prevPos = _humObjHandler.HumanoidObject.Position;
			
			if (Vector3.Distance(_humObjHandler.HumanoidObject.Position
				    , _humObjHandler.HumanoidObject.HumAiKnowelegeBase.CurrentAiJob.GetTargetPos(
					    _humObjHandler.HumanoidObject.Position
					    )) < 2 || staiCounter > 5)
			{
				_humObjHandler.HumanoidObject.HumAiKnowelegeBase.CompleteCurrentAIJob();
			}
		}
	}
}