﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class LootUnvisAIState : EntityUnvisAIState
	{
		public LootUnvisAIState(HumanoidObjectHandler humObjHandler) : base(humObjHandler)
		{
			_aiStateType = AiStateTypes.lootAction;
		}
		
		public override void Update(float deltaTime)
		{
			BaseAIFunctions.LootJobTryComlete(_humObjHandler.HumanoidObject);
		}
	}
}