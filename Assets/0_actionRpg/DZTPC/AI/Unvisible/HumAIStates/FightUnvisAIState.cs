﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class FightUnvisAIState : EntityUnvisAIState
	{
		private float damagInterval = 10;

		private float nextTimeDamage = 0;
		
		public FightUnvisAIState(HumanoidObjectHandler humObjHandler) : base(humObjHandler)
		{
			_aiStateType = AiStateTypes.fightingMove;
			nextTimeDamage = Time.time;

		}
		
		
		
		public override void Update(float deltaTime)
		{
			SetDamageToTarget();
		}

		void SetDamageToTarget()
		{
			if(Time.time < nextTimeDamage)
				return;
			
			nextTimeDamage = Time.time + damagInterval;

			LifeObject life = TPCObjectsManager.Get(_humObjHandler.HumanoidObject.HumAiKnowelegeBase.CurrentAiJob.TargetId) as LifeObject;

			if (life == null)
				return;
			
			if (life.IsDead())
			{
				_humObjHandler.HumanoidObject.HumAiKnowelegeBase.CompleteCurrentAIJob();
				return;
			}
			
			if(Vector3.Distance(life.Position, _humObjHandler.HumanoidObject.Position) > 5)
				return;
			
			
			life.SetDamage(new DamagParameters(_humObjHandler.HumanoidObject,
				life, _humObjHandler.HumanoidObject.GetParametr(LifeObject.DependetParams.attackMeele)));
		}
	}
}