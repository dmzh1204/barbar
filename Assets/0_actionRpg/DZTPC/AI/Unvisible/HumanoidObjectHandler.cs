using UnityEngine;

namespace DZTPC
{
    public class HumanoidObjectHandler : HumJobGeneratorHandler
    {
        private PathFinder _pathFinder;

        public PathFinder PathFinder => _pathFinder;

        private UnvisibleAIController _aiController;

        public HumanoidObjectHandler(int id, float currentTime, bool isVisible) : base(id, currentTime, isVisible)
        {
            _unvisibleUpdater += UnvisibleUpdate;
            _pathFinder = new PathFinder();

            _aiController = new UnvisibleAIController(this);
        }

        void UnvisibleUpdate(float deltaTime)
        {
            _aiController.Update(deltaTime);

#if UNITY_EDITOR
            DrawDebug(deltaTime);
#endif
        }

#if UNITY_EDITOR
        void DrawDebug(float deltaTime)
        {
            Debug.DrawRay(_humanoidObject.Position, Vector3.up, Color.yellow, deltaTime);
        }
#endif
    }
}