﻿using System.Collections;
using System.Collections.Generic;
using baseLogic.uiScreens;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace DZTPC
{
    public class UserController : MonoBehaviour
    {
        static CharacterController _cc = null;

        static UserController _inst = null;


        public static CharacterController PlayerCharacter
        {
            get { return _cc; }
        }

        public static LifeObject PlayerLifeObj
        {
            get
            {
                if (_cc == null)
                    return null;
                return _cc.Params;
            }
        }


        public static bool IsPCControl
        {
            get
            {
                #if !MOBILE_INPUT
                return true;
                #endif
                return false;
            }
        }


        public static UserController Inst
        {
            set { _inst = value; }
        }

        public static System.Action<SurfaseStates> OnCurrentSurfStateChanged;
        public static System.Action<LifeObject.FightingStates> OnCurrentFightingStateChanged;


        const string _hAxis = "Horizontal";
        const string _vAxis = "Vertical";
        const string _jumpInpStr = "Jump";
        const string _fire1 = "Fire1";
        const string _fire2 = "Fire2";
        const string _fire3 = "Fire3";
        const string _submit = "Submit";

        public const float _delayForPowerAttack = 1f;
        public const float _intervalForPowerAttack = 0.5f;
        public static int LastWeaponType = 0;

        private float _lastAttackTime = 0;

        // Use this for initialization
        void OnEnable()
        {
            LastWeaponType = 0;
            Inst = this;
            _cc = GetComponent<CharacterController>();

            //if (_cc != null)
            {
                _cc.OnCurrentSurfStateChanged += OnSurfStateChanged;
                _cc.Params.OnFightingStateChanged += OnFightStateChanged;
                _cc.Params.OnDamage += OnDamage;

                OnSurfStateChanged(_cc.CurSurfState);
                OnFightStateChanged(_cc.Params.CurrentFightState);
            }


            CameraController.Target = transform;
        }

        void OnDisable()
        {
            if (_inst == this)
                _inst = null;

            if (_cc != null)
            {
                _cc.OnCurrentSurfStateChanged -= OnSurfStateChanged;
                _cc.Params.OnFightingStateChanged -= OnFightStateChanged;
                _cc.Params.OnDamage -= OnDamage;
            }

            _cc = null;
            CameraController.Target = null;
        }


        void OnDamage(DamagParameters damag)
        {
            _lastAttackTime = Time.time;
        }

        void OnSurfStateChanged(SurfaseStates state)
        {
            if (OnCurrentSurfStateChanged != null)
                OnCurrentSurfStateChanged(_cc.CurSurfState);
        }

        void OnFightStateChanged(LifeObject.FightingStates state)
        {
            if (state == LifeObject.FightingStates.melee && _cc.SelectedObject != null &&
                _cc.SelectedObject is LifeObject == false)
            {
                _cc.SelectedObject = null;
            }

            if (state == LifeObject.FightingStates.shoot && _cc.SelectedObject != null)
            {
                _cc.SelectedObject = null;
            }

            if (OnCurrentFightingStateChanged != null)
                OnCurrentFightingStateChanged(_cc.Params.CurrentFightState);
        }

        private void DodgeHandler()
        {
            if (_cc.Params.CurrentFightState != HumanoidObject.FightingStates.melee 
                || _cc.SelectedObject == null 
                || Vector3.Distance(_cc.transform.position, _cc.SelectedObject.position) > 3)
                return;

            var localVectorMove = _cc.transform.InverseTransformDirection(_cc.MoveVector);
            const float moveLimit = 0.4f;
            if (localVectorMove.x > moveLimit)
                _cc.AvalibleActions.DoActions(CCSingleActions.DodgeRight);
            
            if (localVectorMove.x < -moveLimit)
                _cc.AvalibleActions.DoActions(CCSingleActions.DodgeLeft);

        }

        // Update is called once per frame
        void Update()
        {
            if (Camera.main == null)
            {
                _cc.MoveVector = Vector3.zero;
                return;
            }

            if (DialogController.DialogInProgress)
                _lastAttackTime = Time.time;

            Vector3 cameraForward = Camera.main.transform.forward;
            cameraForward.y = 0;
            cameraForward.Normalize();

            Vector3 cameraRight = Camera.main.transform.right;
            cameraRight.y = 0;
            cameraRight.Normalize();

            float vAxis = CrossPlatformInputManager.GetAxis(_vAxis);
            float hAxis = CrossPlatformInputManager.GetAxis(_hAxis);

            if (_cc.Params.CurrentFightState == HumanoidObject.FightingStates.none)
            {
                CameraController.AddTurnAxis(hAxis * Mathf.Abs(hAxis) * 4);
                _cc.MoveVector = cameraForward * vAxis;
            }
            else
            {
                Vector3 axises = new Vector3(hAxis, 0, vAxis);

                float angle = Vector3.Angle(Vector3.forward, axises);

                if (angle < 22.5f || angle > 157.5f)
                    hAxis = 0;
                else if (angle < 112.5f && angle > 67.5f)
                    vAxis = 0;

                _cc.MoveVector = cameraForward * vAxis + cameraRight * hAxis;

                DodgeHandler();
            }

            if (UIScreensManager.CurrentScreen.ScreenType != UIScreen.ScreenTypes.gameController)
                return;

            if (_cc.CurSurfState == SurfaseStates.onGround && CrossPlatformInputManager.GetButtonDown(_jumpInpStr))
                    _cc.MoveVector += Vector3.up;

            if ((_cc.CurSurfState == SurfaseStates.underWater
                || _cc.CurSurfState == SurfaseStates.onConerClimb
                || _cc.CurSurfState == SurfaseStates.onVStair) 
                && CrossPlatformInputManager.GetButton(_jumpInpStr))
                    _cc.MoveVector += Vector3.up;

            if (_cc.CurSurfState == SurfaseStates.onVStair
                || _cc.CurSurfState == SurfaseStates.onConerClimb
                || _cc.CurSurfState == SurfaseStates.underWater
                || _cc.CurSurfState == SurfaseStates.onWater)
            {
                if (CrossPlatformInputManager.GetButton(_fire1))
                    _cc.MoveVector += Vector3.down;
            }
            else
            {
                DetectTargetForFight(3, 20);
                    
                if (_cc.Params.CurrentFightState != HumanoidObject.FightingStates.none)
                {
                    if (CrossPlatformInputManager.GetButtonDown(_submit))
                    {
                        _lastAttackTime = Time.time;

                        if (_cc.AvalibleActions.IsAvalible(CCSingleActions.ToShooting))
                        {
                            LastWeaponType = 1;
                            _cc.AvalibleActions.DoActions(CCSingleActions.ToShooting);
                        }
                        else
                        {
                            if (_cc.AvalibleActions.IsAvalible(CCSingleActions.ToMeleeFight))
                            {
                                LastWeaponType = 0;
                                _cc.AvalibleActions.DoActions(CCSingleActions.ToMeleeFight);
                            }
                        }
                    }
                    
                    if (_cc.SelectedObjID >= 0)
                    {
                        _lastAttackTime = Time.time;

                        if (TPCObjectsManager.Get(_cc.SelectedObjID) is LifeObject
                            && (TPCObjectsManager.Get(_cc.SelectedObjID) as LifeObject).IsDead())
                            _cc.SelectedObjID = -1;
                    }
                    
                    if (CrossPlatformInputManager.GetButtonDown(_fire1))
                        Attack(cameraForward);
                    if (CrossPlatformInputManager.GetButtonDown(_fire2))
                        Attack(cameraForward, true);

                    if (_cc.Params.CurrentFightState == HumanoidObject.FightingStates.shoot)
                    {
                        _cc.LookVector = Camera.main.transform.forward;
                    }


                    if (CrossPlatformInputManager.GetButtonDown(_fire3) || _lastAttackTime + 10 < Time.time)
                    {
                        _cc.AvalibleActions.DoActions(CCSingleActions.ToNoFight);
                    }
                }
                else
                {
                    _cc.SelectedObjID = -1;
                    foreach (int id in TPCObjectsManager.GetObjectsForUse(_cc.Params.UniqID))
                    {
                        _cc.SelectedObjID = id;
                        break;
                    }

                    if (CrossPlatformInputManager.GetButtonDown(_submit))
                    {
                        if (_cc.SelectedObjID >= 0
                            && _cc.AvalibleActions.IsAvalible(CCSingleActions.StartUseObject)
                            && TPCObjectsManager.Get(_cc.SelectedObjID) is BedRepairIntrObj)
                        {
                            RootInitializer.Save(0);
                        }

                        _cc.AvalibleActions.DoActions(CCSingleActions.StartUseObject);
                    }

                    if (CrossPlatformInputManager.GetButtonDown(_fire1))
                    {
                        _lastAttackTime = Time.time;

                        if (LastWeaponType == 0
                            || _cc.AvalibleActions.IsAvalible(CCSingleActions.ToShooting) == false)
                        {
                            _cc.AvalibleActions.DoActions(CCSingleActions.ToMeleeFight);
                        }
                        else
                        {
                            _cc.AvalibleActions.DoActions(CCSingleActions.ToShooting);
                        }
                    }
                }


                
            }

            if (_cc.MoveVector.x != 0 || _cc.MoveVector.z != 0)
            {
                _cc.LookVector = cameraForward;
            }
        }

        private void Attack(Vector3 cameraForward, bool isBlock = false)
        {
            
                _lastAttackTime = Time.time;

                CCSingleActions attackAction = isBlock ? CCSingleActions.AttackToBlock : CCSingleActions.SimpleAttack;

                if (_cc.AvalibleActions.DoActions(attackAction))
                {
                    _cc.LookVector = cameraForward;

                    DetectTargetForFight();
                }
        }


        private void DetectTargetForFight(float distMultiplier = 2, float angle = 90)
        {
            if (_cc.SelectedObject != null || _cc.Params.CurrentFightState != HumanoidObject.FightingStates.melee)
            {
                return;
            }

            foreach (int id in TPCObjectsManager.GetVisible(_cc.Params.UniqID))
            {
                LifeObject life = TPCObjectsManager.Get(id) as LifeObject;

                if (life == null)
                    continue;
                if (Vector3.Distance(life.Position, _cc.Params.Position) <
                    _cc.Params.GetParametr(LifeObject.DependetParams.attackDistCurrent) * distMultiplier
                    && Vector3.Angle(_cc.LookVector.normalized, (life.Position - _cc.Params.Position).normalized) < angle
                    && life.IsDead() == false
                )
                {
                    _cc.SelectedObjID = id;
                    return;
                }
            }
        }
    }
}