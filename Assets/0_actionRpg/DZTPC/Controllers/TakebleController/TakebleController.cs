﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class TakebleController : MonoBehaviour
    {
        TakebleObject _params = null;

        public TakebleObject Params
        {
            get
            {
                return _params;
            }
            set
            {
                _params = value;
                Init();
            }
        }

        void Init()
        {
            RaycastHit rh;
            if (Physics.Raycast(transform.position + Vector3.up, Vector3.down, out rh, 5, 1))
            {
                transform.position = rh.point + Vector3.up * 0.1f;
                transform.rotation = Quaternion.AngleAxis(_params.Rotation.eulerAngles.y, rh.normal);
                _params.Position = transform.position;
                _params.Rotation = transform.rotation;
            }

            _params.OnTakedBy += TakeBy;
        }

        void OnDisable()
        {
            _params.OnTakedBy -= TakeBy;
        }

        void TakeBy(int id)
        {
            Destroy(gameObject);
        }

    }
}