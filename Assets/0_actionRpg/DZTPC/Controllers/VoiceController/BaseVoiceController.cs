﻿using System.Collections;
using System.Collections.Generic;
using baseLogic.Audio;
using UnityEngine;

namespace DZTPC
{



	public class BaseVoiceController
	{
		private CharacterController _cc;

		// Use this for initialization
		public BaseVoiceController(CharacterController cc)
		{
			_cc = cc;
		}
		
		public void Stop()
		{
			_cc.AvalibleActions.OnActions -= OnAction;
		}
		public void Start()
		{
			_cc.AvalibleActions.OnActions += OnAction;
		}

		void OnAction(CCSingleActions actionType)
		{
			AudioSystem.PlayEffect(GetAudioEffect(actionType), _cc.Params.Position);
		}

		public AudioClip GetAudioEffect(CCSingleActions actionType)
		{
			return GameDataBase.GetAudioEffectForAction(_cc.Params, actionType);
		}
	}
}