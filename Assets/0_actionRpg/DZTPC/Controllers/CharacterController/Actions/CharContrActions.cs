﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DZTPC
{
    public class CharContrActions
    {
        CharacterController _cc = null;

        public const float MaxEndurence = 1;

        private float _currentEndurence = 1;

        public float CurrentEndurence
        {
            get { return _currentEndurence; }
            set { _currentEndurence = value; }
        }

        public float AccumulatedEndurance => _accumulatedEndurance;

        private float _accumulatedEndurance = 0;

        float _delayProcess = -1;

        float _attackDurationMultiplier = 1;

        private float _prevDodgeTime = 0;

        public bool InProcess
        {
            get
            {
                if (_delayProcess > 0)
                    return true;

                return false;
            }
        }

        public bool CanRotate => CanMove || _cc.Params.CurrentFightState == LifeObject.FightingStates.shoot
                                         || _jobs.Count == 0 && _cc.Params.CurrentFightState ==
                                         LifeObject.FightingStates.melee;

        public bool CanMove => _cc.Params.CurrentInteractionIo == InteractiveObject.InteractionType.none 
                               && _delayProcess <= 0
                               && (_currentEndurence > 0 || _cc.Params.CurrentFightState == LifeObject.FightingStates.none);

        public CharContrActions()
        {
        }

        public CharContrActions(CharacterController cc)
        {
            _cc = cc;
        }

        public void ReInit(CharacterController cc)
        {
            _cc = cc;
        }

        public void AddDelay(float delay) => _delayProcess = _delayProcess < 0 ? delay : _delayProcess + delay;

        private class ActionUsage
        {
            private List<float> usageTimes = new List<float>();

            private const float ResetTime = 8;

            public float OnUse()
            {
                usageTimes.Add(Time.fixedTime);
                return GetEndurMult(); 
            }

            private float GetEndurMult()
            {
                while (usageTimes.Count > 0 && Time.fixedTime - usageTimes[0] > ResetTime)
                    usageTimes.RemoveAt(0);

                if (usageTimes.Count > 2)
                    return 2f;

                if (usageTimes.Count > 4)
                    return 3f;
                        
                return 1;
            }
        }

        private ActionUsage attackUsage = new ActionUsage();
        private ActionUsage blockUsage = new ActionUsage();
        private ActionUsage dodgeUsage = new ActionUsage();

        private float EndurenceCost(CCSingleActions sa)
        {
            switch (sa)
            {
                case CCSingleActions.SimpleAttack:
                    return 0.1f * attackUsage.OnUse();
                    break;

                case CCSingleActions.AttackToBlock:
                    return 0.05f * blockUsage.OnUse();
                    break;

                case CCSingleActions.CounterBlock:
                    return -0.2f;
                    break;

                case CCSingleActions.DodgeBack:
                case CCSingleActions.DodgeForward:
                case CCSingleActions.DodgeRight:
                case CCSingleActions.DodgeLeft:
                    return 0.05f * dodgeUsage.OnUse();
                    break;
            }

            return 0;
        }

        public bool IsAvalible(CCSingleActions sa)
        {
            if (_cc.Params.IsDead() && sa != CCSingleActions.Death)
            {
                return false;
            }

            if (sa == CCSingleActions.StopUseObject)
            {
                if (DialogController.DialogInProgress && _cc.HumanObj == UserController.PlayerCharacter.HumanObj)
                    return false;

                if (_cc.HumanObj != null && _cc.HumanObj.CurrentInterObject >= 0)
                {
                    return true;
                }

                return false;
            }

            if (DialogController.DialogInProgress)
                return false;

            if (sa == CCSingleActions.CounterBlock)
            {
                ActionJob actJob = null;
                foreach (var job in _jobs)
                {
                    if (job is AttackJob)
                    {
                        actJob = job;
                        break;
                    }
                }

                if (actJob != null)
                {
                    return true;
                }

                return false;
            }

            /*if (EndurenceCost(sa) > _currentEndurence)
            {
                return false;
            }*/


            if (sa == CCSingleActions.Damage || sa == CCSingleActions.Death)
                return true;

            if (InProcess)
                return false;

            if ((_cc.HumanObj == null || _currentEndurence <= 0 || Time.fixedTime - _prevDodgeTime < 1) &&
                (sa == CCSingleActions.DodgeBack ||
                 sa == CCSingleActions.DodgeForward ||
                 sa == CCSingleActions.DodgeLeft ||
                 sa == CCSingleActions.DodgeRight ||
                 sa == CCSingleActions.AttackToBlock))
                return false;

            if (sa == CCSingleActions.Eating && (_cc.SelectedObjID == -1
                                                 || TPCObjectsManager.Get(_cc.SelectedObjID) is FoodObject == false
                                                 || (TPCObjectsManager.Get(_cc.SelectedObjID) as TakebleObject)
                                                 .TakedBy != _cc.Params.UniqID))
            {
                return false;
            }

            if ((sa == CCSingleActions.StartUseObject
                 || sa == CCSingleActions.Drop)
                && _cc.SelectedObjID == -1)
                return false;

            if (sa == CCSingleActions.ToShooting && (
                    (_cc.HumanObj != null && _cc.HumanObj.GetCurrentEquipedID(HumanoidObject.EquipTypes.bow) < 0) ||
                    _cc.Params.CurrentFightState == HumanoidObject.FightingStates.shoot))
                return false;

            if (sa == CCSingleActions.ToMeleeFight &&
                _cc.Params.CurrentFightState == HumanoidObject.FightingStates.melee)
                return false;

            if (sa == CCSingleActions.ToNoFight &&
                _cc.Params.CurrentFightState == HumanoidObject.FightingStates.none)
                return false;

            ///проверка наличия стрел
            if (sa == CCSingleActions.SimpleAttack && _cc.Params.CurrentFightState == LifeObject.FightingStates.shoot)
            {
                WeaponObject weapon
                    = TPCObjectsManager.Get(_cc.HumanObj.GetCurrentEquipedID(HumanoidObject.EquipTypes.bow)) as
                        WeaponObject;

                if (weapon == null)
                    return false;

                if (weapon.WeaponType == WeaponObject.WeaponTypes.bows &&
                    _cc.Params.GetCollectebleCount(CollectebleObjects.Types.arrows) > 0)
                    return true;


                if (weapon.WeaponType == WeaponObject.WeaponTypes.crossBow &&
                    _cc.Params.GetCollectebleCount(CollectebleObjects.Types.bolts) > 0)
                    return true;

                return false;
            }

            if (_cc.CurSurfState == SurfaseStates.onGround)
                return true;

            return false;
        }

        private void DoDodge(Vector3 dir)
        {
            _prevDodgeTime = Time.fixedTime;
            _delayProcess = 0.4f;

            if (_cc.SelectedObject != null)
            {
                var curDist = Vector3.Distance(_cc.transform.position, _cc.SelectedObject.position);
                var nextPos = _cc.transform.position + dir * 4;

                var nextDist = Vector3.Distance(nextPos, _cc.SelectedObject.position);

                var nextVectorToTarget = (_cc.SelectedObject.position - _cc.transform.position).normalized;

                nextPos += nextVectorToTarget * (nextDist - curDist);
                dir = (nextPos - _cc.transform.position).normalized;
                _cc.transform.forward = nextVectorToTarget;
                _cc.LookVector = nextVectorToTarget;
            }

            _cc.Push(dir * 4, 0.4f);
        }

        public bool DoActions(CCSingleActions sa)
        {
            if (!IsAvalible(sa))
                return false;


            if (_currentEndurence > 0)
            {
                _currentEndurence -= EndurenceCost(sa);
                _attackDurationMultiplier = 1;
            }
            else
            {
                _attackDurationMultiplier = 2.0f;
            }
            

            foreach (var job in _jobs)
            {
                job.OnDoAction(sa);
            }

            OnActions?.Invoke(sa);

            if (sa != CCSingleActions.Damage
                && sa != CCSingleActions.Death)
                _delayProcess = 1.0f;

            switch (sa)
            {
                case CCSingleActions.DodgeForward:
                    DoDodge(_cc.transform.forward);
                    break;
                case CCSingleActions.DodgeBack:
                    DoDodge(-_cc.transform.forward);
                    break;
                case CCSingleActions.DodgeRight:
                    DoDodge(_cc.transform.right);
                    break;
                case CCSingleActions.DodgeLeft:
                    DoDodge(-_cc.transform.right);
                    break;

                case CCSingleActions.CounterBlock:

                    foreach (var job in _jobs.ToArray())
                        if (job is AttackJob)
                            _jobs.Remove(job);

                    _delayProcess = 0;

                    break;

                case CCSingleActions.BreackedAttack:

                    foreach (var job in _jobs.ToArray())
                        if (job is AttackJob)
                            _jobs.Remove(job);

                    _delayProcess = 1;
                    break;

                case CCSingleActions.SimpleAttack:
                    _delayProcess = AttacksDuration;
                    _jobs.Add(new AttackJob(Time.fixedTime + AttacksDuration / 2, _cc, _attackDurationMultiplier == 1));
                    break;
                
                case CCSingleActions.AttackToBlock:
                    _delayProcess = 0.6f;
                    _jobs.Add(new DoSomthing(Time.fixedTime + _delayProcess, _cc));
                    break;

                case CCSingleActions.StartUseObject:
                    _jobs.Add(new TakeJob(Time.fixedTime + 0.5f, _cc, _cc.SelectedObjID));
                    break;

                case CCSingleActions.StopUseObject:
                    _jobs.Add(new StopUseJob(Time.fixedTime, _cc));
                    break;

                case CCSingleActions.Eating:
                    _jobs.Add(new Eating(Time.fixedTime + 0.5f, _cc, _cc.SelectedObjID));
                    break;

                case CCSingleActions.ToNoFight:
                    _jobs.Add(new DoSomthing(Time.fixedTime + 0.5f, _cc, 
                        () => _cc.Params.CurrentFightState = HumanoidObject.FightingStates.none));
                    break;

                case CCSingleActions.ToMeleeFight:
                    _jobs.Add(new DoSomthing(Time.fixedTime + 0.5f, _cc,  
                        () => _cc.Params.CurrentFightState = HumanoidObject.FightingStates.melee));
                    break;

                case CCSingleActions.ToShooting:
                    _jobs.Add(new DoSomthing(Time.fixedTime + 0.5f, _cc,  
                        () => _cc.Params.CurrentFightState = HumanoidObject.FightingStates.shoot));
                    break;
            }

            _currentAction = sa;

            if (sa == CCSingleActions.Death)
                _jobs.Clear();


            return true;
        }

        public System.Action<CCSingleActions> OnActions;


        public void Update()
        {
            EndurenceGeneration(Time.fixedDeltaTime);
            _delayProcess -= Time.fixedDeltaTime;
            ActionsHandler();
        }


        private CCSingleActions _currentAction = CCSingleActions.none;
        public CCSingleActions CurrentAction => _delayProcess > 0 ? _currentAction : CCSingleActions.none;


        public float AttacksDuration
        {
            get { return _cc.Params.GetParametr(LifeObject.DependetParams.attackTime) * _attackDurationMultiplier; }
        }

        List<ActionJob> _jobs = new List<ActionJob>();

        void ActionsHandler()
        {
            foreach (var aj in _jobs.ToArray())
            {
                if (aj.TryExecute())
                    _jobs.Remove(aj);
            }
        }

        void EndurenceGeneration(float deltaTime)
        {
            const float EnduranceAccumSpeed = 0.1f;

            _currentEndurence = Mathf.Clamp(_currentEndurence, 0, MaxEndurence);

            if (_currentEndurence >= MaxEndurence)
            {
                _accumulatedEndurance = 0;
                return;
            }

            if (_accumulatedEndurance + _currentEndurence >= MaxEndurence)
            {
                _currentEndurence = MaxEndurence;
                _accumulatedEndurance = 0;
            }
            else
            {
                _accumulatedEndurance += deltaTime * EnduranceAccumSpeed;
            }
        }
    }
}