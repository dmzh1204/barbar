﻿using System;
using System.Collections;
using System.Collections.Generic;
using RPGDataBase;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DZTPC
{
    public class ActionJob
    {
        protected CharacterController _cc = null;

        protected float _time = 0;
        protected bool _isBrecked = false;

        public ActionJob(float time, CharacterController cc)
        {
            _time = time;
            _cc = cc;
        }

        public virtual bool TryExecute()
        {
            if (_time > Time.fixedTime)
                return false;

            return true;
        }

        public virtual void OnDoAction(CCSingleActions actionType)
        {
        }
    }

    public class AttackJob : ActionJob
    {
        private bool pushCompleted;

        public AttackJob(float time, CharacterController cc, bool canPush) : base(time, cc)
        {
            pushCompleted = !canPush;
        }


        public override bool TryExecute()
        {
            if (!pushCompleted && _time - Time.fixedTime <= 0.15f &&
                _cc.Params.CurrentFightState == HumanoidObject.FightingStates.melee)
            {
                pushCompleted = true;

                float distToObj = 2;

                if (_cc.SelectedObject != null)
                {
                    distToObj = Vector3.Distance(_cc.transform.position, _cc.SelectedObject.position);
                    if (distToObj > _cc.Params.GetParametr(LifeObject.DependetParams.attackDistCurrent))
                        _cc.Push(_cc.transform.forward * Mathf.Min(2, distToObj - 1), 0.1f);
                }

                if (_cc.transform.InverseTransformDirection(_cc.MoveVector).z > 0.5f)
                    _cc.Push(_cc.transform.forward * Mathf.Min(2, distToObj - 1), 0.1f);
            }

            if (base.TryExecute() == false)
                return false;

            if (_isBrecked)
                return true;

            if (_cc.Params.CurrentFightState == HumanoidObject.FightingStates.shoot)
            {
                WeaponObject weapon =
                    TPCObjectsManager.Get(_cc.HumanObj.GetCurrentEquipedID(HumanoidObject.EquipTypes.bow))
                        as WeaponObject;
                if (weapon == null)
                    return true;

                if (weapon.WeaponType == WeaponObject.WeaponTypes.bows)
                {
                    GlobalTPCEvents.OnShootBow?.Invoke(_cc.transform.position);
                    ArrowController.ShootArrow(_cc.Params, _cc.ShootPoint, _cc.LookVector);
                    _cc.Params.RemoveCollectebleCount(CollectebleObjects.Types.arrows, 1);
                }

                if (weapon.WeaponType == WeaponObject.WeaponTypes.crossBow)
                {
                    GlobalTPCEvents.OnShootCrossbow?.Invoke(_cc.transform.position);
                    ArrowController.ShootBolt(_cc.Params, _cc.ShootPoint, _cc.LookVector);
                    _cc.Params.RemoveCollectebleCount(CollectebleObjects.Types.bolts, 1);
                }

                return true;
            }

            //if (_cc.Params.CurrentFightState == HumanoidObject.FightingStates.melee)
            {
                LifeObject lifeToDamag = null;
                foreach (int id in TPCObjectsManager.GetClosest(_cc.Params.UniqID))
                {
                    LifeObject life = TPCObjectsManager.Get(id) as LifeObject;

                    if (life == null || life.IsDead())
                        continue;
                    if (Vector3.Distance(life.Position, _cc.Params.Position) <
                        _cc.Params.GetParametr(LifeObject.DependetParams.attackDistCurrent)
                        && Vector3.Angle(_cc.transform.forward.normalized,
                            (life.Position - _cc.Params.Position).normalized) <
                        RPGMath.MeleeAttackAngle
                    )
                    {
                        lifeToDamag = life;
                        break;
                    }
                }

                if (lifeToDamag != null)
                {
                    CharacterController _otherCC = CharacterController.GetController(lifeToDamag);

                    ///Если сработал блок у атакуемого
                    if (!_cc.Params.GodMode &&
                        Random.value < GetBaseBlockChance(_otherCC, lifeToDamag))
                    {
                        GlobalTPCEvents.OnAttackBlocked?.Invoke(_cc.transform.position +
                                                                (_otherCC.transform.position - _cc.transform.position) /
                                                                2
                                                                + Vector3.up);

                        ///Если сработает блок у атакующего то у атакуемого вместо атаки контр атака
                        if (Random.value < RPGMath.GetBlockChance(lifeToDamag, _cc.Params))
                            _otherCC.AvalibleActions.DoActions(CCSingleActions.CounterBlock);

                        _cc.AvalibleActions.DoActions(CCSingleActions.BreackedAttack);
                        return true;
                    }

                    if (_otherCC != null)
                    {
                        float pushDist = Mathf.Clamp(
                            _cc.Params.GetParametr(LifeObject.DependetParams.attackDistMeele) * 0.8f -
                            Vector3.Distance(_cc.transform.position, _otherCC.transform.position), 0, 5);

                        if (pushDist > 0)
                            _otherCC.Push((_otherCC.transform.position - _cc.transform.position).normalized *
                                          pushDist);
                    }

                    lifeToDamag.SetDamage(new DamagParameters(_cc.Params, lifeToDamag,
                        _cc.Params.GodMode
                            ? 100000
                            : _cc.Params.GetParametr(HumanoidObject.DependetParams.attackCurrent)));
                }
                else
                {
                    if (Random.value < GlobalSettings.Inst.GPSettings.DelayOnMissChance)
                    {
                        _cc.AvalibleActions.AddDelay(GlobalSettings.Inst.GPSettings.DelayOnMiss);
                        _cc.AvalibleActions.CurrentEndurence -= 0.2f;
                    }

                    if (_cc.HumanObj != null &&
                        _cc.HumanObj.HoldInHandMeleeWeapon())
                        GlobalTPCEvents.OnAttackMellee?.Invoke(_cc.transform.position);
                }
            }

            return true;
        }

        private float GetBaseBlockChance(CharacterController _otherCC, LifeObject lifeToDamag)
        {
            if (_otherCC == null ||
                Vector3.Angle(_cc.transform.forward, -_otherCC.transform.forward) > RPGMath.MeleeAttackAngle)
                return 0;

            if (_otherCC.AvalibleActions.IsAvalible(CCSingleActions.CounterBlock))
                return RPGMath.GetBlockChance(_cc.Params, lifeToDamag);
            
            if(_otherCC.AvalibleActions.CurrentAction == CCSingleActions.AttackToBlock)
                return RPGMath.GetBlockChance(_cc.Params, lifeToDamag) * 2;

            return 0;
        }

        public override void OnDoAction(CCSingleActions actionType)
        {
            base.OnDoAction(actionType);

            switch (actionType)
            {
                case CCSingleActions.Death:
                    _isBrecked = true;
                    break;

                case CCSingleActions.Damage:
                    if (_cc.Params.CurrentFightState == HumanoidObject.FightingStates.shoot)
                    {
                        _isBrecked = true;
                    }

                    break;
            }
        }
    }

    public class TakeJob : ActionJob
    {
        int _takeObjectID;

        public TakeJob(float time, CharacterController cc, int takeObjectID) : base(time, cc)
        {
            _takeObjectID = takeObjectID;
        }

        public override bool TryExecute()
        {
            if (base.TryExecute() == false)
                return false;

            if (_takeObjectID >= 0 && TPCObjectsManager.Get(_takeObjectID) != null)
                (TPCObjectsManager.Get(_takeObjectID) as InteractiveObject).StartUse(_cc.Params.UniqID);

            return true;
        }
    }

    public class StopUseJob : ActionJob
    {
        public StopUseJob(float time, CharacterController cc) : base(time, cc)
        {
        }

        public override bool TryExecute()
        {
            if (base.TryExecute() == false)
                return false;

            if (_cc.HumanObj.CurrentInterObject < 0)
                return true;

            (TPCObjectsManager.Get(_cc.HumanObj.CurrentInterObject) as InteractiveObject)
                .StopUse(_cc.HumanObj.UniqID);

            return true;
        }
    }

    public class DoSomthing : ActionJob
    {
        private Action action;

        public DoSomthing(float time, CharacterController cc, Action action = null) :
            base(time, cc)
        {
            this.action = action;
        }

        public override bool TryExecute()
        {
            if (base.TryExecute() == false)
                return false;
            action?.Invoke();
            return true;
        }
    }

    public class Eating : ActionJob
    {
        int _selected = -1;

        public Eating(float time, CharacterController cc, int idSelected) : base(time, cc)
        {
            _selected = idSelected;
            cc.SelectedObjID = -1;
        }

        public override bool TryExecute()
        {
            if (base.TryExecute() == false)
                return false;

            if (_selected < 0)
                return true;

            FoodObject food = (TPCObjectsManager.Get(_selected) as FoodObject);
            food.Eat();

            return true;
        }
    }
}