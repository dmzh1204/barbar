﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace DZTPC
{
	public class ArrowController : MonoBehaviour
	{
		public static void ShootBolt(LifeObject shooter, Vector3 startPoint, Vector3 direction)
		{
			ShootArrow(shooter, startPoint, direction);
		}
		public static void ShootArrow(LifeObject shooter, Vector3 startPoint, Vector3 direction)
		{
			GameObject arrow = new GameObject("Arrow");
			arrow.transform.position = startPoint;
			arrow.transform.forward = direction;
			arrow.AddComponent<ArrowController>().Init(shooter);
			
		}

		float _maxDist = 30;
		float _dist = 0;
		private TrailRenderer _trailRenderer;
		[SerializeField] private Material _matForTrail;
		private LifeObject _shooter;
		List<LifeObject> _posibleAttacked = new List<LifeObject>();
		public void Init(LifeObject shooter)
		{
			_shooter = shooter;
			_maxDist = shooter.GetParametr(LifeObject.DependetParams.attackDistCurrent);
			_trailRenderer = gameObject.AddComponent<TrailRenderer>();
			_trailRenderer.time = 0.07f;
			_trailRenderer.startWidth = 0.05f;

			_trailRenderer.material = GameDataBase.GetArrowTrailMaterial();
				
			_dist = 0;
			
			foreach (int id in TPCObjectsManager.GetClosest(_shooter.UniqID))
			{
				LifeObject life = TPCObjectsManager.Get(id) as LifeObject;
				if(life != null)
					_posibleAttacked.Add(life);
			}
		}

		private void FixedUpdate()
		{
			const float speed = 100;

			float deltaDist = speed * Time.fixedDeltaTime;

			bool collided = false;
			
			if (Physics.Raycast(transform.position, transform.forward, deltaDist, 1))
			{
				collided = true;
				_dist += _maxDist;
			}
			
			transform.position += transform.forward * deltaDist;
			
			_dist += deltaDist;

			

			foreach (var life in _posibleAttacked)
			{
				Vector3 centerPos = life.Position + Vector3.up * life.GetParametr(LifeObject.SpecifedParams.height);
				
				
				
				if(Vector3.Distance(centerPos, transform.position) > Mathf.Max(deltaDist, life.GetParametr(LifeObject.SpecifedParams.height))
				   || life.IsDead())
					continue;
				
				life.SetDamage(new DamagParameters(_shooter, life, _shooter.GetParametr(LifeObject.DependetParams.attackCurrent), 1));
				Destroy(gameObject);
				return;
			}
			

			if (_dist > _maxDist)
			{
				if (collided)
					GlobalTPCEvents.OnAttackBlocked(transform.position - (transform.forward * deltaDist));
				
				Destroy(gameObject);
			}
		}
	}
}