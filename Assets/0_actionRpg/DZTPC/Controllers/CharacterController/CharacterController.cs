﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class CharacterController : MonoBehaviour
    {
        public event Action OnFixedUpdated;
        
        LifeObject _params = null;
        //new HumanoidObject(0, Vector3.zero, new Quaternion(), 70);

        public LifeObject Params
        {
            get { return _params; }
            set
            {
                if(_params != null)
                    _allControllers.Remove(_params);
                _params = value;
                _humanObj = _params as HumanoidObject;
                Init();
            }
        }

        public Vector3 ShootPoint
        {
            get
            {
                return transform.position + Vector3.up * _params.GetParametr(LifeObject.SpecifedParams.height) * 0.7f;
            }
        }

        private HumanoidObject _humanObj = null;

        public HumanoidObject HumanObj
        {
            get { return _humanObj; }
        }


        Vector3 _moveVector = Vector3.zero;

        public Vector3 MoveVector
        {
            get { return _moveVector; }
            set
            {
                _moveVector = value;
                _moveVector = Vector3.ClampMagnitude(_moveVector, 1);
            }
        }

        Vector3 _lookVector = Vector3.forward;

        public Vector3 LookVector
        {
            get { return _lookVector; }
            set
            {
                if (_params.IsDead() == false)
                {
                    _lookVector = value;
                    _lookVector.Normalize();
                }
            }
        }

        int _selectedObjID = -1;


        Transform _selectedObject = null;

        public Transform SelectedObject
        {
            get { return _selectedObject; }
            set
            {
                _selectedObject = value;
                if (_selectedObject == null)
                {
                    _selectedObjID = -1;
                    return;
                }

                _selectedObjID = ObjectsHandlers.GetID(_selectedObject.gameObject);
            }
        }

        SurfaseStates _curSurfState = SurfaseStates.onGround;

        public SurfaseStates CurSurfState
        {
            get { return _curSurfState; }
        }

        Vector3 _surfaceNormal = Vector3.up;

        public Vector3 SurfaceNormal
        {
            get { return _surfaceNormal; }
            set { _surfaceNormal = value; }
        }

        public System.Action<SurfaseStates> OnCurrentSurfStateChanged;

        Rigidbody _rigidbody = null;

        public Rigidbody RigBody
        {
            get { return _rigidbody; }
        }

        Collider _collider = null;

        float _colliderToFootDist = 0.7f;

        public float ColliderToFootDist
        {
            get { return _colliderToFootDist; }
        }

        Vector3 _waterStream = Vector3.zero;

        public Vector3 WaterStream
        {
            get { return _waterStream; }
            set { _waterStream = value; }
        }


        CharcterUpdateHandler MoveUpdHandler
        {
            get { return _moveUpdHandler; }
            set
            {
                if (_moveUpdHandler != null)
                    _moveUpdHandler.OnDestroy();

                _moveUpdHandler = value;
            }
        }

        CharcterUpdateHandler _moveUpdHandler = null;

        CharcterUpdateHandler SurfChecker
        {
            get { return _surfChecker; }
            set
            {
                if (_surfChecker != null)
                    _surfChecker.OnDestroy();

                _surfChecker = value;
            }
        }

        CharcterUpdateHandler _surfChecker = new CharcterUpdateHandler();

        public System.Action OnClimbingUp = null;

        CharContrActions _avalibleActions = new CharContrActions();

        public CharContrActions AvalibleActions
        {
            get { return _avalibleActions; }
        }

        public int SelectedObjID
        {
            get { return _selectedObjID; }

            set
            {
                if(_selectedObjID == value)
                    return;
                
                _selectedObjID = value;
                GameObject go = ObjectsHandlers.Get(_selectedObjID);
                if (go != null)
                    _selectedObject = go.transform;
                else
                    _selectedObject = null;
            }
        }

        EquipBuilder _equipBuilder = null;
        BaseVoiceController _voiceController = null;

        static Dictionary<LifeObject, CharacterController> _allControllers = new Dictionary<LifeObject, CharacterController>();

        public static CharacterController GetController(LifeObject life)
        {
            if (_allControllers.ContainsKey(life))
            {
                return _allControllers[life];
            }
            else
            {
                return null;
            }
        }

        private Vector3 pushDir;
        private float pushVelocity;
        private float pushTimer = -1;
        
        public void Push(Vector3 v, float pushDuration = 0.2f)
        {
            pushVelocity = Mathf.Clamp(v.magnitude / pushDuration, 0, 20);
            pushTimer = pushDuration;
            pushDir = v.normalized;
        }
        
        void OnEnable()
        {
            if (_equipBuilder != null)
                _equipBuilder.Start();
            //Init();
            
            _voiceController?.Start();
            
        }

        void OnDisable()
        {
            if (_equipBuilder != null)
                _equipBuilder.Stop();


            _params.OnDamage -= OnDamage;
            _params.OnDeath -= OnDeath;
            
            _voiceController?.Stop();

            if(_params != null)
                _allControllers.Remove(_params);
        }

        void Init()
        {
            _allControllers.Add(_params, this);

            if (_avalibleActions == null)
                _avalibleActions = new CharContrActions(this);
            else
                _avalibleActions.ReInit(this);

            _lookVector = transform.forward;

            _rigidbody = GetComponent<Rigidbody>();

            if (_rigidbody != null)
                Destroy(_rigidbody);

            _rigidbody = gameObject.AddComponent<Rigidbody>();
            _rigidbody.freezeRotation = true;
            //_rigidbody.drag = 10;

            _collider = GetComponent<Collider>();

            gameObject.layer = 1;

            if (_collider != null)
                Destroy(_collider);

            _collider = gameObject.AddComponent<CapsuleCollider>();
            (_collider as CapsuleCollider).height = 1.3f;
            (_collider as CapsuleCollider).radius = 0.25f;
            (_collider as CapsuleCollider).center =
                Vector3.up * ((_collider as CapsuleCollider).height / 2 + _colliderToFootDist - 0.2f);

            _collider.material = GameDataBase.GetCharsPhysMaterial();
            
            if (_params.IsDead())
                _collider.enabled = false;

            SurfChecker = new SurfaceCheckerUpdHandler(this, OnSurfStateChanged);
            MoveUpdHandler = null;
            OnSurfStateChanged(_curSurfState);

            _params.OnDamage += OnDamage;
            _params.OnDeath += OnDeath;

            if (_humanObj != null)
            {
                _equipBuilder = new EquipBuilder(this);
                _equipBuilder.Start();
            }
            
            _voiceController = new BaseVoiceController(this);
            _voiceController.Start();
        }


        void OnSurfStateChanged(SurfaseStates targetSurfState)
        {
            if (_curSurfState == targetSurfState && MoveUpdHandler != null)
                return;

            if (_curSurfState == SurfaseStates.onConerClimb || _curSurfState == SurfaseStates.Interaction)
            {
                _collider.enabled = true;
            }


            switch (targetSurfState)
            {
                case SurfaseStates.inAir:
                    _rigidbody.useGravity = true;
                    MoveUpdHandler = new InAirUpdHandler(this);
                    break;

                case SurfaseStates.onGround:
                    _rigidbody.useGravity = false;
                    MoveUpdHandler = new MoveOnGroundUpdHandler(this);
                    if (_curSurfState == SurfaseStates.inAir)
                    {
                        MoveUpdHandler.DelayToWork = 0.5f;
                    }

                    break;
                
                case SurfaseStates.Interaction:
                    _rigidbody.useGravity = false;
                    _collider.enabled = false;
                    _rigidbody.velocity = Vector3.zero;
                    MoveUpdHandler = new InteractionUpdHandler(this);
                    break;

                case SurfaseStates.onWater:
                    _rigidbody.useGravity = false;
                    MoveUpdHandler = new SwimOnSurfaceUpdHandler(this);
                    break;

                case SurfaseStates.underWater:
                    _rigidbody.useGravity = false;
                    MoveUpdHandler = new SwimOnSurfaceUpdHandler(this);
                    break;

                case SurfaseStates.onConerClimb:
                    _collider.enabled = false;
                    _rigidbody.velocity = Vector3.zero;
                    _rigidbody.useGravity = false;
                    MoveUpdHandler = new ConerClimbingUpdHandler(this);
                    break;

                default:
                    _rigidbody.useGravity = false;
                    MoveUpdHandler = new CharcterUpdateHandler(this);
                    break;
            }

            _curSurfState = targetSurfState;

            if (OnCurrentSurfStateChanged != null)
                OnCurrentSurfStateChanged(_curSurfState);
        }

        void FixedUpdate()
        {
            //TPCObjectsManager.GetObjectsForTake(_params.UniqID);
            //Debug.Log(TPCObjectsManager.GetObjectsForTake(_params.UniqID).Length);

            if (_selectedObject != null && _params.CurrentFightState == HumanoidObject.FightingStates.melee &&
                _params.IsDead() == false)
            {
                Vector3 v = _selectedObject.position - transform.position;
                LookVector = v;

                if (v.sqrMagnitude > 100)
                    SelectedObject = null;
            }
          
            if (_params.IsDead())
            {
                _moveVector = Vector3.zero;
            }
            
            SurfChecker.Update();
            
            MoveUpdHandler.Update();

            if (InPushing)
            {
                pushTimer -= Time.fixedDeltaTime;
                _rigidbody.velocity = pushTimer < 0 ? Vector3.zero : pushDir * pushVelocity;
            }

            _params.Position = transform.position;
            _params.Rotation = transform.rotation;
            
            AvalibleActions.Update();
            OnFixedUpdated?.Invoke();
        }

        public bool InPushing => pushTimer >= 0;

        public float GetNormalizedHealth()
        {
            return _params.GetParametr(LifeObject.SpecifedParams.currentHealth) /
                   _params.GetParametr(LifeObject.DependetParams.maxHealth);
        }

        /// <summary>
        /// дамаг, позиция эффекта, направление эффекта
        /// </summary>
        public static event Action<DamagParameters, Vector3, Vector3> GlobalOnDamageEffect;
        void OnDamage(DamagParameters damag)
        {
            var damageVector = damag.Source == null || damag.Target == null
                ? Vector3.up
                : (damag.Source.Position - damag.Target.Position).normalized;
            GlobalOnDamageEffect?.Invoke(
                damag
                , transform.position + Vector3.up * _params.GetParametr(LifeObject.SpecifedParams.height) / 2
                , damageVector);
            MoveUpdHandler.DelayToWork = damag.StunTime;
            AvalibleActions.DoActions(CCSingleActions.Damage);
        }

        void OnDeath()
        {
            AvalibleActions.DoActions(CCSingleActions.Death);
            _collider.enabled = false;
            SelectedObjID = -1;
        }
    }
}