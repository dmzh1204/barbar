﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public class ClothControllerV2 : MonoBehaviour {
	
	[SerializeField] private Transform _prefubForDropped = null;
	private Animator _parentAnimator = null;
	private List<GameObject> _newObjects = new List<GameObject>();
	private List<GameObject> _hidedObjects = new List<GameObject>();

	// Use this for initialization
	void Start () {
		_parentAnimator = GetComponentInParent<Animator>();
		if (_parentAnimator != null && _parentAnimator.isHuman)
		{
			InitEquiped();
		}
		else
		{
			InitDroped();
		}
	}

	void InitEquiped()
	{
		SkinnedMeshRenderer targetSkm = null;
		List<SkinnedMeshRenderer> currentScms = new List<SkinnedMeshRenderer>();

		currentScms.AddRange(transform.GetComponentsInChildren<SkinnedMeshRenderer>());
		
		foreach (var skm in transform.root.GetComponentsInChildren<SkinnedMeshRenderer>())
		{
			if(currentScms.Contains(skm))
				continue;
			skm.gameObject.SetActive(false);
			_hidedObjects.Add(skm.gameObject);
			targetSkm = skm;
		}

		if(targetSkm != null)
		foreach (var current in currentScms)
		{
			RetargetSkinMesh(targetSkm, current);
		}
		
		gameObject.SetActive(false);
		
		_parentAnimator.Rebind();
		transform.root.GetComponentInChildren<CharAnimation>().OnRebindAnimator();
	}

	void RetargetSkinMesh(SkinnedMeshRenderer targetSkm, SkinnedMeshRenderer currentSkm)
	{
		currentSkm.transform.parent = targetSkm.transform.parent;
		_newObjects.Add(currentSkm.gameObject);
		currentSkm.bones = targetSkm.bones;
		currentSkm.rootBone = targetSkm.rootBone;
	}
	
	void InitDroped()
	{
		_prefubForDropped.parent = transform.parent;
		_prefubForDropped.gameObject.SetActive(true);
		Destroy(gameObject);
	}

	void OnDestroy()
	{
		foreach (var obj in _hidedObjects)
		{
			if(obj == null)
				continue;
			obj.SetActive(true);
		}
		
		foreach (var obj in _newObjects)
		{
			if (obj == null)
				continue;
			Destroy(obj);
		}
	}
}
