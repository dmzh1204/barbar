﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using baseLogic.utilites;

public class ClothController : MonoBehaviour
{

	[SerializeField] private Transform _prefubForDropped = null;

	private Animator _parentAnimator = null;

	[SerializeField]
	private bool _turnOffHair = false;
	
	private List<GameObject> _newObjects = new List<GameObject>();
	private List<GameObject> _hidedObjects = new List<GameObject>();
	
	// Use this for initialization
	void Start ()
	{
		_parentAnimator = GetComponentInParent<Animator>();
		if (_parentAnimator != null && _parentAnimator.isHuman)
		{
			InitEquiped();
		}
		else
		{
			InitDroped();
		}
	}

	void InitEquiped()
	{
		Destroy(_prefubForDropped.gameObject);
		
		if (_turnOffHair)
		{
			Transform hairTrans = TransformHelper.FindTransform(_parentAnimator.transform, "hair");

			if (hairTrans != null)
			{
				hairTrans.gameObject.SetActive(false);
				_hidedObjects.Add(hairTrans.gameObject);
			}
		}
		
		Transform[] rootBones = TransformHelper.FindTransform(_parentAnimator.transform, "body").GetComponent<SkinnedMeshRenderer>().bones;
		//Transform rootBone = FindTransform(_parentAnimator.transform, "body").GetComponent<SkinnedMeshRenderer>().rootBone;

		
		foreach (var meshRender in GetComponentsInChildren<MeshRenderer>())
		{
			Transform targetBone = TransformHelper.FindTransform(_parentAnimator.transform, meshRender.transform.parent.name);
			Vector3 targetLocPos = meshRender.transform.localPosition;
			Quaternion targetLocRot = meshRender.transform.localRotation;

			meshRender.transform.parent = targetBone;
			meshRender.transform.localPosition = targetLocPos;
			meshRender.transform.localRotation = targetLocRot;
			
			_newObjects.Add(meshRender.gameObject);
		}
		
		foreach (var skinMesh in GetComponentsInChildren<SkinnedMeshRenderer>())
		{
			skinMesh.transform.parent = _parentAnimator.transform;
			
			SkinMeshBonesRetargeting.RetargetBones(skinMesh, rootBones);
			
			/*skinMesh.bones = rootBones;
			
			skinMesh.rootBone = TransformHelper.FindTransform(_parentAnimator.transform, skinMesh.rootBone.name);*/
			
			_newObjects.Add(skinMesh.gameObject);
		}
		
		_newObjects.Add(gameObject);
		gameObject.SetActive(false);
	}

	void InitDroped()
	{
		_prefubForDropped.parent = transform.parent;
		_prefubForDropped.gameObject.SetActive(true);
		Destroy(gameObject);
	}
	
	

	void OnDestroy() {

		foreach (var obj in _newObjects)
		{
			if(obj == null)
				continue;
			Destroy(obj);
		}
		
		foreach (var obj in _hidedObjects)
		{
			if(obj == null)
				continue;
			obj.SetActive(true);
		}
	}
	
	
}
