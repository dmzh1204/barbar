﻿using UnityEngine;

namespace DZTPC
{
	public class MouthBlendShapeController : CharacterFace
	{
		public int MouthIndex;
		
		private SkinnedMeshRenderer _skm;

		protected override void SetVol(float value = 0)
		{
			if (_skm == null)
				_skm = GetComponent<SkinnedMeshRenderer>();
			
			if(_skm == null)
				return;
			
			_skm.SetBlendShapeWeight(MouthIndex, value * 100);
		}
	}
}