﻿using System;
using System.Collections;
using UnityEngine;

namespace DZTPC
{
	public abstract class CharacterFace : MonoBehaviour
	{
		void Start() => SetVol();

		protected virtual void SetVol(float value = 0) {}

		protected virtual void LateUpdate() {}

		public void StartTalk(float duration) => StartCoroutine(TalkCoroutine(duration));

		private IEnumerator TalkCoroutine(float duration)
		{
			var time = 0f;
			while (duration > 0)
			{
				SetVol((Mathf.Sin(time * 15) + 1) / 4);
				
				time += Time.deltaTime;
					
				duration -= Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}

			SetVol();
		}
	}
}