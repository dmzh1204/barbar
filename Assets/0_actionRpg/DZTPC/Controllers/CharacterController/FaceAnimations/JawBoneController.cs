﻿using UnityEngine;

namespace DZTPC
{

	public class JawBoneController : CharacterFace
	{

		public Vector3 closePosition;
		public Vector3 closeRotation;
		
		public Vector3 openPosition;		
		public Vector3 openRotation;


		private Vector3 _currentPos;
		private Vector3 _currentRot;


		protected override void SetVol(float value = 0)
		{
			value = Mathf.Clamp01(value);	
			_currentPos = Vector3.Lerp(closePosition, openPosition, value);
			_currentRot = Quaternion
				.Lerp(Quaternion.Euler(closeRotation), Quaternion.Euler(openRotation), value).eulerAngles;
		}

		protected override void LateUpdate()
		{
			transform.localPosition = _currentPos;
			transform.localEulerAngles = _currentRot;
		}

		[ContextMenu("ShowOpen")]
		private void ShowOpen()
		{
			transform.localPosition = openPosition;
			transform.localEulerAngles = openRotation;
		}
		[ContextMenu("ShowClose")]
		private void ShowClose()
		{
			transform.localPosition = closePosition;
			transform.localEulerAngles = closeRotation;
		}
		[ContextMenu("SetOpen")]
		private void SetOpen()
		{
			openPosition = transform.localPosition;
			openRotation = transform.localEulerAngles;
		}
		[ContextMenu("SetClose")]
		private void SetClose()
		{
			closePosition = transform.localPosition;
			closeRotation = transform.localEulerAngles;
		}
		
	}
}