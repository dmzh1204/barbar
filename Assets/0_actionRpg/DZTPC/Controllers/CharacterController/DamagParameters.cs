﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class DamagParameters
    {
        public LifeObject Source = null;
        public LifeObject Target = null;

        public float CutDamage = 0;
        
        public float StunTime;

        public bool isForward = true;
        
        public DamagParameters(LifeObject source, LifeObject target, float cutDamage, float stunTime = 0.5f, float critChanseMultiplier = 1)
        {
            Source = source;
            Target = target;
            StunTime = stunTime;
            CutDamage = cutDamage;
            if (GlobalSettings.Inst.GPSettings.CriticalAttackChance * critChanseMultiplier > Random.value)
                CutDamage *= GlobalSettings.Inst.GPSettings.CriticalAttackMultiplier;
        }

    }
}