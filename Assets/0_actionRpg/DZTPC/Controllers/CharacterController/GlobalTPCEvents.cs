﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{

	public class GlobalTPCEvents
	{

		public static System.Action<Vector3> OnAttackBlocked;
		
		public static System.Action<Vector3> OnShootBow;
		public static System.Action<Vector3> OnShootCrossbow;
		public static System.Action<Vector3> OnAttackMellee;

		public static System.Action<Vector3> OnLanding;
		public static System.Action<Vector3> OnWaterSplash;
		
		
		


	}
	
}