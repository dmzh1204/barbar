﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DZTPC
{
    public class InteractionUpdHandler : CharcterUpdateHandler
    {
        Vector3 _targetPos;
        Quaternion _targetRot;  
        Vector3 _prevPos;
        Quaternion _prevRot;
        
        public InteractionUpdHandler(CharacterController cc) : base(cc)
        {
            _prevPos = _cc.transform.position;
            _prevRot = _cc.transform.rotation;
            _targetPos = _cc.Params.Position;
            _targetRot = _cc.Params.Rotation;
            _lerpPosTime = 0;
        }

        private float _lerpPosTime = 0;

        private bool _exiting = false;
        
        public override void Update()
        {
            base.Update();

            if (_lerpPosTime < 1)
            {
                _lerpPosTime += Time.fixedDeltaTime * 4;
                _cc.transform.position = Vector3.Lerp(_prevPos, _targetPos, _lerpPosTime);
                _cc.transform.rotation = Quaternion.Lerp(_prevRot, _targetRot, _lerpPosTime);
            }
            
            if (_exiting == false && _cc.HumanObj.CurrentInterObject < 0)
            {
                _exiting = true;
                _prevPos = _cc.transform.position;
                _prevRot = _cc.transform.rotation;
                _targetPos = _cc.Params.Position;
                _targetRot = _cc.Params.Rotation;
                _lerpPosTime = 0;
                
            }
        }

        /*public override void OnDestroy()
        {
            base.OnDestroy();
            _cc.transform.position = _cc.Params.Position;
            _cc.transform.rotation = _cc.Params.Rotation;
        }*/
    }
}