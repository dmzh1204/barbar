﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class CharcterUpdateHandler {

        protected CharacterController _cc = null;

        public float DelayToWork = 0;

        public CharcterUpdateHandler(CharacterController cc = null)
        {
            _cc = cc;
        }

        public virtual void Update()
        {

        }

        public virtual void OnDestroy()
        {

        }
    }
}