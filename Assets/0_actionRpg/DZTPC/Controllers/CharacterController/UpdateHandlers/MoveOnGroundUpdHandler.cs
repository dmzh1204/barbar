﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DZTPC
{
public class MoveOnGroundUpdHandler : CharcterUpdateHandler
{
    private const float MaxVelChangePerSec = 20f;
        
        public MoveOnGroundUpdHandler(CharacterController cc) : base(cc)
        {

        }

        public override void Update()
        {
            base.Update();
            MoveOnGroundHandler();
        }

        void MoveOnGroundHandler()
        {
            if (_cc.AvalibleActions.CanRotate && !DialogController.DialogInProgress)
            {
                _cc.transform.forward = Vector3.RotateTowards(
                    _cc.transform.forward
                    , Vector3.ProjectOnPlane(_cc.LookVector, Vector3.up)
                    , Time.fixedDeltaTime * 5, Time.fixedDeltaTime * 5);
            }
            
            if(_cc.InPushing)
                return;
            
            Vector3 velocityDeltaToTarget = (_cc.MoveVector * _cc.Params.GetParametr(LifeObject.DependetParams.maxMoveSpeed)) - _cc.RigBody.velocity;

            if (DelayToWork > 0)
            {
                DelayToWork -= Time.fixedDeltaTime;
                velocityDeltaToTarget = -_cc.RigBody.velocity;
            }

            if (_cc.AvalibleActions.CanMove == false || DialogController.DialogInProgress)
            {
                velocityDeltaToTarget = -_cc.RigBody.velocity;
            }

            _cc.RigBody.AddForce(velocityDeltaToTarget, ForceMode.VelocityChange);
            
            if (_cc.SelectedObject && _cc.Params.CurrentFightState == LifeObject.FightingStates.melee)
            {
                Vector3 predictedPos = _cc.transform.position + _cc.RigBody.velocity * Time.fixedDeltaTime;

                const float minDist = 1.2f;
                
                if (Vector3.Distance(predictedPos, _cc.SelectedObject.position) < minDist)
                {

                    Vector3 targetPos = _cc.SelectedObject.position -
                                        (_cc.SelectedObject.position - _cc.transform.position).normalized * minDist;
                    
                    _cc.RigBody.velocity = Vector3.ClampMagnitude((targetPos - _cc.transform.position) / Time.fixedDeltaTime, 5);

                }
            }
        }
    }
}