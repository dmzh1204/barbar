﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class RootUpdHandler : CharcterUpdateHandler{

        CharcterUpdateHandler _surfaceChecker = new CharcterUpdateHandler();
        CharcterUpdateHandler _moveHandler = new CharcterUpdateHandler();

        public RootUpdHandler(CharacterController cc) : base(cc)
        {
            
        }

        public override void Update()
        {
            base.Update();
            _surfaceChecker.Update();
            _moveHandler.Update();

        }

        /*void GroundCheck()
        {
            if (_cc.CurSurfState != SurfaseStates.onGround && _cc.CurSurfState != SurfaseStates.inAir)
            {
                return;
            }

            RaycastHit rh;

            float bonusDist = 0.1f - Mathf.Clamp(_rigidbody.velocity.y * Time.fixedDeltaTime, -10, 0);

            if (Physics.Raycast(_cc.transform.position + Vector3.up * _colliderToFootDist, Vector3.down, out rh, _colliderToFootDist + bonusDist, 1)
                && rh.normal.y > 0.8f)
            {
                _rigidbody.useGravity = false;
                _cc.transform.position = Vector3.MoveTowards(_cc.transform.position, rh.point, Time.fixedDeltaTime * _rigidbody.velocity.sqrMagnitude);
                //transform.position = rh.point;
            }
            else
            {
                _cc.CurSurfState = SurfaseStates.inAir;
                _rigidbody.useGravity = true;
            }

            if (Physics.Raycast(_cc.transform.position + Vector3.up * _colliderToFootDist, Vector3.down, out rh, _colliderToFootDist + bonusDist, 16))
            {
                _cc.CurSurfState = SurfaseStates.onWater;
                _rigidbody.useGravity = false;
                _cc.transform.position = rh.point;
            }


        }*/
    }
}