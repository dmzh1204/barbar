﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DZTPC
{
    public class SwimOnSurfaceUpdHandler : CharcterUpdateHandler
    {
        public SwimOnSurfaceUpdHandler(CharacterController cc) : base(cc)
        {

        }

        public override void Update()
        {
            base.Update();
            SwimOnSurfaceHandler();
        }

        void SwimOnSurfaceHandler()
        {
            Vector3 move = _cc.MoveVector;

            if (_cc.CurSurfState == SurfaseStates.onWater && move.y > 0)
            {
                move.y = 0;
            }

            if (Physics.Raycast(_cc.transform.position + Vector3.up, Vector3.down, 1.5f, 1) && move.y < 0)
                move.y = 0;
                
            Vector3 velocityDeltaToTarget = (move * _cc.Params.GetParametr(LifeObject.DependetParams.maxMoveSpeed) / 2) - _cc.RigBody.velocity + _cc.WaterStream;

            _cc.RigBody.AddForce(velocityDeltaToTarget, ForceMode.VelocityChange);

            
            _cc.transform.forward = Vector3.RotateTowards(
                _cc.transform.forward
                , Vector3.ProjectOnPlane(_cc.LookVector, Vector3.up)
                , Time.fixedDeltaTime * 5, Time.fixedDeltaTime * 5);
        }
    }
}