﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DZTPC
{
    public class SurfaceCheckerUpdHandler : CharcterUpdateHandler
    {
        float _waterSurfaceDist = 0;

        private System.Action<SurfaseStates> _callForSurfStateChange;

        private float _charHeight;
        
        public SurfaceCheckerUpdHandler(CharacterController cc, System.Action<SurfaseStates> callForSurfStateChange) : base(cc)
        {
            _callForSurfStateChange = callForSurfStateChange;
            //_characterHeight = cc.GetComponent<CapsuleCollider>().center.y + cc.GetComponent<CapsuleCollider>().height / 2;
            _cc.OnClimbingUp += OnClimbingUp;
            _charHeight = _cc.Params.GetParametr(LifeObject.SpecifedParams.height);
        }

        public override void Update()
        {
            base.Update();
            GroundCheck();
            WaterCheck();
            CheckClimb();
            InteractionCheck();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            _cc.OnClimbingUp -= OnClimbingUp;
        }

        private float _delayExitInteractionState = 0.5f;
        
        void InteractionCheck()
        {
            if(_cc.HumanObj == null)
                return;
            
            if (_cc.CurSurfState == SurfaseStates.Interaction)
            {

                
                if (_cc.HumanObj.CurrentInterObject < 0
                    || TPCObjectsManager.Get(_cc.HumanObj.CurrentInterObject) is BedRepairIntrObj == false)
                {
                    if (_delayExitInteractionState < 0)
                    {
                        _delayExitInteractionState = 0.5f;
                    }
                    
                    _delayExitInteractionState -= Time.fixedDeltaTime;

                    if(_delayExitInteractionState <= 0)
                        _callForSurfStateChange?.Invoke(SurfaseStates.onGround);
                }
                else
                {
                    _delayExitInteractionState = -1;

                }
            }
            else
            {
                if (_cc.HumanObj.CurrentInterObject > 0 &&
                    TPCObjectsManager.Get(_cc.HumanObj.CurrentInterObject) is BedRepairIntrObj)
                {
                    _callForSurfStateChange?.Invoke(SurfaseStates.Interaction);
                }

                _delayExitInteractionState = -1;
            }
        }

        void GroundCheck()
        {
            if (_cc.CurSurfState != SurfaseStates.onGround && _cc.CurSurfState != SurfaseStates.inAir)
            {
                return;
            }

            RaycastHit rh;

            float bonusDist = 0.1f - Mathf.Clamp(_cc.RigBody.velocity.y * Time.fixedDeltaTime, -5, 5);

            float height = _cc.Params.GetParametr(LifeObject.SpecifedParams.height);

            if (Physics.Raycast(_cc.transform.position + Vector3.up * height, Vector3.down, out rh, height / 2 + bonusDist, 16))
            {
                //if (rh.distance < _characterHeight / 2)
                {
                    if (_cc.CurSurfState == SurfaseStates.inAir)
                    {
                        GlobalTPCEvents.OnWaterSplash(rh.point);
                    }
                    if (_callForSurfStateChange != null)
                        _callForSurfStateChange(SurfaseStates.onWater);
                    
                    _waterSurfaceDist = 0;
                    _cc.transform.position = rh.point - Vector3.up * height / 2;
                    return;
                }
            }

            Debug.DrawLine(_cc.transform.position + Vector3.up * _cc.ColliderToFootDist
                , _cc.transform.position + Vector3.up * _cc.ColliderToFootDist + Vector3.down * (_cc.ColliderToFootDist + bonusDist));
            
            if (Physics.Raycast(_cc.transform.position + Vector3.up * _cc.ColliderToFootDist, Vector3.down, out rh, _cc.ColliderToFootDist + bonusDist, 1)
                && rh.normal.y > 0.5736f)
            {
                _cc.SurfaceNormal = rh.normal;
                
                if (_cc.MoveVector.y > 0)
                {
                    Vector3 jumpVector = _cc.MoveVector * 
                                         (_cc.Params.UniqID == GlobalSettings.PlayerUniqID && 
                                         GlobalSettings.PlayerModifications.ParkurMode ? 10 : 6);
                    //jumpVector.y /= 2;
                    _cc.RigBody.AddForce(jumpVector, ForceMode.VelocityChange);
                    if (_callForSurfStateChange != null)
                        _callForSurfStateChange(SurfaseStates.inAir);
                    return;
                }
                
                _cc.transform.position = Vector3.MoveTowards(_cc.transform.position, rh.point, Time.fixedDeltaTime * _cc.RigBody.velocity.sqrMagnitude);

                if (_cc.CurSurfState == SurfaseStates.inAir)
                {

                    float damagByLanding = 0;

                    const float damagLimitSpeed = 8;
                    
                    if (_cc.RigBody.velocity.y < -damagLimitSpeed)
                    {
                        damagByLanding = Mathf.Pow((_cc.RigBody.velocity.y + damagLimitSpeed), 3);
                        damagByLanding = Mathf.Abs(damagByLanding);
                    }

                    if (damagByLanding > 0 && 
                        (_cc.Params.UniqID != GlobalSettings.PlayerUniqID || !GlobalSettings.PlayerModifications.ParkurMode))
                    {
                        _cc.Params.SetDamage(new DamagParameters(null, _cc.Params, 
                            _cc.Params.UniqID == GlobalSettings.PlayerUniqID ? damagByLanding : 0, 0));
                    }
                    
                    GlobalTPCEvents.OnLanding(rh.point);
                }
                
                if (_callForSurfStateChange != null)
                    _callForSurfStateChange(SurfaseStates.onGround);
            }
            else
            {
                if (_callForSurfStateChange != null)
                    _callForSurfStateChange(SurfaseStates.inAir);
            }




        }


        void WaterCheck()
        {
            if (_cc.CurSurfState != SurfaseStates.onWater && _cc.CurSurfState != SurfaseStates.underWater)
            {
                return;
            }

            RaycastHit rh;

            if (_cc.CurSurfState == SurfaseStates.onWater && Physics.Raycast(_cc.transform.position + Vector3.up * _cc.ColliderToFootDist, Vector3.down, out rh, _cc.ColliderToFootDist, 1))
            {
                _cc.transform.position = rh.point;
                
                if (_callForSurfStateChange != null)
                    _callForSurfStateChange(SurfaseStates.onGround);
                return;
            }


            if (Physics.Raycast(_cc.transform.position + Vector3.up * (_charHeight + _waterSurfaceDist)
                , Vector3.down, out rh, _charHeight + _waterSurfaceDist, 16))
            {
                Vector3 surfNormal = rh.normal;
                surfNormal.y = 0;

                _cc.WaterStream = Vector3.ClampMagnitude(surfNormal * 50, 5);

                if (_cc.CurSurfState == SurfaseStates.onWater)
                {
                    if (_cc.MoveVector.y >= 0)
                    {
                        _cc.transform.position = rh.point - Vector3.up * _charHeight / 2;
                    }
                    else
                    {
                        if (_callForSurfStateChange != null)
                            _callForSurfStateChange(SurfaseStates.underWater);

                        _waterSurfaceDist = 10;
                    }
                }

                if (_cc.CurSurfState == SurfaseStates.underWater)
                {
                    if (Vector3.Distance(rh.point, _cc.transform.position) < _charHeight / 2)
                    {
                        if (_callForSurfStateChange != null)
                            _callForSurfStateChange(SurfaseStates.onWater);
                        _waterSurfaceDist = 0;
                        return;
                    }
                    
                    _waterSurfaceDist = Vector3.Distance(rh.point, _cc.transform.position) + 10;

                }
            }
            else
            {
                if (_callForSurfStateChange != null)
                    _callForSurfStateChange(SurfaseStates.inAir);

                return;
            }



        }

        float _climbDalay = 0;
        void CheckClimb()
        {
            if (_climbDalay > 0)
            {
                _climbDalay -= Time.fixedDeltaTime;
                return;
            }
            
            if (_cc.CurSurfState != SurfaseStates.onConerClimb && _cc.CurSurfState != SurfaseStates.onVStair 
                                                               && _cc.CurSurfState != SurfaseStates.inAir)
            {
                return;
            }

            if(_cc.Params.UniqID != GlobalSettings.PlayerUniqID)
                return;

            const float handLangth = 0.5f;

            RaycastHit rh;

            Vector3 upPointHead = _cc.transform.position + Vector3.up * _charHeight;
            Vector3 upPointHand = _cc.transform.position + Vector3.up * (_charHeight + handLangth * 2);

            if (_cc.CurSurfState == SurfaseStates.onConerClimb || _cc.CurSurfState == SurfaseStates.onVStair)
            {
                if (Physics.Raycast(upPointHead, -_cc.transform.up, _charHeight, 1))
                {
                    ExitFromClimb();
                    return;
                }
            }

            if (Physics.Raycast(upPointHand, _cc.transform.forward, 1, 1))
            {
                ExitFromClimb();
                return;
            }


            if (Physics.Raycast(upPointHead, _cc.transform.forward, out rh, 1, 1) == false)
            {
                ExitFromClimb();
                return;
            }



            Vector3 normal = rh.normal;
            normal.y = 0;
            normal = normal.normalized;

            Vector3 cornerPoint = rh.point;

            if (Physics.Raycast(upPointHand + _cc.transform.forward, Vector3.down, out rh, 1, 1) == false || rh.normal.y < 0.8f)
            {
                ExitFromClimb();
                return;
            }

            /*if (Physics.Raycast(upPointHand, Vector3.down, 1, 1))
            {
                ExitFromClimb();
                return;
            }*/

            cornerPoint.y = rh.point.y;




            var pointForClimb = cornerPoint - Vector3.up * (_charHeight + handLangth) - _cc.transform.forward * 0.2f;

            var vectorToPointForClimb = pointForClimb - _cc.transform.position;

            if (Physics.Raycast(_cc.transform.position, vectorToPointForClimb, vectorToPointForClimb.magnitude, 1))
            {
                ExitFromClimb();
                return;
            }

            _cc.transform.position = pointForClimb;
            
            _cc.transform.forward = -normal;


            if (_cc.CurSurfState != SurfaseStates.onConerClimb && _callForSurfStateChange != null)
                _callForSurfStateChange(SurfaseStates.onConerClimb);

            if (_cc.MoveVector.y < 0)
            {
                _climbDalay = 1;
                _callForSurfStateChange(SurfaseStates.inAir);
            }
        }

        void ExitFromClimb()
        {
            _climbDalay = 0.5f;
            if (_callForSurfStateChange == null)
                return;
            
            if (_cc.CurSurfState == SurfaseStates.onConerClimb || _cc.CurSurfState == SurfaseStates.onVStair)
            {
                _cc.RigBody.velocity = Vector3.zero;
                _callForSurfStateChange(SurfaseStates.inAir);
                
                RaycastHit rh;
                if (Physics.Raycast(_cc.transform.position, _cc.transform.forward, 1, 1) == false 
                    && Physics.Raycast(_cc.transform.position + Vector3.up * 0.5f, Vector3.down, 1, 1) == false 
                    && Physics.Raycast(_cc.transform.position - _cc.transform.forward, _cc.transform.forward, out rh, 2, 1))
                {
                    _cc.transform.position = rh.point - _cc.transform.forward;
                }   
                return;
            }

            if (_cc.CurSurfState != SurfaseStates.inAir)
            {
                _callForSurfStateChange(SurfaseStates.inAir);
            }

        }

        void OnClimbingUp()
        {
            _climbDalay = 1;
        }

   
    }
}