﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DZTPC
{
    public class ConerClimbingUpdHandler : CharcterUpdateHandler
    {

        System.Action updater;
        float _delayForUp = 1f;
        public ConerClimbingUpdHandler(CharacterController cc) : base(cc)
        {
            updater = Move;
        }

        public override void Update()
        {
            base.Update();
            updater();
        }

        void Move()
        {
            if (_delayForUp > 0)
                _delayForUp -= Time.fixedDeltaTime;
            
            if (_cc.MoveVector.y > 0 && _delayForUp <= 0)
            {
                if(_cc.OnClimbingUp != null)
                    _cc.OnClimbingUp();
                
                _climbUpEndPoint = _cc.transform.position + Vector3.up * (_cc.Params.GetParametr(LifeObject.SpecifedParams.height) + 0.5f) + _cc.transform.forward;
                updater = ClimbUp;
                return;
            }

            var velocity = _cc.MoveVector * _cc.Params.GetParametr(LifeObject.DependetParams.maxMoveSpeed) / 4;
            
            if (Physics.Raycast(_cc.transform.position, velocity
                , Mathf.Max(0.3f, velocity.magnitude), 1))
                velocity = Vector3.zero;
            
            _cc.RigBody.velocity = velocity;
        }

        Vector3 _climbUpEndPoint;

        void ClimbUp()
        {
            if (Mathf.Abs(_cc.transform.position.y - _climbUpEndPoint.y) > 0.1f)
            {
                _cc.transform.position = Vector3.MoveTowards(_cc.transform.position, new Vector3(_cc.transform.position.x, _climbUpEndPoint.y, _cc.transform.position.z), 5 * Time.fixedDeltaTime);

                return;
            }

            if (Vector3.Distance(_cc.transform.position, _climbUpEndPoint) > 0.1f)
            {
                _cc.transform.position = Vector3.MoveTowards(_cc.transform.position, _climbUpEndPoint, 3 * Time.fixedDeltaTime);
                return;
            }

            updater = Move;
        }
    }
}