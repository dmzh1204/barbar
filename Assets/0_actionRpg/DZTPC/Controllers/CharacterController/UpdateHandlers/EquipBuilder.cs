﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class EquipBuilder
    {
        CharacterController _cc = null;

        Dictionary<HumanoidObject.EquipTypes, Transform>
            _ammos = new Dictionary<HumanoidObject.EquipTypes, Transform>();

        Dictionary<int, HumanoidObject.EquipTypes> _idsOnEquipType = new Dictionary<int, HumanoidObject.EquipTypes>();
        PivotPoints _pivots;

        public EquipBuilder(CharacterController cc)
        {
            _cc = cc;
            _pivots = _cc.GetComponentInChildren<PivotPoints>();
        }

        public void Start()
        {
            _cc.HumanObj.OnEquipChanged += OnEquipChanged;
            _cc.Params.OnFightingStateChanged += OnFightingModeChanged;
            _cc.AvalibleActions.OnActions += OnDoAction;

            /*_cc.Params.OnTake += OnTake;

            foreach (int id in _cc.Params.Inventory)
            {
                OnTake(id);
            }*/

            foreach (HumanoidObject.EquipTypes type in _cc.HumanObj.GetEquipedTypes())
            {
                OnEquipChanged(type, _cc.HumanObj.GetCurrentEquipedID(type));
            }
        }

        public void Stop()
        {
            _cc.AvalibleActions.OnActions -= OnDoAction;

            _cc.HumanObj.OnEquipChanged -= OnEquipChanged;
            _cc.Params.OnFightingStateChanged -= OnFightingModeChanged;
            //_cc.Params.OnTake -= OnTake;
        }

        /*void OnTake(int id)
        {
            EquipebleObject eqObj = TPCObjectsManager.Get(id) as EquipebleObject;
            
            if(eqObj == null)
                return;
                        
            if(_cc.HumanObj.GetCurrentEquipedID(eqObj.EuipType) < 0)
                _cc.HumanObj.Equip(eqObj);
        }*/
        
        void OnDoAction(CCSingleActions action)
        {
            if (action == CCSingleActions.Eating)
            {
                GameObject eatedGoModel;
                eatedGoModel = TPCObjectsManager.Get(_cc.SelectedObjID).Instantiate();
                _pivots.PlacingGOInPivot(eatedGoModel.transform, PivotPoints.Types.rightHand);
                GameObject.Destroy(eatedGoModel, 1);
            }
        }

        void OnFightingModeChanged(HumanoidObject.FightingStates state)
        {
            foreach (HumanoidObject.EquipTypes type in _cc.HumanObj.GetEquipedTypes())
            {
                if (type == HumanoidObject.EquipTypes.armor)
                    continue;

                OnEquipChanged(type, _cc.HumanObj.GetCurrentEquipedID(type));
            }
        }

        void OnEquipChanged(HumanoidObject.EquipTypes type, int id)
        {
            RemovePrevius(id);
            BuildNew(type, id);
        }

        void RemovePrevius(int id)
        {
            if (_idsOnEquipType.ContainsKey(id))
            {
                GameObject.Destroy(_ammos[_idsOnEquipType[id]].gameObject);
                _ammos.Remove(_idsOnEquipType[id]);
                _idsOnEquipType.Remove(id);
            }
        }

        void BuildNew(HumanoidObject.EquipTypes type, int id)
        {
            if (id < 0 || type == HumanoidObject.EquipTypes.none)
                return;

            GameObject go = InstGO(id);
            
            if (go != null)
            {
                PivotPoints.Types _targetPivot = GetTargetPivot(id);
                _pivots.PlacingGOInPivot(go.transform, _targetPivot);
                _ammos.Add(type, go.transform);
                _idsOnEquipType.Add(id, type);
            }
        }


        PivotPoints.Types GetTargetPivot(int id)
        {
            BaseTPCGameObject btgo = TPCObjectsManager.Get(id);
            EquipebleObject equipebeObj = btgo as EquipebleObject;
            if (equipebeObj == null)
                return PivotPoints.Types.none;


            switch (equipebeObj.EuipType)
            {
                case HumanoidObject.EquipTypes.bow:
                    if (_cc.Params.CurrentFightState == HumanoidObject.FightingStates.shoot)
                    {
                        if (_cc.HumanObj.GetCurrentEquiped(HumanoidObject.EquipTypes.bow).WeaponType == WeaponObject.WeaponTypes.bows)
                        {
                            return PivotPoints.Types.leftHand;
                        }
                        else
                        {
                            return PivotPoints.Types.rightHand;
                        }
                    }
                    else
                        return PivotPoints.Types.leftSpine;
                

                case HumanoidObject.EquipTypes.shield:
                    if (_cc.Params.CurrentFightState == HumanoidObject.FightingStates.melee
                        && _cc.HumanObj.GetCurrentEquipedID(HumanoidObject.EquipTypes.meleeOneHand) >= 0)
                        return PivotPoints.Types.leftHand;
                    else
                        return PivotPoints.Types.leftSpine;

                case HumanoidObject.EquipTypes.armor:
                    return PivotPoints.Types.body;

                case HumanoidObject.EquipTypes.meleeOneHand:
                    if (_cc.Params.CurrentFightState == HumanoidObject.FightingStates.melee)
                        return PivotPoints.Types.rightHand;
                    else
                        return PivotPoints.Types.leftBelt;

                case HumanoidObject.EquipTypes.meleeTwoHand:
                    if (_cc.Params.CurrentFightState == HumanoidObject.FightingStates.melee)
                        return PivotPoints.Types.rightHand;
                    else
                        return PivotPoints.Types.rightSpine;
            }


            return PivotPoints.Types.none;
        }

        GameObject InstGO(int id)
        {
            return TPCObjectsManager.Get(id).Instantiate();
        }
    }
}