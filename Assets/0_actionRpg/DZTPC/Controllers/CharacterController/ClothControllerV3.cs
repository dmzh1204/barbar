﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public class ClothControllerV3 : MonoBehaviour
{

	[SerializeField] private int clothIndex = 0;
	[SerializeField] private Transform _prefubForDropped = null;
	private Animator _parentAnimator = null;

	// Use this for initialization
	void Start () {
		_parentAnimator = GetComponentInParent<Animator>();
		if (_parentAnimator != null && _parentAnimator.isHuman)
		{
			InitEquiped();
		}
		else
		{
			InitDroped();
		}
	}

	void InitEquiped()
	{
		GetComponentInParent<ClothPreloader>()?.DressCloth(clothIndex);
	}

	void InitDroped()
	{
		_prefubForDropped.parent = transform.parent;
		_prefubForDropped.gameObject.SetActive(true);
		Destroy(gameObject);
	}

	void OnDestroy()
	{
		GetComponentInParent<ClothPreloader>()?.UndressCloth(clothIndex);
	}
}
