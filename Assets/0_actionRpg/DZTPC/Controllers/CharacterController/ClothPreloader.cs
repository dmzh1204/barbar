﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DZTPC
{

	public class ClothPreloader : MonoBehaviour
	{

		private Animator _antr;
		private SkinnedMeshRenderer _baseModel;
		private SkinnedMeshRenderer[] _clothModels;
		
		[SerializeField]
		private SkinnedMeshRenderer[] _clothPrefubs;

		
		
		void Awake()
		{
			_antr = GetComponent<Animator>();
			_baseModel = GetComponentInChildren<SkinnedMeshRenderer>();
			
			_clothModels = new SkinnedMeshRenderer[_clothPrefubs.Length];

			for (int i = 0; i < _clothModels.Length; i++)
			{
				_clothModels[i] = Instantiate(_clothPrefubs[i], _baseModel.transform.parent);
				_clothModels[i].bones = _baseModel.bones;
				_clothModels[i].rootBone = _baseModel.rootBone;
				_clothModels[i].gameObject.SetActive(false);
			}
			_antr.Rebind();
		}

		public void DressCloth(int index)
		{
			_baseModel.gameObject.SetActive(false);

			if (_clothModels != null && index < _clothModels.Length)
			{
				_clothModels[index].gameObject.SetActive(true);
			}
		}

		public void UndressCloth(int index = -1)
		{

			if (index < 0)
			{
				foreach (var cloth in _clothModels)
				{
					cloth.gameObject.SetActive(false);
				}
			}
			else
			{
				_clothModels[index].gameObject.SetActive(false);
			}

			bool noCloth = true;
			
			foreach (var cloth in _clothModels)
			{
				if (cloth.gameObject.activeSelf)
				{
					noCloth = false;
					break;
				}
			}
			
			if(noCloth)
				_baseModel.gameObject.SetActive(true);

		}
	}
	
}