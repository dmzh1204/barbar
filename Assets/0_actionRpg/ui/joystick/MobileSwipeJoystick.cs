﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityStandardAssets.CrossPlatformInput;

public class MobileSwipeJoystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public enum AxisOption
    {
        Both,
        OnlyHorizontal,
        OnlyVertical
    }

    public AxisOption AxesToUse = AxisOption.Both;

    public string HorizontalAxisName = "Horizontal";
    public string VerticalAxisName = "Vertical";

    private bool _useX;
    private bool _useY;

    private CrossPlatformInputManager.VirtualAxis _horizontalVirtualAxis = null;
    private CrossPlatformInputManager.VirtualAxis _verticalVirtualAxis = null;

    
    //private float screeMultiplier = 1f / 1280f;

    private void OnEnable()
    {
        CreateVirtualAxes();
    }

    private void Start()
    {
        _rectTransform = (RectTransform) transform;
    }

    public Vector2 Delta = Vector2.zero;

    private void UpdateVirtualAxes(Vector2 value)
    {
        Delta += value;

        OnDragJoystick.Invoke(value);
        
    }

    [Serializable]
    public class EventDataJoystick : UnityEvent<Vector2>
    {
    }

    public EventDataJoystick OnDragJoystick;

    private void CreateVirtualAxes()
    {
        /*if (CrossPlatformInputManager.AxisExists(HorizontalAxisName))
            CrossPlatformInputManager.UnRegisterVirtualAxis(HorizontalAxisName);
        if (CrossPlatformInputManager.AxisExists(VerticalAxisName))
            CrossPlatformInputManager.UnRegisterVirtualAxis(VerticalAxisName);*/

        // set axes to use
        _useX = (AxesToUse == AxisOption.Both || AxesToUse == AxisOption.OnlyHorizontal);
        _useY = (AxesToUse == AxisOption.Both || AxesToUse == AxisOption.OnlyVertical);

        // create new axes based on axes to use
        if (_useX)
        {
            if (CrossPlatformInputManager.AxisExists(HorizontalAxisName))
                _horizontalVirtualAxis = CrossPlatformInputManager.VirtualAxisReference(HorizontalAxisName);
            else
            {
                _horizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(HorizontalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(_horizontalVirtualAxis);
            }
        }

        if (_useY)
        {
            if (CrossPlatformInputManager.AxisExists(VerticalAxisName))
                _verticalVirtualAxis = CrossPlatformInputManager.VirtualAxisReference(VerticalAxisName);
            else
            {
                _verticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(VerticalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(_verticalVirtualAxis);
            }
        }
    }


    private RectTransform _rectTransform;
    private Vector2 _startPos;

    public void OnDrag(PointerEventData data)
    {
        
        Vector2 delta = data.delta;

        delta.x /= (float)Screen.width;
        delta.y /= (float)Screen.width;
                
        //Debug.Log("drag=" + data.delta + " recalc=" + delta + " dr=" + data.dragging);
        
        UpdateVirtualAxes(delta);
    }

    public void OnPointerUp(PointerEventData data)
    {
        UpdateVirtualAxes(Vector3.zero);
    }


    public void OnPointerDown(PointerEventData data)
    {
        _startPos = data.pressPosition;
    }

    void Update()
    {
        if (_useX)
            _horizontalVirtualAxis.Update(Delta.x);

        if (_useY)
            _verticalVirtualAxis.Update(Delta.y);
        
        Delta = Vector2.zero;
    }

    private void OnDisable()
    {
        // remove the joysticks from the cross platform input
        /*if (_useX)
        {
            _horizontalVirtualAxis.Remove();
        }

        if (_useY)
        {
            _verticalVirtualAxis.Remove();
        }*/
    }
}