﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class MobileJoystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public enum AxisOption
    {
        Both,
        OnlyHorizontal,
        OnlyVertical
    }

    public Animator JoyAnimator;
    public RectTransform Stick;

    public AxisOption AxesToUse = AxisOption.Both;
    [Range(0, 2f)] public float MovementRangeMultiplier = 1f;
    
    public string HorizontalAxisName = "Horizontal";
    public string VerticalAxisName = "Vertical";

    private bool _useX;
    private bool _useY;
    
    private RectTransform _rectTransform;

    private CrossPlatformInputManager.VirtualAxis _horizontalVirtualAxis;
    private CrossPlatformInputManager.VirtualAxis _verticalVirtualAxis;
    
    private CanvasScaler CanvasScaler;

    private void OnEnable()
    {
        CanvasScaler = GetComponentInParent<CanvasScaler>();
        CreateVirtualAxes();
        
        Stick.localPosition = Vector3.zero;
    }

    private void Start()
    {
        _rectTransform = (RectTransform) transform;
        
        JoyAnimator.SetBool("IsActiveJoy", false);
    }

    public Vector2 CurrentJoystickPosition { get; private set; }
    public Vector2 CurrentJoystickPositionUnclamped { get; private set; }

    Vector2 _prevAxasValues = Vector3.zero;

    private void UpdateVirtualAxes(Vector2 value)
    {
        if (value == _prevAxasValues)
            return;
        
        _prevAxasValues = value;

        if (_useX)
            _horizontalVirtualAxis.Update(value.x);

        if (_useY)
            _verticalVirtualAxis.Update(value.y);

        onDragJoystick.Invoke(value);
    }

    [Serializable]
    public class EventDataJoystick : UnityEvent<Vector2>
    {
    }

    public EventDataJoystick onDragJoystick;

    private void CreateVirtualAxes()
    {
        if (CrossPlatformInputManager.AxisExists(HorizontalAxisName))
            CrossPlatformInputManager.UnRegisterVirtualAxis(HorizontalAxisName);
        if (CrossPlatformInputManager.AxisExists(VerticalAxisName))
            CrossPlatformInputManager.UnRegisterVirtualAxis(VerticalAxisName);

        // set axes to use
        _useX = (AxesToUse == AxisOption.Both || AxesToUse == AxisOption.OnlyHorizontal);
        _useY = (AxesToUse == AxisOption.Both || AxesToUse == AxisOption.OnlyVertical);

        // create new axes based on axes to use
        if (_useX)
        {
            _horizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(HorizontalAxisName);
            CrossPlatformInputManager.RegisterVirtualAxis(_horizontalVirtualAxis);
        }

        if (_useY)
        {
            _verticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(VerticalAxisName);
            CrossPlatformInputManager.RegisterVirtualAxis(_verticalVirtualAxis);
        }
    }

    private Vector2 _dragStartPosition;

    public void OnDrag(PointerEventData data)
    {
        float canvasScale = Screen.width / CanvasScaler.referenceResolution.x;
        
        // Convert from screen coords
        Vector2 offset = data.position - new Vector2(_rectTransform.position.x , _rectTransform.position.y);
        offset /= canvasScale;
        offset -= _dragStartPosition;
        offset /= _rectTransform.rect.width / 2 * MovementRangeMultiplier;
        CurrentJoystickPositionUnclamped = offset;
        
        // Clamp
        //if(offset.magnitude > 1) offset.Normalize();
        offset.x = Mathf.Clamp(offset.x, -1, 1);
        offset.y = Mathf.Clamp(offset.y, -1, 1);
        
        CurrentJoystickPosition = offset;
        UpdateVirtualAxes(CurrentJoystickPosition);

        // Back to screen coords, to set sprite position to clamped value
        offset *= _rectTransform.rect.width / 2 * MovementRangeMultiplier;
        Stick.localPosition = offset;

        JoyAnimator.SetBool("IsActiveJoy", true);
    }

    public void OnPointerUp(PointerEventData data)
    {
        Stick.localPosition = Vector3.zero;
        UpdateVirtualAxes(Vector3.zero);

        JoyAnimator.SetBool("IsActiveJoy", false);
    }


    public void OnPointerDown(PointerEventData data)
    {
        float canvasScale = Screen.width / CanvasScaler.referenceResolution.x;
        
        Vector2 offset = data.position - new Vector2(_rectTransform.position.x , _rectTransform.position.y);
        offset /= canvasScale;

        _dragStartPosition = offset;
    }

    private void OnDisable()
    {
        // remove the joysticks from the cross platform input
        if (_useX)
        {
            _horizontalVirtualAxis.Remove();
        }

        if (_useY)
        {
            _verticalVirtualAxis.Remove();
        }
    }
}