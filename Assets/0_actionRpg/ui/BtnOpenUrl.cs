﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BtnOpenUrl : MonoBehaviour, IPointerClickHandler
{

	[SerializeField] private string _url = "https://vk.com/";

	public void OnPointerClick(PointerEventData eventData)
	{
		Application.OpenURL(_url);
	}

}
