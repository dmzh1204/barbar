﻿using System;
#if !UNITY_WEBGL
using System.Threading.Tasks;
#endif
using baseLogic.uiScreens;
using UnityEngine;
using UnityEngine.UI;

namespace DZTPC.UI
{
    public class ComixController : MonoBehaviour
    {
        [SerializeField] private Transform _rootForComix = null;

        [SerializeField] private GameObject _skipBtnObject = null;

        private static ComixController _inst = null;

        private Comix _currentComix = null;

        // Use this for initialization
        void Start()
        {
            _inst = this;
            _skipBtnObject.GetComponent<Button>().onClick.AddListener(SkipComix);
        }

#if !UNITY_WEBGL
        public static async void StartShowComix(int comixIndex, Action onComixComplete)
                #endif
#if UNITY_WEBGL
        public static void StartShowComix(int comixIndex, Action onComixComplete)
#endif
        {
            UIScreensManager.ShowScreen(UIScreen.ScreenTypes.comix);

#if !UNITY_WEBGL
            await Task.Delay(TimeSpan.FromSeconds(1));
                #endif

            _inst._skipBtnObject.SetActive(false);

            _inst.Invoke("ActivateSkipBtn", 5);

            Comix comixPrefub = GameDataBase.GetComix(comixIndex);

            if (comixPrefub == null)
            {
                onComixComplete?.Invoke();
                return;
            }

            _inst._currentComix = Instantiate(comixPrefub, _inst._rootForComix).Init(onComixComplete);
        }

        private void ActivateSkipBtn()
        {
            _skipBtnObject.SetActive(true);
        }

        private void SkipComix()
        {
            if (_currentComix == null)
                return;

            Destroy(_currentComix.gameObject);
        }
    }
}