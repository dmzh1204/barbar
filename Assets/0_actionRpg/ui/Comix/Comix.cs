﻿using System;
#if !UNITY_WEBGL
using System.Threading.Tasks;
#endif
using baseLogic.Audio;
using UnityEngine;

namespace DZTPC.UI
{
    public class Comix : MonoBehaviour
    {
        [Serializable]
        public class Slide
        {
            public CanvasGroup canvGroup;
            public float duration;
        }

        [SerializeField] Slide[] _slides = new Slide[0];

        public Slide[] Slides => _slides;

        private Action _onComixComplete = null;

        private int _currentSlideNum = 0;

        private float currentSlideTime = 0;

        [SerializeField] private float musicStartPos = 0;
        private AudioSource _audSrc = null;

        private bool _isInited = false;

        public Comix Init(Action onComixComplete)
        {
            _currentSlideNum = 0;
            _onComixComplete = onComixComplete;
            
            return this;
        }

        public void Complete()
        {
            Destroy(gameObject);
        }

#if !UNITY_WEBGL
        private async void OnEnable()
    #else
        private void OnEnable()
    #endif
        {

            _audSrc = GetComponentInChildren<AudioSource>();
            

            foreach (var sl in _slides)
            {
                sl.canvGroup.gameObject.SetActive(false);
                sl.canvGroup.alpha = 0;
            }

            #if !UNITY_WEBGL
            await Task.Delay(TimeSpan.FromSeconds(1));
            #endif

            AudioSystem.SetActive(false);
            _audSrc.Play();
            _audSrc.volume = 0;
            _audSrc.time = musicStartPos;
            _currentSlideNum = 0;
            
            _slides[_currentSlideNum].canvGroup.gameObject.SetActive(true);

            _isInited = true;

        }

        private void OnDisable()
        {
            AudioSystem.SetActive(true);

            _onComixComplete?.Invoke();
        }

        void Update()
        {
            if(_isInited == false)
                return;
            
            if (_audSrc != null)
            {
                float audTargetVol = 1;

                if (_currentSlideNum == _slides.Length - 1 && currentSlideTime > _slides[_currentSlideNum].duration - 2)
                {
                    audTargetVol = 0;
                }

                _audSrc.volume = Mathf.MoveTowards(_audSrc.volume, audTargetVol, Time.unscaledDeltaTime);
            }


            if (_currentSlideNum >= _slides.Length)
            {
                Destroy(gameObject);
                return;
            }

            currentSlideTime += Time.unscaledDeltaTime;

            if (currentSlideTime > _slides[_currentSlideNum].duration - 1)
            {
                _slides[_currentSlideNum].canvGroup.alpha = _slides[_currentSlideNum].duration - currentSlideTime;
            }
            else
            {
                _slides[_currentSlideNum].canvGroup.alpha = currentSlideTime;
            }

            if (currentSlideTime > _slides[_currentSlideNum].duration)
            {
                _currentSlideNum++;
                if(_currentSlideNum < _slides.Length)
                    _slides[_currentSlideNum].canvGroup.gameObject.SetActive(true);
                currentSlideTime = 0;
            }
        }
    }
}