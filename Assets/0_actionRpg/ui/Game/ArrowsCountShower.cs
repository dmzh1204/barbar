﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;

public class ArrowsCountShower : MonoBehaviour
{
	public GameObject BoltsDificit;
	public GameObject ArrowsDificit;
	private Text _text = null;
	
	
	
	// Use this for initialization
	void Start ()
	{
		_text = GetComponent<Text>();
		UpdateText();
		InvokeRepeating("UpdateText", Random.value, 1);
	}

	void UpdateText()
	{
		if (UserController.PlayerCharacter == null)
			return;

		WeaponObject weaponObject = UserController.PlayerCharacter.HumanObj.GetCurrentEquiped(HumanoidObject.EquipTypes.bow);

		if (weaponObject == null)
		{
			_text.text = "";
			return;
		}

		int count = 0;
		if (weaponObject.WeaponType == WeaponObject.WeaponTypes.bows)
		{
			count = UserController.PlayerCharacter.HumanObj.GetCollectebleCount(CollectebleObjects.Types.arrows);
			_text.text = "" + count;
			BoltsDificit?.SetActive(false);
			ArrowsDificit?.SetActive(count == 0);
		}
		else
		{
			count = UserController.PlayerCharacter.HumanObj.GetCollectebleCount(CollectebleObjects.Types.bolts);
			_text.text = "" + count;
			ArrowsDificit?.SetActive(false);
			BoltsDificit?.SetActive(count == 0);
		}
	}
	
}
