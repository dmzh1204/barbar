﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using DZTPC;
using UnityEngine;

public class ReviewDialogController : MonoBehaviour {

	const string reviewCompl = "DlgReviewAccepted";

	// Use this for initialization
	private void Start ()
	{
		HumAIKnowelegeBase.OnQuestComplete += OnQuestCompleted;
	}

	private void OnDestroy()
	{
		HumAIKnowelegeBase.OnQuestComplete -= OnQuestCompleted;
	}

	private void OnQuestCompleted(int quest)
	{
#if UNITY_ANDROID || UNITY_EDITOR
		
		if(PlayerPrefs.GetInt(reviewCompl, 0) != 0)
			return;
		
		if(quest != 2 && 
		   quest != 5 &&
		   quest != 9)
			return;
		
		Invoke("Show", 2);
		
#endif

	}

	private void Show()
	{
		SystemDialog.Show("uiReviewDlgTitle".Localize(), 
			"uiRewiewDlgMessage".Localize(), 
			"uiRewiewDlgOk".Localize(), 
			"uiCancel".Localize(), () =>
			{
				PlayerPrefs.SetInt(reviewCompl, 1);
				Application.OpenURL(@"https://play.google.com/store/apps/details?id=com.dz.barbarian");
			});
	}
}
