﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class InteractionScreen : MonoBehaviour
{
    [SerializeField] private Text _objName;
    [SerializeField] private GameObject _backBtn = null;

    [SerializeField] private InteractionOptionBtn _btnPrefub;

    [SerializeField] private GameObject _dialogPanel = null;
    [SerializeField] private GameObject _tradingPanel = null;

    private HumanoidObject _humObj = null;

    // Use this for initialization
    private void OnEnable()
    {
        _humObj = UserController.PlayerCharacter.HumanObj;
        _humObj.OnInteractionStateChanged += OnInteractionStateChanged;
        OnInteractionStateChanged();
    }

    private void OnDisable()
    {
        _humObj.OnInteractionStateChanged -= OnInteractionStateChanged;
    }

    void OnInteractionStateChanged()
    {
        if (_humObj.CurrentInterObject < 0)
        {
            return;
        }

        var humNPC = TPCObjectsManager.Get(_humObj.CurrentInterObject) as HumanoidObject;

        _backBtn.SetActive(humNPC?.CurrentDialog == null || humNPC.CurrentInteractionIo == InteractiveObject.InteractionType.tradeContainer);

        bool isTrading = _humObj.CurrentInterInteractionHum == InteractiveObject.InteractionType.tradeContainer;

        _dialogPanel.SetActive(isTrading == false);
        _tradingPanel.SetActive(isTrading);

        _objName.text = LocalizeAdapter.GetText(TPCObjectsManager.Get(_humObj.CurrentInterObject).GetName());

        foreach (var btn in _btnPrefub.transform.parent.GetComponentsInChildren<InteractionOptionBtn>())
        {
            Destroy(btn.gameObject);
        }

        if (_humObj.AvalibleInterAction == null)
            return;

        foreach (var act in _humObj.AvalibleInterAction)
        {
            Instantiate(_btnPrefub, _btnPrefub.transform.parent).Init(UserController.PlayerCharacter.HumanObj, act);
        }
    }
}