﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using DZTPC;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InteractionOptionBtn : MonoBehaviour, IPointerClickHandler
{
    
    
    private HumanoidObject _hum;
    private InteractiveObject.ActionAndDescription _actDiscr;

    public void Init(HumanoidObject hum, InteractiveObject.ActionAndDescription actDiscr)
    {
        _hum = hum;
        _actDiscr = actDiscr;
        gameObject.SetActive(true);
        
        GetComponentInChildren<Text>().text = LocalizeAdapter.GetText(actDiscr.Description);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _hum.StartActionOnActiveObject(_actDiscr.Type);
    }
}