﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class ClearAxisOnEnable : MonoBehaviour
{

	[SerializeField] private string[] _axises = {"Mouse X", "Mouse Y"};
	
	// Use this for initialization
	void OnDisable () {

		foreach (string axisName in _axises)
		{
			if(CrossPlatformInputManager.AxisExists(axisName))
				CrossPlatformInputManager.UnRegisterVirtualAxis(axisName);
			
		}
		
	}
	
}
