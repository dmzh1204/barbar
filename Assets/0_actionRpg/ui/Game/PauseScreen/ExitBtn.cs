﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using baseLogic.sceneLoading;
using UnityEngine;
using UnityEngine.UI;

public class ExitBtn : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Button>().onClick.AddListener(() =>
		{
			SystemDialog.Show("uiExit".Localize(), "uiExitMessage".Localize(),"uiAccept".Localize(), "uiCancel".Localize(),
				() =>
				{
					SceneLoader.LoadScene(2, 0.5f);
				}, () => { },null);
		});
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
