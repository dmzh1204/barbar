﻿using System;
using System.Collections;
using System.Collections.Generic;
using baseLogic;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;

public class PauseSaveSlots : MonoBehaviour
{

	public Button Prefab;
	
	// Use this for initialization
	private void OnEnable () => Redraw();
	

	private void Redraw()
	{
		foreach (var btn in GetComponentsInChildren<Button>())
			Destroy(btn.gameObject);

		for (var i = 1; i <= RootInitializer.SaveSlotsCount; i++)
		{
			var slotIndex = i;
			Action saveAction = () =>
			{
				if (!RootInitializer.IsSaveAvailable())
				{
					CenterNotificationController.Show("uiSaveInposible".Localize());
					return;
				}
				
				if (RootInitializer.IsSaveFileExist(slotIndex))
				{
					SystemDialog.Show("uiSaving".Localize(), "uiSaveRewriteMessage".Localize(),"uiAccept".Localize(), "uiCancel".Localize(),
						() =>
						{
							RootInitializer.Save(slotIndex);
							Redraw();
						}, () => { },null);
					return;
				}
				
				RootInitializer.Save(slotIndex);
				Redraw();
			};
			
			var btn = Instantiate(Prefab, Prefab.transform.parent);
			btn.gameObject.SetActive(true);
			var text = btn.GetComponentInChildren<Text>();
			text.text = RootInitializer.GetSaveLabel(i);
			text.color	= RootInitializer.IsSaveSlotCorrect(i) ? Color.white : Color.magenta;
			btn.onClick.AddListener(saveAction.Invoke);
		}
	}
}
