﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;

public class EnemiePanel : MonoBehaviour
{

	public GameObject PanelObject;

	public Text EnemieName;

	public RectTransform EnemieHealthBar;

	private LifeObject _enemieObj = null;

	private int enemieID = -1;

	private int EnemieId
	{
		get { return enemieID; }
		set
		{
			if(enemieID == value)
				return;
			
			enemieID = value;

			PanelObject.SetActive(enemieID >= 0);
			if (enemieID < 0)
			{
				_enemieObj = null;
				return;
			}

			_enemieObj = TPCObjectsManager.Get(enemieID) as LifeObject;
			
			EnemieName.text = LocalizeAdapter.GetText(TPCObjectsManager.Get(enemieID).GetName());
		}
	}

	// Use this for initialization
	void Start ()
	{
		EnemieId = -2;
		EnemieId = -1;
		InvokeRepeating("Work",0, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		if(_enemieObj == null)
			return;
		
		EnemieHealthBar.localScale = new Vector3(
			Mathf.Clamp01(_enemieObj.GetParametr(LifeObject.SpecifedParams.currentHealth) / _enemieObj.GetParametr(LifeObject.DependetParams.maxHealth))
			,1,1);
		
	}

	void Work()
	{
		NeedShowPanel();
	}

	void NeedShowPanel()
	{

		if (UserController.PlayerCharacter == null
		    || UserController.PlayerCharacter.SelectedObjID < 0
		    || UserController.PlayerCharacter.Params.CurrentFightState == LifeObject.FightingStates.none)
		{
			EnemieId = -1;
			return;
		}

		EnemieId = UserController.PlayerCharacter.SelectedObjID;
	}
}
