﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPanel : MonoBehaviour
{
	public RectTransform HealthBar;
	public RectTransform StaminBar;
	public RectTransform CurrentEnduranceBar;
	public RectTransform AccumEnduranceBar;
	
	public Text StatusText;
	
	
	// Use this for initialization
	void Start () {
		InvokeRepeating("Work", 0, 2.123f);
	}
	
	// Update is called once per frame
	void Update () {
		
		if (UserController.PlayerLifeObj == null)
			return;

		HealthBar.localScale = new Vector3(Mathf.Clamp01(
				UserController.PlayerLifeObj.GetParametr(LifeObject.SpecifedParams.currentHealth) 
				/ UserController.PlayerLifeObj.GetParametr(LifeObject.DependetParams.maxHealth)
			)
			, 1, 1);		
		StaminBar.localScale = new Vector3(Mathf.Clamp01(
				UserController.PlayerLifeObj.GetParametr(LifeObject.SpecifedParams.energy) 
				/ UserController.PlayerLifeObj.GetParametr(LifeObject.DependetParams.maxEnergy)
			)
			, 1, 1);

		AccumEnduranceBar.localScale = new Vector3(Mathf.Clamp01(
				(UserController.PlayerCharacter.AvalibleActions.CurrentEndurence 
				+ UserController.PlayerCharacter.AvalibleActions.AccumulatedEndurance)
				/ CharContrActions.MaxEndurence
			)
			, 1, 1);
		
		CurrentEnduranceBar.localScale = new Vector3(Mathf.Clamp01(
				UserController.PlayerCharacter.AvalibleActions.CurrentEndurence 
				/ CharContrActions.MaxEndurence
			)
			, 1, 1);
	}

	private string sttHungry = "sttHungry";
	private string sttHealthLow = "sttHealthLow";
	private string sttStaminaLow = "sttStaminaLow";
	private string sttNeedSleep = "sttNeedSleep";
	private string sttAllWell = "sttAllWell";
	private string sttRefresh = "sttRefresh";
	private string sttEndurance = "sttEndurence";
	
	void Work()
	{
		if (UserController.PlayerLifeObj == null)
			return;
		
		
		
		if (UserController.PlayerLifeObj.GetParametr(LifeObject.SpecifedParams.hunger) > 0)
		{
			StatusText.text = LocalizeAdapter.GetText(sttHungry);
			return;
		}

		if (UserController.PlayerCharacter.AvalibleActions.CurrentEndurence <= 0)
		{
			StatusText.text = LocalizeAdapter.GetText(sttEndurance);
			return;
		}

		if (UserController.PlayerLifeObj.GetParametr(LifeObject.SpecifedParams.energy) < 
		    UserController.PlayerLifeObj.GetParametr(LifeObject.DependetParams.maxEnergy) / 4f)
		{
			StatusText.text = LocalizeAdapter.GetText(sttNeedSleep);
			return;
		}

		if (UserController.PlayerLifeObj.GetParametr(LifeObject.SpecifedParams.currentHealth) 
		    / UserController.PlayerLifeObj.GetParametr(LifeObject.DependetParams.maxHealth) < 0.3f)
		{
			StatusText.text = LocalizeAdapter.GetText(sttHealthLow);
			return;
		}
		
		StatusText.text = LocalizeAdapter.GetText(sttAllWell);
	}
}
