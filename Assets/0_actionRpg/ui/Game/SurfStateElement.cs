﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    public class SurfStateElement : MonoBehaviour
    {
        [SerializeField]
        SurfaseStates[] _surfStates = new SurfaseStates[0];
        
        [SerializeField]
        HumanoidObject.FightingStates[] _fightStates = new HumanoidObject.FightingStates[0];

        UnityStandardAssets.CrossPlatformInput.ButtonHandler _btnHandler = null;

        void OnEnable()
        {
            _btnHandler = GetComponent<UnityStandardAssets.CrossPlatformInput.ButtonHandler>();
        }

        public void SetCurState(SurfaseStates surfState, HumanoidObject.FightingStates fightState)
        {
            foreach (SurfaseStates ss in _surfStates)
                if (ss == surfState)
                {
                    foreach (HumanoidObject.FightingStates fs in _fightStates)
                        if (fs == fightState)
                        {
                            gameObject.SetActive(true);
                            return;
                        }
                    break;
                }
            
            

            gameObject.SetActive(false);

            if (_btnHandler != null && !UserController.IsPCControl)
                _btnHandler.SetUpState();
        }
    }
}