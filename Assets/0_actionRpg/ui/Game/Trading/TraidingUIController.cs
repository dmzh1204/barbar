﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;

public class TraidingUIController : MonoBehaviour
{
    public Text OtherName;
    public Text OtherCost;
    public Text MyCost;
    
    [SerializeField] private ButtonItem _itemPrefub = null;

    [SerializeField] private RectTransform _myPosibleProductsList = null;
    [SerializeField] private RectTransform _sellerPosibleProductsList = null;

    [SerializeField] private RectTransform _myProductsList = null;
    [SerializeField] private RectTransform _sellerProductsList = null;

    [SerializeField] private Button _confirmBtn = null;

    private ContainerObject _myContainer = null;
    private ContainerObject _otherContainer = null;

    List<TradeElement> _mySelectetElements = new List<TradeElement>();
    List<TradeElement> _otherSelectetElements = new List<TradeElement>();

    // Use this for initialization
    private void OnEnable()
    {
        _myContainer = UserController.PlayerCharacter.HumanObj;
        _otherContainer =
            TPCObjectsManager.Get(UserController.PlayerCharacter.HumanObj.CurrentInterObject) as ContainerObject;

        UserController.PlayerCharacter.HumanObj.OnInteractionStateChanged += OnIterChange;

        OtherName.text = _otherContainer.GetName().Localize();
        Fill();

        OnIterChange();
    }

    void OnDisable()
    {
        UserController.PlayerCharacter.HumanObj.OnInteractionStateChanged -= OnIterChange;

        Clear();
    }

    void OnIterChange()
    {
        bool canConfirm = false;

        if (UserController.PlayerCharacter.HumanObj.AvalibleInterAction != null)
        {
            foreach (var act in UserController.PlayerCharacter.HumanObj.AvalibleInterAction)
            {
                if (act.Type == InteractiveObject.InteractionType.tradeConfirm)
                {
                    canConfirm = true;
                }
            }
        }

        _confirmBtn.interactable = canConfirm;
    }

    void Fill()
    {
        Clear();

        LifeObject lo = _otherContainer as LifeObject;

        if (lo != null && lo.IsDead() == false)
        {
            int priceBalance = 0;

            foreach (var elem in _myContainer.SelectedTradeElements.ToArray())
            {
                if (elem.TradeElType == TradeElement.TradeElemTypes.collecteble &&
                    elem.CollectType == CollectebleObjects.Types.golds)
                {
                    _myContainer.SelectedTradeElements.Remove(elem);
                    continue;
                }

                priceBalance -= elem.GetPrice(GlobalSettings.Inst.GPSettings.BaseTraderBuyMultiplier);
            }

            foreach (var elem in _otherContainer.SelectedTradeElements.ToArray())
            {
                if (elem.TradeElType == TradeElement.TradeElemTypes.collecteble &&
                    elem.CollectType == CollectebleObjects.Types.golds)
                {
                    _otherContainer.SelectedTradeElements.Remove(elem);
                    continue;
                }

                priceBalance += elem.GetPrice();
            }


            if (priceBalance > 0 && _myContainer.GetCollectebleCount(CollectebleObjects.Types.golds) >= priceBalance)
            {
                _myContainer.SelectedTradeElements.Insert(0,
                    new TradeElement(_myContainer.UniqID, CollectebleObjects.Types.golds, priceBalance));
            }

            if (priceBalance < 0)
            {
                int goldCount = -priceBalance;

                if (_otherContainer.GetCollectebleCount(CollectebleObjects.Types.golds) >= goldCount)

                    _otherContainer.SelectedTradeElements.Insert(0,
                        new TradeElement(_otherContainer.UniqID, CollectebleObjects.Types.golds, goldCount));
            }
        }
        

        foreach (var elem in _myContainer.PosibleTradeElements)
        {
            Instantiate(_itemPrefub, _myPosibleProductsList.transform).InitTrade(elem, MyItemToSelected);
        }

        foreach (var elem in _otherContainer.PosibleTradeElements)
        {
            Instantiate(_itemPrefub, _sellerPosibleProductsList.transform).InitTrade(elem, OtherItemToSelected);
        }

        float price = 0;
        foreach (var elem in _myContainer.SelectedTradeElements)
        {
            Instantiate(_itemPrefub, _myProductsList.transform).InitTrade(elem, MyItemToAssortiment);
            
            price += elem.GetPrice(GlobalSettings.Inst.GPSettings.BaseTraderBuyMultiplier);
        }

        MyCost.text = price.ToString();

        price = 0;
        foreach (var elem in _otherContainer.SelectedTradeElements)
        {
            Instantiate(_itemPrefub, _sellerProductsList.transform).InitTrade(elem, OtherItemToAssortiment);
            price += elem.GetPrice();
        }
        OtherCost.text = price.ToString();
        if (lo == null || lo.IsDead())
        {
            OtherCost.text = "";
            MyCost.text = "";
        }

        if (lo != null && lo.IsDead() == false)
        {
            foreach (var btn in GetComponentsInChildren<ButtonItem>())
            {
                if (btn.TradeElement.TradeElType == TradeElement.TradeElemTypes.collecteble
                    && btn.TradeElement.CollectType == CollectebleObjects.Types.golds)
                {
                    btn.OnClick = null;
                }
            }
        }

        if (_otherContainer.CurrentUserId >= 0)
            _otherContainer.StartActionByUser(_otherContainer.CurrentInteractionIo);
    }

    void Clear()
    {
        foreach (var btn in GetComponentsInChildren<ButtonItem>())
        {
            Destroy(btn.gameObject);
        }
    }

    void MyItemToSelected(ButtonItem item)
    {
        TradeElement.MoveToOtherCollection(item.TradeElement
            , _myContainer.PosibleTradeElements
            , _myContainer.SelectedTradeElements);
        Fill();
    }

    void MyItemToAssortiment(ButtonItem item)
    {
        TradeElement.MoveToOtherCollection(item.TradeElement
            , _myContainer.SelectedTradeElements
            , _myContainer.PosibleTradeElements);
        Fill();
    }

    void OtherItemToSelected(ButtonItem item)
    {
        TradeElement.MoveToOtherCollection(item.TradeElement
            , _otherContainer.PosibleTradeElements
            , _otherContainer.SelectedTradeElements);
        Fill();
    }

    void OtherItemToAssortiment(ButtonItem item)
    {
        TradeElement.MoveToOtherCollection(item.TradeElement
            , _otherContainer.SelectedTradeElements
            , _otherContainer.PosibleTradeElements);
        Fill();
    }


    // Update is called once per frame
    void Update()
    {
    }

    public void Confirm()
    {
        UserController.PlayerCharacter.HumanObj.StartActionOnActiveObject(
            InteractiveObject.InteractionType.tradeConfirm);
        /*foreach (var elem in _otherContainer.SelectedTradeElements)
        {
            elem.MoveTo();
        }*/
    }
}