﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using RPGDataBase;
using UnityEngine;
using UnityEngine.UI;

public class TextExpierenceForNextLevel : MonoBehaviour {

	// Use this for initialization
	void OnEnable () {
		GetComponent<Text>().text = 
			RPGMath.GetExpirence((int)UserController.PlayerLifeObj.GetParametr(LifeObject.DependetParams.level) + 1).ToString();
	}
	
}
