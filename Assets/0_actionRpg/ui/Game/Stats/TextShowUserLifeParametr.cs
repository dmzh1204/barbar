﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;

public class TextShowUserLifeParametr : MonoBehaviour
{
	[SerializeField]
	private LifeObject.DependetParams _paramTypeDepen = LifeObject.DependetParams.level;
	

	[SerializeField] private bool _specifedMode = false;
	
	[SerializeField]
	private LifeObject.SpecifedParams _paramTypeSpec = LifeObject.SpecifedParams.expirence;
	// Use this for initialization
	void OnEnable ()
	{

		UserController.PlayerLifeObj.OnParamChanged += OnChanged;
		OnChanged(_paramTypeSpec);
		
	}

	private void OnDisable()
	{
		if(UserController.PlayerLifeObj == null)
			return;
		
		UserController.PlayerLifeObj.OnParamChanged -= OnChanged;

	}

	void OnChanged(LifeObject.SpecifedParams type)
	{
		if (_specifedMode)
		{
			GetComponent<Text>().text = ((int)UserController.PlayerLifeObj.GetParametr(_paramTypeSpec)).ToString();
		}
		else
		{
			GetComponent<Text>().text = ((int)UserController.PlayerLifeObj.GetParametr(_paramTypeDepen)).ToString();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
