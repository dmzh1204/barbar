﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using baseLogic.monetization;
using DZTPC;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RewardedAdBtnRefreshChar : MonoBehaviour, IPointerClickHandler
{

	[SerializeField] private GameObject _image = null;

	private bool needRefresh = false;

	private bool NeedRefresh
	{
		get
		{
			return needRefresh;
		}
		set
		{
			if(needRefresh == value)
				return;
			
			needRefresh = value;
			_image.SetActive(needRefresh);
		}
	}

	// Use this for initialization
	private void OnEnable()
	{
		InvokeRepeating("Check", Random.value*2, 2);
	}

	private void OnDisable()
	{
		CancelInvoke("Check");
		NeedRefresh = false;
	}

	void Check()
	{
		if(UserController.PlayerLifeObj == null)
			return;

		NeedRefresh = AdsManager.IsRewardAdAvailable &&
		              (UserController.PlayerLifeObj.GetParametr(LifeObject.SpecifedParams.hunger) >= 0
		               || UserController.PlayerLifeObj.GetParametr(LifeObject.SpecifedParams.energy) <= 0
		               || UserController.PlayerLifeObj.GetParametr(LifeObject.SpecifedParams.currentHealth) / 
		               UserController.PlayerLifeObj.GetParametr(LifeObject.DependetParams.maxHealth) <= 0.5f);
	}
	
	public void OnPointerClick(PointerEventData eventData)
	{
		if(NeedRefresh == false)
			return;

		NeedRefresh = false;
		AdsManager.ShowRewardedAd(OnAdComleted, OnFail);
	}

	void OnAdComleted()
	{
		UserController.PlayerLifeObj.SetParametr(LifeObject.SpecifedParams.hunger, float.MinValue);
		UserController.PlayerLifeObj.SetParametr(LifeObject.SpecifedParams.energy, float.MaxValue);
		UserController.PlayerLifeObj.SetParametr(LifeObject.SpecifedParams.currentHealth
			, UserController.PlayerLifeObj.GetParametr(LifeObject.DependetParams.maxHealth));
		
		CenterNotificationController.Show(
			LocalizeAdapter.GetText("strengthComeBack")
			, CenterNotificationController.Types.basic);
	}

	void OnFail()
	{
		CenterNotificationController.Show(
			LocalizeAdapter.GetText("adFailed")
			, CenterNotificationController.Types.basic);
	}
}
