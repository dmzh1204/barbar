﻿using System;
using System.Collections;
using System.Collections.Generic;
using baseLogic;
using baseLogic.monetization;
using DZTPC;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class RewardedAdBtnAddCollecteble : MonoBehaviour, IPointerClickHandler
{
	public GameObject _image = null;

	public CollectebleObjects.Types[] Types = new []{CollectebleObjects.Types.golds};

	private bool needRefresh = false;

	private bool NeedRefresh
	{
		get
		{
			return needRefresh;
		}
		set
		{
			if(needRefresh == value)
				return;
			
			needRefresh = value;
			_image.SetActive(needRefresh);
		}
	}

	private int count;
	// Use this for initialization
	private void OnEnable()
	{
		NeedRefresh = true;
		NeedRefresh = false;
		Check();
		InvokeRepeating("Check", Random.value*2, 2);
		var text = GetComponentInChildren<Text>(true);

		count = GlobalSettings.Inst.GPSettings.RewardedAdPrice;

		switch (Types[0])
		{
			case CollectebleObjects.Types.arrows:
				count /= GlobalSettings.Inst.GPSettings.ArrowsCost;
				break;
			case CollectebleObjects.Types.bolts:
				count /= GlobalSettings.Inst.GPSettings.BoltsCost;
				break;
		}
		
		if(text != null)
			text.text = $"+{count}";
	}

	private void OnDisable()
	{
		CancelInvoke("Check");
		NeedRefresh = false;
	}

	private void Check()
	{
		if(UserController.PlayerLifeObj == null)
			return;

		NeedRefresh = AdsManager.IsRewardAdAvailable;
	}
	
	public void OnPointerClick(PointerEventData eventData)
	{
		if(NeedRefresh == false)
			return;

		NeedRefresh = false;
		AdsManager.ShowRewardedAd(OnAdCompleted);
	}

	
		
	

	private void OnAdCompleted()
	{
		foreach (var type in Types)		
			UserController.PlayerLifeObj.AddCollectebleCount(type, count);
	}
}
