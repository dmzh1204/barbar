﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public class AttackNoneStateController : MonoBehaviour
{

	[SerializeField] private GameObject _meleeSprite;
	[SerializeField] private GameObject _shootingSprite;
	
	// Use this for initialization
	void OnEnable () {
		
		
			
		if (UserController.LastWeaponType == 0)
		{
			_meleeSprite.SetActive(true);
			_shootingSprite.SetActive(false);
		}
		else
		{
			_meleeSprite.SetActive(false);
			_shootingSprite.SetActive(true);
		}
	}
	
	// Update is called once per frame
	void OnDisable () {
		
	}
}
