﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataTimeShower : MonoBehaviour
{

	private Text text;
	
	private void Awake()
	{
		text = GetComponent<Text>();
		InvokeRepeating("UpdateTime",1,1);
	}

	private void OnEnable()
	{
		UpdateTime();
	}

	private void UpdateTime()
	{
		text.text = GameGlobalTimer.CurrentData.ToString();
	}
}
