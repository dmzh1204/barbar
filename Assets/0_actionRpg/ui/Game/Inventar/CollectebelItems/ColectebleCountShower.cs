﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;

public class ColectebleCountShower : MonoBehaviour, IContainerUI
{

	[SerializeField] private CollectebleObjects.Types _type = CollectebleObjects.Types.golds;

	private Text _text = null;
	
	
	private ContainerObject _containerObject = null;

	public void Init(int containerID)
	{
		_text = GetComponent<Text>();
		_containerObject = TPCObjectsManager.Get(containerID) as ContainerObject;
		CancelInvoke("UpdateText");
		UpdateText();
		InvokeRepeating("UpdateText", 1, 1);
	}

	void UpdateText()
	{
		_text.text = "" + _containerObject.GetCollectebleCount(_type);
	}
}
