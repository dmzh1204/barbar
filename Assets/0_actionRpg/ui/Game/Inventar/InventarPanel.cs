﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public class InventarPanel : MonoBehaviour, IContainerUI
{
	[SerializeField]
	private ButtonItem _btnItemPrefub = null;

	List<ButtonItem> _btns = new List<ButtonItem>();

	private ContainerObject _containerObject = null;
	
	public void Init(int containerID)
	{
		_containerObject = TPCObjectsManager.Get(containerID) as ContainerObject;
		_containerObject.OnDrop += OnInvChanged; 
		_containerObject.OnTake += OnInvChanged; 
		LoadItems();
	}
	


	void OnDisable()
	{
		_containerObject.OnDrop -= OnInvChanged; 
		_containerObject.OnTake -= OnInvChanged;
	}

	void OnInvChanged(int id)
	{
		LoadItems();
	}
	
	void LoadItems()
	{
		foreach (var btn in _btns)
		{
			Destroy(btn.gameObject);
		}
		
		_btns.Clear();
		
		foreach (int id in _containerObject.Inventory)
		{
			ButtonItem btnItm = (ButtonItem)Instantiate(_btnItemPrefub, transform);
			btnItm.InvetoryInit(id);
			_btns.Add(btnItm);
		}

		
	}

}
