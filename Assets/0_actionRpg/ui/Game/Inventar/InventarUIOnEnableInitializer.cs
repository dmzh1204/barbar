﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public class InventarUIOnEnableInitializer : MonoBehaviour {

	public enum Modes
	{
		player,
		selectedByPlayer,
	}

	[SerializeField] private Modes _mode = Modes.player;
	
	// Use this for initialization
	private void OnEnable()
	{
		int id = 0;

		id = UserController.PlayerCharacter.Params.UniqID;
		
		if (_mode == Modes.selectedByPlayer)
		{
			id = UserController.PlayerCharacter.SelectedObjID;
		}
		
		if(TPCObjectsManager.Get(id) is ContainerObject == false)
			return;
		
		foreach (var uiElement in GetComponentsInChildren<IContainerUI>())
		{
			uiElement.Init(id);
		}
	}
}
