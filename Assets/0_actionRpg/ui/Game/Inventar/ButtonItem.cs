﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using baseLogic;
using DZTPC;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonItem : MonoBehaviour
{
    enum BtnItemModes
    {
        inventory,
        trading,
    }

    public Image Icon = null;
    public Image Selection = null;

    public int ID = -1;

    private BtnItemModes _currentMode = BtnItemModes.inventory;

    private TradeElement _tradeElement;
    public TradeElement TradeElement => _tradeElement;

    [SerializeField] private GameObject _infoBtnGO = null;
    [SerializeField] private GameObject _dropBtnGO = null;
    [SerializeField] private Text _textCount = null;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private HumanoidObject _caracter;

    public void InvetoryInit(int id)
    {
        _currentMode = BtnItemModes.inventory;
        ID = id;

        _caracter = (TPCObjectsManager.Get((TPCObjectsManager.Get(ID) as TakebleObject).TakedBy) as HumanoidObject);

        _caracter.OnEquipChanged += OnEquiped;
        Icon.sprite = TPCObjectsManager.Get(ID).GetIcon();
        EquipebleObject eo = TPCObjectsManager.Get(ID) as EquipebleObject;

        if (eo != null && eo.EuipedBy >= 0)
        {
            Selection.enabled = true;
        }
        else
        {
            Selection.enabled = false;
        }

        gameObject.SetActive(true);
        _dropBtnGO.SetActive(!Selection.enabled);
    }

    private Action<ButtonItem> _onClick;

    public Action<ButtonItem> OnClick
    {
        get { return _onClick; }
        set { _onClick = value; }
    }

    public void InitTrade(TradeElement tradeElement, Action<ButtonItem> onClick)
    {
        _onClick = onClick;

        _currentMode = BtnItemModes.trading;
        _tradeElement = tradeElement;

        if (_tradeElement.TradeElType == TradeElement.TradeElemTypes.takebleItem)
        {
            Icon.sprite = TPCObjectsManager.Get(_tradeElement.Id).GetIcon();
        }

        if (_tradeElement.TradeElType == TradeElement.TradeElemTypes.collecteble)
        {
            Icon.sprite = GameDataBase.GetCollectebleSprite(_tradeElement.CollectType);
            _textCount.gameObject.SetActive(true);
            _textCount.text = "" + _tradeElement.CollectebleCount;
        }


        gameObject.SetActive(true);
        _dropBtnGO.SetActive(false);
    }


    void OnDisable()
    {
        if (_caracter != null)
            _caracter.OnEquipChanged -= OnEquiped;
    }

    void OnEquiped(HumanoidObject.EquipTypes type, int id)
    {
        if (id != ID)
            return;

        Selection.enabled = type != HumanoidObject.EquipTypes.none;
        _dropBtnGO.SetActive(!Selection.enabled);
    }

    public void OnMainClick()
    {
        switch (_currentMode)
        {
            case BtnItemModes.inventory:
                InventoryOnClick();
                break;
            case BtnItemModes.trading:
                TraidingOnClick();
                break;
        }
    }

    public void OnInfoClick()
    {
        if (_currentMode == BtnItemModes.trading)
        {
            if (_tradeElement.TradeElType == TradeElement.TradeElemTypes.takebleItem)
            {
                ID = _tradeElement.Id;
            }
            else
            {
                CenterNotificationController.Show(
                    LocalizeAdapter.GetText(_tradeElement.CollectType.ToString())
                );
                return;
            }
        }

        BaseTPCGameObject tgo = TPCObjectsManager.Get(ID);

        EquipebleObject eqob = tgo as EquipebleObject;

        string bonusText = "";

        if (eqob != null)
        {
            LifeObject.DependetParams[] depParams = new LifeObject.DependetParams[]
            {
                LifeObject.DependetParams.defenceMelee,
                LifeObject.DependetParams.defenceShoot,
                LifeObject.DependetParams.attackMeele,
                LifeObject.DependetParams.attackShoot,
            };

            foreach (var p in depParams)
            {
                float bonus = eqob.GetBonuce(p, UserController.PlayerCharacter.HumanObj);

                if (bonus > 0)
                {
                    bonusText += LocalizeAdapter.GetText(p.ToString()) + " +" + (int) bonus +
                                 Environment.NewLine;
                }
            }
        }

        FoodObject food = tgo as FoodObject;

        if (food != null)
        {
            bonusText += LocalizeAdapter.GetText("hunger") + " -" + (GameDataBase.Get(food.PrefubId).paramBonus / 10);
        }

        CenterNotificationController.Show(
            LocalizeAdapter.GetText(tgo.GetName()) + Environment.NewLine
                                                   + LocalizeAdapter.GetText(tgo.GetDescription()) +
                                                   Environment.NewLine
                                                   + bonusText + Environment.NewLine +
                                                   $"<color=orange>{"Gold".Localize()}: {tgo.GetPrice()}</color>"
        );
    }

    public void OnDropClick()
    {
        TakebleObject tgo = TPCObjectsManager.Get(ID) as TakebleObject;
        tgo.StartUse(-1);
    }

    void InventoryOnClick()
    {
        EquipebleObject eo = TPCObjectsManager.Get(ID) as EquipebleObject;

        if (eo != null)
        {
            if (eo.EuipedBy < 0)
            {
                _caracter.Equip(eo);
            }
            else
            {
                _caracter.UnEquip(eo);
            }
        }

        FoodObject fo = TPCObjectsManager.Get(ID) as FoodObject;
        if (fo != null)
        {
            UserController.PlayerCharacter.SelectedObjID = ID;
            UserController.PlayerCharacter.AvalibleActions.DoActions(CCSingleActions.Eating);
        }
    }

    void TraidingOnClick()
    {
        _onClick?.Invoke(this);
    }
}