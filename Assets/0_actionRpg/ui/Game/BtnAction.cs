﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using DZTPC;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class BtnAction : MonoBehaviour, IPointerClickHandler
{


	public int ID = -1;

	public Text Txt = null;

	private float _showTime = 0;

	Vector3 _posOffset = Vector3.zero;
	
	public void SetID(int id)
	{
		ID = id;

		if (ID < 0)
		{
			gameObject.SetActive(false);
			return;
		}
		
		if(Time.time < _showTime)
			return;
		
		if(TPCObjectsManager.Get(ID) == null)
			return;
		
		_posOffset = Vector3.zero;

		LifeObject lifeObj = (TPCObjectsManager.Get(ID) as LifeObject);

		if (lifeObj != null && lifeObj.IsDead() == false)
		{
			_posOffset = Vector3.up * lifeObj.GetParametr(LifeObject.SpecifedParams.height);
		}
		
		transform.position = Camera.main.WorldToScreenPoint(TPCObjectsManager.Get(ID).Position + _posOffset);
		
		Txt.text = LocalizeAdapter.GetText(TPCObjectsManager.Get(ID).GetName());
		
		gameObject.SetActive(true);
	}


	public void OnPointerClick(PointerEventData eventData)
	{
		SetID(-1);
		_showTime = Time.time + 2f;
	}

	const string _submit = "Submit";

	void Update()
	{
		if (CrossPlatformInputManager.GetButtonDown(_submit))
			OnPointerClick(null);
		
		if(TPCObjectsManager.Get(ID) == null)
			return;
		
		transform.position = Camera.main.WorldToScreenPoint(TPCObjectsManager.Get(ID).Position + _posOffset);
	}

	
}
