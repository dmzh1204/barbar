﻿using System.Collections;
using System.Collections.Generic;
using baseLogic.sceneLoading;
using DZTPC;
using DZTPC.UI;
using UnityEngine;

public class ComixQuestController : MonoBehaviour
{
	private List<int> CompletedQuests => UserController.PlayerCharacter.HumanObj.HumAiKnowelegeBase.completedQuests;
	
	// Use this for initialization
	void Start ()
	{
		HumAIKnowelegeBase.OnQuestComplete += OnQuestComplete;
		HumAIKnowelegeBase.OnQuestActivate += OnQuestActivate;
	}

	private void OnDestroy()
	{
		HumAIKnowelegeBase.OnQuestComplete -= OnQuestComplete;
		HumAIKnowelegeBase.OnQuestActivate -= OnQuestActivate;
	}

	private void OnQuestComplete(int quest)
	{
		///если убили главу города и демона
		if(CompletedQuests.Contains(28) && CompletedQuests.Contains(26))
			Show(2);
		
	}

	private void OnQuestActivate(int quest)
	{
		///если отправляемся домой на корабле
		if(quest == 30)
			Show(1);
	}

	private void Show(int index)
	{
		ComixController.StartShowComix(index, () => SceneLoader.LoadScene(2, 0.5f));
	}
}
