﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public class ActionShower : MonoBehaviour
{
    public BtnAction btnAction = null;
    public GameObject btnActionFixed = null;

    private void OnEnable()
    {
        InvokeRepeating("Work", 1, 0.1f);
    }
    private void OnDisable()
    {
        CancelInvoke("Work");
        btnActionFixed.SetActive(false);
        btnAction.SetID(-1);
    }

    void Work()
    {
        if (UserController.PlayerCharacter == null)
            return;
        
        if (UserController.PlayerCharacter != null && UserController.PlayerCharacter.Params.CurrentFightState != LifeObject.FightingStates.none)
        {
            btnAction.SetID(-1);
            btnActionFixed.SetActive(false);
            return;
        }
        
        btnAction.SetID(UserController.PlayerCharacter.SelectedObjID);
        btnActionFixed.SetActive(UserController.PlayerCharacter.SelectedObjID >= 0);
    }
}