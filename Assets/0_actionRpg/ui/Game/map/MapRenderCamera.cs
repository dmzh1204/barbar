﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC.UI
{
	public class MapRenderCamera : MonoBehaviour
	{
		private static RenderTexture _render;

		public static RenderTexture Render => _render;

		public static Camera Camera => _camera;

		private static Camera _camera;

		// Use this for initialization
		void Start()
		{
			_camera = GetComponent<Camera>();
			//_render = new RenderTexture(256, 256, 16, RenderTextureFormat.ARGB32);
			//_render = new RenderTexture(256, 256, 16);
			//_render = new RenderTexture(256, 256, 16, RenderTextureFormat.Default);
			_render = new RenderTexture(512, 512, 32, RenderTextureFormat.Default);
			_camera.targetTexture = _render;
			_camera.Render();
			gameObject.SetActive(false);
		}

	}
}