﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DZTPC.UI
{
	public class MiniMapController : MonoBehaviour
	{
		private Vector3 _offsetPos;

		private RectTransform _rectTrans;
		// Use this for initialization
		void OnEnable()
		{
			if (MapRenderCamera.Camera == null)
			{
				Destroy(this);
				return;
			}
			_offsetPos = MapRenderCamera.Camera.transform.position;

			_rectTrans = GetComponent<RectTransform>();

			GetComponent<RawImage>().texture = MapRenderCamera.Render;
			_rectTrans.sizeDelta = new Vector2(MapRenderCamera.Camera.orthographicSize * 2
				, MapRenderCamera.Camera.orthographicSize * 2);
			
		}

		// Update is called once per frame
		void Update()
		{
			if(UserController.PlayerLifeObj == null)
				return;
			
			if(Camera.main == null)
				return;

			Vector3 pos = UserController.PlayerLifeObj.Position;

			pos = MapRenderCamera.Camera.transform.position - pos;
			
			_rectTrans.pivot = new Vector2(
				0.5f - pos.x / _rectTrans.sizeDelta.x,
				0.5f - pos.z / _rectTrans.sizeDelta.y
				);
			
			_rectTrans.anchoredPosition = Vector2.one;
			_rectTrans.eulerAngles = new Vector3(0,0,Camera.main.transform.eulerAngles.y);
			
		}
	}
}