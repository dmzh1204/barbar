﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DZTPC.UI
{

	public class PauseMapController : MonoBehaviour
	{
		[SerializeField]
		private RectTransform _playerMarker;
		
		private RectTransform _mapRect;

		// Use this for initialization
		void OnEnable()
		{
			GetComponent<RawImage>().texture = MapRenderCamera.Render;
			_mapRect = GetComponent<RectTransform>();
		}

		// Update is called once per frame
		void Update()
		{
			Vector3 pos = UserController.PlayerLifeObj.Position;

			pos = pos - MapRenderCamera.Camera.transform.position;

			pos /= (MapRenderCamera.Camera.orthographicSize);

			_playerMarker.anchoredPosition = new Vector3(
				                         pos.x * (_mapRect.rect.width / 2),
				                         pos.z * (_mapRect.rect.height / 2),
				                         0
				                         );
			
			
			_playerMarker.eulerAngles = new Vector3(0,0,
				-UserController.PlayerLifeObj.Rotation.eulerAngles.y);
			
			_playerMarker.localScale = Vector3.one * (1 + Mathf.Sin(Time.time * 10) / 5);
		}
	}
}