﻿using System;
using System.Collections;
using System.Collections.Generic;
using baseLogic;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;

public class LogShower : MonoBehaviour
{

	public Transform entryPrefub = null;
	
	List<Transform> spawnedList = new List<Transform>();
	
	// Use this for initialization
	private void OnEnable()
	{
		OnDisable();
		foreach (var entry in UserController.PlayerCharacter.HumanObj.HumAiKnowelegeBase.journalLogs)
		{
			Transform t = Instantiate(entryPrefub, entryPrefub.parent);
			t.gameObject.SetActive(true);
			t.GetComponentInChildren<Text>().text = GameDataBase.Get(entry.target).name.Localize() + ":" + Environment.NewLine + entry.message.Localize();
			spawnedList.Add(t);
		}
	}
	
	private void OnDisable()
	{
		foreach (var t in spawnedList)
		{
			Destroy(t.gameObject);
		}
		spawnedList.Clear();
	}
	
	
}
