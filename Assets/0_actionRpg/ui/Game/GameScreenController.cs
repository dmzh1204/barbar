﻿using System;
using System.Collections;
using System.Collections.Generic;
using baseLogic;
using baseLogic.sceneLoading;
using baseLogic.uiScreens;
using DZTPC;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class GameScreenController : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        Invoke("Init", 1);
    }

    void Init()
    {
        UserController.PlayerCharacter.HumanObj.OnInteractionStateChanged += OnPlayerInteractionObjectChange;

        LifeObject.GlobalOnDeath += OnDeath;

        OnPlayerInteractionObjectChange();
    }

    private void OnDestroy()
    {
        if (UserController.PlayerCharacter == null)
            return;

        UserController.PlayerCharacter.HumanObj.OnInteractionStateChanged -= OnPlayerInteractionObjectChange;

        LifeObject.GlobalOnDeath -= OnDeath;
    }

    private void Update()
    {
        if (UIScreensManager.CurrentScreen.ScreenType == UIScreen.ScreenTypes.comix)
            return;
        if (UserController.PlayerCharacter == null
            || UserController.PlayerCharacter.Params.IsDead())
        {
            return;
        }

        if (DialogController.TalkingNow)
        {
            UIScreensManager.ShowScreen(UIScreen.ScreenTypes.dialogScreen);
        }
        else
        {
            if (UIScreensManager.CurrentScreen.ScreenType == UIScreen.ScreenTypes.dialogScreen)
            {
                OnPlayerInteractionObjectChange();
            }
        }

        const string _pauseBtn = "Cancel";
        if (CrossPlatformInputManager.GetButtonDown(_pauseBtn))
        {
            if (UIScreensManager.CurrentScreen.ScreenType == UIScreen.ScreenTypes.pauseGame ||
                UIScreensManager.CurrentScreen.ScreenType == UIScreen.ScreenTypes.inventory)
            {
                OnPlayerInteractionObjectChange();
                return;
            }

            if (UIScreensManager.CurrentScreen.ScreenType == UIScreen.ScreenTypes.gameController)
            {
                if (RootInitializer.Inst.TutorialMode)
                {
                    SystemDialog.Show("tutorialSkip".Localize(), "tutorialSkipMessage".Localize(), "tutorialSkip".Localize(),
                        "uiCancel".Localize(),
                        () => { SceneLoader.LoadScene(2, 0.5f); }, () => { }, null);
                }
                else
                {
                    UIScreensManager.ShowScreen(UIScreen.ScreenTypes.pauseGame);
                }

                return;
            }

            if (UIScreensManager.CurrentScreen.ScreenType == UIScreen.ScreenTypes.interactionOptions)
            {
                UserController.PlayerCharacter.AvalibleActions.DoActions(CCSingleActions.StopUseObject);
                return;
            }
        }
    }

    void OnDeath(LifeObject life)
    {
        if (UIScreensManager.CurrentScreen.ScreenType == UIScreen.ScreenTypes.comix)
            return;
        if (life != UserController.PlayerLifeObj)
            return;

        UIScreensManager.ShowScreen(UIScreen.ScreenTypes.failGame);
    }

    void OnPlayerInteractionObjectChange()
    {
        if (UIScreensManager.CurrentScreen.ScreenType == UIScreen.ScreenTypes.comix)
            return;

        if (UserController.PlayerCharacter.Params.IsDead())
        {
            return;
        }

        if (DialogController.TalkingNow)
        {
            return;
        }

        if (UserController.PlayerCharacter.HumanObj.CurrentInterObject < 0)
        {
            UIScreensManager.ShowScreen(UIScreen.ScreenTypes.gameController);
            if (DialogController.DialogInProgress || DialogController.TalkingNow)
                throw new Exception("ErrorBarbarian. Incorrect Screen Showed In Dialog");
        }
        else
        {
            UIScreensManager.ShowScreen(UIScreen.ScreenTypes.interactionOptions);
        }
    }
}