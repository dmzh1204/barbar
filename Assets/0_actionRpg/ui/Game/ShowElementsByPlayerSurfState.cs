﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DZTPC
{
    
    public class ShowElementsByPlayerSurfState : MonoBehaviour
    {
        SurfStateElement[] _elements = new SurfStateElement[0];

        // Use this for initialization
        void OnEnable()
        {
            UserController.OnCurrentSurfStateChanged += OnSurfStateChanged;
            UserController.OnCurrentFightingStateChanged += OnFightStateChanged;

            _elements = GetComponentsInChildren<SurfStateElement>(true);
            
            Change();
        }

        void OnDisable()
        {
            UserController.OnCurrentSurfStateChanged -= OnSurfStateChanged;
            UserController.OnCurrentFightingStateChanged -= OnFightStateChanged;


        }
	
        // Update is called once per frame
        void OnSurfStateChanged(SurfaseStates state)
        {
            Change();
        }
        
        void OnFightStateChanged(HumanoidObject.FightingStates state)
        {		
            Change();
        }

        void Change()
        {
            if (UserController.PlayerCharacter == null)
                return;
            
            foreach (SurfStateElement sse in _elements)
            {
                sse.SetCurState(UserController.PlayerCharacter.CurSurfState, UserController.PlayerCharacter.Params.CurrentFightState);
            }
        }

    }

}