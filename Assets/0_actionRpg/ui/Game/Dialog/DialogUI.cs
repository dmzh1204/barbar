﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using UnityEngine;
using UnityEngine.UI;

public class DialogUI : MonoBehaviour
{

	public Text textField;
		
	// Use this for initialization
	void Start ()
	{
		DialogController.OnNextStep += OnNextStep;
		OnNextStep();
	}

	private void OnDestroy()
	{
		DialogController.OnNextStep -= OnNextStep;
	}

	public void OnClick()
	{
		DialogController.DelayToNextStep = 0;
	}
	
	void OnNextStep()
	{
		if(DialogController.TalkingNow == false)
			return;

		textField.text = DialogController.DialogebleNpc.CurrentDialog.dialog[DialogController.CurrentStep].replicKey.Localize();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
