﻿using System.Collections;
using System.Collections.Generic;
using baseLogic.uiScreens;
using UnityEngine;

public class ShowScreenOnStart : MonoBehaviour
{

	[SerializeField] private UIScreen.ScreenTypes _scrType = UIScreen.ScreenTypes.mainMenu;
	// Use this for initialization
	void Start () {
		UIScreensManager.ShowScreen(_scrType);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
