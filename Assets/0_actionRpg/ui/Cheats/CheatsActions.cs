﻿using System;
using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;

public static class CheatsActions {

	
	
	public static void AddFood()
	{
		if(UserController.PlayerCharacter == null)
			return;

		foreach (var food in GameDataBase.GetAll<GameDataBase.Food>())
		{
			var obj = food.GenerateNew(UserController.PlayerCharacter.transform.position, new Quaternion()) as TakebleObject;
			obj.StartUse(UserController.PlayerCharacter.HumanObj.UniqID);
		}
	}
	
	public static void AddCollect(CollectebleObjects.Types type, int count)
	{
		if(UserController.PlayerCharacter == null)
			return;
		UserController.PlayerCharacter.HumanObj.AddCollectebleCount(type, count);
	}

	public static void AddItems(IEnumerable<int> ids)
	{
		if(UserController.PlayerCharacter == null)
			return;
		
		foreach (var id in ids)
		{
			var weapon = GameDataBase.Get(id);
			if(weapon == null)
				continue;
			var obj = weapon.GenerateNew(UserController.PlayerCharacter.transform.position, new Quaternion()) as TakebleObject;
			obj.StartUse(UserController.PlayerCharacter.HumanObj.UniqID);
		}
	}

	public static void Teleport(Vector3 pos)
	{
		if(UserController.PlayerCharacter == null)
			return;

		UserController.PlayerCharacter.transform.position = pos;
	}
}
