﻿using System;
using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;

public class CheatsController : MonoBehaviour
{

	public enum CheatsTypes
	{
		none,
		mainMnu,
		items,
		quests,
		teleportation,
	}
	
	public GameObject Content;
	public Button BtnPrefub;

	private CheatsTypes currentCheatType = CheatsTypes.none;
	
	public void OnToggle()
	{
		Content.SetActive(!Content.activeSelf);
		if(Content.activeSelf)
		SwitchPanel(CheatsTypes.mainMnu);
	}

	public void OnBack()
	{
		if(currentCheatType == CheatsTypes.mainMnu)
			OnToggle();
		else
			SwitchPanel(CheatsTypes.mainMnu);
	}

	private void SwitchPanel(CheatsTypes type)
	{
		if(type == currentCheatType)
			return;

		currentCheatType = type;

		RebuildButtons();
	}

	private void ClearBtns()
	{
		foreach (var b in BtnPrefub.transform.parent.GetComponentsInChildren<Button>())
		{
			Destroy(b.gameObject);
		}
	}
	
	private void RebuildButtons()
	{
		ClearBtns();
		switch (currentCheatType)
		{
			case CheatsTypes.none:
				break;
			case CheatsTypes.mainMnu:
				AddBtn("Items", () => SwitchPanel(CheatsTypes.items));
				//AddBtn("Quests", () => SwitchPanel(CheatsTypes.quests));
				AddBtn("Teleport", () => SwitchPanel(CheatsTypes.teleportation));
				AddBtn("GodMode", () => UserController.PlayerLifeObj.GodMode = !UserController.PlayerLifeObj.GodMode);
				break;
			case CheatsTypes.items:
				AddBtn("Add Simple Set", () => CheatsActions.AddItems(new[] {2000, 2003, 2035, 2023, 2029, 2020}));
				AddBtn("Add Max Set", () => CheatsActions.AddItems(new[] {2037, 2004, 2034, 2027, 2022, 2017}));
				AddBtn("Add Food", CheatsActions.AddFood);
				AddBtn("Add Gold", () => CheatsActions.AddCollect(CollectebleObjects.Types.golds, 100));
				AddBtn("Add Arrows", () => CheatsActions.AddCollect(CollectebleObjects.Types.arrows, 100));
				AddBtn("Add Bolts", () => CheatsActions.AddCollect(CollectebleObjects.Types.bolts, 100));
				AddBtn("Add All NoTradeble", () =>
				{
					var ids = new List<int>();
					for (int i = 0; i < 50; i++)
					{
						ids.Add(i + 7000);
					}
					CheatsActions.AddItems(ids);
				});
				break;
			case CheatsTypes.quests:
				break;
			case CheatsTypes.teleportation:
				AddBtn("To Farm", () => CheatsActions.Teleport(Vector3.zero));
				AddBtn("To City", () => CheatsActions.Teleport(new Vector3(60, 16, -124)));
				AddBtn("To Arlin", () => CheatsActions.Teleport(new Vector3(-168, 0, -30)));
				AddBtn("To Monastir", () => CheatsActions.Teleport(new Vector3(-70, 20.22f, -278)));
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
	}

	private void AddBtn(string name, Action onClick)
	{
		var btn = Instantiate(BtnPrefub, BtnPrefub.transform.parent);
		btn.GetComponentInChildren<Text>().text = name;
		btn.onClick.AddListener(() => onClick?.Invoke());
		btn.gameObject.SetActive(true);
	}
}
