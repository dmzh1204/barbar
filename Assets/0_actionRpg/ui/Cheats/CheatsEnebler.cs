﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatsEnebler : MonoBehaviour
{
	public GameObject Cheats;

	private static CheatsEnebler inst;
	
	private static bool enabled;

	public static void SetActive(bool active)
	{
		enabled = active;
		inst.Cheats.SetActive(enabled);
	}
	
	// Use this for initialization
	void Start ()
	{
		inst = this;
		#if UNITY_EDITOR
		Cheats.SetActive(true);
		return;
		#endif
		
		Cheats.SetActive(enabled);
	}
}
