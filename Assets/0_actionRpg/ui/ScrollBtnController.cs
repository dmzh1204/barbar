﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollBtnController : MonoBehaviour
{

	[SerializeField] private ScrollRect _scrol = null;
	[SerializeField] private Directions _direction = Directions.down;
	public enum Directions
	{
		up,
		down,
		left,
		right,
	}

	private Button _btn = null;
	
	// Use this for initialization
	void OnEnable ()
	{
		_btn = GetComponent<Button>();
		_scrol.onValueChanged.AddListener(OnScrollChanged);
		_btn.onClick.AddListener(OnClick);
		OnScrollChanged(_scrol.normalizedPosition);
	}

	void OnDisable()
	{
		_scrol.onValueChanged.RemoveListener(OnScrollChanged);
		_btn.onClick.RemoveListener(OnClick);

	}

	void OnScrollChanged(Vector2 value)
	{
		switch (_direction)
		{
			case Directions.down:
				_btn.interactable = value.y > 0;
				break;
			case Directions.up:
				_btn.interactable = value.y < 1;
				break;
			case Directions.left:
				_btn.interactable = value.x > 0;
				break;
			case Directions.right:
				_btn.interactable = value.x < 1;
				break;
		}
	}

	void OnClick()
	{
		_scrol.verticalNormalizedPosition = Mathf.Clamp(_scrol.verticalNormalizedPosition, 0.001f, 0.999f);
		_scrol.horizontalNormalizedPosition = Mathf.Clamp(_scrol.horizontalNormalizedPosition, 0.001f, 0.999f);

		switch (_direction)
		{
			case Directions.down:
				
				_scrol.velocity = Vector2.up * _scrol.viewport.rect.height;
				break;
			case Directions.up:
				_scrol.velocity = Vector2.down * _scrol.viewport.rect.height;
				break;
			case Directions.left:
				_scrol.velocity = Vector2.right * _scrol.viewport.rect.width;
				break;
			case Directions.right:
				_scrol.velocity = Vector2.left * _scrol.viewport.rect.width;
				break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		//_scrol.v
	}
}
