﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using DZTPC;
using UnityEngine;

public class InteractionNotificator : MonoBehaviour {
	
	private HumanoidObject _initedHumObject = null;

	// Use this for initialization
	void Start () {
		
	}

	private void OnDisable()
	{
		if(_initedHumObject == null)
			return;
		
		_initedHumObject.OnInteractionFailure -= OnInteractionFailure;

		_initedHumObject = null;
	}

	// Update is called once per frame
	void Update () {
		if(_initedHumObject != null || UserController.PlayerCharacter == null || UserController.PlayerCharacter.HumanObj == null)
			return;

		_initedHumObject = UserController.PlayerCharacter.HumanObj;
		_initedHumObject.OnInteractionFailure += OnInteractionFailure;
	}

	void OnInteractionFailure(int idIntrObj, InteractiveObject.FailureTypes failType, int count)
	{
		var textStr = failType.ToString().Localize() + (count != 0 ? " " + count : "");
		
		CenterNotificationController.Show(textStr);
	}
}
