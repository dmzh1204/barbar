﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CenterNotificationController : MonoBehaviour
{
	private const float _minDelay = 1;
	private const float _maxDelay = 2;
	private const float _tutorialDelay = 3;

	public enum Types
	{
		basic,
		levelUp,
		tutorial,
	}
	
	public class Notification
	{
		private Types _type = Types.basic;

		public Types Type => _type;

		public string Text => _text;

		public float ShowingTime => _showingTime;

		private string _text = "";

		private float _showingTime = 0;

		public Notification(string text, Types type)
		{
			_text = text;
			_type = type;
		}

		public void ShowUpdate(float deltaTime)
		{
			_showingTime += deltaTime;
		}
	}

	 static CenterNotificationController _inst;
	
	Queue<Notification> _notifList = new Queue<Notification>();

	private Notification _currentNotif = null;

	[Serializable]
	public struct TextField
	{
		public Types type;
		public Text text;

	
	}
	
	[SerializeField] private TextField[] _textFields = new TextField[0];

	private void Start()
	{
		_inst = this;
	}

	// Update is called once per frame
	void Update () {
		if (_currentNotif == null && _notifList.Count > 0)
		{
			_currentNotif = _notifList.Dequeue();
			ShowNotif(_currentNotif);
		}

		if (_currentNotif != null)
		{
			_currentNotif.ShowUpdate(Time.deltaTime);

			float showLimit = _maxDelay;
			if (_notifList.Count > 0)
			{
				showLimit = _minDelay;
			}

			if (_currentNotif.Type == Types.tutorial)
				showLimit = _tutorialDelay;

			if (_currentNotif.ShowingTime > showLimit)
			{
				HideNotif(_currentNotif);
				_currentNotif = null;
			}
		}
	}
	
	void ShowNotif(Notification notif)
	{
		foreach (var tf in _textFields)
		{
			if (tf.type == notif.Type)
			{
				tf.text.text = notif.Text;
				break;
			}
		}
	}
		
	void HideNotif(Notification notif)
	{
		foreach (var tf in _textFields)
		{
			if (tf.type == notif.Type)
			{
				tf.text.text = "";
				break;
			}
		}	
	}

	public static void Show(string text, Types type = Types.basic)
	{
		_inst._notifList.Enqueue(new Notification(text, type));
	}
}
