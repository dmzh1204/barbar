﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using baseLogic.Audio;
using DZTPC;
using UnityEngine;

public class ExpierenceNotificator : MonoBehaviour
{
	private float _prevExpierence = 0;
	private float _prevLevel = 0;

	private LifeObject _initedLifeObject = null;

	private const string _newLevelMessage = "Level Up";
	private const string _epirenceMessage = "exp";

	[SerializeField] private AudioClip _clipOnLevelUp;
	
	private void OnDisable()
	{
		if (_initedLifeObject != null)
		{
			_initedLifeObject.OnParamChanged -= OnExpierenceChanged;
		}
	}

	private void Update()
	{
		if(_initedLifeObject != null || UserController.PlayerLifeObj == null)
			return;

		_initedLifeObject = UserController.PlayerLifeObj;
		_initedLifeObject.OnParamChanged += OnExpierenceChanged;

		_prevExpierence = _initedLifeObject.GetParametr(LifeObject.SpecifedParams.expirence);
		_prevLevel = _initedLifeObject.GetParametr(LifeObject.DependetParams.level);

	}

	void OnExpierenceChanged(LifeObject.SpecifedParams paramType)
	{
		float newExpirence = _initedLifeObject.GetParametr(LifeObject.SpecifedParams.expirence);
		float newLevel = _initedLifeObject.GetParametr(LifeObject.DependetParams.level);
		
		float deltaXP = newExpirence - _prevExpierence;
		if (deltaXP > 0)
		{
			CenterNotificationController.Show("+" + deltaXP);
			
			if (newLevel - _prevLevel > 0)
			{
				CenterNotificationController.Show(LocalizeAdapter.GetText(_newLevelMessage)
					, CenterNotificationController.Types.levelUp);
				AudioSystem.PlayEffect2D(_clipOnLevelUp);
			}
		}
		
		
		
		_prevExpierence = newExpirence;
		_prevLevel = newLevel;
	}
}
