﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemDialog : MonoBehaviour
{
	public Text Header;
	public Text Message;
	public Button AcceptBtn;
	public Button CancelBtn;

	public CanvasGroup CanvasGroup;
	
	private static SystemDialog inst;

	private float targetAlpha = 0;
	
	public static void Show(string header, string message,string accept, string cancel, 
		Action onAccept, Action onCancel = null, Action onBack = null)
	{
		inst.CanvasGroup.alpha = 0;
		inst.targetAlpha = 1;
		inst.Header.text = header;
		inst.Message.text = message;
		inst.AcceptBtn.GetComponentInChildren<Text>().text = accept;
		inst.CancelBtn.GetComponentInChildren<Text>().text = cancel;
		inst.AcceptBtn.onClick.RemoveAllListeners();
		inst.CancelBtn.onClick.RemoveAllListeners();
		inst.AcceptBtn.onClick.AddListener(() =>
		{
			Close();
			onAccept?.Invoke();
		});
		inst.CancelBtn.onClick.AddListener(() =>
		{
			Close();
			onCancel?.Invoke();
		});
	}

	private static void Close()
	{
		inst.CanvasGroup.alpha = 1;
		inst.targetAlpha = 0;
	}
	
	// Use this for initialization
	private void Start ()
	{
		inst = this;
	}

	private void Update()
	{
		CanvasGroup.alpha = Mathf.MoveTowards(CanvasGroup.alpha, targetAlpha, 2);
		CanvasGroup.interactable = CanvasGroup.alpha == 1;
		CanvasGroup.blocksRaycasts = CanvasGroup.alpha > 0;
		CanvasGroup.gameObject.SetActive(CanvasGroup.blocksRaycasts);
	}
}
