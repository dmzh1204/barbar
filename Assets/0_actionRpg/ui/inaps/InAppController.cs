﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using DZTPC;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class InAppController : MonoBehaviour
{
    public enum InAppTypes
    {
        ringOfStrength,
        ringOfDexterity,
        amuletOfIntelect,
        amuletOfDefense,
        worldMap,
        saveSlots10,
        devConsole,
        parkur,
        dontNeedEatSleep,
    }

    public InAppTypes InAppType = InAppTypes.ringOfStrength;
    public Image Icon;
    [SerializeField] private GameObject _activatorElementForEnable = null;
    [SerializeField] private GameObject _buttomTriggerEnable = null;
    [SerializeField] private GameObject _inAppBtn = null;


    private const string _anyInappBuyed = "_anyInappBuyed";

    // Use this for initialization
    void Start()
    {
        //PlayerPrefs.DeleteAll();

        _activatorElementForEnable.SetActive(IsEnabled());
        _buttomTriggerEnable.SetActive(IsBuyed());

        SetActiveInAppBtn(!IsBuyed());
        HandleModifInApp();
    }

    private void OnDisable()
    {
        baseLogic.monetization.AdsManager.DefaultAdsEnabled = PlayerPrefs.GetInt(_anyInappBuyed, 0) == 0;

        HandleModifInApp();
    }
    
    bool IsBuyed()
    {
        return PlayerPrefs.GetInt(InAppType.ToString() + "buyed", 0) != 0;
    }

    bool IsEnabled()
    {
        return PlayerPrefs.GetInt(InAppType.ToString() + "enebled", 0) != 0 && IsBuyed();
    }

    public void SetBuyed(Product product)
    {
        Invoke("DelayedBuyHandler", 0.5f);
    }

    void DelayedBuyHandler()
    {
        PlayerPrefs.SetInt(InAppType.ToString() + "buyed", 1);

        SetActiveInAppBtn(false);

        _buttomTriggerEnable.SetActive(true);

        SetEnabled(true);
        CenterNotificationController.Show(LocalizeAdapter.GetText("purchaseCompleted"));

        PlayerPrefs.SetInt(_anyInappBuyed, 1);
        baseLogic.monetization.AdsManager.DefaultAdsEnabled = false;
    }

    void SetActiveInAppBtn(bool isActive)
    {
        _inAppBtn.GetComponent<Button>().interactable = isActive;
        _inAppBtn.GetComponent<Image>().raycastTarget = isActive;
        _inAppBtn.GetComponent<IAPButton>().priceText.enabled = isActive;
    }

    public void SetEnabled(bool isEnable)
    {
        if (IsBuyed() == false)
        {
            PlayerPrefs.SetInt(InAppType.ToString() + "enebled", 0);
            return;
        }

        PlayerPrefs.SetInt(InAppType.ToString() + "enebled", isEnable ? 1 : 0);
        _activatorElementForEnable.SetActive(IsEnabled());
        HandleModifInApp();
    }

    public void EnableTrigger()
    {
        if (IsBuyed() == false)
            return;

        SetEnabled(!IsEnabled());
    }

    void HandleModifInApp()
    {
        switch (InAppType)
        {
            case InAppTypes.ringOfStrength:
                GlobalSettings.PlayerModifications.StrengthBonus = IsEnabled() ? 5 : 0;
                break;
            case InAppTypes.ringOfDexterity:
                GlobalSettings.PlayerModifications.DexterityBonus = IsEnabled() ? 5 : 0;
                break;
            case InAppTypes.amuletOfIntelect:
                GlobalSettings.PlayerModifications.ExpirenceMultiplier = IsEnabled() ? 2f : 1;
                break;
            case InAppTypes.amuletOfDefense:
                GlobalSettings.PlayerModifications.DefenseBonus = IsEnabled() ? 5 : 0;
                break;
            case InAppTypes.parkur:
                GlobalSettings.PlayerModifications.ParkurMode = IsEnabled();
                break;
            case InAppTypes.dontNeedEatSleep:
                GlobalSettings.PlayerModifications.DontNeedSleepAndEat = IsEnabled();
                break;
            case InAppTypes.saveSlots10:
                RootInitializer.SaveSlotsCount = IsEnabled() ? 10 : 1;
                #if UNITY_EDITOR
                RootInitializer.SaveSlotsCount = 10;
                #endif
                break;
            case InAppTypes.devConsole:
                CheatsEnebler.SetActive(IsEnabled());
                #if UNITY_EDITOR
                CheatsEnebler.SetActive(true);
                #endif
                break;
        }
    }

    public void OnFailedPurchase(Product product, PurchaseFailureReason reason)
    {
        if (IsBuyed())
            return;

        if (reason == PurchaseFailureReason.DuplicateTransaction)
        {
            SetBuyed(product);
            return;
        }

        CenterNotificationController.Show(LocalizeAdapter.GetText("purchaseFailed"));
    }
}