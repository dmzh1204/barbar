﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class InAppNameCorrector : MonoBehaviour
{
	private Text _name = null;

	private string _prevNameString = "";
	// Use this for initialization
	void Start ()
	{
		_name = GetComponent<Text>();

		if (_name == null)
		{
			Destroy(this);
			return;
		}

		//_name.text = "safsdfsdfsd [sdfsdgf]";
		
		InvokeRepeating("CheckText", Random.value, 1);
		
	}

	void CheckText()
	{
		if(_prevNameString == _name.text)
			return;

		if (_name.text.Contains(" [")
		    || _name.text.Contains(" (")
		    || _name.text.Contains("[")
		    || _name.text.Contains("("))
		{
			_name.text = _name.text.Split(new []{" [", " (", "[", "("}, StringSplitOptions.None)[0];
		}
		
		_prevNameString = _name.text;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
