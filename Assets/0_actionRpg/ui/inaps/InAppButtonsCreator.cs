﻿using System;
using UnityEngine;
#if UNITY_PURCHASING
using UnityEngine.Purchasing;
#endif

public class InAppButtonsCreator : MonoBehaviour
{
	public InAppController Prefub;
	
	[Serializable]
	public class BtnData
	{
		public string ID;
		public InAppController.InAppTypes Type;
		public Sprite Icon;
	}

	public BtnData[] Buttons;
	#if UNITY_PURCHASING

	// Use this for initialization
	void Start () {

		foreach (var button in Buttons)
		{
			var newBtn = Instantiate(Prefub, Prefub.transform.parent);

			var newIapBtn = newBtn.GetComponentInChildren<IAPButton>();

			newIapBtn.productId = button.ID;
			newBtn.InAppType = button.Type;
			newBtn.Icon.sprite = button.Icon;
			newBtn.gameObject.SetActive(true);
		}
		
	}
	
	#endif
}
