﻿using UnityEngine;

public class MouseCursorHider : MonoBehaviour {

	#if !MOBILE_INPUT
	void Awake() => Set(true);
	
	void OnEnable () => Set(false);
	
	void OnDisable() => Set(true);

	void Set(bool visible)
	{
		Cursor.visible = visible;
		Cursor.lockState = visible ? CursorLockMode.None : CursorLockMode.Locked;
	}
	#endif
}
