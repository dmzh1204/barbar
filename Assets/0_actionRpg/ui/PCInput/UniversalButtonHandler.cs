﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

public class UniversalButtonHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	public string Name = "Cancel";

	public bool SimulateMobileClick;
	public bool OnPCModeOnly;

	public void OnPointerDown(PointerEventData eventData)
	{
		#if MOBILE_INPUT
		if(!OnPCModeOnly)
			CrossPlatformInputManager.SetButtonDown(Name);
		#endif
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		#if MOBILE_INPUT
		if(!OnPCModeOnly)
			CrossPlatformInputManager.SetButtonUp(Name);
		#endif
	}

	private async void Update()
	{
#if !MOBILE_INPUT
		if(!CrossPlatformInputManager.GetButtonDown(Name))
			return;

		await Task.Delay(200);
		
if(!SimulateMobileClick)	
return;

		var e = new PointerEventData(EventSystem.current);
		foreach (var pointerClickHandler in GetComponents<IPointerClickHandler>())
			pointerClickHandler?.OnPointerClick(e);
		foreach (var pointerClickHandler in GetComponents<IPointerDownHandler>())
			pointerClickHandler?.OnPointerDown(e);
		foreach (var pointerClickHandler in GetComponents<IPointerUpHandler>())
			pointerClickHandler?.OnPointerUp(e);

#endif
	}
}
