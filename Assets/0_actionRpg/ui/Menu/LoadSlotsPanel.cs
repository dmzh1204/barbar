﻿using System;
using System.Collections;
using System.Collections.Generic;
using baseLogic;
using baseLogic.sceneLoading;
using DZTPC;
using UnityEngine;
using UnityEngine.UI;

public class LoadSlotsPanel : MonoBehaviour
{
	public Button CloseBtn;

	public Button SlotPrefab;
		
	// Use this for initialization
	private void Start () {
		CloseBtn.onClick.AddListener(() =>
		{
			gameObject.SetActive(false);
		});
	}

	private void OnEnable()
	{
		foreach (var btn in SlotPrefab.transform.parent.GetComponentsInChildren<Button>())
			Destroy(btn.gameObject);
		
		for (var i = 0; i <= RootInitializer.SaveSlotsCount; i++)
		{
			if(!RootInitializer.IsSlotSaved(i))
				continue;
			var slotIndex = i;
			Action saveAction = () =>
			{
				if (RootInitializer.IsSaveSlotCorrect(slotIndex))
				{
					RootInitializer.CurrentSaveIndex = slotIndex;
					SceneLoader.LoadScene(3, 0.5f);
					gameObject.SetActive(false);
				}
				else
				{
					SystemDialog.Show("uiSaveFileErrorTitle".Localize(), 
						"uiSaveFileErrorMessage".Localize(), 
						"uiContinue".Localize(), 
						"uiCancel".Localize(), () =>
					{
						RootInitializer.CurrentSaveIndex = slotIndex;
						SceneLoader.LoadScene(3, 0.5f);
						gameObject.SetActive(false);
					});
				}
			};
			
			var btn = Instantiate(SlotPrefab, SlotPrefab.transform.parent);
			btn.gameObject.SetActive(true);
			var text = btn.GetComponentInChildren<Text>();
			text.text = RootInitializer.GetSaveLabel(i);
			text.color	= RootInitializer.IsSaveSlotCorrect(i) ? Color.white : Color.magenta;
			btn.onClick.AddListener(saveAction.Invoke);
		}
	}
}
