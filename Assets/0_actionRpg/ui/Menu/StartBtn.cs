﻿using baseLogic;
using baseLogic.sceneLoading;
using DZTPC;
using DZTPC.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StartBtn : MonoBehaviour, IPointerClickHandler
{

	public GameObject forNewGame;
	
	public GameObject forContinueGame;

	public Text ContinueSaveNameText;

	public Button SettingsBtn;

	public GameObject SettingsPanel;
	
	private void Start()
	{
		SettingsBtn.onClick.AddListener(()=>SettingsPanel.SetActive(true));
	}
	
	// Use this for initialization
	private void OnEnable()
	{
		RootInitializer.CurrentSaveIndex = RootInitializer.GetLastSavedSlot();
		forNewGame.SetActive(RootInitializer.CurrentSaveIndex < 0);
		forContinueGame.SetActive(RootInitializer.CurrentSaveIndex >= 0);
		SettingsBtn.gameObject.SetActive(RootInitializer.CurrentSaveIndex >= 0);
		ContinueSaveNameText.text = RootInitializer.GetSaveLabel(RootInitializer.CurrentSaveIndex);
		ContinueSaveNameText.color = RootInitializer.IsSaveSlotCorrect(RootInitializer.CurrentSaveIndex)
			? Color.white
			: Color.magenta;
	}
	
	public void OnPointerClick(PointerEventData eventData)
	{
		//init data base

		if (RootInitializer.CurrentSaveIndex >= 0)
		{
			if (RootInitializer.IsSaveSlotCorrect(RootInitializer.CurrentSaveIndex) == false)
			{
				SystemDialog.Show("uiSaveFileErrorTitle".Localize(), 
					"uiSaveFileErrorMessage".Localize(), 
					"uiContinue".Localize(), 
					"uiCancel".Localize(), () => SceneLoader.LoadScene(3, 0.5f));
				return;
			}
			SceneLoader.LoadScene(3, 0.5f);
			return;
		}
		
		ComixController.StartShowComix(0, () =>
			{
				SceneLoader.LoadScene(3, 0.5f);
			});
	}
}
