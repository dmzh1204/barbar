﻿using System.Collections;
using System.Collections.Generic;
using DZTPC;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SaveBtn : MonoBehaviour, IPointerClickHandler {

	// Use this for initialization
	private void OnEnable()
	{

	}

	private void OnDisable()
	{
		
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		RootInitializer.Save(0);
	}
}
