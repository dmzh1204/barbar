﻿using System.Collections;
using System.Collections.Generic;
using baseLogic.sceneLoading;
using DZTPC;
using DZTPC.UI;
using UnityEngine;
using UnityEngine.UI;

public class StartNewGameBtn : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Button>().onClick.AddListener(() =>
		{
			GetComponentInParent<LoadSlotsPanel>().gameObject.SetActive(false);
			RootInitializer.CurrentSaveIndex = -1;
			ComixController.StartShowComix(0, () =>
			{
				SceneLoader.LoadScene(3, 0.5f);
			});
		});
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
