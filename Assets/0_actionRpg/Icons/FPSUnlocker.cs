﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSUnlocker : MonoBehaviour {

	// Use this for initialization
	void Start () {
		#if !UNITY_WEBGL
        Application.targetFrameRate = 60;
		#endif
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
