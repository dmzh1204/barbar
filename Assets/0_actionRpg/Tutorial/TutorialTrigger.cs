﻿using System.Collections;
using System.Collections.Generic;
using baseLogic;
using baseLogic.sceneLoading;
using DZTPC;
using UnityEngine;
using CharacterController = DZTPC.CharacterController;

public class TutorialTrigger : MonoBehaviour
{
    public string TextMessage;

    public bool ExitFromTutorial;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<UserController>() == null)
            return;


        GetComponent<Collider>().enabled = false;

        if (ExitFromTutorial)
            Invoke("Exit", 4);
        
        if (TextMessage != null && TextMessage != "")
            CenterNotificationController.Show(TextMessage.Localize(), CenterNotificationController.Types.tutorial);
    }

    private void Exit() => SceneLoader.LoadScene(2, 0.5f);
}