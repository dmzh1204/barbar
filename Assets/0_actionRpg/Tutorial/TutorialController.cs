﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using baseLogic;
using DZTPC;
using UnityEngine;

public class TutorialController : MonoBehaviour
{
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);    
        
        UserController.PlayerLifeObj.AddToParametr(LifeObject.SpecifedParams.expirence, 50000);
        UserController.PlayerLifeObj.AddToParametr(LifeObject.SpecifedParams.dexterity, 10);
        UserController.PlayerLifeObj.AddToParametr(LifeObject.SpecifedParams.strength, 10);
        UserController.PlayerLifeObj.AddToParametr(LifeObject.SpecifedParams.currentHealth
            , UserController.PlayerLifeObj.GetParametr(LifeObject.DependetParams.maxHealth));
        
        yield return new WaitForSeconds(3);    
        CenterNotificationController.Show("tutorialNotification_01".Localize(), CenterNotificationController.Types.tutorial);
        yield return new WaitForSeconds(3);    
        CenterNotificationController.Show("tutorialNotification_02".Localize(), CenterNotificationController.Types.tutorial);
        yield return new WaitForSeconds(3);    
        CenterNotificationController.Show("tutorialNotification_03".Localize(), CenterNotificationController.Types.tutorial);

        PlayerPrefs.SetInt("TutorialComplited", 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
