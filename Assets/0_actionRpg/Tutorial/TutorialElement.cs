﻿using DZTPC;
using UnityEngine;

public class TutorialElement : MonoBehaviour
{
    public bool ShowInTutorial;
    
    // Start is called before the first frame update
    private void Start() => Check();
    private void OnEnable() => Check();
    
    private void Check() => gameObject.SetActive(ShowInTutorial == RootInitializer.Inst.TutorialMode);
}
