﻿Shader "Vertex Color Splat Surf Shader" {
    Properties {
        _Splat1 ("Base (RGB)", 2D) = "white" {}
        _Splat2 ("Base (RGB)", 2D) = "white" {}
        _Splat3 ("Base (RGB)", 2D) = "white" {}
        _Splat4 ("Base (RGB)", 2D) = "white" {}
        //_Splat5 ("Base (RGB)", 2D) = "white" {}
    }
		SubShader {
			Tags { "RenderType"="Opaque" }
			LOD 150

		CGPROGRAM
		#pragma surface surf Lambert noforwardadd
       
        sampler2D _Splat1;
        sampler2D _Splat2;
        sampler2D _Splat3;
        sampler2D _Splat4;
        //sampler2D _Splat5;
 
        struct Input {
            float2 uv_Splat1;
            float2 uv_Splat2;
            float2 uv_Splat3;
            float2 uv_Splat4;
            //float2 uv_Splat5;
            float4 color: Color; // Vertex color
        };
 
        void surf (Input IN, inout SurfaceOutput o) {

            half4 splat1 = tex2D (_Splat1, IN.uv_Splat1);
            half4 splat2 = tex2D (_Splat2, IN.uv_Splat2);
            half4 splat3 = tex2D (_Splat3, IN.uv_Splat3);
            half4 splat4 = tex2D (_Splat4, IN.uv_Splat4);
            //half4 splat5 = tex2D (_Splat5, IN.uv_Splat5);

            o.Albedo = lerp(lerp(lerp(splat2.rgb, splat1.rgb, IN.color.r), splat3.rgb, IN.color.b), splat4.rgb, IN.color.a);
        }
        ENDCG
    }
    FallBack "Diffuse"
}