// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:32903,y:32742,varname:node_4013,prsc:2|diff-4770-OUT;n:type:ShaderForge.SFN_VertexColor,id:1170,x:32505,y:32739,varname:node_1170,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:7880,x:32345,y:32831,ptovrint:False,ptlb:Splat_1,ptin:_Splat_1,varname:node_7880,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:e0f6e2f766fe4d04ab8c253b1618272b,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5298,x:32345,y:33020,ptovrint:False,ptlb:Splat_2,ptin:_Splat_2,varname:node_5298,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1c6c94e6f0b33b84794b532ebc14e8a3,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:9683,x:32345,y:33195,ptovrint:False,ptlb:Splat_3,ptin:_Splat_3,varname:_Splat_3,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1c6c94e6f0b33b84794b532ebc14e8a3,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:2482,x:32345,y:33385,ptovrint:False,ptlb:Splat_4,ptin:_Splat_4,varname:_Splat_4,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1c6c94e6f0b33b84794b532ebc14e8a3,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:6948,x:32685,y:32934,varname:node_6948,prsc:2|A-7880-RGB,B-5298-RGB,T-1170-R;n:type:ShaderForge.SFN_Lerp,id:1499,x:32685,y:33093,varname:node_1499,prsc:2|A-6948-OUT,B-9683-RGB,T-1170-G;n:type:ShaderForge.SFN_Lerp,id:6975,x:32635,y:33220,varname:node_6975,prsc:2|A-1499-OUT,B-2482-RGB,T-1170-B;n:type:ShaderForge.SFN_Lerp,id:4770,x:32635,y:33347,varname:node_4770,prsc:2|A-6975-OUT,B-4553-RGB,T-1170-A;n:type:ShaderForge.SFN_Tex2d,id:4553,x:32345,y:33576,ptovrint:False,ptlb:Splat_5,ptin:_Splat_5,varname:_Splat_5,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1c6c94e6f0b33b84794b532ebc14e8a3,ntxv:0,isnm:False;proporder:7880-5298-9683-2482-4553;pass:END;sub:END;*/

Shader "Shader Forge/DZVertexBlend5Splats" {
    Properties {
        _Splat_1 ("Splat_1", 2D) = "white" {}
        _Splat_2 ("Splat_2", 2D) = "white" {}
        _Splat_3 ("Splat_3", 2D) = "white" {}
        _Splat_4 ("Splat_4", 2D) = "white" {}
        _Splat_5 ("Splat_5", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Splat_1; uniform float4 _Splat_1_ST;
            uniform sampler2D _Splat_2; uniform float4 _Splat_2_ST;
            uniform sampler2D _Splat_3; uniform float4 _Splat_3_ST;
            uniform sampler2D _Splat_4; uniform float4 _Splat_4_ST;
            uniform sampler2D _Splat_5; uniform float4 _Splat_5_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 _Splat_1_var = tex2D(_Splat_1,TRANSFORM_TEX(i.uv0, _Splat_1));
                float4 _Splat_2_var = tex2D(_Splat_2,TRANSFORM_TEX(i.uv0, _Splat_2));
                float4 _Splat_3_var = tex2D(_Splat_3,TRANSFORM_TEX(i.uv0, _Splat_3));
                float4 _Splat_4_var = tex2D(_Splat_4,TRANSFORM_TEX(i.uv0, _Splat_4));
                float4 _Splat_5_var = tex2D(_Splat_5,TRANSFORM_TEX(i.uv0, _Splat_5));
                float3 diffuseColor = lerp(lerp(lerp(lerp(_Splat_1_var.rgb,_Splat_2_var.rgb,i.vertexColor.r),_Splat_3_var.rgb,i.vertexColor.g),_Splat_4_var.rgb,i.vertexColor.b),_Splat_5_var.rgb,i.vertexColor.a);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Splat_1; uniform float4 _Splat_1_ST;
            uniform sampler2D _Splat_2; uniform float4 _Splat_2_ST;
            uniform sampler2D _Splat_3; uniform float4 _Splat_3_ST;
            uniform sampler2D _Splat_4; uniform float4 _Splat_4_ST;
            uniform sampler2D _Splat_5; uniform float4 _Splat_5_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _Splat_1_var = tex2D(_Splat_1,TRANSFORM_TEX(i.uv0, _Splat_1));
                float4 _Splat_2_var = tex2D(_Splat_2,TRANSFORM_TEX(i.uv0, _Splat_2));
                float4 _Splat_3_var = tex2D(_Splat_3,TRANSFORM_TEX(i.uv0, _Splat_3));
                float4 _Splat_4_var = tex2D(_Splat_4,TRANSFORM_TEX(i.uv0, _Splat_4));
                float4 _Splat_5_var = tex2D(_Splat_5,TRANSFORM_TEX(i.uv0, _Splat_5));
                float3 diffuseColor = lerp(lerp(lerp(lerp(_Splat_1_var.rgb,_Splat_2_var.rgb,i.vertexColor.r),_Splat_3_var.rgb,i.vertexColor.g),_Splat_4_var.rgb,i.vertexColor.b),_Splat_5_var.rgb,i.vertexColor.a);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
