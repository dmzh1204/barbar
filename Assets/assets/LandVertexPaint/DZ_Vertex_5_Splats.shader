﻿Shader "DZ/Vertex_5_Splats" {
    Properties {
        _Splat1 ("Base (RGB)", 2D) = "white" {}
        _Splat2 ("Base (RGB)", 2D) = "white" {}
        _Splat3 ("Base (RGB)", 2D) = "white" {}
        _Splat4 ("Base (RGB)", 2D) = "white" {}
        _Splat5 ("Base (RGB)", 2D) = "white" {}
    }
		SubShader {
			Tags { "RenderType"="Opaque" }
			LOD 150

		CGPROGRAM
		//#pragma surface surf Lambert noforwardadd
		#pragma surface surf Lambert

        sampler2D _Splat1;
        sampler2D _Splat2;
        sampler2D _Splat3;
        sampler2D _Splat4;
        sampler2D _Splat5;
 
        struct Input {
            float2 uv_Splat1;
            float4 color: Color; // Vertex color
        };
 
        void surf (Input IN, inout SurfaceOutput o) {

	        /*half4 splat1 = tex2D (_Splat1, IN.uv_Splat1);
	            half4 splat2 = tex2D (_Splat2, IN.uv_Splat1);
	            half4 splat3 = tex2D (_Splat3, IN.uv_Splat1);
	            half4 splat4 = tex2D (_Splat4, IN.uv_Splat1);
	            half4 splat5 = tex2D (_Splat5, IN.uv_Splat1);

	            o.Albedo = splat1.rgb * IN.color.r;

	            o.Albedo += splat2.rgb * IN.color.g;

	            o.Albedo += splat3.rgb * IN.color.b;
	            o.Albedo += splat4.rgb * IN.color.a;
	            o.Albedo += splat5.rgb * (1 - (IN.color.r + IN.color.g + IN.color.b + IN.color.a));*/

            /*if(IN.color.a > 0.1)
	        {
	            half4 splat4 = tex2D (_Splat4, IN.uv_Splat1);
	        	o.Albedo = splat4;
				return;
	        }

	        if(IN.color.r > 0 || IN.color.g > 0 || IN.color.b > 0)
	        {
	            half4 splat1 = tex2D (_Splat1, IN.uv_Splat1);
	            half4 splat2 = tex2D (_Splat2, IN.uv_Splat1);
	            half4 splat3 = tex2D (_Splat3, IN.uv_Splat1);

	            o.Albedo = splat1.rgb * IN.color.r;

	            o.Albedo += splat2.rgb * IN.color.g;

	            o.Albedo += splat3.rgb * IN.color.b;
	            return;
            }

            half4 splat5 = tex2D (_Splat5, IN.uv_Splat1);
            o.Albedo = splat5;*/

            if(IN.color.r > 0.1)
            { 
            	o.Albedo = tex2D (_Splat1, IN.uv_Splat1);
            	return;
            }
            if(IN.color.g > 0.1)
            { 
            	o.Albedo = tex2D (_Splat2, IN.uv_Splat1);
            	return;
            }
            if(IN.color.b > 0.1)
            { 
            	o.Albedo = tex2D (_Splat3, IN.uv_Splat1);
            	return;
            }
            if(IN.color.a > 0.1)
            { 
            	o.Albedo = tex2D (_Splat4, IN.uv_Splat1);
            	return;
            }

            o.Albedo = tex2D (_Splat5, IN.uv_Splat1);

        }
        ENDCG
    }
    FallBack "Diffuse"
}