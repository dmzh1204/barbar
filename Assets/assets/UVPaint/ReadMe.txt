﻿
            Thank you for buying our product!
==================================================================
            UVPaint (Skinned Mesh Decal System)
      Copyright © 2015-2016 iRobi Entertainment
             Our WEB: http://irobi.uz
            Support: ganthales_78@mail.ru
==================================================================

  
  
  
  Change Log:

v1.7a

<Notice: This is the last version that supports Unity 4>
•  Added "Shadow Painting" feature. (based on depth map)
•  Added Transparent shaders support.
•  Added NPOT textures support.
•  Added "Zombie Race" game demo.
•  Fixed trouble with Repeat Decals (wrap mode:Repeat now supports)


v1.5e
•  Now plugin works up to 3x faster;
•  Now works perfectly with Unity 5.4 and high;
•  Added Documentation;
•  Added Component "Ignore UVPaint";
•  Added UVPaint Options for more convenience;
•  Added FlexiMode (Ability to choose Texture Property of the Shader. For example: _BumpMap);
•  Added Rotation in degrees option;
•  Added function to save Result in Disk Space;
•  Added Deep Alpha option;
•  Added Ability to switch Decaling to Shared Material;
•  Added global Alpha option;
•  Fixed bug with pink material on Unity 5.4 and high;

v1.2b
•  Fixed bug with Android "Black" textures;
•  Added Full Restore tool for turning back the original texture;
•  Added Decal States (save points) for Restore Tool;
•  Small update existing scenes;

v1.1a
•  Fixed bug with lost textures;
•  Fixed issues with dll (console errors);
•  Fixed issues Post Fixe Texture;
•  Increased spot distance;
•  Added erase tool for turning back the original texture;
•  Updated existing scenes;
•  Added new demo scene "Shooter" (demonstrates blood, color decals and erase tool).
                    