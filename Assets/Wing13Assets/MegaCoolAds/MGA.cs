﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public class MGA : MonoBehaviour {
    public bool ShowAtStart = true;
    public string [] AdvertiserCode = new string[10];
    static string [] AllNetworks = new string[100]; 
    static int NetsAdded = 0; 

    private string AdvCode = "";
	public string [] DontShow = new string[10];
    
    static int NetsChoisen = 0;
    static int AdChoisen = 0;

	static int Count = 0;

    public Texture2D Cross; 
    public Texture2D Background;

	#if UNITY_WEBGL && !UNITY_EDITOR
	[DllImport("__Internal")]
	private static extern void GetRotator(string thisGamObjectName,string str);

	[DllImport("__Internal")]
    private static extern void OpenPopUp(string AdScript);

    [DllImport("__Internal")]
    private static extern void LinkAndPic(string Link, string Pic);
	#endif

    private Rect CanselCross; private Rect AdPlacePicR; private Rect AdPlace;
	private Vector2 BannerPlace; private float BannerWidth = 0.0f; 

	static UnityWebRequest ActionPayR;
	static WWW AdBanner; 
    private Texture2D Banner;
	static int AdStatus = -2; private float DelayAd = 0.0f;
	static bool WaitingAd = false;
	static public bool AdCanBeShown = true;
	public static string [] URLArray = new string[100];

    public string [] AdUrl = new string[100];
    public string [] AdPicUrl = new string[100];
    private static int FoundAdUrls = 0;
    private static int FoundAdPic = 0;
	private static string NameOfThisObject = "";

	private static string AdressLine = "";
	private static string RotatorGetCode = "";

    public GUIStyle EmptyGUI;

	void Awake () {
                  Count = AdvertiserCode.Length - 1;
                  NetsAdded = 0;

                  while (Count!=-1) {
                                    if (AdvertiserCode [Count] != "") {
                                                                       AllNetworks [NetsAdded] = AdvertiserCode [Count];
                                                                       NetsAdded++; 
                                                                      }
                            Count--;}

		AdCanBeShown = true;
		NameOfThisObject = this.gameObject.name;

  #if UNITY_WEBGL && !UNITY_EDITOR
    AdressLine = Application.absoluteURL;
    if (DontShow[0]!="") {if (AdressLine.Contains(DontShow[0])==true) {AdStatus = 10;}}
    if (DontShow[1]!="") {if (AdressLine.Contains(DontShow[1])==true) {AdStatus = 10;}}
    if (DontShow[2]!="") {if (AdressLine.Contains(DontShow[2])==true) {AdStatus = 10;}}
    if (DontShow[3]!="") {if (AdressLine.Contains(DontShow[3])==true) {AdStatus = 10;}}
    if (DontShow[4]!="") {if (AdressLine.Contains(DontShow[4])==true) {AdStatus = 10;}}
    if (DontShow[5]!="") {if (AdressLine.Contains(DontShow[5])==true) {AdStatus = 10;}}
 #endif

	}

	void Start () {if (ShowAtStart==true) {ShowAd ();}

		           BannerWidth = Screen.width * 0.6f; BannerPlace.x = Screen.width * 0.2f; BannerPlace.y = Screen.height * 0.05f;
		           AdPlace = new Rect(BannerPlace.x,BannerPlace.y,BannerWidth,Screen.height * 1f);
		           CanselCross = new Rect(BannerPlace.x + BannerWidth - Screen.width * 0.05f,BannerPlace.y - Screen.height * 0f,Screen.height * 0.1f,Screen.height * 0.1f);
		           AdPlacePicR = new Rect(BannerPlace.x + Screen.width * 0.05f,BannerPlace.y + Screen.height * 0.05f,BannerWidth - Screen.width * 0.1f,Screen.height * 0.89f);
	              }

	public void CathData (string Data) {if (AdStatus==0) {URLArray = Data.Split("\""[0]); AdStatus = 2;}}

	public static void Initilize () {NetsChoisen = Random.Range (0,NetsAdded);

       if (WaitingAd==true) {URLArray = AllNetworks[NetsChoisen].Split("\""[0]);
		                     Count = URLArray.Length - 1;

		while (Count>-1) {if (URLArray[Count].Contains("//")==true) {RotatorGetCode = URLArray[Count];}
				          Count--;} 

		#if UNITY_STANDALONE || UNITY_EDITOR
	    ActionPayR = UnityWebRequest.Get(RotatorGetCode);
		ActionPayR.Send ();
		AdStatus = 0;
		#endif
				
		#if UNITY_WEBGL && !UNITY_EDITOR
        AdStatus = 2;
		#endif

		WaitingAd = false;
		AdCanBeShown = false;}
	}

	public static void ShowAd () {if (AdStatus!=10) {WaitingAd = true; AdStatus = -1;}}

	void Update () {

		if (AdStatus==-1 && AdCanBeShown==true && WaitingAd==true) {Initilize ();}
		WaitingAd = false;

		if (AdStatus==0) {if (ActionPayR.isDone==true) {AdStatus = 1;}}
		if (AdStatus==1) {AdvCode = ActionPayR.downloadHandler.text.Replace("\'", "\"");
                          URLArray = AdvCode.Split("\""[0]); 
                          AdStatus = 2;}

		if (AdStatus==2) {Count = URLArray.Length-1;
                          FoundAdPic = 0;
                          FoundAdUrls = 0;

                           while (Count!=-1) {
  if (URLArray[Count].Contains("http")==true) {
 if (URLArray[Count].Contains(".jpg")==true || URLArray[Count].Contains(".png")==true || URLArray[Count].Contains(".gif")==true) {AdPicUrl[FoundAdPic] = URLArray[Count];
                                                                                                                                 FoundAdPic++;}
 else {AdUrl[FoundAdUrls] = URLArray[Count]; FoundAdUrls++;}
                                              }

                                     Count--;}

AdChoisen = Random.Range (0,FoundAdUrls);

#if UNITY_STANDALONE || UNITY_EDITOR
                                   if (AdPicUrl[AdChoisen].Contains(".gif")==true) {Debug.Log ("GIF is not supported."); AdStatus = 0;}
                                   else {AdBanner = new WWW (AdPicUrl[AdChoisen]); AdStatus = 3;}
#endif

        #if UNITY_WEBGL && !UNITY_EDITOR
        LinkAndPic(AdUrl [AdChoisen], AdPicUrl[AdChoisen]);
        #endif
		                 }
		if (AdStatus==3) {if (AdBanner.isDone==true) {Banner = AdBanner.texture; AdStatus = 4;}}

		#if UNITY_WEBGL && !UNITY_EDITOR
		if (AdStatus==2) {
		 if (AllNetworks[NetsChoisen].Contains("javascript")==true) {RotatorGetCode = "<script type=\"text/javascript\" src=\"" + RotatorGetCode + "\"></script>";}
		 if (AllNetworks[NetsChoisen].Contains("iframe")==true) {RotatorGetCode = "<iframe src=\"" + RotatorGetCode + "\" scrolling=\"no\" width = 500 height = 500 frameBorder = 0></iframe>";}
         
         OpenPopUp(RotatorGetCode);
		AdStatus = -1;}
		#endif
	}
   //Standalone mode
	void OnGUI () {
		GUI.depth = -99;
		if (AdStatus==4) {
			if (Background!=null) {GUI.DrawTexture (AdPlace,Background);}
			if (GUI.Button(CanselCross,Cross,EmptyGUI)) {AdStatus = -1;}

			if (GUI.Button(AdPlacePicR,Banner,EmptyGUI)) {Application.OpenURL (AdUrl [AdChoisen]);
                                                          AdStatus = -1;}

		}
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(MGA))]
public class MGAEditor : Editor {
 override public void OnInspectorGUI() {
  var MGA = target as MGA;

EditorGUILayout.BeginHorizontal();
MGA.ShowAtStart = EditorGUILayout.Toggle ("Show at start:", MGA.ShowAtStart);
EditorGUILayout.SelectableLabel("or call MGA.ShowAd();");
EditorGUILayout.EndHorizontal();

MGA.AdvertiserCode[0] = EditorGUILayout.TextField ("Rotator code", MGA.AdvertiserCode[0]);
MGA.AdvertiserCode[1] = EditorGUILayout.TextField ("Rotator code", MGA.AdvertiserCode[1]);
MGA.AdvertiserCode[2] = EditorGUILayout.TextField ("Rotator code", MGA.AdvertiserCode[2]);
MGA.AdvertiserCode[3] = EditorGUILayout.TextField ("Rotator code", MGA.AdvertiserCode[3]);
MGA.AdvertiserCode[4] = EditorGUILayout.TextField ("Rotator code", MGA.AdvertiserCode[4]);

EditorGUILayout.BeginHorizontal();
if(GUILayout.Button("Register on ActionPay")) {Application.OpenURL("https://actionpay.net/ref:NzI2MzEzOTYyODE4");}
if(GUILayout.Button("Register on RunCPA")) {Application.OpenURL("https://click.runcpa.com/getoffer/49232-e69e29dde1994835bb8ba2f42502a21a-44");}
EditorGUILayout.EndHorizontal();

EditorGUILayout.PrefixLabel("Don't show on:");

EditorGUILayout.BeginHorizontal();
MGA.DontShow[0]= EditorGUILayout.TextField (MGA.DontShow[0]); 
MGA.DontShow[1]= EditorGUILayout.TextField (MGA.DontShow[1]);
EditorGUILayout.EndHorizontal();

EditorGUILayout.BeginHorizontal();
MGA.DontShow[2]= EditorGUILayout.TextField (MGA.DontShow[2]);
MGA.DontShow[3]= EditorGUILayout.TextField (MGA.DontShow[3]);
EditorGUILayout.EndHorizontal();

EditorGUILayout.BeginHorizontal();
MGA.DontShow[4]= EditorGUILayout.TextField (MGA.DontShow[4]);
MGA.DontShow[5]= EditorGUILayout.TextField (MGA.DontShow[5]);
EditorGUILayout.EndHorizontal();

if(GUI.changed){EditorUtility.SetDirty(MGA); EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());}
 }
}
#endif